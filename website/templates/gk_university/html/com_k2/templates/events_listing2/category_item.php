<?php

/**
 * @package		K2
 * @author		GavickPro http://gavick.com
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

// parse event data
$event_info = array(
	"date" => '',
	"when" => '',
	"timestamp" => ''
);
$event_data = array();
$event_data_count = preg_match('@<div class="gkEvent">.*?</div>@mis', $this->item->introtext, $event_data);

if($event_data_count > 0) {
	$event_datetimes = array();
	$event_datetimes_count = preg_match_all('@<time.*?datetime="(.*?)".*?>(.*?)</time>@', $event_data[0], $event_datetimes);

	if($event_datetimes_count > 0) {
		$event_timestamp = strtotime($event_datetimes[1][0]);
		$event_info['timestamp'] = date(DATE_W3C, $event_timestamp);
		$event_info['date'] = strftime('%a', $event_timestamp) . '<small>' . strftime('%b %e', $event_timestamp) . '</small>';
		$event_info['when'] = $event_datetimes[2][0] . ' - ' . $event_datetimes[2][1] . ' @ ' . $event_datetimes[2][2] . ' - ' . $event_datetimes[2][3];
	}
}

?>

<article> <?php echo $this->item->event->BeforeDisplay; ?> <?php echo $this->item->event->K2BeforeDisplay; ?>		
		
				<header<?php if($event_info['date'] == '') : ?> class="nodate"<?php endif; ?>>
						<?php if(isset($this->item->editLink)): ?>
						<a class="catItemEditLink modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>">
							<?php echo JText::_('K2_EDIT_ITEM'); ?>
						</a>
						<?php endif; ?>
				
						<?php if($event_info['date'] != '') : ?>
						<time datetime="<?php echo $event_info['timestamp']; ?>">
							<?php echo $event_info['date']; ?>
						</time>
						<?php endif; ?>
				
						<?php if($this->item->params->get('catItemTitle')): ?>
						<h2>
								<?php if ($this->item->params->get('catItemTitleLinked')): ?>
								<a href="<?php echo $this->item->link; ?>"><?php echo $this->item->title; ?></a>
								<?php else: ?>
								<?php echo $this->item->title; ?>
								<?php endif; ?>
								<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
								<sup><?php echo JText::_('K2_FEATURED'); ?></sup>
								<?php endif; ?>
						</h2>
						<?php endif; ?>
				
						<?php if($event_info['when'] != '') : ?>
						<ul>
								<li>
									<strong><?php echo JText::_('TPL_GK_LANG_WHEN'); ?></strong>
									<?php echo $event_info['when']; ?> 
								</li>
						</ul>
						<?php endif; ?>
                                                <?php if ($this->item->params->get('catItemReadMore')): ?>
	<!-- Item "read more..." link -->
	<div class="catItemReadMore">
		<a class="k2ReadMore" href="<?php echo $this->item->link; ?>">
			<?php echo JText::_('K2_READ_MORE'); ?>
		</a>
	</div>
	<?php endif; ?>
				</header>
                                
				
				<?php echo $this->item->event->AfterDisplayTitle; ?> 
				<?php echo $this->item->event->K2AfterDisplayTitle; ?>
				
				<?php echo $this->item->event->AfterDisplay; ?> 
				<?php echo $this->item->event->K2AfterDisplay; ?>
		
</article>
