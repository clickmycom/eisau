<?php

/**
 *
 * Default view
 *
 * @version             1.0.0
 * @package             Gavern Framework
 * @copyright			Copyright (C) 2010 - 2011 GavickPro. All rights reserved.
 *               
 */
 
// No direct access.
defined('_JEXEC') or die;
//
$app = JFactory::getApplication();
$user = JFactory::getUser();
// getting User ID
$userID = $user->get('id');
// getting params
$option = JRequest::getCmd('option', '');
$view = JRequest::getCmd('view', '');
// defines if com_users
define('GK_COM_USERS', $option == 'com_users' && ($view == 'login' || $view == 'registration'));
// other variables
$btn_login_text = ($userID == 0) ? JText::_('TPL_GK_LANG_LOGIN') : JText::_('TPL_GK_LANG_LOGOUT');
// make sure that the modal will be loaded
JHTML::_('behavior.modal');
//
$page_suffix_output = $this->API->get('template_pattern', 'none') != 'none' ? 'pattern' . $this->API->get('template_pattern', 'none') . ' ' : '';
$page_suffix_output .= $this->page_suffix;

if($option == 'com_community') {
	$page_suffix_output .= ' jomsocial';
}
if($option == 'com_community' && $view == 'frontpage' && $userID != 0) {
	$page_suffix_output .= ' jomsocial-user';
}
if($option == 'com_community' && $view == 'frontpage' && $userID == 0) {
	$page_suffix_output .= ' jomsocial-guest';
}

$tpl_page_suffix = $page_suffix_output != '' ? ' class="'.$page_suffix_output.'" ' : '';

?>
<!DOCTYPE html>
<html lang="<?php echo $this->APITPL->language; ?>" <?php echo $tpl_page_suffix; ?>>
<head>
	<?php $this->layout->addTouchIcon(); ?>
	<?php if(
			$this->browser->get('browser') == 'ie6' || 
			$this->browser->get('browser') == 'ie7' || 
			$this->browser->get('browser') == 'ie8' || 
			$this->browser->get('browser') == 'ie9'
		) : ?>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<?php endif; ?>
    <?php if($this->API->get('rwd', 1)) : ?>
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<?php else : ?>
		<meta name="viewport" content="width=<?php echo $this->API->get('template_width', 1020)+80; ?>">
	<?php endif; ?>
    <jdoc:include type="head" />
    <?php $this->layout->loadBlock('head'); ?>
	<?php $this->layout->loadBlock('cookielaw'); ?>
</head>
<body<?php echo $tpl_page_suffix; ?><?php if($this->browser->get("tablet") == true) echo ' data-tablet="true"'; ?><?php if($this->browser->get("mobile") == true) echo ' data-mobile="true"'; ?><?php $this->layout->generateLayoutWidths(); ?> data-zoom-size="<?php echo $this->API->get('gk_zoom_size', '150'); ?>">	
	<?php
	     // put Google Analytics code
	     echo $this->social->googleAnalyticsParser();
	?>
	<?php if ($this->browser->get('browser') == 'ie8' || $this->browser->get('browser') == 'ie7' || $this->browser->get('browser') == 'ie6') : ?>
	<!--[if lte IE 8]>
	<div id="ieToolbar"><div><?php echo JText::_('TPL_GK_LANG_IE_TOOLBAR'); ?></div></div>
	<![endif]-->
	<?php endif; ?>

	<div id="gkBg">
		<?php if(count($app->getMessageQueue())) : ?>
		<jdoc:include type="message" />
		<?php endif; ?>
		
	    <header id="gkHeader"<?php if(!$this->layout->isFrontpage() && !in_array('frontpage', explode(' ', $page_suffix_output))): ?> class="gkPage"<?php endif; ?>>		
			<div id="gkHeaderNav">
				<div class="gkPage">	
					<?php $this->layout->loadBlock('logo'); ?>
	                 
	                 <?php if($this->API->get('show_menu', 1)) : ?>
	                 <div id="gkMainMenu" class="gkMenuClassic">
	                         <?php
	                 		$this->mainmenu->loadMenu($this->API->get('menu_name','mainmenu')); 
	                 	    $this->mainmenu->genMenu($this->API->get('startlevel', 0), $this->API->get('endlevel',-1));
	                 	?>
	                 </div>
	                 <?php endif; ?>
	                 
	                 <?php if($this->API->get('show_menu', 1)) : ?>
	                 <div id="gkMobileMenu" class="gkPage">
	                     <i id="static-aside-menu-toggler" class="fa fa-bars"></i>
	                 </div>
	                 <?php endif; ?>
		    	</div>
	    	</div>
	    	
	    	<?php if($this->API->modules('header_bg')) : ?>
	    	<div id="gkHeaderBg">
	    		<jdoc:include type="modules" name="header_bg" style="none" />
			</div>
	    	<?php endif; ?>
	    	
	    	<?php if(
	    		$this->API->modules('header_left') ||
	    		$this->API->modules('header_right')
	    	) : ?>
		    	<div id="gkHeaderModules" class="gkPage">
			    	<?php if($this->API->modules('header_left')) : ?>
			    	<div id="gkHeaderLeft">
			    		<jdoc:include type="modules" name="header_left" style="gk_style" />
			    	</div>
			    	<?php endif; ?>
			    	
			    	<?php if($this->API->modules('header_right')) : ?>
			    	<div id="gkHeaderRight">
			    		<jdoc:include type="modules" name="header_right" style="gk_style" />
			    	</div>
			    	<?php endif; ?>
		    	</div>
	    	<?php endif; ?>
	    </header>
	    
	    <?php if($this->API->modules('header_bottom')) : ?>
	    <div id="gkHeaderBottom"<?php if(!$this->layout->isFrontpage() && !in_array('frontpage', explode(' ', $page_suffix_output))): ?> class="gkPage"<?php endif; ?>>
	    	<div class="gkPage">
	    		<jdoc:include type="modules" name="header_bottom" style="none" />
	    	</div>
	    </div>
	    <?php endif; ?>
	    
	    <?php

	    if(
	    	$this->getPageTitleOverride() && 
	    	!$this->layout->isFrontpage() && 
	    	!in_array('frontpage', explode(' ', $page_suffix_output)) &&
	    	!in_array('jomsocial', explode(' ', $page_suffix_output))
	    ) {
	    	if(!$this->API->modules('header')) {
	    	    $active_menu_item = JFactory::getApplication()->getMenu()->getActive();

	    	    if($active_menu_item) {
	    	    	$header_text = $active_menu_item->params->get('menu-anchor_title');
	    	    	if($header_text == '') {
	    	    		$header_text = $active_menu_item->title;
	    	    	}

	    	    	echo '<h2 id="gkPageTitle" class="gkPage"><span>' . $header_text . '</span></h2>';
	    	    }
	    	}
	    }

	    ?>
	
		<div id="gkPageContent"<?php if(!$this->layout->isFrontpage() && !in_array('frontpage', explode(' ', $page_suffix_output))): ?> class="gkPage"<?php endif; ?>>
	    	<div class="gkPage">
	    		<?php if($this->API->modules('breadcrumb')) : ?>
	    		<div id="gkBreadcrumb">
	    			<div class="gkPage">
	    				<jdoc:include type="modules" name="breadcrumb" style="none" />
	    			</div>
	    		</div>
	    		<?php endif; ?>
	    	
	    		<div<?php if($this->API->modules('sidebar')) : ?> data-sidebar-pos="<?php echo $this->API->get('sidebar_position', 'right'); ?>"<?php endif; ?>>
			    	<div id="gkContent">					
						<div id="gkContentWrap"<?php if($this->API->modules('inset')) : ?> data-inset-pos="<?php echo $this->API->get('inset_position', 'right'); ?>"<?php endif; ?>>
							<?php if($this->API->modules('top1')) : ?>
							<section id="gkTop1" class="gkCols3<?php if($this->API->modules('top1') > 1) : ?> gkNoMargin<?php endif; ?>">
								<div>
									<jdoc:include type="modules" name="top1" style="gk_style"  modnum="<?php echo $this->API->modules('top1'); ?>" modcol="3" />
								</div>
							</section>
							<?php endif; ?>
							
							<?php if($this->API->modules('top2')) : ?>
							<section id="gkTop2" class="gkCols3<?php if($this->API->modules('top2') > 1) : ?> gkNoMargin<?php endif; ?>">
								<div>
									<jdoc:include type="modules" name="top2" style="gk_style" modnum="<?php echo $this->API->modules('top2'); ?>" modcol="3" />
								</div>
							</section>
							<?php endif; ?>
							
							<?php if($this->API->modules('mainbody_top')) : ?>
							<section id="gkMainbodyTop">
								<jdoc:include type="modules" name="mainbody_top" style="gk_style" />
							</section>
							<?php endif; ?>	
							
							<section id="gkMainbody">
								<?php if(($this->layout->isFrontpage() && !$this->API->modules('mainbody')) || !$this->layout->isFrontpage()) : ?>
									<jdoc:include type="component" />
								<?php else : ?>
									<jdoc:include type="modules" name="mainbody" style="gk_style" />
								<?php endif; ?>
							</section>
							
							<?php if($this->API->modules('mainbody_bottom')) : ?>
							<section id="gkMainbodyBottom">
								<jdoc:include type="modules" name="mainbody_bottom" style="gk_style" />
							</section>
							<?php endif; ?>
						</div>
						
						<?php if($this->API->modules('inset')) : ?>
		                <aside id="gkInset" class="dark-area">
		                        <jdoc:include type="modules" name="inset" style="gk_style" />
		                </aside>
		                <?php endif; ?>
			    	</div>
			    	
			    	<?php if($this->API->modules('sidebar')) : ?>
			    	<aside id="gkSidebar">
			    		<div>
			    			<jdoc:include type="modules" name="sidebar" style="gk_style" />
			    		</div>
			    	</aside>
			    	<?php endif; ?>
		    	</div>
			</div>
		</div>
		
		<?php if($this->API->modules('bottom1')) : ?>
		<section id="gkBottom1"<?php if($this->API->modules('bottom1') == 1) : ?> class="gkSingleModule"<?php endif; ?>>
			<?php if($this->API->modules('bottom1') > 1) : ?><div class="gkCols6 gkPage"><?php endif; ?>
				<jdoc:include type="modules" name="bottom1" style="gk_style" modnum="<?php echo $this->API->modules('bottom1'); ?>" />
			<?php if($this->API->modules('bottom1') > 1) : ?></div><?php endif; ?>
		</section>
		<?php endif; ?>
	    
	    <?php if($this->API->modules('bottom2')) : ?>
	    <section id="gkBottom2"<?php if($this->API->modules('bottom2') == 1) : ?> class="gkSingleModule"<?php endif; ?>>
	    	<?php if($this->API->modules('bottom2') > 1) : ?><div class="gkCols6 gkPage"><?php endif; ?>
	    		<jdoc:include type="modules" name="bottom2" style="gk_style" modnum="<?php echo $this->API->modules('bottom2'); ?>" />
	    	<?php if($this->API->modules('bottom2') > 1) : ?></div><?php endif; ?>
	    </section>
	    <?php endif; ?>
	    
	    <?php if($this->API->modules('bottom3')) : ?>
	    <section id="gkBottom3"<?php if($this->API->modules('bottom3') == 1) : ?> class="gkSingleModule"<?php endif; ?>>
	    	<?php if($this->API->modules('bottom3') > 1) : ?><div class="gkCols6 gkPage"><?php endif; ?>
	    		<jdoc:include type="modules" name="bottom3" style="gk_style" modnum="<?php echo $this->API->modules('bottom3'); ?>" />
	    	<?php if($this->API->modules('bottom3') > 1) : ?></div><?php endif; ?>
	    </section>
	    <?php endif; ?>
	    
	    <?php if($this->API->modules('bottom4')) : ?>
	    <section id="gkBottom4"<?php if($this->API->modules('bottom4') == 1) : ?> class="gkSingleModule"<?php endif; ?>>
	    	<?php if($this->API->modules('bottom4') > 1) : ?><div class="gkCols6 gkPage"><?php endif; ?>
	    		<jdoc:include type="modules" name="bottom4" style="gk_style" modnum="<?php echo $this->API->modules('bottom4'); ?>" />
	    	<?php if($this->API->modules('bottom4') > 1) : ?></div><?php endif; ?>
	    </section>
	    <?php endif; ?>
	    
	    <?php if($this->API->modules('bottom5')) : ?>
	    <section id="gkBottom5"<?php if($this->API->modules('bottom5') == 1) : ?> class="gkSingleModule"<?php endif; ?>>
	    	<?php if($this->API->modules('bottom5') > 1) : ?><div class="gkCols6 gkPage"><?php endif; ?>
	    		<jdoc:include type="modules" name="bottom5" style="gk_style" modnum="<?php echo $this->API->modules('bottom5'); ?>" />
	    	<?php if($this->API->modules('bottom5') > 1) : ?></div><?php endif; ?>
	    </section>
	    <?php endif; ?>
	    
	    <?php if($this->API->modules('lang')) : ?>
	    <div id="gkLang">
	    	<div class="gkPage">
	         	<jdoc:include type="modules" name="lang" style="gk_style" />
	         </div>
	    </div>
	    <?php endif; ?>
    </div>
    
    <?php $this->layout->loadBlock('footer'); ?>
   	<?php $this->layout->loadBlock('social'); ?>
   	<?php $this->layout->loadBlock('tools/login'); ?>
   		
   	<i id="close-menu" class="fa fa-times"></i>
   	<nav id="aside-menu">
   		<div>
   			<?php
   				$this->asidemenu->loadMenu($this->API->get('menu_name','mainmenu')); 
   		    	$this->asidemenu->genMenu($this->API->get('startlevel', 0), $this->API->get('endlevel',-1));
   			?>
   		</div>
   	</nav>	
   		
	<jdoc:include type="modules" name="debug" />
</body>
</html>