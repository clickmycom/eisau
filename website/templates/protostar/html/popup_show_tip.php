<!-- คลิกหน้าRegister I accept the Terms and Conditions  แล้วแสดง popup -->
<div id="animatedModal" style="display: none !important;">
    <div id="btn-close-modal" class="close-animatedModal closebt-container" style="position: relative;width: 100%;text-align: center;margin-top: 40px;">
        <img class="closebt" src="<?php echo JUri::root(true); ?>/templates/protostar/img/closebt.svg">
    </div>

    <div id="modal-container" class="container">
        <div itemprop="articleBody" class="articleBody" style="background:#fff !important; padding: 30px !important;margin-bottom: 50px !important;border-radius: 5px;">
            <div class="page-header">
                <h2 itemprop="name" style="color: #1279bf  !important;">
                    Terms &amp; Conditions			</h2>
            </div>
            <p>In these terms and conditions, “we” “us” “EISAU” and “our” refers to Educational Infrastructure Services Australia Pty Ltd.  Your access to and use of all information on this website including purchase of our service/s is provided subject to the following terms and conditions.  The information is intended for residents of Australia only.</p>
            <p>We reserve the right to amend this Notice at any time and your use of the website following any amendments will represent your agreement to be bound by these terms and conditions as amended.  We therefore recommend that each time you access our website you read these terms and conditions.
                <br/>
                <br/>
            </p>
            <h3><span style="color: #1279bf;"><strong>Registered Users</strong></span></h3>
            <ol>
                <li>In order to access the services provided on this website, you must become a registered user.  You must complete registration by providing certain information as set out on our membership/registration page.  Please refer to our Privacy Policy linked on our website for information relating to our collection, storage and use of the details you provide on registration.</li>
                <li>You agree to ensure that your registration details are true and accurate at all times and you undertake to update your registration details from time to time when they change.</li>
                <li>On registration, we provide you with a password. On registration, if you are a provider, you agree to pay for our services as set out on our website. </li>
                <li>By registering on the EISAU website, providers acknowledge that they have the necessary insurances and certificates to ensure compliance.</li>
                <li>We reserve the right to terminate your registration at any time if you breach these terms and conditions.</li>
                <li>Our services are intended to be used by registered users within Australia only.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Our Website Services</strong></span></h3>
            <ol>
                <li>Our services are provided to adults over the age of eighteen (18) years.  By proceeding to purchase through our website, you acknowledge that you are over 18 years of age.</li>
                <li>All charges are in Australian Dollars (AUD) and are inclusive of GST.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Site Access</strong></span></h3>
            <ol>
                <li>Except as permitted under the <em>Copyright</em> <em>Act</em> 1968 (Cth), you are not permitted to copy, reproduce, republish, distribute or display any of the information on this website without our prior written permission.</li>
                <li>The licence granted herein to access and use the information on our website prohibits to use any data mining robots or other extraction tools.  The licence also prohibits you to metatag or mirror our website without our prior written permission. We reserve all our rights, and in particular our right immediately to exclude you from the site.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Hyperlinks</strong></span></h3>
            <ol>
                <li>This website may from time to time contain hyperlinks to other websites.  Such links are provided for convenience only and we take no responsibility for the content and maintenance of or privacy compliance by any linked website.  Any hyperlink on our website to another website does not imply our endorsement, support, or sponsorship of the operator of that website nor of the information and/or products which they provide.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Intellectual Property Rights</strong></span></h3>
            <ol>
                <li>The copyright to all content on this website including applets, graphics, images, layouts and text belongs to us or we have a right to use those materials. Your access to our website does not license you to use the content in any commercial way without our prior written permission.</li>
                <li>All trademarks, brands and logos generally identified either with the symbols TM or ® which are used on this website are either owned by us or we have a licence to use them. Your access to our website does not license you to use those marks in any commercial way without our prior written permission.</li>
                <li>Any comment, testimonial, feedback, idea or suggestion (called “Comments or Testimonials”) which you provide to us through this website becomes our property.  If in future we use your Comments and Testimonials in promoting our website or in any other way, we will not be liable for any similarities which may appear from such use.  Furthermore, you agree that we are entitled to use your Comments and Testimonials for any commercial or non-commercial purpose without compensation to you or to any other person who has transmitted your Comments and Testimonials.</li>
                <li>If you provide us with Comments or Testimonials, you acknowledge that you are responsible for the content of such material including its legality, originality and copyright.</li>
                <li>Testimonials supplied by schools will remain valid for five years.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Disclaimers</strong></span></h3>
            <ol>
                <li>Whilst we take all due care in providing our services, we do not provide any warranty either express or implied including without limitation warranties of merchantability or fitness for a particular purpose.</li>
                <li>To the extent permitted by law, any condition or warranty which would otherwise be implied into these terms and conditions is excluded. </li>
                <li>We also take all due care in ensuring that our website is free of any virus, worm, Trojan horse and/or malware, however we are not responsible for any damage to your computer system which arises in connection with your use of our website or any linked website.</li>
                <li>We host third party content on our website such as advertisements and endorsements belonging to other traders.  Responsibility for the content of such material rests with the owners of that material and we are not responsible for any errors or omissions in such material.</li>
                <li> This is referral site only. EISAU (Educational Infrastructure Services Australia) publishes testimonials but we do not warrant the accuracy or truthfulness of these testimonials. Users of the site need to make their own enquiries and satisfy themselves as to themselves as to the quality or suitability of the providers who use this site. EISAU is not party to any contract that you may enter into with a provider and you must satisfy yourself as to the Terms and Conditions made with those providers.</li>
                <li>Providers will be required to agree to the first testimonial provided by a School on their behalf. The source of subsequent testimonials will be checked by the EISAU administrator and published on the website without the consent of the provider to whom the testimonial refers.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Limitation of Liability</strong></span></h3>
            <ol>
                <li>To the full extent permitted by law, our liability for breach of an implied warranty or condition is limited to the supply of the services again, or payment of the costs of having those services supplied again.</li>
                <li>We accept no liability for any loss whatsoever including consequential loss suffered by you arising from services we have supplied.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Indemnity</strong></span></h3>
            <ol>
                <li>By accessing our website, you agree to indemnify and hold us harmless from all claims, actions, damages, costs and expenses including legal fees arising from or in connection with your use of our website.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Jurisdiction</strong></span></h3>
            <ol>
                <li>These terms and conditions are to be governed by and construed in accordance with the laws of NSW and any claim made by either party against the other which in any way arises out of these terms and conditions will be heard in NSW and you agree to submit to the jurisdiction of those Courts.</li>
                <li>If any provision in these terms and conditions is invalid under any law the provision will be limited, narrowed, construed or altered as necessary to render it valid but only to the extent necessary to achieve such validity.  If necessary the invalid provision will be deleted from these terms and conditions and the remaining provisions will remain in full force and effect.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Privacy</strong></span></h3>
            <ol>
                <li>We undertake to take all due care with any information which you may provide to us when accessing our website. However we do not warrant and cannot ensure the security of any information which you may provide to us.  Information you transmit to us is entirely at your own risk although we undertake to take reasonable steps to preserve such information in a secure manner.</li>
                <li>Our compliance with privacy legislation is set out in our separate Privacy Policy which may be accessed from our home page.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Refunds Policy</strong></span></h3>
            <ol>
                <li>Providers purchase an annual membership and listing on the EISAU website. Monies paid are not refundable.</li>
                <li>Payments to EISAU will not be refunded in circumstances where it is necessary to remove a provider from the website.</li>
            </ol>
        </div>
    </div>
</div>