<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitenffame');

if($task == "edit" || $layout == "form" )
{
    $fullWidth = 1;
}
else
{
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Use of Google Font
if ($this->params->get('googleFont'))
{
    $doc->addStyleSheet('//fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
    $doc->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}

// Template color
if ($this->params->get('templateColor'))
{
    $doc->addStyleDeclaration("

	a {
		color: " . $this->params->get('templateColor') . ";
	}
	.nav-list > .active > a,
	.nav-list > .active > a:hover,
	.dropdown-menu li > a:hover,
	.dropdown-menu .active > a,
	.dropdown-menu .active > a:hover,
	.nav-pills > .active > a,
	.nav-pills > .active > a:hover,
	.btn-primary {
		background: " . $this->params->get('templateColor') . ";
	}");
}

// Check for a custom CSS file
$userCss = JPATH_SITE . '/templates/' . $this->template . '/css/user.css';

if (file_exists($userCss) && filesize($userCss) > 0)
{
    $this->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/user.css');
}

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
    $span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
    $span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
    $span = "span9";
}
else
{
    $span = "col-md-12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <jdoc:include type="head" />
    <!--[if lt IE 9]><script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/css/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/css/styles.css">

    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/popup/css/normalize.min.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/popup/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/plugins/system/yt/includes/site/css/style.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/css/common.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/css/gallery.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/css/template.css?7206ba68ba1d6f216a78f5500200d541">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/modules/mod_jbusinessdirectory/assets/style.css">
    <link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/js/sweetalert/sweetalert.css">

    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/jquery.raty.min.js"></script>
    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/common.js"></script>
    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/utils.js"></script>
    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/jquery.opacityrollover.js"></script>
    <script src="<?php echo JUri::root(true); ?>/templates/protostar/js/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo JUri::root(true); ?>/components/com_jbusinessdirectory/assets/js/jquery-ui.js"></script>
    <script src="<?php echo JUri::root(true); ?>/media/system/js/punycode.js"></script>
    <script src="<?php echo JUri::root(true); ?>/media/system/js/validate.js"></script>
    <script src="<?php echo JUri::root(true); ?>/media/jui/js/bootstrap.min.js"></script>


</head>

<style type="text/css">
    #_tk_form_contact_provider {
                display: none !important;
}
</style>
<body style="background-color: #e9ebee;" class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
echo ($this->direction == 'rtl' ? ' rtl' : '');
?>">

<link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/js/popup.css">

<!-- Body -->
<div class="body">

    <!-- Top Header -->
    <section class="top-header text-right">
        <div class="container" id="top_login">
            <ul class="list-inline" >

                <li>
                    <div class=" account-urls first">
                        <jdoc:include type="modules" name="position-menulogin" />
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <!-- Header -->
    <header class="header" role="banner">
        <div class="header-inner clearfix container">
            <a class="brand pull-left" href="<?php echo $this->baseurl; ?>/index.php/">
                <?php echo $logo; ?>
                <?php if ($this->params->get('sitedescription')) : ?>
                    <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
                <?php endif; ?>
            </a>
        </div>
    </header>
    <?php if ($this->countModules('position-1')) : ?>
        <section data-name="menu" style="background-color: #FFF;    padding-bottom: 5px;">
            <div class="container" >
                <jdoc:include type="modules" name="position-1"  />
            </div>
        </section>
    <?php endif; ?>

    <div id="<?=$app->getMenu()->getActive()->route =='home'?'w_a1':'detail_pages'?>" class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
        <jdoc:include type="modules" name="banner" style="xhtml" />
        <div class="row-fluid yahoo" >
            <?php if ($this->countModules('position-8')) : ?>
                <!-- Begin Sidebar -->
                <div id="sidebar" class="col-md-3 col-sm-3 col-xs-11">
                    <div class="sidebar-nav">
                        <jdoc:include type="modules" name="position-8" style="xhtml" />
                    </div>
                </div>
                <!-- End Sidebar -->
            <?php endif; ?>

            <?php
                $onpage=false;
                $login_admin='col-lg-9';
                if($app->getMenu()->getActive()->route =='blog' || $app->getMenu()->getActive()->route =='news-feeds/latest-news'){
                    $onpage=true;
                }
                $ex_o = explode("/",$app->getMenu()->getActive()->route);
                if($ex_o[0] =='login'){
            ?>
            <style>
                .ui-dir-button{
                    width: auto !important;
                    padding: 12px 20px !important;
                }
                fieldset.boxed{
                        padding: 0% 0% !important;
                }
            </style>
            <?php
                    $login_admin='';
                }

            ?>

            <?php
                if( $ex_o[0] =='news-cat'){
                    echo "<main id='content' role='main' class='yahoo_a news-cat col-lg-8 '>";
                }else{
            ?>
                    <main id="content" role="main" class="yahoo_a <?=$ex_o[0]?> <?=$onpage==true?'col-lg-8':$login_admin?> ">
            <?php
                }
            ?>

                <!-- Begin Content -->
                <jdoc:include type="modules" name="position-3" style="xhtml" />
                <jdoc:include type="message" />
                <jdoc:include type="component" />
                <jdoc:include type="modules" name="position-2" style="none" />
                <!-- End Content -->
            </main>

            <!-- Begin Sidebar -->
            <div id="sidebar_can_remove" class="col-md-3 c" >
                <div class="sidebar-nav">
                    <jdoc:include type="modules" name="position-090" style="xhtml" />
                </div>
            </div>
            <!-- End Sidebar -->

            <?php if ($this->countModules('position-7')) : ?>
                <div id="<?=$app->getMenu()->getActive()->route =='home'?'search_home_page':'wel_not_home'?>">
                    <div id="aside" class="col-lg-3 yahoo_b l-21px" style="margin-top: 0.66%;">
                        <!-- Begin Right Sidebar -->
                        <jdoc:include type="modules" name="position-7" style="well" />
                        <!-- End Right Sidebar -->
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>

<jdoc:include type="modules" name="full-width" style="none" />

<!-- Footer -->
<footer class="footer" role="contentinfo">
    <jdoc:include type="modules" name="footer" style="none" />
    <jdoc:include type="modules" name="footer_a" style="none" />
    <jdoc:include type="modules" name="footer_b" style="none" />
    <jdoc:include type="modules" name="footer_d" style="none" />
    <jdoc:include type="modules" name="footer_e" style="none" />
    <jdoc:include type="modules" name="footer_f" style="none" />
    <jdoc:include type="modules" name="footer_g" style="none" />
    <jdoc:include type="modules" name="footer_h" style="none" />
    <jdoc:include type="modules" name="footer_i" style="none" />
    <div style="background-color: #EE3928" class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
    </div>
</footer>





















<jdoc:include type="modules" name="debug" style="none" />

<link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/owl-carousel/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo JUri::root(true); ?>/templates/protostar/owl-carousel/owl.theme.css" type="text/css">
<script type="text/javascript" src="<?php echo JUri::root(true); ?>/templates/protostar/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(true); ?>/templates/protostar/js/scrollview.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(true); ?>/templates/protostar/popup/js/animatedModal.min.js"></script>




<!-- Section Include -->
<?php
    include_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/js/checkSearch.php');

    if($app->getMenu()->getActive()->route == "schools/register" || $app->getMenu()->getActive()->route =="schools/suggest-a-provider" || $app->getMenu()->getActive()->route=="become-a-provider/provider-register"){
        include_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/html/popup_show_tip.php');
    }

    if($app->getMenu()->getActive()->route =='home'){
        include_once (JPATH_ROOT.'/templates/'.$app->getTemplate().'/css/only_homepage.css');
    }
?>
<script src="<?php echo JUri::root(true);?>/templates/protostar/js/jqeury-index.js"></script>

<script type="text/javascript">
    // เช็คว่ามีการเลือก Industry หรือยัง
jQuery("#sub_categories").on("click", function(){
    if(jQuery("#categories").val()==0){
        sweetAlert("Please select an industry first.", "", "error");
    }
});

if(jQuery("#gruemenu > ul > li:last-child > ul").hasClass('sub-menu') == true){
    jQuery("i.fa-sign-in").hide();
    jQuery("#gruemenu > ul > li:last-child > a").text("Account");
    jQuery("#gruemenu > ul > li:last-child > a").attr("href","<?php echo JUri::root(true); ?>/index.php/login/control/useroptions");
    jQuery("#gruemenu > ul > li:last-child > a").attr('style', 'padding: 14px 33px 14px 18px !important');
}

</script>


    <style type="text/css">
        .formControlLabel{
            color: #000 !important;
        }

        @media screen (max-width: 1024px) {
            #companies-search.vertical .form-field select{
                max-width: -webkit-fill-available !important;
            }
        }

        @media screen (max-width: 1200px) {
            #txt_welcome_section {
                margin-top: -85px !important;
            }
        }

      ul#slider108 img {
        border-bottom-left-radius: 6px;
        border-bottom-right-radius: 6px;
      }



    </style>





<div id="animatedModal_for_provider" style="display: none !important;">
    <div id="btn-close-modal" class="close-animatedModal closebt-container" style="position: relative;width: 100%;text-align: center;margin-top: 40px;">
        <img class="closebt" src="<?php echo JUri::root(true); ?>/templates/protostar/img/closebt.svg">
    </div>

    <div id="modal-container" class="container">
        <div itemprop="articleBody" class="articleBody" style="background:#fff !important; padding: 30px !important;margin-bottom: 50px !important;border-radius: 5px;">
            <div class="page-header">
                <h2 itemprop="name" style="color: #1279bf  !important;">
                    Terms &amp; Conditions			</h2>
            </div>
            <p>In these terms and conditions, “we” “us” “EISAU” and “our” refers to Educational Infrastructure Services Australia Pty Ltd.  Your access to and use of all information on this website including purchase of our service/s is provided subject to the following terms and conditions.  The information is intended for residents of Australia only.</p>
            <p>We reserve the right to amend this Notice at any time and your use of the website following any amendments will represent your agreement to be bound by these terms and conditions as amended.  We therefore recommend that each time you access our website you read these terms and conditions.
                <br/>
                <br/>
            </p>
            <h3><span style="color: #1279bf;"><strong>Registered Users</strong></span></h3>
            <ol>
                <li>In order to access the services provided on this website, you must become a registered user.  You must complete registration by providing certain information as set out on our membership/registration page.  Please refer to our Privacy Policy linked on our website for information relating to our collection, storage and use of the details you provide on registration.</li>
                <li>You agree to ensure that your registration details are true and accurate at all times and you undertake to update your registration details from time to time when they change.</li>
                <li>On registration, we provide you with a password. On registration, if you are a provider, you agree to pay for our services as set out on our website. </li>
                <li>By registering on the EISAU website, providers acknowledge that they have the necessary insurances and certificates to ensure compliance.</li>
                <li>We reserve the right to terminate your registration at any time if you breach these terms and conditions.</li>
                <li>Our services are intended to be used by registered users within Australia only.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Our Website Services</strong></span></h3>
            <ol>
                <li>Our services are provided to adults over the age of eighteen (18) years.  By proceeding to purchase through our website, you acknowledge that you are over 18 years of age.</li>
                <li>All charges are in Australian Dollars (AUD) and are inclusive of GST.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Site Access</strong></span></h3>
            <ol>
                <li>Except as permitted under the <em>Copyright</em> <em>Act</em> 1968 (Cth), you are not permitted to copy, reproduce, republish, distribute or display any of the information on this website without our prior written permission.</li>
                <li>The licence granted herein to access and use the information on our website prohibits to use any data mining robots or other extraction tools.  The licence also prohibits you to metatag or mirror our website without our prior written permission. We reserve all our rights, and in particular our right immediately to exclude you from the site.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Hyperlinks</strong></span></h3>
            <ol>
                <li>This website may from time to time contain hyperlinks to other websites.  Such links are provided for convenience only and we take no responsibility for the content and maintenance of or privacy compliance by any linked website.  Any hyperlink on our website to another website does not imply our endorsement, support, or sponsorship of the operator of that website nor of the information and/or products which they provide.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Intellectual Property Rights</strong></span></h3>
            <ol>
                <li>The copyright to all content on this website including applets, graphics, images, layouts and text belongs to us or we have a right to use those materials. Your access to our website does not license you to use the content in any commercial way without our prior written permission.</li>
                <li>All trademarks, brands and logos generally identified either with the symbols TM or ® which are used on this website are either owned by us or we have a licence to use them. Your access to our website does not license you to use those marks in any commercial way without our prior written permission.</li>
                <li>Any comment, testimonial, feedback, idea or suggestion (called “Comments or Testimonials”) which you provide to us through this website becomes our property.  If in future we use your Comments and Testimonials in promoting our website or in any other way, we will not be liable for any similarities which may appear from such use.  Furthermore, you agree that we are entitled to use your Comments and Testimonials for any commercial or non-commercial purpose without compensation to you or to any other person who has transmitted your Comments and Testimonials.</li>
                <li>If you provide us with Comments or Testimonials, you acknowledge that you are responsible for the content of such material including its legality, originality and copyright.</li>
                <li>Testimonials supplied by schools will remain valid for five years.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Disclaimers</strong></span></h3>
            <ol>
                <li>Whilst we take all due care in providing our services, we do not provide any warranty either express or implied including without limitation warranties of merchantability or fitness for a particular purpose.</li>
                <li>To the extent permitted by law, any condition or warranty which would otherwise be implied into these terms and conditions is excluded. </li>
                <li>We also take all due care in ensuring that our website is free of any virus, worm, Trojan horse and/or malware, however we are not responsible for any damage to your computer system which arises in connection with your use of our website or any linked website.</li>
                <li>We host third party content on our website such as advertisements and endorsements belonging to other traders.  Responsibility for the content of such material rests with the owners of that material and we are not responsible for any errors or omissions in such material.</li>
                <li> This is referral site only. EISAU (Educational Infrastructure Services Australia) publishes testimonials but we do not warrant the accuracy or truthfulness of these testimonials. Users of the site need to make their own enquiries and satisfy themselves as to themselves as to the quality or suitability of the providers who use this site. EISAU is not party to any contract that you may enter into with a provider and you must satisfy yourself as to the Terms and Conditions made with those providers.</li>
                <li>Providers will be required to agree to the first testimonial provided by a School on their behalf. The source of subsequent testimonials will be checked by the EISAU administrator and published on the website without the consent of the provider to whom the testimonial refers.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Limitation of Liability</strong></span></h3>
            <ol>
                <li>To the full extent permitted by law, our liability for breach of an implied warranty or condition is limited to the supply of the services again, or payment of the costs of having those services supplied again.</li>
                <li>We accept no liability for any loss whatsoever including consequential loss suffered by you arising from services we have supplied.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Indemnity</strong></span></h3>
            <ol>
                <li>By accessing our website, you agree to indemnify and hold us harmless from all claims, actions, damages, costs and expenses including legal fees arising from or in connection with your use of our website.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Jurisdiction</strong></span></h3>
            <ol>
                <li>These terms and conditions are to be governed by and construed in accordance with the laws of NSW and any claim made by either party against the other which in any way arises out of these terms and conditions will be heard in NSW and you agree to submit to the jurisdiction of those Courts.</li>
                <li>If any provision in these terms and conditions is invalid under any law the provision will be limited, narrowed, construed or altered as necessary to render it valid but only to the extent necessary to achieve such validity.  If necessary the invalid provision will be deleted from these terms and conditions and the remaining provisions will remain in full force and effect.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Privacy</strong></span></h3>
            <ol>
                <li>We undertake to take all due care with any information which you may provide to us when accessing our website. However we do not warrant and cannot ensure the security of any information which you may provide to us.  Information you transmit to us is entirely at your own risk although we undertake to take reasonable steps to preserve such information in a secure manner.</li>
                <li>Our compliance with privacy legislation is set out in our separate Privacy Policy which may be accessed from our home page.
                    <br />
                    <br />
                </li>
            </ol>
            <h3><span style="color: #1279bf;"><strong>Refunds Policy</strong></span></h3>
            <ol>
                <li>Providers purchase an annual membership and listing on the EISAU website. Monies paid are not refundable.</li>
                <li>Payments to EISAU will not be refunded in circumstances where it is necessary to remove a provider from the website.</li>
            </ol>
        </div>
    </div>
</div>




</body>
</html>
