
<script type="text/javascript">

    <?php if($params->get('showMap')){ ?>
        loadMapScript();
    <?php }?>

    <?php if($params->get('autocomplete')){?>
            jQuery("#categories").chosen();
            jQuery("#citySearch").chosen();
            jQuery("#regionSearch").chosen();
            jQuery("#countrySearch").chosen();
            jQuery("#typeSearch").chosen();
    <?php } ?>

    jQuery(document).ready(function(){
            <?php if($params->get('showZipcode')){ ?>
            jQuery("#geo-location-radius").ionRangeSlider({
                grid: true,
                min: 0,
                max: 500,
                from: <?php echo !empty($radius)?$radius: "0" ?>,
                to: 500,
            });
            <?php } ?>

        jQuery("main[id^='content'] div[class^='moduletable'] div div[id^='djslider-loader108'] div[id^='djslider108'] div[id^='clone_wall_wat'] div[id^='companies-search'] div[id^='searchform'] form select[id^='categories']").change(function(){
            jQuery("#sub_categories").change(function(){
                jQuery('#categories').removeAttr("name");
                jQuery('#sub_categories').attr('name', 'categorySearch');
            });
            html= "<option>Select a Category</option>";
            var arrayFromPHP = <?php echo json_encode($categories);?>;
            var this_category = jQuery(this).val();
        });
    });

    function checkSearch(){
        jQuery("#searchkeyword").removeClass("required");
        <?php if($params->get('mandatoryKeyword')){ ?>
        if(document.getElementById('searchkeyword') && jQuery("#searchkeyword").val().length == 0){
            jQuery("#searchkeyword").focus();
            jQuery("#searchkeyword").addClass("required");
            return false;
        }
        <?php } ?>

        jQuery("#categories_chosen").removeClass("required");
        <?php if($params->get('mandatoryCategories')){ ?>
        var foo = document.getElementById('categories');
        if (foo)
        {
            if (foo.selectedIndex == 0)
            {
                jQuery("#categories").focus();
                jQuery("#categories").addClass("required");
                jQuery("#categories_chosen").addClass("required");
                return false;
            }
        }
        <?php } ?>

        jQuery("#typeSearch").removeClass("required");
        jQuery("#typeSearch_chosen").removeClass("required");
        <?php if($params->get('mandatoryCities')){ ?>
        var foo = document.getElementById('typeSearch');
        if (foo)
        {
            if (foo.selectedIndex == 0)
            {
                jQuery("#typeSearch").focus();
                jQuery("#typeSearch").addClass("required");
                jQuery("#typeSearch_chosen").addClass("required");
                return false;
            }
        }
        <?php } ?>

        jQuery("#zipcode").removeClass("required");
        <?php if($params->get('mandatoryZipCode')){ ?>
        if(document.getElementById('zipcode') && jQuery("#zipcode").val().length == 0){
            jQuery("#zipcode").focus();
            jQuery("#zipcode").addClass("required");
            return false;
        }
        <?php } ?>

        jQuery("#citySearch").removeClass("required");
        jQuery("#citySearch_chosen").removeClass("required");
        <?php if($params->get('mandatoryCities')){ ?>
        var foo = document.getElementById('citySearch');
        if (foo)
        {
            if (foo.selectedIndex == 0)
            {
                jQuery("#citySearch").focus();
                jQuery("#citySearch").addClass("required");
                jQuery("#citySearch_chosen").addClass("required");
                return false;
            }
        }
        <?php } ?>

        jQuery("#regionSearch").removeClass("required");
        jQuery("#regionSearch_chosen").removeClass("required");
        <?php if($params->get('mandatoryRegions')){ ?>
        var foo = document.getElementById('regionSearch');
        if (foo)
        {
            if (foo.selectedIndex == 0)
            {
                jQuery("#regionSearch").focus();
                jQuery("#regionSearch").addClass("required");
                jQuery("#regionSearch_chosen").addClass("required");
                return false;
            }
        }
        <?php } ?>

        jQuery("#countrySearch").removeClass("required");
        jQuery("#countrySearch_chosen").removeClass("required")
        <?php if($params->get('mandatoryCountries')){ ?>
        var foo = document.getElementById('countrySearch');
        if (foo)
        {
            if (foo.selectedIndex == 0)
            {
                jQuery("#countrySearch").focus();
                jQuery("#countrySearch").addClass("required");
                jQuery("#countrySearch_chosen").addClass("required");

                return false;
            }
        }
        <?php } ?>
        return true;
    }
 

</script>