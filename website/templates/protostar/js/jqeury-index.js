(function ($) {
    $(document).ready(function() {

        jQuery("#gruemenu > ul > li> a[title^='menu_about_us']").click(function() {
            
            return false;
        });

        jQuery("#gruemenu > ul > li> a[title^='menu-news-feeds']").click(function() {

            return false;
        });

        jQuery("#gruemenu > ul > li> a[title^='menu-schools']").click(function() {

            return false;
        });

        jQuery("#gruemenu > ul > li> a[title^='menu_become_a_provider']").click(function() {
            return false;
        });

        //Mobile
        jQuery("#sidr-main div:nth-child(2) > ul > li>  a[title^='menu_about_us']").click(function() {
            return false;
        });

        jQuery("#sidr-main div:nth-child(2) > ul > li> a[title^='menu-news-feeds']").click(function() {
            return false;
        });

        jQuery("#sidr-main div:nth-child(2) > ul > li> a[title^='menu-schools']").click(function() {
            return false;
        });

        jQuery("#sidr-main div:nth-child(2) > ul > li> a[title^='menu_become_a_provider']").click(function() {
            return false;
        });

        jQuery(".gruemenu_home").css({"float": "left","padding-left": "0px"});

        $(".je_dropbtn").click(function(){
            if ($('#top_login ul li:last-child').css('float') == 'left'){
                $("#loginBox130").css("right", "auto");
            }
        });


        //ทำการ Clone เมนู
        jQuery("div[id^='gruemenu'] ul li[class^='item-137 '] > ul li ul").css("display","block");

        var _loq =  jQuery("div[id^='gruemenu'] ul li[class^='item-137'] > ul").clone();

        jQuery("#gruemenu ul li:nth-child(5) ul li:nth-child(2) ul").remove();

        if(jQuery("#gruemenu ul li[class^='item-311'] > a").attr('title') == 'menu_become_a_provider'){
            jQuery("#gruemenu ul li[class^='item-311']  > ul li:nth-child(2)").append(_loq);
            jQuery("#gruemenu ul li[class^='item-311'] > ul ul ").find('a').each(function() {
                jQuery(this).attr('href',jQuery(this).attr('href')+'#m=become_provider');
            });
            //jQuery("#gruemenu ul li[class^='item-311']  > ul li:nth-child(2) >a").attr('href',jQuery("#gruemenu ul li[class^='item-311']  > ul li:nth-child(2) >a").attr('href')+'#m=become_provider');

            // jQuery("#gruemenu ul li[class^='item-311 has-sub parent'] li:nth-child(2)").attr('title') == 'menu_become_a_provider').append(_loq);
        }
        jQuery("#gruemenu ul li[class^='item-311'] > ul >li:nth-child(2) >a").click(function() {
            return false;
        });

        jQuery("#gruemenu ul li[class^='item-322']  > ul li:last-child > a").attr('href',jQuery("#gruemenu ul li[class^='item-322']  > ul li:last-child > a").attr('href')+'#m=for_schools');

        if($("#content").hasClass("yahoo_slide")== true){
            //ถ้ามีไสค์หน้าแรก
            jQuery(".gruemenu_home").css("color","#fff");

            jQuery("input[id^='zipcode']").css({'font-size':'15px'});

            $("#content").removeClass("span9");
            $("#content").addClass('span12');

            //Make Search Bar to image

            jQuery("div[id^='w_a1'] div[class^='row-fluid'] div[id^='aside'] div[class^='well'] ").addClass('top_search_category').removeClass('well').attr("id","clone_wall_wat");
            jQuery("div[id^='clone_wall_wat'] h4").remove();
            jQuery("div[id^='clone_wall_wat'] img").remove();

            jQuery("div[id^='clone_wall_wat']").prepend("<h1 style='font-weight: 400;font-size: 43px;margin:16px !important;' class='h1_txt_search_page'>Search for a Provider</h1>").fadeIn();


            jQuery("p[id^='txt_usethe']").css({"font-size": "16px","font-weight":"500","margin-left":"-7px","padding-bottom":"4px","word-spacing":"-0.5px","margin":'7px 3px 7px 7px'});

            //หน้าแรกช่วนค้นหา Search For A Provider
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(2)").addClass('top_search_category_inline');
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(3)").addClass('top_search_category_inline');
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(4)").addClass('top_search_category_inline');
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(5)").addClass('top_search_category_inline');
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(3)").after('<br>');





            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(2) select[id^='categories']").css({"background":"transparent","font-weight":"400","color":"#fff","font-size":"15px"});
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(3) select[id^='sub_categories']").css({"background":"transparent","font-weight":"400","color":"#fff","font-size":"15px"});
            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(5) select[id^='typeSearch']").css({"background":"transparent","font-weight":"400","color":"#fff","font-size":"15px"});

            jQuery("div[id^='companies-search'] div[id^='searchform'] form div div:nth-child(6) input[class^='search-field']").css({"margin-top":"-4.2px","height":"38px"});


            jQuery("div[class^='body']").css({"background-color": "#fff"});
            jQuery("#companies-search.vertical .dir-icon-bullseye").css('top','0px');



            jQuery("input[class^='search-field ui-autocomplete-input']").css({"width":"100%"});
            jQuery("i[class^='dir-icon-bullseye']").hide();


            jQuery("#index_testimo > div[class^='owl-controls clickable']").remove();

            jQuery("#clone_wall_wat").css("background-color","none","important");

            jQuery("#searchkeyword").css({"width":"100%","font-weight":"500","font-size":"15px"});
            jQuery("#companies-search.vertical .form-field select").css('background','#a0a0a0','important');

            //จัดช่องค้นหาหน้าแรก Search For A Provider
            jQuery("div[id^='companies-search'] div[id^='searchform'] form > div > div:nth-child(1)").addClass('top_search_category_inline');
            var clone_txt_tophome = jQuery("div[id^='companies-search'] div[id^='searchform'] form > div > div:nth-child(1)").remove().clone();
            jQuery("div[id^='companies-search'] div[id^='searchform'] form > div").append(clone_txt_tophome);

            jQuery("#clone_wall_wat").hide();
            jQuery("#clone_wall_wat").delay(1000).fadeIn();

            jQuery("div[class^='span3 yahoo_b_1']").css("margin","-100px");

            jQuery("#Map_blue").attr("onclick","window.open('index.php/schools/register')");
            jQuery("#Map_red").attr("onclick","window.open('index.php/become-a-provider/register')");
            jQuery("#aside").attr("class","span3 yahoo_b");

            jQuery("#companies-search.vertical .form-field #zipcode").css("padding","0");
            jQuery("#companies-search.vertical .form-field #searchkeyword").css("padding","0");
        }

        jQuery(".sidr ul li ul .sidr-class-item-344").remove();

        //ถ้าไม่มีไลค์
        if(!jQuery("#content").hasClass("yahoo_slide")){
            jQuery(".well").css("display","block");
            jQuery("#companies-search.vertical input[type='text']").addClass('bg_fff');


            

            //เช็คตัวจำนวนว่ามีกี่จำนวน
            if(jQuery("#results-container > div[id^='result']").length < 20){
                //jQuery("div[class^='pagination']").remove();
            }

           

            //if(jQuery("#aside > div[class^='well']").css({"margin-right":"-6px","border-radius":"-6px","border-radius":"0px","border-top-left-radius":"6px","border-bottom-left-radius":"6px","margin-top":"24px"});
            if(jQuery("#aside").hasClass("yahoo_b")){
                jQuery("#content").attr("class","yahoo_a col-lg-9");
               
                jQuery("main#content").addClass("p-r-0");
                jQuery("div.well").css({'border-radius':'0px','border-bottom-left-radius':'9px','border-top-left-radius':'9px'});

                setTimeout(function(){
                    //ใส่ ไลค์ Testimonail ใต้ ไอที่ค้นหา เมนู  FIND A PROVIDER
                    var clone_testimonail = "<div class='owl-carousel' id='testimo' style='width: 100%;  background-position: 0px 0px;'>";
                    clone_testimonail +="<strong><div style='padding: 15px 30px;text-align: center;'>I encourage schools to register and use EISAU. It has saved our school a lot of time and money!</div></strong>";
                    clone_testimonail +="<strong><div style='padding: 15px 10px;text-align: center;'>Through registering my company on EISAU, I have gained new work in schools. Thank you.</div></strong>";
                    clone_testimonail +="<strong><div style='padding: 15px 22px;text-align: center;'>EISAU offers so much for schools, and it's FREE for schools to use. Our school loves it!</div></strong>";
                    clone_testimonail +="<strong><div style='padding: 15px 10px;text-align: center;'>What a great way for businesses to connect with schools. I like being promoted through testimonials.</div></strong>";
                    clone_testimonail +="</div>";

                    jQuery("#aside").prepend(clone_testimonail);

                    jQuery(".owl-carousel").owlCarousel({
                        loop: false,
                        autoplay: false,
                        autoPlaySpeed: 5000,
                        items: 1,
                        animateOut: 'slideOutUp',
                        animateIn: 'slideInUp',
                        responsiveClass:true,
                        responsive:{
                            0:{
                                items:1
                            },
                            600:{
                                items:1
                            },
                            1180:{
                                items:1
                            },
                            1997:{
                                items:1
                            }
                        }

                    });

                    var owl = jQuery(".owl-carousel");
                    owl.trigger('owl.play',6000);

                    jQuery("div[class^='owl-controls clickable']").css({"display":"none"});
                    jQuery("#testimo > div").css({"background":"rgba(128, 128, 128, 0.14)","border-bottom-left-radius":"9px","border-top-left-radius":"9px"});


                    //แรกปุ่มสมัครสมาชิกใต้เมนูค้นหา
                    html_btn_sub_search = "<p id='wat____004'>";
                    html_btn_sub_search +='<a href="register">';
                    html_btn_sub_search +="<button type='submit' class='ui-dir-button ui-dir-button-green search-dir-button' id='btn_sub_search_register_school'>";
                    html_btn_sub_search +="<span class='ui-button-text' style='font-weight: 600;    font-style: italic;    font-size: 14px;'>School Staff register with us today</span>";
                    html_btn_sub_search +="<i class='fa fa-pencil-square-o' aria-hidden='true' id='icon_btn_sub_search_register_school'></i></button></a>";
                    html_btn_sub_search +="</p>";
                    jQuery("#aside").append(html_btn_sub_search);

                    var clone_abcde_DK = jQuery("#aside div[class^='well']:nth-child(3)").clone();
                    jQuery("#aside div[class^='well']:nth-child(3)").remove();
                    jQuery("#aside").append(clone_abcde_DK);
                    jQuery("#companies-search.vertical .form-field select").css('background','#fff','important');

                    //ย้ายช่องค้นหาไปด่านล่างสุด
                    if(jQuery(".well").css('display')=='block'){
                        var clone_search_news = jQuery("#companies-search #searchform form > div[class^='form-container'] > div:first-child").clone();
                        jQuery("#companies-search #searchform form > div[class^='form-container'] > div:first-child").remove();
                        jQuery("#companies-search #searchform form > div[class^='form-container'] ").append(clone_search_news);
                    }

                    //สร้างปุ่ม Become a Provider.
                    var make_button_becom = '<p id="wat____0043333"><a href="shop/join-membership-listing">';
                    make_button_becom += "<button type='submit' class='ui-dir-button ui-dir-button-green search-dir-button' id='btn_sub_search_register_school'>";
                    make_button_becom += "<span class='ui-button-text' style='font-weight: 600;    font-style: italic;    font-size: 16px;'>Become a Provider.</span>";
                    make_button_becom += "<i class='fa fa-pencil-square-o' aria-hidden='true' id='icon_btn_sub_search_register_school'></i>";
                    make_button_becom += "</button></a></p>";

                    if(jQuery("#testimo").css('display')=='block'){
                        jQuery("#aside").prepend(make_button_becom);
                    }



                },200);

            }

            // jQuery("#companies-search.vertical .form-field").css({'margin':'7px 0px','width':'100%'});
            // jQuery("#companies-search.vertical .form-field #zipcode").addClass("text_well");
            // jQuery("#companies-search.vertical .form-field #searchkeyword").addClass("text_well");
            jQuery("#companies-search.vertical .form-field").addClass("details_wel");

        }//จบถ้าไม่มีไลค์

        var pathname = window.location.hash;
        if(pathname =='#m=become_provider'){
            jQuery("#gruemenu ul li").removeClass('active');
            jQuery("#gruemenu ul li[class^='item-311']").addClass('active');

            jQuery("#main_cat,.main_cat").find('a').each(function() {

                var old_url = jQuery(this).attr('href');
                var url = old_url;
                new_url = url.split('#m=become_provider')[0]+'#m=become_provider';
                jQuery(this).attr('href',new_url);
            });
        }else if(pathname =='#m=for_schools'){
            jQuery("#gruemenu ul li").removeClass('active');
            jQuery("#gruemenu ul li[class^='item-322']").addClass('active');

            jQuery("#main_cat,.main_cat").find('a').each(function() {

                var old_url = jQuery(this).attr('href');
                var url = old_url;
                new_url = url.split('#m=for_schools')[0]+'#m=for_schools';
                jQuery(this).attr('href',new_url);
            });

        }



        if(jQuery("table[id^='main_cat']").attr('id')=='main_cat'){
            if(jQuery("[class^='dir-icon-bullseye'").css("top") == "-4px"){
                jQuery("[class^='dir-icon-bullseye'").css("top","0px");
                jQuery("[class^='dir-icon-bullseye'").attr('style','top:0px !important');
            }

        }
        if(jQuery("table[id^='sub_cat']").attr('id')=='sub_cat'){

            if(jQuery("[class^='dir-icon-bullseye'").css("top") == "-4px"){
                jQuery("[class^='dir-icon-bullseye'").css("top","0px");
                jQuery("[class^='dir-icon-bullseye'").attr('style','top:0px !important');

                jQuery("table[id^='sub_cat'] img").css({"border-radius":"10px","height":"auto","width":"90%","padding":"6px"});

            }

        }


        $("#w_a1 .yahoo #content #one-page-container #reviews .review .review-actions ul li:nth-child(2) a").text(function () {
            return $(this).text().replace("review", "testimonial");
        });


        $("#new-review-response #dialog-container .dialogContent h3").text(function () {
            return $(this).text().replace("review", "Testimonial");
        });

        $("#new-review-response #dialog-container .dialogContent .dialogContentBody #reviewResponseFrm  .review-repsonse > fieldset > div:nth-child(4) > label").text(function () {
            return $(this).text().replace("Review", "Testimonial");
        });


        //เช็คResposive image Register School
        //1024px
        if(jQuery("#blue_school").css("width")=='361px'){

            jQuery("#Map_blue area").attr('coords','63,417,229,469');
        }


        //768

        if(jQuery("#red_school").css("width")=='361px'){

            jQuery("#Map_red area").attr('coords','61,409,282,461');
        }
        if(jQuery("#blue_school").css("width")=='350px'){
            jQuery("#Map_blue area").attr('coords','58,401,255,446');
        }
        if(jQuery("#red_school").css("width")=='350px'){
            jQuery("#Map_red area").attr('coords','58,401,255,446');
        }
        //425
        if(jQuery("#blue_school").css("width")=='387px'){
            jQuery("#Map_blue area").attr('coords','64,443,282,494');
            jQuery("div[id^='clone_wall_wat'] h1").remove();
            jQuery("div[id^='clone_wall_wat']").prepend("<img src='../images/EISAU/img/Find-Provider.png' class='img_find_provider'>").fadeIn();
        }
        if(jQuery("#red_school").css("width")=='387px'){
            jQuery("#Map_red area").attr('coords','64,443,282,494');
        }
        //375
        if(jQuery("#blue_school").css("width")=='342px'){
            jQuery("#Map_blue area").attr('coords','56,390,248,435');
        }
        if(jQuery("#red_school").css("width")=='342px'){
            jQuery("#Map_red area").attr('coords','56,390,248,435');
        }


        //Resposive web selection Areyou a school  display 425px
        if(jQuery(".triangle_red").css('display') == 'none'){
            var x = jQuery("#_div_blue_school_00A1").clone();
            var y = jQuery("#sec_div_areyou_2 div").clone();
            jQuery("#sec_div_areyou_2 div").remove();
            jQuery("#_div_blue_school_00A1").remove();

            jQuery("#sec_div_areyou_2").append(x);
            jQuery("#_div_blue_school_00A1").append(y);

        }

        if(jQuery("div").hasClass("blognewsletters-new")){
            jQuery("#aside").addClass("fix_important_20");
        }

        jQuery("strong[class^='formRequired']").text("*").css({"color":"red"});

       

        //หน้า newsletters  ทำให้มันเปิดหน้าตามลิงตค์ที่มีแค่อันเดียว
        if(jQuery("div").hasClass('blognewsletters-new-00000')){
            jQuery("h2[itemprop^='name']").remove();
        }


        jQuery("a[class^='btn']").each(function(){
            jQuery(this).attr("target","_blank");
        });


        // หน้า Testimonials from Happy Clients!

        if(jQuery("#aside div[class^='well']:nth-child(2) h4").text() != "Find a Provider"){
            jQuery("#testimo").remove();
            var clone_abcde = jQuery("#wat____004").clone();
            jQuery("#wat____004").remove();
            jQuery("#aside").prepend(clone_abcde);
        }
        jQuery("#aside > div[class^='well']:last").css({
            "border":"none"
        });

        
    });
})(jQuery);

function enableGeoLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setGeoLocation);
        //console.debug("geolocation enabled");
        jQuery("#loading-geo-locaiton").show();
    }else{
        //console.debug("geolocation not supported");
    }

    jQuery("#enable-geolocation").addClass("active");
    jQuery("#disable-geolocation").removeClass("active");
    jQuery("#geolocation").val(1);
}

function disableGeoLocation(){
    jQuery("#enable-geolocation").removeClass("active");
    jQuery("#disable-geolocation").addClass("active");
    jQuery("#geolocation").val(0);
    jQuery("#loading-geo-locaiton").hide();
    jQuery("#geo-latitude").val('');
    jQuery("#geo-longitude").val('');
}


function setGeoLocation(position){
    jQuery("#loading-geo-locaiton").hide();
    //  console.debug("set geolocation");
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    jQuery("#geo-latitude").val(latitude);
    jQuery("#geo-longitude").val(longitude);
}

if(jQuery("#gruemenu > ul > li:last-child > ul").hasClass('sub-menu') == true){
    jQuery("i.fa-sign-in").hide();
    jQuery("#gruemenu > ul > li:last-child > a").text("Account");
    jQuery("#gruemenu > ul > li:last-child > a").attr("href","<?php echo JUri::root(true); ?>/index.php/login/control/useroptions");  
    jQuery("#gruemenu > ul > li:last-child > a").attr('style', 'padding: 14px 33px 14px 18px !important');
}

jQuery("._button_homee_login").append("  <i class='fa fa-sign-in' aria-hidden='true'></i>");



