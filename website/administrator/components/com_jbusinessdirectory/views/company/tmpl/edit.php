

<?php
/**
 * @package    JBusinessDirectory
 * @subpackage  com_jbusinessdirectory
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');

$attributeConfig = $this->item->defaultAtrributes;
$enablePackages = $this->appSettings->enable_packages;

$app = JFactory::getApplication();
$showSteps = JRequest::getVar("showSteps",false);
$options = array(
	'onActive' => 'function(title, description) {
		description.setStyle("display", "block");
		title.addClass("open").removeClass("closed");
	}',
	'onBackground' => 'function(title, description) {
		description.setStyle("display", "none");
		title.addClass("closed").removeClass("open");
	}',
	'startOffset' => 0,  // 0 starts on the first tab, 1 starts the second, etc...
	'useCookie' => true, // this must not be a string. Don't use quotes.
);

?>

<?php
	if($app->getClientId() == 0){
		include 'components/com_jbusinessdirectory/views/companies/tmpl/layout_font_end.php';
	}
	if($app->getClientId() == 1){
		include '../components/com_jbusinessdirectory/views/companies/tmpl/layout_backend.php';
	}
			
		
?>
