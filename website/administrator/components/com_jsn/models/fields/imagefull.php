<?php
/**
* @copyright	Copyright (C) 2013 Jsn Project company. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* @package		Easy Profile
* website		www.easy-profile.com
* Technical Support : Forum -	http://www.easy-profile.com/support.html
*/

defined('_JEXEC') or die;




JFormHelper::loadFieldClass('hidden');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     Joomla.Libraries
 * @subpackage  Form
 * @since       3.1
 */
class JFormFieldImagefull extends JFormFieldHidden
{
	public $type = 'Imagefull';
	
	protected function getInput()
	{
		$doc = JFactory::getDocument();
		$dir = $doc->direction;

		$jsn_config = JComponentHelper::getParams('com_jsn');
			
		$this->element['class']='';
		$attribs=array(
			'style' => 'float:left;width:50px;margin-right:10px;border-radius:2px;margin-bottom:5px;',
			'class' => 'img_'.$this->element['name']
		);
		if($dir=='rtl')
		{
			$attribs=array(
				'style' => 'float:right;width:50px;margin-left:10px;border-radius:2px;margin-bottom:5px;',
				'class' => 'img_'.$this->element['name']
			);
		}
		$html=array();

		// Check Mobile
		$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
		$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
		$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
		$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$windowsphone = strpos($_SERVER['HTTP_USER_AGENT'],"Windows Phone");
		if($windowsphone || $iphone || $android || $palmpre || $ipod || $berry == true) $mobile=true;
		else $mobile=false;

		// Set Default
		if($this->element['name']=='avatar') $default_image='components/com_jsn/assets/img/default.jpg';
		else $default_image='components/com_jsn/assets/img/no_image.gif';

		if(isset($this->element['default'])) $default_image=$this->element['default'];
		if(isset($this->element['removed_default'])) $default_image=$this->element['removed_default'];

		// Clear value inherit by default value
		if($this->value==$default_image) $this->value='';

		// Image Preview
		if(empty($this->value) || $this->value=='true') 
		{
			$this->value='';
			$img_src=$default_image;
			$html[]=JHtml::_('image', $default_image ,$this->element['alt'],$attribs);
		}
		else 
		{
			$img_src=$this->value;
			$html[]=JHtml::_('image', preg_replace('~_(?!.*_)~', 'mini_', $this->value) ,$this->element['alt'],$attribs);
		}

		// Add Bootstrap JS
		JHtml::_('bootstrap.framework');
		
		$script='
		jQuery(function($) {
			$(\'input[name="'.str_replace(']', '_delete]', $this->name).'"]\').change(function(){
				if($(this).is(":checked")){
					$(\'#jform_'.$this->element['name'].'\').attr(\'oldvalue\',$(\'#jform_'.$this->element['name'].'\').val());
					$(\'#jform_'.$this->element['name'].'\').val(\'\');
					$(\'#jform_'.$this->element['name'].'\').change();
				} 
				else 
				{
					$(\'#jform_'.$this->element['name'].'\').val($(\'#jform_'.$this->element['name'].'\').attr(\'oldvalue\'));
					$(\'#jform_'.$this->element['name'].'\').change();
				} 
			});
			$(\'#jform_upload_'.$this->element['name'].'\').change(function(){
				$(\'#jform_'.$this->element['name'].'\').val(\'true\');
				$(\'#jform_'.$this->element['name'].'\').change();
			});
		});';
		$doc->addScriptDeclaration( $script );

		$html[]='<input type="file" name="jform[upload_'.$this->element['name'].']" id="jform_upload_'.$this->element['name'].'" accept="image/*">';
		$html[]=parent::getInput();
		
		if($this->element['required']!='true' && $this->value) $html[]='<fieldset class="checkboxes" style="clear:both;"><label class="checkbox"><input type="checkbox" name="'.str_replace(']', '_delete]', $this->name).'"/><span>'.JText::_('COM_JSN_DELETE_IMAGE').'</span></label></fieldset>';
		$html[]='<div style="clear:both"></div>';
		return implode('', $html);
		
	}
	
	public function getImage()
	{
		$attribs=array(
			'class' => $this->element['imageclass']
		);
		$html=array();
		
		if($this->value) $html[]=JHtml::_('image', $this->value,$this->element['alt'],$attribs);
		return implode('', $html);
	}
	
	public function getValue()
	{
		return $this->value;
	}
	
	
	
}
