<?php
/**
* @copyright	Copyright (C) 2013 Jsn Project company. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* @package		Easy Profile
* website		www.easy-profile.com
* Technical Support : Forum -	http://www.easy-profile.com/support.html
*/

defined('_JEXEC') or die;


global $_FIELDTYPES;
$tmp_form=JFactory::getApplication()->input->get->getArray();
if(isset($tmp_form['id'])) $_FIELDTYPES['image']='COM_JSN_FIELDTYPE_IMAGE';

class JsnImageFieldHelper
{
	public static function create($alias)
	{
		$db = JFactory::getDbo();
		$query = "ALTER TABLE #__jsn_users ADD ".$db->quoteName($alias)." VARCHAR(255)";
		$db->setQuery($query);
		$db->query();
	}
	
	public static function delete($alias)
	{
		$db = JFactory::getDbo();
		$query = "ALTER TABLE #__jsn_users DROP COLUMN ".$db->quoteName($alias);
		$db->setQuery($query);
		$db->query();
	}
	
	public static function getXml($item)
	{
		require_once(JPATH_SITE.'/components/com_jsn/helpers/helper.php');
		$hideTitle= ($item->params->get('hidetitle',0) && JFactory::getApplication()->input->get('view','profile')=='profile' && JFactory::getApplication()->input->get('option','')=='com_jsn') || ($item->params->get('hidetitleedit',0) && (JFactory::getApplication()->input->get('layout','')=='edit' || JFactory::getApplication()->input->get('view','')=='registration'));
		if(JFactory::getApplication()->input->get('view','profile')=='profile' && JFactory::getApplication()->input->get('option','')=='com_jsn' && $item->params->get('titleprofile','')!='') $item->title=$item->params->get('titleprofile','');
		$defaultvalue=($item->params->get('image_defaultvalue','')!='' ? 'default="'.$item->params->get('image_defaultvalue','').'"' : '');//(isset($item->params['image_defaultvalue']) && $item->params['image_defaultvalue']!='' ? 'default="'.$item->params['image_defaultvalue'].'"' : '');
		
		if($item->params->get('field_readonly','')==1 && JFactory::getApplication()->isSite()) $readonly='readonly="true"';
		elseif($item->params->get('field_readonly','')==2 && JFactory::getApplication()->input->get('view')!='registration' && JFactory::getApplication()->isSite()) $readonly='readonly="true"';
		else $readonly='';

		$xml='
			<field
				name="'.$item->alias.'"
				type="imagefull"
				id="'.$item->alias.'"
				imageclass="'.$item->alias.' '.$item->params->get('image_class','')/*$item->params['image_class']*/.'"
				description="'.JsnHelper::xmlentities(($item->description)).'"
				accept="image/*"
				label="'.($hideTitle ? '' : JsnHelper::xmlentities($item->title)).'"
				alt="'.$item->params->get('image_alt','')/*$item->params['image_alt']*/.'"
				'.$defaultvalue.'
				'.$readonly.'
				width="'.$item->params->get('image_width','200')/*$item->params['image_width']*/.'"
				height="'.$item->params->get('image_height','200')/*$item->params['image_height']*/.'"
				width_thumb="'.$item->params->get('image_thumbwidth','75')/*$item->params['image_width']*/.'"
				height_thumb="'.$item->params->get('image_thumbheight','75')/*$item->params['image_height']*/.'"
				cropwebcam="'.$item->params->get('image_cropwebcam','0')/*$item->params['image_height']*/.'"
				required="'.($item->required && JFactory::getApplication()->input->get('jform',null,'array')==null ? 'true' : 'false' ).'"
				requiredfile="'.($item->required ? 'true' : 'false' ).'"
				validate="image"
			/>
		';
		return $xml;
	}
	
	public static function loadData($field, $user, &$data)
	{
		$alias=$field->alias;
		$alias_mini=$field->alias.'_mini';

		$jsn_config = JComponentHelper::getParams('com_jsn');

		if($field->params->get('field_readonly','')==1 && JFactory::getApplication()->isSite()) return;
		if($field->params->get('field_readonly','')==2 && JFactory::getApplication()->input->get('task')=='profile.save' && JFactory::getApplication()->isSite()) return;

		if(isset($user->$alias) && $user->$alias!='' && file_exists(JPATH_SITE.'/'.$user->$alias))
		{
			$data->$alias=$user->$alias;
			if(strpos($user->$alias,'/profiler/')) $data->$alias_mini=str_replace('_', 'mini_', $user->$alias);
			else $data->$alias_mini=$user->$alias;
		}
		elseif($field->params->get('image_defaultvalue','')!=''/*isset($field->params['image_defaultvalue']) && $field->params['image_defaultvalue']!=''*/)
		{
			$data->$alias=$field->params->get('image_defaultvalue','');//$field->params['image_defaultvalue'];
			if(isset($user->$alias) && strpos($user->$alias,'/profiler/')) $data->$alias_mini=str_replace('_', 'mini_', $field->params->get('image_defaultvalue','')/*$field->params['image_defaultvalue']*/);
			else $data->$alias_mini=$field->params->get('image_defaultvalue','');
		}
		elseif($alias == 'avatar')
		{
			$data->$alias='components/com_jsn/assets/img/default.jpg';
			$data->$alias_mini='components/com_jsn/assets/img/default.jpg';
		}
	}
	
	public static function storeData($field, $data, &$storeData)
	{
		$jsn_config = JComponentHelper::getParams('com_jsn');
		
		// Set Upload Dir
		$upload_dir=JPATH_SITE.'/images/profiler/';
		if(!file_exists($upload_dir)) 
		{ 
			mkdir($upload_dir); 
		}

		// Get Alias
		$alias=$field->alias;
		if(isset($data[$alias])) $storeData[$alias]=$data[$alias];

		// Delete Image
		$jform=JFactory::getApplication()->input->post->getArray();
		if(isset($jform['jform'][$field->alias.'_delete']))
		{
			// Delete old file
			foreach (glob($upload_dir.$alias.$data['id'].'*') as $deletefile)
			{
				unlink($deletefile);
			}
			
			$storeData[$alias]='';
			return;
		}

		// Single file array
		$jform=JFactory::getApplication()->input->files->get('jform',null,'raw');
		if(isset($jform['upload_'.$alias])) $jform_file=$jform['upload_'.$alias];
		if(isset($jform_file['name']) && strlen($jform_file['name'])>4)
			$fileArray=array(
				'name' => $jform_file['name'],
				'type' => $jform_file['type'],
				'tmp_name' => $jform_file['tmp_name'],
				'error' => $jform_file['error'],
				'size' => $jform_file['size'],
			);
		else
			$fileArray=array();

		if(file_exists(JPATH_ADMINISTRATOR.'/components/com_k2/lib/class.upload.php')) require_once(JPATH_ADMINISTRATOR.'/components/com_k2/lib/class.upload.php');
		else require_once(JPATH_ADMINISTRATOR.'/components/com_jsn/assets/class.upload.php');
		$foo = new Upload($fileArray);
		if ($foo->uploaded)
		{
			// Delete old file
			foreach (glob($upload_dir.$alias.$data['id'].'*') as $deletefile)
			{
				unlink($deletefile);
			}
			
			$md5=md5(time().rand());
			// Store & Resize Image Thumbs
			$filename=$alias.$data['id'].'mini_'.$md5;
			$foo->file_new_name_body = $filename;
			$foo->image_resize = true;
			$foo->image_ratio_crop = true;
			$foo->image_convert = 'jpg';
			if($field->params->get('image_thumbwidth',100)/*['image_thumbwidth']*/>0) $foo->image_x = $field->params->get('image_thumbwidth',100);//$field->params['image_thumbwidth'];
			if($field->params->get('image_thumbheight',100)/*['image_thumbheight']*/>0) $foo->image_y = $field->params->get('image_thumbheight',100);//$field->params['image_thumbheight'];
			//die($foo->image_x);
			$foo->Process($upload_dir);
			// Store & Resize Image
			$filename=$alias.$data['id'].'_'.$md5;
			$foo->file_new_name_body = $filename;
			$foo->image_resize = true;
			$foo->image_ratio_crop = true;
			$foo->image_convert = 'jpg';
			if($field->params->get('image_width',500)/*['image_width']*/>0) $foo->image_x = $field->params->get('image_width',500);//$field->params['image_width'];
			if($field->params->get('image_height',500)/*['image_height']*/>0) $foo->image_y = $field->params->get('image_height',500);//$field->params['image_height'];
			$foo->Process($upload_dir);
			if ($foo->processed)
			{
				$storeData[$alias]='images/profiler/'.$foo->file_dst_name;
				$foo->Clean();
			}
		}
		elseif(isset($_SESSION['_tmp_img'.$alias])){
			$md5=md5(time().rand());
			$filename=$alias.$data['id'].'_'.$md5.'.jpg';
			$filename_mini=$alias.$data['id'].'mini_'.$md5.'.jpg';
			$path = JPATH_SITE.'/tmp/';
	    	$file = $_SESSION['_tmp_img'.$alias];
			if(file_exists($path . str_replace('.jpg','_big.jpg',$file)) && file_exists($path . str_replace('.jpg','_big.jpg',$file))){
				rename($path . str_replace('.jpg','_big.jpg',$file),JPATH_SITE.'/images/profiler/'.$filename);
				rename($path . str_replace('.jpg','_small.jpg',$file),JPATH_SITE.'/images/profiler/'.$filename_mini);
				$storeData[$alias]='images/profiler/'.$filename;
			} 
			unset($_SESSION['_tmp_img'.$alias]);
		}
	}
	
	public static function image($field)
	{
		$value=$field->__get('value');
		if (empty($value))
		{
			return JHtml::_('users.value', $value);
		}
		else
		{
			return $field->getImage();
		}
		
	}
	

}
