<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class JBusinessDirectorySubscriptionsViewJBusinessDirectorySubscriptions extends JViewLegacy
{
	function display($tpl = null){
		$app =& JFactory::getApplication();
		$app->redirect('index.php?option=com_jbusinessdirectory&view=paymentprocessors');		
		parent::display($tpl);
	}	
}