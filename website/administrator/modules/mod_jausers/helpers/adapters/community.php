<?php
/**
 * $JA#COPYRIGHT$
 */
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' ); 


/**
 * @param object $params
 * @author JA	
 **/
class ModJAUsersHelpersAdaptersCommunity
{
	public static function getComSelection()
	{
		$selection = array(
			'cu.avatar', 
			'concat("index.php?option=com_community&view=users&layout=edit&id=",u.id) as url'
		);	
		return $selection;
	}

	public static function joinComponent($db, $query, $params)
	{
		$query->leftJoin($db->quoteName('#__community_users','cu').'on (u.id = cu.userid)');
	}
}

