<?php
/**
 * $JA#COPYRIGHT$
 */
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' ); 


/**
 * @param object $params
 * @author JA	
 **/
class ModJAUsersHelpersAdaptersEasysocial
{
	public static function getComSelection()
	{
		$selection = array(
			'concat("media/com_easysocial/avatars/users/",u.id,"/",sa.square) as avatar', 
			'concat("index.php?option=com_easysocial&view=users&layout=form&id=",u.id) as url'
		);	
		return $selection;
	}

	public static function joinComponent($db, $query, $params)
	{
		$query->leftJoin($db->quoteName('#__social_avatars','sa').'on (u.id = sa.uid)');
	}
}

