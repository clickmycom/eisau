<?php
/**
 * $JA#COPYRIGHT$
 */
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' ); 


/**
 * @param object $params
 * @author JA	
 **/
class ModJAUsersHelpersAdaptersK2
{
	
	public static function getComSelection()
	{
		$selection = array(
				'concat("media/k2/users/",ku.image) as avatar',
				'concat("index.php?option=com_k2&view=user&cid=",u.id) as url'
			);
		return $selection;
	}
	public static function joinComponent($db, $query, $params)
    {
		$query->leftJoin($db->quoteName('#__k2_users','ku').'on (u.id = ku.userID)');
				
		$k2group = $params->get('k2group');    	
	    if(!empty($k2group)) 
	    {
    		$allgroups = false;
    		foreach ($params->get('k2group') as $key) {
    			if ($key == 0) {
    				$allgroups = true;
    				break;
    			}
    		}
    		if (!$allgroups) {
			    $k2groups = implode(',', $params->get('k2group'));
			    $query->where($db->quoteName('ku.group').'in ('.$k2groups.')');
    		}
    	}
    }
}

