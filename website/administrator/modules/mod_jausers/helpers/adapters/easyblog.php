<?php
/**
 * $JA#COPYRIGHT$
 */
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' ); 


/**
 * @param object $params
 * @author JA	
 **/
class ModJAUsersHelpersAdaptersEasyblog
{
	public static function getComSelection()
	{
		$selection = array(
				'concat("images/easyblog_avatar/",eu.avatar) as avatar',
				'concat("index.php?option=com_easyblog&view=bloggers&layout=form&id=",u.id) as url'
			);
		return $selection;
	}
	public static function joinComponent($db, $query, $params)
	{
		$query->leftJoin($db->quoteName('#__easyblog_users', 'eu').'on (u.id = eu.id)' )
				->leftJoin($db->quoteName('#__easyblog_acl_group','eag').'on (ugm.group_id = eag.content_id)' );
		switch ($params->get('author')) {
			case '0':
			case '1':
				$query->where($db->quoteName('eag.acl_id').'= 1 and'.$db->quoteName('eag.status').'='.$params->get('author'));
				break;
			default:
				break;
		}
	}
}