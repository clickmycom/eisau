<?php
/**
 * $JA#COPYRIGHT$
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
/**
 *
 * Mod JA Users helper class
 * @author JA
 *
 */
class ModJAUsersHelpersHelper
{
	public function urlViewAll ($com)
	{	
		$url_viewlall = '';
		switch ($com) {
			case 'k2':
				$url_viewall = 'index.php?option=com_k2&view=users';
				break;
			case 'community':
				$url_viewall = 'index.php?option=com_community&view=users';
				break;
			case 'easysocial':
				$url_viewall = 'index.php?option=com_easysocial&view=users';
				break;
			default:
				$url_viewall = 'index.php?option=com_users&view=users';
				break;
		}
		return $url_viewall;
	}

	public function getUsergroup($query, $db, $params) 
	{
		$usergroup = $params->get('usergroup');
		if(!empty($usergroup)) 
		{
			$allgroups = false;
			foreach ($params->get('usergroup') as $key) {
				if ($key == 0) {
					$allgroups = true;
					break;
				}
			}
			if (!$allgroups) {
				$groups = implode(',', $params->get('usergroup'));
				$query->where($db->quoteName('ugm.group_id').'in ('.$groups.')');
			}
		}

	}

	public function getLimit($query, $params)
	{
		$count = $params->get('count');
		$query->setLimit($count);
	}
	public function getOrder($query, $db)
	{
		$query->order($db->quoteName('u.registerDate').'desc'   );
	}
	public function getActive($query, $db, $params)
	{	
		switch ($params->get('activation')) {
			case '0':
				$query->where($db->quoteName('u.activation').' = 1');
				break;
			case '1':
				$query->where($db->quoteName('u.activation').' !=1');
				break;
			default:
				break;
		}

	}

	public function getBlock($query, $db, $params)
	{
		switch ($params->get('block')) {
			case '0':
			case '1':
				$query->where($db->quoteName('u.block').'='.$params->get('block'));
				break;
			default:
				break;
		}
	}

	public function getAvatar($user, $module)
	{
		if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
		if (is_file(JPATH_ROOT.DS.$user->avatar)) {
			$user->avatar = JURI::root().$user->avatar;
		} else {
			$user->avatar = JURI::base().'modules/'.$module->module.'/assets/images/noavatar.jpg';
		}

	}

	public function getUrl($user)
	{
		$user->url = JURI::base().$user->url; 
	}

	public function getTime($user)
	{
		if ( version_compare( JVERSION, '3.0', '<' ) == 1) {             
			$reg = JFactory::getDate($user->registerDate)->toformat('%d %b %Y');
			$yesterday = JFactory::getDate('now - 1 day')->toformat('%d %b %Y');
			$today = JFactory::getDate('now')->toformat('%d %b %Y');
		}
		 else {
			$reg = JFactory::getDate($user->registerDate)->format('d M Y');
			$yesterday = JFactory::getDate('now -1 day')->format('d M Y');
			$today = JFactory::getDate('now')->format('d M Y');
		}
		$regUnix = JFactory::getDate($user->registerDate)->toUnix();
		$nowUnix = JFactory::getDate('now')->toUnix();
		$timeReg = $nowUnix - $regUnix;         
		$user->regDate = $reg;
		if ($reg == $yesterday) {
			$user->regDate = 'Yesterday';
		}elseif ($reg == $today) {
			$user->regDate = 'Today';
		} else {
			$user->regDate = $reg;
		}
		if ($timeReg > 172800) {
			$user->new = 0;
		} else {
			$user->new = 1;
		}  
	}

	public function getOnline($user)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('u.id')	
				->from($db->quoteName('#__users','u'))
				->leftJoin($db->quoteName('#__session','s').'on (u.id = s.userid)')
				->where($db->quoteName('u.id').'='.$user->id)
				->where($db->quoteName('s.client_id').'= 0')
				->where($db->quoteName('s.guest').'= 0');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if (!empty($result)) {
			$user->online = 1;
		} else {
			$user->online = 0;
		}
	}

	public function getListUsers($params,$module, $com)
	{   
		$db    = JFactory::getDbo();
		$user  = JFactory::getUser();
		$query = $db->getQuery(true);
		if(!empty($com))
		{
			$helper = 'ModJAUsersHelpersAdapters'.ucfirst($com);
		}
		$comSelection = $helper::getComSelection();
		$selection = array ('u.*',);
		$selection = array_merge($selection, $comSelection);
		$query->select($selection) 
				->from($db->quoteName('#__users','u'))
				->leftJoin($db->quoteName('#__user_usergroup_map','ugm').'on (u.id = ugm.user_id)');
		$helper::joinComponent($db, $query, $params);
		$this->getUsergroup($query, $db, $params);
		$this->getActive($query, $db, $params);
		$this->getBlock($query, $db, $params);
		$this->getLimit($query, $params);
		$this->getOrder($query, $db);
		$query->group('u.id');
		$db->setQuery($query);
		$users = $db->loadObjectList(); 
		foreach ($users as $user) {
			$this->getAvatar($user, $module);
			$this->getUrl($user);
			$this->getTime($user);
			$this->getOnline($user);
		}
		return $users;
	}

	public function getStatus($com) {
		$db = JFactory::getDbo();
		$db->setQuery("SELECT enabled FROM #__extensions WHERE element = '".$com."'");
		$status = $db->loadResult();
		if(isset($status)) {
			if (JComponentHelper::isEnabled($com, true)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getComponentsList()
	{
		$path = dirname(__FILE__).'/adapters';
		$scan = scandir ($path);
		$files = preg_grep('/(.*?).php/', $scan);
		$coms = array();
		foreach ($files as $file) {
			$value = str_replace('.php','',$file);
			$com = 'com_'.$value;
			if ($this->getStatus($com)) {
				$coms[] = $value;
 			}
		}
		return $coms;
	}

}
