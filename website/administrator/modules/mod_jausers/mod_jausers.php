<?php
/**
 * $JA#COPYRIGHT$
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JLoader::registerPrefix('ModJAUsers', JPATH_ADMINISTRATOR.'/modules/'.$module->module);
$assets = new ModJAUsersAssetsAsset();
$style = $assets->addStyleSheet($module);
$helper = new ModJAUsersHelpersHelper();
$coms = $helper->getComponentsList();
//DISPLAYING
require JModuleHelper::getLayoutPath($module->module, $params->get('layout', 'default'));
