<?php
/**
 * $JA#COPYRIGHT$
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' ); 

class ModJAUsersAssetsAsset {

	function addStyleSheet($module) {
		$doc = JFactory::getDocument();
		$basepath = JURI::base(true).'/modules/' . $module->module . '/assets/';
		$doc->addStyleSheet($basepath.'css/jausers.css');
	}
}