<?php
/**
 * $JA#COPYRIGHT$
 */

// No direct access
defined('_JEXEC') or die();

class JFormFieldK2Group extends JFormField
{
    protected $type = 'k2group';

    function getInput()
    {
        require_once(__DIR__.'/../../helpers/helper.php');
        $helper = new ModJAUsersHelpersHelper();
        $option = array();
        $option[] = JHTML::_('select.option', '' ,'All Groups');  
        if ($helper->getStatus('com_k2')) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select(array('id','name')) ->from($db->quoteName('#__k2_user_groups'));
            $db->setQuery($query);
            $groups = $db->loadObjectList();
            if (!empty($groups)) {
                foreach ($groups as $group) {
                    $option[] = JHTML::_('select.option', $group->id ,$group->name);  
                }
            }
        }
        $lists = JHTML::_('select.genericlist', $option, "{$this->name}[]", ' multiple="multiple"  size="10" ', 'value', 'text', $this->value);
        return $lists;
    }
}