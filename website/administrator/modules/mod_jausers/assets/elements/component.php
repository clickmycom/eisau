<?php
/**
 * $JA#COPYRIGHT$
 */

// No direct access
defined('_JEXEC') or die();

class JFormFieldComponent extends JFormField
{
	protected $type = 'component';

	function getInput()
	{
		require_once(__DIR__.'/../../helpers/helper.php');
		$helper = new ModJAUsersHelpersHelper();
		$coms = $helper->getComponentsList();
		$option = array();
		foreach ($coms as $com) {
			$option[] = JHTML::_('select.option', $com, JText::_(strtoupper('jausers_'.$com)));           
		 }         
		$lists = JHTML::_('select.genericlist', $option, $this->name, '', 'value', 'text', $this->value);
		return $lists;
	}
}