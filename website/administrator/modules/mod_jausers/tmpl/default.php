<?php
/**
 * $JA#COPYRIGHT$
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="ja-users ja-users<?php echo $module->id?><?php echo $params->get('moduleclass_sfx')?>">
<?php 
if(!empty($coms)) :
$key = array_search($params->get('component'),$coms);
$arr = array('startOffset' => $key); 
?>
<?php echo JHtml::_('tabs.start','jausers-tabs',$arr); ?>
<?php
foreach ($coms as $com):
	$com = str_replace('.php','',$com);
	$users = $helper->getListUsers($params,$module,$com);
	if (!empty($users)) :
	echo JHtml::_('tabs.panel', JText::_(strtoupper('jausers_'.$com)),$com);
	?>
	<div id="<?php echo $com ?>" class="tab">
	<?php $count = substr_count(json_encode($users), '"new":1'); ?>
	<?php if($count > 0) : ?>
	<div class="ja-users-toolbar">
		<div class="alert alert-info">
		<button data-dismiss="alert" class="close" type="button">×</button>
			<?php echo $count ?> <?php echo JText::_('JAUSERS_NEW_MEMBERS') ?>
		</div>
	</div>
	<?php endif ?>
		<ul class="ja-users-list">
		<?php
		foreach ($users as $user) :
		?>
		<li>
		<?php if ($user->new == 1): ?><span class="badge badge-new">New</span><?php endif ?> 
			<a class="user-avatar <?php echo ($user->online == 1)? 'user-online': '' ?>" href="<?php echo $user->url ?>" title="<?php echo $user->name?>">
				<img src="<?php echo $user->avatar ?>" alt="<?php echo $user->name?>" />
			</a>
			<a class="user-name" href="<?php echo $user->url ?>" title="<?php echo $user->name; ?>">
				<?php 
					if ($params->get('username') == 0) {
						echo $user->name;
					} else {
						echo $user->username;
					}
				?>
			</a>
			<time class="join-date">
				<span class="icon-clock">&nbsp;</span><?php echo $user->regDate; ?>
			</time>
		</li>
		<?php
		endforeach;
		?>
		</ul>

		<div class="ja-users-foot">
			<a href=" <?php echo JURI::base().$helper->urlViewAll($com); ?> ">
				<?php echo JText::_('JAUSERS_VIEW_ALL') ?>
			</a>
		</div>
	</div>	
	<?php 
	endif; 

endforeach;
endif;
echo JHtml::_('tabs.end');
?>
</div>
