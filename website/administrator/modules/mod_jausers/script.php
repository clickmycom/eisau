<?php 

defined('_JEXEC') or die;

class mod_jausersInstallerScript
{
	function postflight($type, $parent)
	{
		if (strtolower($type) == 'install') {
			$db = JFactory::getDbo();
			$this->addParams($db,$parent);
			$this->addModuleMenu($db, $parent);
	        echo 'New module has been installed!';
		}
	}
	function addParams($db, $parent)
	{
		$query = $db->getQuery(true);
		$params = array (
		$db->quoteName('published').'= 1',
		$db->quoteName('position').'='.$db->quote('cpanel'),
		$db->quoteName('params') .'='. $db->quote('
			{"username":"0","component":"users","count":"8","block":"","activation":"","usergroup":[""],
			"k2group":[""],"author":"","layout":"_:default","moduleclass_sfx":"","cache":"1",
			"cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3",
			"header_class":"","style":"0"}')
			);
		$query->update($db->quoteName('#__modules')) 
				->set($params)
				->where($db->quoteName('module').'='.$db->quote($parent->get('element')));
        $db->setQuery($query);
        $db->execute();
	}

	function addModuleMenu($db, $parent)
	{
		$menu = new stdClass();
		$menu->moduleid = $this->getModuleId($db, $parent);
		$menu->menuid = 0;
		$result = $db->insertObject('#__modules_menu',$menu);
	}

	function getModuleId($db, $parent)
	{
		$query = $db->getQuery(true);
		$query->select($db->quoteName('id')) 
				->from($db->quoteName('#__modules'))
				->where($db->quoteName('module').'='.$db->quote($parent->get('element')));
        $db->setQuery($query);
		$module = $db->loadObject();
		return $module->id;
	}
}