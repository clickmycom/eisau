<?php
/**
 * @package      JA Admin
 *
 * @author       JoomlArt
 * @copyright    Copyright (C) 2012-2013. All rights reserved.
 * @license      http://www.gnu.org/licenses/gpl.html GNU/GPL, see LICENSE.txt
 */

defined('_JEXEC') or die();

class pkg_ja_adminInstallerScript
{
    /**
     * Called before any type of action
     *
     * @param     string              $route      Which action is happening (install|uninstall|discover_install)
     * @param     jadapterinstance    $adapter    The object responsible for running this script
     *
     * @return    boolean                         True on success
     */
    public function preflight($route, JAdapterInstance $adapter)
    {
        return true;
    }


    /**
     * Called after any type of action
     *
     * @param     string              $route      Which action is happening (install|uninstall|discover_install)
     * @param     jadapterinstance    $adapter    The object responsible for running this script
     *
     * @return    boolean                         True on success
     */
    public function postflight($route, JAdapterInstance $adapter)
    {
        // Enable the helper plugin right after install it
        if ( $route == 'install' ) 
        {      
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->update('#__extensions')
                ->set('enabled=1')
                ->where(array('element=' . $db->quote('admintplhelper'), 'type=' . $db->quote('plugin')));
            $db->setQuery($query);   
            $db->execute();     
        }
    }
}

