<?php 
$items = $displayData['items'];
$q = $displayData['keyword'];

$ctype = '';

if ($items && count($items)): ?>
<ul>
<?php foreach ($items as $item) :?>
	<?php if ($ctype != $item->type): 
		$ctype = $item->type;
	?>
	<li class="type"><strong><?php echo $ctype ?></strong></li>
	<?php endif ?>

	<li><?php echo $item->id . ' - ' . $item->title ?></li>
<?php endforeach ?>
</ul>
<?php else: ?>
<ul>
	<li class="type">No result found for <span class="search-query"><?php echo $q ?></span></li>
</ul>
<?php endif ?>