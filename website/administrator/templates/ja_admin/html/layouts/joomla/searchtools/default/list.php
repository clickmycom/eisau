<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$data = $displayData;

// Load the form list fields
$list = $data['view']->filterForm->getGroup('list');
$listStyle = JFactory::getApplication()->getUserState('modules.listStyle');
$option = JFactory::getApplication()->input->get('option');
$listModules = ($option == 'com_modules');
$gridView = ($listModules && (empty($listStyle) || $listStyle == 'grid'));
?>

<?php if ($gridView) : ?>
	<a href="index.php?option=com_modules&listStyle=list" class="btn btn-default" title="<?php echo JText::_('TPL_LIST_VIEW') ?>"><i class="icon-list"></i><?php echo JText::_('TPL_LIST_VIEW') ?></a>
<?php else: ?>
	<?php if ($listModules): ?>
	<a href="index.php?option=com_modules&listStyle=grid" class="btn btn-default" title="<?php echo JText::_('TPL_GRID_VIEW') ?>"><i class="icon-grid"></i><?php echo JText::_('TPL_GRID_VIEW') ?></a>
	<?php endif ?>
	<?php if ($list) : ?>
		<div class="ordering-select hidden-phone">
			<?php foreach ($list as $fieldName => $field) : ?>
				<div class="js-stools-field-list">
					<?php echo $field->input; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endif ?>