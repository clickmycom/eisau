<?php 
$list = $displayData['list'];
$q = $displayData['keyword'];
$searchType = $displayData['searchType'];

if ($list && count($list)): ?>
<ul>
<?php foreach ($list as $type => $groups) :?>
	<li class="type"><strong><?php echo $type ?></strong><span class="btn-search-more<?php if ($searchType): ?> btn-search-more-clear<?php endif ?> pull-right" data-type="<?php echo $type ?>">More</span></li>
	<?php foreach ($groups as $group => $items) :?>
	<li class="group"><strong><?php echo $group ?></strong></li>
	<?php foreach ($items as $item) :?>
	<li><a href="<?php echo $item->link ?>"><?php echo $item->title ?></a></li>
	<?php endforeach ?>
	<?php endforeach ?>
<?php endforeach ?>
</ul>
<?php else: ?>
<ul>
	<li class="type">No result found for <span class="search-query"><?php echo $q ?></span></li>
</ul>
<?php endif ?>