<?php
$link = $displayData['link'];
$groups = $displayData['userGroups'];
	
// Load language
$lang = JFactory::getLanguage();
$lang->load('tpl_'._ADMIN_TPL, JPATH_ADMINISTRATOR, $lang->getTag(), true);

?>

<li id="<?php echo $link['timeStamp']; ?>" class="link-item">
	<span class="handle"><i class="fa fa-ellipsis-v"></i></span>
	<!-- Image Icon -->
	<span class="ql-icon <?php if ((!isset($link['image']) && !isset($link['icon'])) ||
																	(isset($link['image']) && $link['image'] == '' && $link['icon'] == '')
																) : ?>hidden<?php endif; ?>">
		<?php if (isset($link['image']) && $link['image'] != '') : ?>
			<img src="../<?php echo $link['image']; ?>" />
		<?php else : ?>
			<span class="<?php echo $link['icon']; ?>"></span>
		<?php endif; ?>
	</span><!-- End Image Icon -->

	<div class="link-title">
		<h5><?php echo ucfirst($link['title']); ?></h5>
		<span><?php echo $link['url']; ?></span>
	</div>
	<div class="ql-show-on" style="display:none">
		<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:
		<strong><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></strong>
	</div>
	<a class="btn-edit-link" href="javascript:void(0);" data-toggle="collapse" data-target="#ql-<?php echo $link['timeStamp']; ?>" 
		data-timestamp="<?php echo $link['timeStamp']; ?>" 
		data-title="<?php echo $link['title']; ?>" 
		data-url="<?php echo $link['url']; ?>" 
		data-img="<?php echo $link['icon']; ?>" 
		data-image="<?php if (isset($link['image'])) echo $link['image']; else echo ''; ?>"
		data-acl='<?php if (in_array('all', $link['acl'])) : echo json_encode(array('all')); else : echo json_encode($link['acl']); endif ?>'
		>
			<span class="fa fa-edit"></span>
	</a>
	<a class="btn-delete-customlink" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><span class="fa fa-trash"></span></a>
	<div id="ql-<?php echo $link['timeStamp']; ?>" class="collapse edit-link">
		<ul>
			<li><input type="hidden" name="link-timeStamp" value="<?php echo $link['timeStamp']; ?>"/></li>
			<li>
				<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE'); ?></label>
				<input type="text" name="link-title" size="30" value="<?php echo $link['title']; ?>" />
			</li>
			<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL'); ?></label><input type="text" name="link-url" size="40" value="<?php echo $link['url']; ?>" /></li>
			<li>
				<div class="gr-access-settings" style="display:none;">
					<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></label>
					<select name="acl-<?php echo $link['timeStamp']; ?>" class="usergr-list" multiple="true">
						<?php if (in_array('all', $link['acl'])) : ?>
							<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
						<?php 
							foreach ($groups as $gr) : 
								if (in_array($gr->id, $link['acl'])) : ?>
								<option value="<?php echo $gr->id ?>" disabled="true">
									<?php for($i = 0; $i < $gr->level; $i++) : echo '-'; endfor; echo $gr->title ?>
								</option> 
							<?php endif;
							endforeach;
						else :
							foreach ($groups as $gr) : 
								if (in_array($gr->id, $link['acl'])) : ?>
									<option value="<?php echo $gr->id ?>" selected="true">
										<?php for($i = 0; $i < $gr->level; $i++) : echo '-'; endfor; echo $gr->title ?>
									</option>
								<?php endif;
							endforeach;
						endif; ?>
							
					</select>
				</div>
			</li>
			<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON'); ?></label><input type="text" name="link-icon" value="<?php echo $link['icon']; ?>" /></li>
			<li>
				<div class="link-media">
					<input id="image_url" type="text" name="link-image" value="<?php if (isset($link['image'])) : echo $link['image']; else : echo ''; endif; ?>" />
					<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
				</div>
			</li>
			<li><a class="btn btn-apply" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp'] ?>"><i class="fa fa-floppy-o"></i><?php echo JText::_('JAPPLY'); ?></a></li>
			<li><a class="btn btn-cancel" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><i class="fa fa-close"></i><?php echo JText::_('JCANCEL'); ?></a></li>
		</ul>
	</div>
</li>
<script>
	jQuery(document).ready(function($) {
		$.getScript('<?php echo JUri::base() . 'templates/'. _ADMIN_TPL . '/js/quicklink.js'; ?>', function() {});
	});
</script>