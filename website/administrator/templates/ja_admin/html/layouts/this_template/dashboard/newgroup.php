<?php 
// Load language
$lang = JFactory::getLanguage();
$lang->load('tpl_'._ADMIN_TPL, JPATH_ADMINISTRATOR, $lang->getTag(), true);

// Get UserGroups
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$groups = $model->getItems();
if (count($groups)) {
	foreach ($groups as $key => $group) {
		if(!in_array($group->id, array(6,7,8)) && !in_array($group->parent_id, array(6,7,8))) {
			unset($groups[$key]);
		}
	}
}

$groupId = $displayData['groupId'];
$name = $displayData['groupname'];
?>
<li id="<?php echo $groupId ?>" class="group-quicklink">
	<div class="group-title">
		<input type="hidden" value="<?php echo $groupId ?>" >
		<h4><?php echo ucfirst($name); ?></h4>
		<button class="btn btn-edit-gr" data-title="<?php echo $displayData['groupname']; ?>" data-toggle="collapse"><i class="fa fa-edit"></i></button>
		<a class="btn btn-remove-gr" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
		<span><i class="fa fa-angle-left"></i><i class="fa fa-angle-right"></i></span>
	</div>

	<div id="editGroup" class="collapse">
		<ul>
			<li>
				<label for="group-name">Group Name:</label>
				<input type="text" name="edit-group-name" value="<?php echo $displayData['groupname']; ?>" />
			</li>
			<li>
				<div>
					<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON'); ?>:
					<select name="<?php echo $name.'-'.$groupId ?>" class="usergr-list" multiple="true">
						<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?></option>
						<?php foreach($groups as $group) : ?>
							<option value="<?php echo $group->id ?>" disabled="true">
								<?php for($i = 0; $i<$group->level; $i++) : ?>-<?php endfor; echo $group->title; ?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</li>
			<li>
				<span class="btn btn-success btn-apply-gr"><i class="icon-new icon-white"></i><?php echo JText::_('Save Group'); ?></a>
			</li>
			<li>
				<a class="btn btn-cancel-gr" href="javascript:void(0);" data-grname="<?php echo $displayData['groupname']; ?>"><?php echo JText::_('JCANCEL'); ?></a>
			</li>
		</ul>
	</div>
	<div class="gr-show-on">
		<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:
		<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?></h6>
	</div>
	<ul id="newGroup" class="gr-ql-sortable connectedSortable links ui-placeholder" style="min-height: 50px;">
	</ul>
</li>
<script>
	jQuery(document).ready(function($) {
		$.getScript('<?php echo JUri::base() . 'templates/'. _ADMIN_TPL . '/js/quicklink.js'; ?>', function() {});
	});
</script>