<?php
// Load language
$lang = JFactory::getLanguage();
$lang->load('mod_menu', JPATH_ADMINISTRATOR, $lang->getTag(), true);
$lang->load('tpl_'._ADMIN_TPL, JPATH_ADMINISTRATOR, $lang->getTag(), true);

// Get mod_menu params
$module = JModuleHelper::getModule('mod_menu');
$params = new JRegistry($module->params);
$shownew = (boolean) $params->get('shownew', 1);
$showhelp = $params->get('showhelp', 1);
$path = JPATH_ROOT. '/media/'._ADMIN_TPL;
$fileConfigs = $path.'/configs.json';

//Get All Groups
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$userGrps = $model->getItems();
foreach ($userGrps as $key => $userGrp) {
	if (!JAccess::checkGroup($userGrp->id, 'core.login.admin') && $userGrp->id != '8') {
		unset($userGrps[$key]);
	}
}

$menus = array();

// Get Menu Items form file
$menufile = JPATH_ADMINISTRATOR . '/templates/'._ADMIN_TPL.'/admin/assets/links/menus.json';
if (JFile::exists($menufile)) {
	$menus['systems'] = json_decode(@file_get_contents($menufile), true);
}

// Get MenuTypes
require JPATH_ADMINISTRATOR .'/modules/mod_menu/helper.php';
$menuTypes = ModMenuHelper::getMenus();
if (count($menuTypes)) {
	foreach ($menuTypes as $menuType) {
		$item = array();
		$item['timestamp'] = 'mnt-'.$menuType->id;
		$item['title'] = $menuType->title;
		$item['url'] = 'index.php?option=com_menus&view=items&menutype=' . $menuType->menutype;
		$item['icon'] = '';
		$item['parent'] = 'menus';
		$item['level'] = 1;
		$menus['systems']['menus']['items'][] = $item;
		
		//Add new menu item
		$itemAdd = array();
		$itemAdd['timestamp'] = 'mnt-'.$menuType->id.'-add';
		$itemAdd['title'] = JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU_ITEM');
		$itemAdd['url'] = 'index.php?option=com_menus&view=item&layout=edit&menutype=' . $menuType->menutype;
		$itemAdd['icon'] = '';
		$itemAdd['parent'] = 'mnt-'.$menuType->id;
		$itemAdd['level'] = $item['level'] + 1;
		$menus['systems']['menus']['items'][] = $itemAdd;
	}
}

// Get Component Menu Items
$components = ModMenuHelper::getComponents(true);
if (count($components)) {
	$menus['systems']['components']['items'] = array();
	foreach ($components as $component) {
		$item = array();
		$item['timestamp'] = 'cpn-'.$component->id;
		$item['title'] = $component->title;
		$item['url'] = $component->link;
		$item['parent'] = 'components';
		$item['level'] = 2;
		$menus['systems']['components']['items'][] = $item;
		if (count($component->submenu) > 0) {
			foreach($component->submenu as $sub) {
				$item = array();
				$item['timestamp'] = 'cpn-'.$sub->id;
				$item['title'] = $sub->title;
				$item['url'] = $sub->link;
				$item['parent'] = 'cpn-'.$sub->parent_id;
				$item['level'] = 3;
				$menus['systems']['components']['items'][] = $item;
			}
		}
	}
}

// Get custom menu items
if (JFile::exists($fileConfigs)) {
	$customMenuFile = $path . '/links/custommenus.json';
	if (JFile::exists($customMenuFile)) {
		$custom_menus = json_decode(@file_get_contents($customMenuFile), true);
		if (count($custom_menus)) {
			foreach ($custom_menus as $menu) {
				$menu['timestamp'] = isset($menu['id']) ? $menu['id'] : $menu['timestamp'];
				if ($menu['parent'] == 'root') {
					$menu_name = preg_replace('/\s+/','', $menu['title']);
					$menus['systems'][$menu_name] = array();
					$menus['systems'][$menu_name]['id'] = $menu['timestamp'];
					$menus['systems'][$menu_name]['title'] = $menu['title'];
					$menus['systems'][$menu_name]['image'] = $menu['image'];
					$menus['systems'][$menu_name]['icon'] = $menu['icon'];
					$menus['systems'][$menu_name]['type'] = 'custom';
					$menus['systems'][$menu_name]['items'] = array();
				} else {
					foreach ($menus['systems'] as $group => $menuitem) {
						if ($menu['parent'] == $group) {
							$menu['type'] = 'custom';
							$menus['systems'][$group]['items'][] = $menu;
						} else {
							if (count($menus['systems'][$group]['items']) > 0) {
								foreach ($menus['systems'][$group]['items'] as $item) {
									if ($menu['parent'] == $item['timestamp']) {
										$menu['type'] = 'custom';
										$menus['systems'][$group]['items'][] = $menu;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

// Get All of parents of menu item
foreach ($menus['systems'] as $group => $menuitems) {
	if (count($menuitems['items'])) {
		foreach ($menuitems['items'] as $key => $item) {
			$parents = $item['parent'];
			$parent = $item['parent'];
			$level = $item['level'];
			do {
				foreach ($menuitems['items'] as $it) {
					if ($it['timestamp'] == $parent) {
						$parents .= ' '.$it['parent'];
						$parent = $it['parent'];
						$level = $it['level'];
					}
				}
			} while ($level > 2);
			$menus['systems'][$group]['items'][$key]['parents'] = $parents;
		}
	}
}

//Get Menu items by Group Access
if ($menus['systems'] && count($menus['systems'])) {
	foreach ($menus['systems'] as $group => $menuitems) {
		if (count($menuitems['items'])) {
			foreach($menuitems['items'] as $key => $menuitem) {
				$url = $menuitem['url'];
				$menus['systems'][$group]['items'][$key]['acl'] = array();
				foreach ($userGrps as $userGrp) {
					if(JAccess::checkGroup((int)$userGrp->id,'core.login.admin') || $userGrp->id == '8') {
						if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
							preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
							$asset = $matches[2][0];
							switch ($asset) {
								case 'com_cpanel' :
									break;
								case 'com_config' :
								case 'com_admin' :
									if (JAccess::checkGroup((int) $userGrp->id, 'core.admin', $asset) || $userGrp->id == 8) {
										$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
									}
									break;
								default :
									if ($asset == 'com_categories') {
										if (preg_match('/(&extension=)(.+)/', $url)) {
											preg_match_all('/(&extension=)(.+)/', $url, $matches);
											$asset = $matches[2][0];
										}
									}
									if (JAccess::checkGroup((int) $userGrp->id, 'core.manage', $asset) || $userGrp->id == 8) {
										if (preg_match('/(edit)(&.+)?/', $url)) {
											if (JAccess::checkGroup((int) $userGrp->id, 'core.edit', $asset) || $userGrp->id == 8) {
												$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
											}
										} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
											if (JAccess::checkGroup((int) $userGrp->id, 'core.create', $asset) || $userGrp->id == 8) {
												$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
											}
										} else if (preg_match('/(\.delete&?)/', $url)) {
											if (JAccess::checkGroup((int) $userGrp->id, 'core.delete', $asset) || $userGrp->id == 8) {
												$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
											}
										} else {
											$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
										}
									}
									break;
							}
						} else {
							$menus['systems'][$group]['items'][$key]['acl'][] = $userGrp->id;
						}
					}
				}
				
				if (count($menus['systems'][$group]['items'][$key]['acl']) > 1) {
					array_unshift($menus['systems'][$group]['items'][$key]['acl'], 'all');
				}
			}
		}
	}
}

// Get Configurations from the configs file
if (JFile::exists($fileConfigs)) {
	$configs = json_decode(@file_get_contents($fileConfigs), true);
	if (isset($configs['menus'])) {
		$menuConfigs = $configs['menus'];
		// Get all Components while already have the configs file
		$listComponents = array();
		foreach ($menuConfigs['components']['items'] as $cpn) {
			$listComponents[] = $cpn['id'];
		}
		
		foreach ($menus['systems']['components']['items'] as $it) {
			if (!in_array($it['timestamp'], $listComponents)) {
				$newCpn = array();
				$newCpn['id'] = $it['timestamp'];
				if (in_array('all',$it['acl'])) {
					$newCpn['acl'] = array('all');
				} else {
					$newCpn['acl'] = $it['acl'];
				}
				$menuConfigs['components']['items'][] = $newCpn;
			}
		}
		
		// Get all Menus while already have the configs file
		$listMenus = array();
		foreach ($menuConfigs['menus']['items'] as $mn) {
			$listMenus[] = $mn['id'];
		}
		foreach ($menus['systems']['menus']['items'] as $it) {
			if (!in_array($it['timestamp'], $listMenus)) {
				$newmenu = array();
				$newmenu['id'] = $it['timestamp'];
				if (in_array('all',$it['acl'])) {
					$newmenu['acl'] = array('all');
				} else {
					$newmenu['acl'] = $it['acl'];
				}
				$menuConfigs['menus']['items'][] = $newmenu;
			}
		}
	}
}
?>

<div id="quicklink-wrap">
	<div class="group-header">
		<!-- Add New Menu Item -->
		<div class="add-menu">
			<a class="btn btn-success" data-toggle="collapse" href="#addMenuForm" aria-controls="addMenuForm" aria-expanded="true">
				<i class="icon-new icon-white"></i>
				<?php echo JText::_('TPL_ADMIN_DASHBOARD_MENUS_ADD_NEW'); ?>
			</a>
			<div id="addMenuForm" class="collapse">
				<ul>
					<li>
						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
						<input name="menu-title" type="text" />
					</li>
					<li>
						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL') ?>:</label>
						<input name="menu-url" type="text" value="#" />
					</li>
					<li>
						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
						<input name="menu-icon" type="text" />
					</li>
					<li>
						<div class="link-media">
							<input id="image_url" type="text" name="menu-image" value="" />
							<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
						</div>
					</li>
					<li>
						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_MENUS_PARENT') ?></label>
						<select class="menu-group" id="menu-groups">
							<option value="root">-ROOT</option>
							<?php if (isset($menuConfigs)) : 
								foreach ($menuConfigs as $k => $v) : 
									foreach ($menus['systems'] as $group => $menu) : 
										if ($k == $group) :
							?>
											<option value="<?php echo $group.'-'.$menu['id'] ?>">-<?php if (isset($menu['title'])) : echo ucfirst($menu['title']); else : echo ucfirst($group); endif; ?></option>
							<?php endif; 
										if (count($v['items']) && count($menu['items'])) : 
											foreach ($v['items'] as $vItem) :
												foreach ($menu['items'] as $item) :
													if ($vItem['id'] == $item['timestamp']) :
										?>
														<option value="<?php echo $item['timestamp'] ?>">
															<?php for ($i =0; $i < $item['level']; $i++) {
																echo '-';
															} ?>
															<?php echo ucfirst(JText::_($item['title'])); ?>
														</option>
							<?php 			endif;
												endforeach;
											endforeach;
										endif; 
									endforeach; 
								endforeach;
							else : 
								foreach ($menus['systems'] as $group => $menu) : ?>
									<option value="<?php echo $group.'-'.$menu['id'] ?>">-<?php if (isset($menu['title'])) : echo ucfirst($menu['title']); else : echo ucfirst($group); endif; ?></option>
									<?php if (count($menu['items'])) : 
										foreach ($menu['items'] as $item) :
									?>
										<option value="<?php echo $item['timestamp'] ?>">
											<?php for ($i =0; $i < $item['level']; $i++) {
												echo '-';
											} ?>
											<?php echo ucfirst(JText::_($item['title'])); ?>
										</option>
							<?php 
										endforeach;
									endif; 
								endforeach; 
							endif; ?>
						</select>
					</li>
					<li>
						<button class="btn btn-primary btn-addmenu"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_ADD'); ?></button>
						<a class="btn btn-cancel" href="javascript:void(0);">
							<i class="fa fa-close"></i>
							<?php echo JText::_('JCANCEL'); ?>
						</a>
					</li>
				</ul>
			</div>
		</div><!-- End Add New Menu Item -->
	</div>
	<div class="group-body">
		<ul class="menu-group">
			<?php
				if (isset($menuConfigs)) :
					foreach ($menuConfigs as $index => $menuconfig) : 
						foreach ($menus['systems'] as $group => $menuitems) : 
							if (count($menuitems)) :
								if ($menuconfig['id'] == $menuitems['id']) : ?>
									<li id="<?php echo $group.'-'.$menuitems['id'] ?>" class="group-quicklinks">
										<div class="group-title">
											<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
											<a data-toggle="collapse" href="#menu-<?php echo $menuitems['id'] ?>" aria-expanded="true" aria-controls="menu-<?php echo $menuitems['id'] ?>">
												<?php if (isset($menuitems['image']) && $menuitems['image'] != '') : ?>
													<span class="mn-icon">
														<img style="max-width:30px;max-height:30px;" src="../<?php echo $menuitems['image'];?>" />
													</span>
												<?php elseif (isset($menuitems['icon']) && $menuitems['icon'] != '') : ?>
													<span class="mn-icon">
														<i class="<?php echo $menuitems['icon'] ?>"></i>
													</span>
												<?php endif; ?>
												<?php if (isset($menuitems['title'])) : echo ucfirst(JText::_($menuitems['title'])); else : echo ucfirst(JText::_($group)); endif;?>
											</a>
											<div class="mn-show-on">
												<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></h6>
												<select id="<?php echo $group.'-'.$menuitems['id'] ?>" name="<?php echo $group.'_'.$menuitems['id'] ?>" class="usergr-list" multiple="true">
													<?php if (is_array($menuconfig['acl'])) : ?>
														<?php if (in_array('all', $menuconfig['acl'])) : ?>
															<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
															<?php foreach ($userGrps as $userGrp) : 
																switch ($group) :
																	case ('users') :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_users') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" disabled="true">
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'menus' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_menus') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" disabled="true">
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'content' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_content') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" disabled="true">
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'extensions' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage', 'com_installer') || 
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_modules') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_plugins') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_templates') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_languages') ||
																				$userGrp->id == 8
																				) :
																		?>
																			<option value="<?php echo $userGrp->id ?>" disabled="true">
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php	
																		endif;
																		break;
																	case 'helps' :
																		if ($showhelp == 1) :
																		?>
																			<option value="<?php echo $userGrp->id ?>" disabled="true">
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php
																		endif;
																		break;
																	default : ?>
																		<option value="<?php echo $userGrp->id ?>" disabled="true">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																		<?php
																		break;
																endswitch;
															endforeach;
														else : 
														?>
															<option value="all" disabled="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
															<?php foreach ($userGrps as $userGrp) : 
																switch ($group) :
																	case ('users') :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_users') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'menus' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_menus') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'content' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage','com_content') || $userGrp->id == 8) : ?>
																			<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php 
																		endif;
																		break;
																	case 'extensions' :
																		if (JAccess::checkGroup($userGrp->id,'core.manage', 'com_installer') || 
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_modules') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_plugins') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_templates') ||
																				JAccess::checkGroup($userGrp->id,'core.manage', 'com_languages') ||
																				$userGrp->id == 8
																				) :
																		?>
																			<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php	
																		endif;
																		break;
																	case 'helps' :
																		if ($showhelp == 1) :
																		?>
																			<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																				<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																			</option>
																		<?php
																		endif;
																		break;
																	default : ?>
																		<option value="<?php echo $userGrp->id ?>" <?php if (is_array($menuconfig['acl']) && in_array($userGrp->id, $menuconfig['acl'])) : ?>selected="true"<?php endif; ?>>
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																		<?php
																		break;
																endswitch;
															endforeach;
														endif; ?>
													<?php else : ?>
														<option value="all"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
														<?php foreach ($userGrps as $userGrp) : 
															switch ($group) :
																case ('users') :
																	if (JAccess::checkGroup($userGrp->id,'core.manage','com_users')) : ?>
																		<option value="<?php echo $userGrp->id ?>">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																	<?php 
																	endif;
																	break;
																case 'menus' :
																	if (JAccess::checkGroup($userGrp->id,'core.manage','com_menus')) : ?>
																		<option value="<?php echo $userGrp->id ?>">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																	<?php 
																	endif;
																	break;
																case 'content' :
																	if (JAccess::checkGroup($userGrp->id,'core.manage','com_content')) : ?>
																		<option value="<?php echo $userGrp->id ?>">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																	<?php 
																	endif;
																	break;
																case 'extensions' :
																	if (JAccess::checkGroup($userGrp->id,'core.manage', 'com_installer') || 
																			JAccess::checkGroup($userGrp->id,'core.manage', 'com_modules') ||
																			JAccess::checkGroup($userGrp->id,'core.manage', 'com_plugins') ||
																			JAccess::checkGroup($userGrp->id,'core.manage', 'com_templates') ||
																			JAccess::checkGroup($userGrp->id,'core.manage', 'com_languages') ||
																			$userGrp->id == 8
																			) :
																	?>
																		<option value="<?php echo $userGrp->id ?>">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																	<?php	
																	endif;
																	break;
																case 'helps' :
																	if ($showhelp == 1) :
																	?>
																		<option value="<?php echo $userGrp->id ?>">
																			<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																		</option>
																	<?php
																	endif;
																	break;
																default : ?>
																	<option value="<?php echo $userGrp->id ?>">
																		<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																	</option>
																	<?php
																	break;
															endswitch;
														endforeach;
													endif; ?>
												</select>
											</div>
											<?php if (isset($menuitems['type']) && $menuitems['type'] == 'custom') : ?>
												<div class="mn-group-edit">
													<a href="javascript:void(0);" 
														data-timestamp="<?php echo $menuitems['id'] ?>"	
														data-title="<?php echo $menuitems['title'] ?>" 
														data-icons="<?php echo $menuitems['icon'] ?>" 
														data-image="<?php echo $menuitems['image'] ?>">
														<i class="fa fa-edit"></i>
													</a>
													<button class="delete-mn-group"><i class="fa fa-trash"></i></button>
													<div id="edit-mn-<?php echo $menuitems['id'] ?>" class="edit-mn-group">
														<ul id="mn-item-<?php echo $menuitems['id'] ?>" class="mn-item-list">
															<li class="edit-mn-title">
																<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
																<input name="menu-title" value="<?php echo $menuitems['title'] ?>" type="text" />
															</li>
															<li class="edit-mn-icon">
																<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
																<input name="menu-icon" type="text" value="<?php echo $menuitems['icon'] ?>" />
															</li>
															<li>
																<div class="link-media">
																	<input id="image_url" type="text" name="menu-image" value="<?php echo $menuitems['image'] ?>" />
																	<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
																</div>
															</li>
															<li>
																<button class="btn btn-primary btn-mn-group-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_SAVE'); ?></button>
																<a class="btn btn-cancel" href="javascript:void(0);">
																	<i class="fa fa-close"></i>
																	<?php echo JText::_('JCANCEL'); ?>
																</a>
															</li>
														</ul>
													</div>
												</div>
											<?php endif; ?>
										</div>
										<ul class="links collapse menu-item-list" id="menu-<?php echo $menuitems['id'] ?>">
											<?php if (count($menuitems['items'])) :
												if (isset($menuConfigs[$index]['items'])) :
													foreach ($menuConfigs[$index]['items'] as $k => $it) :
														foreach ($menuitems['items'] as $menuitem) :
															if ($it['id'] == $menuitem['timestamp']) :
											?>
																<li id="<?php echo $menuitem['timestamp']?>" parent="<?php echo $menuitem['parents'] ?>" level="<?php echo $menuitem['level'] ?>" class="ja-menu-item <?php if ($menuitem['level'] > 2) : ?>item-child<?php endif; ?>"><div class="mn-item-inner">
																	<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
																	<div class="mn-item-title" <?php if ($menuitem['level'] > 3) : ?>style="margin-left:<?php echo (($menuitem['level'] - 2)*18) ?>px;"<?php endif;?>>
																		<h4>
																			<span class="leaf"><?php if ($menuitem['level'] == 2) : ?>--<?php elseif ($menuitem['level'] > 2) : ?>&#9482; --<?php endif; ?></span> 
																			<?php if (isset($menuitem['image']) && $menuitem['image'] != '') : ?>
																				<span class="mn-icon">
																					<img style="max-width:30px; max-height:30px;" src="../<?php echo $menuitem['image'] ?>" />
																				</span>
																			<?php elseif (isset($menuitem['icon']) && $menuitem['icon'] != '') : ?>
																				<span class="mn-icon">
																					<i class="<?php echo $menuitem['icon'] ?>"></i>
																				</span>
																			<?php endif; ?>
																			<?php echo ucfirst(JText::_($menuitem['title'])); ?>
																		</h4>
																		<small><span class="leaf"><?php if ($menuitem['level'] == 2) : ?>--<?php elseif ($menuitem['level'] > 2) : ?>&#9482; --<?php endif; ?></span> <?php echo $menuitem['url'] ?></small>
																	</div>
																	<div class="mn-show-on">
																		<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:</h6>
																		<select name="jads_menu_<?php echo $menuitem['timestamp'] ?>" class="usergr-list" multiple="true">
																			<?php if (in_array('all', $it['acl'])) : ?>
																				<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
																				<?php foreach ($userGrps as $userGrp) : 
																					if (in_array($userGrp->id, $menuitem['acl'])) : ?>
																						<option value="<?php echo $userGrp->id?>" disabled="true">
																							<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																						</option>
																				<?php 
																					endif;
																				endforeach; 
																			else :
																				if (in_array('all', $menuitem['acl'])) :
																			?>
																					<option value="all" <?php if (count($it['acl'])) : ?>disabled="true"<?php endif; ?>><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
																			<?php
																						foreach ($userGrps as $userGrp) :
																						if (in_array($userGrp->id, $menuitem['acl'])) : ?>
																							<option value="<?php echo $userGrp->id ?>" <?php if (in_array($userGrp->id, $it['acl'])) : ?> selected="true" <?php endif; ?>>
																								<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																							</option>
																				<?php 
																						endif;
																					endforeach;
																				else : 
																					foreach ($userGrps as $userGrp) : 
																						if (in_array($userGrp->id, $menuitem['acl'])) : ?>
																							<option value="<?php echo $userGrp->id ?>" selected="true">
																								<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																							</option>
																			<?php 
																						endif;
																					endforeach;
																				endif;
																			endif; ?>
																		</select>
																	</div>
																	<?php if (isset($menuitem['type']) && $menuitem['type'] == 'custom') : ?>
																		<div class="mn-item-actions">
																			<button class="btn-edit-menu-item"
																					data-timestamp="<?php echo $menuitem['timestamp']?>"
																					data-title="<?php echo $menuitem['title'] ?>"
																					data-url = "<?php echo $menuitem['url'] ?>"
																					data-icons="<?php echo $menuitem['icon'] ?>"
																					data-image="<?php echo $menuitem['image'] ?>"
																					data-parent="<?php if($menuitem['parent'] == $group) :  echo $group.'-'.$menuitems['id']; else : echo $menuitem['parent']; endif;?>">
																				<i class="fa fa-edit"></i>
																			</button>
																			<button class="delete-menu-item" data-title="<?php echo $menuitem['title']?>"><i class="fa fa-trash"></i></button>
																			<div id="edit-menu-item-<?php echo $menuitem['timestamp'] ?>" class="mn-edit-item collapse">
																				<ul>
																					<li class="edit-mn-title">
																						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
																						<input name="menu-title" value="<?php echo $menuitem['title'] ?>" type="text" />
																					</li>
																					<li class="edit-mn-link">
																						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL') ?>:</label>
																						<input name="menu-url" type="text" value="<?php echo $menuitem['url'] ?>" />
																					</li>
																					<li class="edit-mn-icon">
																						<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
																						<input name="menu-icon" type="text" value="<?php echo $menuitem['icon'] ?>" />
																					</li>
																					<li>
																						<div class="link-media">
																							<input id="image_url" type="text" name="menu-image" value="<?php echo $menuitem['image'] ?>" />
																							<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
																						</div>
																					</li>
																					<li>
																						<button class="btn btn-primary btn-mn-item-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_MENU_ITEM_SAVE'); ?></button>
																						<a class="btn btn-cancel" href="javascript:void(0);">
																							<i class="fa fa-close"></i>
																							<?php echo JText::_('JCANCEL'); ?>
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	<?php endif; ?>
																</div></li>
												<?php 
															endif;
														endforeach;
													endforeach;
												endif;
											endif;?>
										</ul>
									</li>
			<?php 
								endif;
							endif;
						endforeach;
					endforeach;
				else : 
					foreach ($menus['systems'] as $group => $menuitems) : 
						if (count($menuitems)) : ?>
							<li id="<?php echo $group.'-'.$menuitems['id'] ?>" class="group-quicklinks">
								<div class="group-title">
									<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
									<a data-toggle="collapse" href="#menu-<?php echo $menuitems['id'] ?>" aria-expanded="true" aria-controls="menu-<?php echo $menuitems['id'] ?>">
										<?php if (isset($menuitems['image']) && $menuitems['image'] != '') : ?>
											<span class="mn-icon">
												<img style="max-width:30px;max-height:30px;" src="../<?php echo $menuitems['image'];?>" />
											</span>
										<?php elseif (isset($menuitems['icon']) && $menuitems['icon'] != '') : ?>
											<span class="mn-icon">
												<i class="<?php echo $menuitems['icon'] ?>"></i>
											</span>
										<?php endif; ?>
										<?php if (isset($menuitems['title'])) : echo ucfirst($menuitems['title']); else : echo ucfirst($group); endif;?>
									</a>
									<div class="mn-show-on">
										<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></h6>
										<select id="<?php echo $group.'-'.$menuitems['id'] ?>" name="<?php echo $group.'_'.$menuitems['id'] ?>" class="usergr-list" multiple="true">
											<option value="all" <?php if ((isset($menuitems['type']) && $menuitems['type'] != 'custom') || !isset($menuitems['type'])) : ?>selected="true" <?php endif; ?>><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
											<?php foreach ($userGrps as $userGrp) : 
												switch ($group) :
													case ('users') :
														if (JAccess::checkGroup($userGrp->id,'core.manage','com_users')) : ?>
															<option value="<?php echo $userGrp->id ?>" disabled="true">
																<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
															</option>
														<?php 
														endif;
														break;
													case 'menus' :
														if (JAccess::checkGroup($userGrp->id,'core.manage','com_menus')) : ?>
															<option value="<?php echo $userGrp->id ?>" disabled="true">
																<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
															</option>
														<?php 
														endif;
														break;
													case 'content' :
														if (JAccess::checkGroup($userGrp->id,'core.manage','com_content')) : ?>
															<option value="<?php echo $userGrp->id ?>" disabled="true">
																<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
															</option>
														<?php 
														endif;
														break;
													case 'extensions' :
														if (JAccess::checkGroup($userGrp->id,'core.manage', 'com_installer') || 
																JAccess::checkGroup($userGrp->id,'core.manage', 'com_modules') ||
																JAccess::checkGroup($userGrp->id,'core.manage', 'com_plugins') ||
																JAccess::checkGroup($userGrp->id,'core.manage', 'com_templates') ||
																JAccess::checkGroup($userGrp->id,'core.manage', 'com_languages') ||
																$userGrp->id == 8
																) :
														?>
															<option value="<?php echo $userGrp->id ?>" disabled="true">
																<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
															</option>
														<?php	
														endif;
														break;
													case 'helps' :
														if ($showhelp == 1) :
														?>
															<option value="<?php echo $userGrp->id ?>" disabled="true">
																<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
															</option>
														<?php
														endif;
														break;
													default : ?>
														<option value="<?php echo $userGrp->id ?>" disabled="true">
															<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
														</option>
														<?php
														break;
												endswitch;
											endforeach; ?>
										</select>
									</div>
									<?php if (isset($menuitems['type']) && $menuitems['type'] == 'custom') : ?>
										<div class="mn-group-edit">
											<a href="javascript:void(0);"
													data-timestamp="<?php echo $menuitems['id']?>"
													data-title="<?php echo $menuitems['title'] ?>"
													data-icons="<?php echo $menuitems['icon'] ?>"
													data-image="<?php echo $menuitems['image'] ?>">
												<i class="fa fa-edit"></i>
											</a>
											<button class="delete-mn-group"><i class="fa fa-trash"></i></button>
											<div id="edit-mn-<?php echo $menuitems['id'] ?>" class="edit-mn-group">
												<ul>
													<li>
														<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
														<input name="menu-title" value="<?php echo $menuitems['title'] ?>" type="text" />
													</li>
													<li>
														<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
														<input name="menu-icon" type="text" value="<?php echo $menuitems['icon'] ?>" />
													</li>
													<li>
														<div class="link-media">
															<input id="image_url" type="text" name="menu-image" value="<?php echo $menuitems['image'] ?>" />
															<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
														</div>
													</li>
													<li>
														<button class="btn btn-primary btn-mn-group-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_SAVE'); ?></button>
														<a class="btn btn-cancel" href="javascript:void(0);">
															<i class="fa fa-close"></i>
															<?php echo JText::_('JCANCEL'); ?>
														</a>
													</li>
												</ul>
											</div>
										</div>
									<?php endif; ?>
								</div>
								<ul class="links collapse menu-item-list" id="menu-<?php echo $menuitems['id'] ?>">
									<?php if (count($menuitems['items'])) :
										foreach ($menuitems['items'] as $menuitem) : 
									?>
											<li id="<?php echo $menuitem['timestamp']?>" parent="<?php echo $menuitem['parents'] ?>" level="<?php echo $menuitem['level'] ?>" class="ja-menu-item <?php if ($menuitem['parent'] != $group) : ?>item-child<?php endif; ?>"><div class="mn-item-inner">
												<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
												<div class="mn-item-title" <?php if ($menuitem['level'] > 3) : ?>style="margin-left:<?php echo (($menuitem['level'] - 2)*18) ?>px;"<?php endif;?>>
													<h4>
														<span class="leaf"><?php if ($menuitem['parent'] == $group) : ?>--<?php else : ?>&#9482; --<?php endif; ?> </span>
														<?php if (isset($menuitem['image']) && $menuitem['image'] != '') : ?>
															<span class="mn-icon">
																<img style="max-width:30px;max-height:30px;" src="../<?php echo $menuitem['image'] ?>" />
															</span>
														<?php elseif (isset($menuitem['icon']) && $menuitem['icon'] != '') : ?>
															<span class="mn-icon">
																<i class="<?php echo $menuitem['icon'] ?>"></i>
															</span>
														<?php endif; ?>
														<?php echo ucfirst(JText::_($menuitem['title'])); ?>
													</h4>
													<small><span class="leaf"><?php if ($menuitem['parent'] == $group) : ?>--<?php else : ?>&#9482; --<?php endif; ?></span> <?php echo $menuitem['url'] ?></small>
												</div>
												<div class="mn-show-on">
													<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:</h6>
													<select name="jads_menu_<?php echo $menuitem['timestamp'] ?>" class="usergr-list" multiple="true">
														<?php if (in_array('all', $menuitem['acl'])) : ?>
															<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL') ?></option>
															<?php foreach ($userGrps as $userGrp) : 
																if (in_array($userGrp->id, $menuitem['acl'])) :
															?>
																	<option value="<?php echo $userGrp->id?>" disabled="true">
																		<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																	</option>
														<?php endif;
															endforeach; 
														else : 
															foreach ($userGrps as $userGrp) :
																if (in_array($userGrp->id, $menuitem['acl'])) :?>
																	<option value="<?php echo $userGrp->id?>" selected="true">
																		<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
																	</option>
														<?php 
																endif;
															endforeach;
														endif; ?>	
													</select>
												</div>
												<?php if (isset($menuitem['type']) && $menuitem['type'] == 'custom') : ?>
													<div class="mn-item-actions">
														<button class="btn-edit-menu-item"
																data-timestamp="<?php echo $menuitem['timestamp']?>"
																data-title="<?php echo $menuitem['title'] ?>"
																data-url = "<?php echo $menuitem['url'] ?>"
																data-icons="<?php echo $menuitem['icon'] ?>"
																data-image="<?php echo $menuitem['image'] ?>"
																data-parent="<?php if($menuitem['parent'] == $group) :  echo $group.'-'.$menuitems['id']; else : echo $menuitem['parent']; endif;?>">
															<i class="fa fa-edit"></i>
														</button>
														<button class="delete-menu-item"><i class="fa fa-trash"></i></button>
														<div id="edit-menu-item-<?php echo $menuitem['timestamp'] ?>" class="mn-edit-item collapse">
															<ul>
																<li class="edit-mn-title">
																	<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
																	<input name="menu-title" value="<?php echo $menuitem['title'] ?>" type="text" />
																</li>
																<li class="edit-mn-link">
																	<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL') ?>:</label>
																	<input name="menu-url" type="text" value="<?php echo $menuitem['url'] ?>" />
																</li>
																<li class="edit-mn-icon">
																	<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
																	<input name="menu-icon" type="text" value="<?php echo $menuitem['icon'] ?>" />
																</li>
																<li>
																	<div class="link-media">
																		<input id="image_url" type="text" name="menu-image" value="<?php echo $menuitem['image'] ?>" />
																		<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
																	</div>
																</li>
																<li>
																	<button class="btn btn-primary btn-mn-item-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_SAVE'); ?></button>
																	<a class="btn btn-cancel" href="javascript:void(0);">
																		<i class="fa fa-close"></i>
																		<?php echo JText::_('JCANCEL'); ?>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												<?php endif; ?>
											</div></li>
										<?php 
										endforeach;
									endif;?>
								</ul>
							</li>
			<?php
						endif;
					endforeach;
				endif; ?>
		</ul>
	</div>
</div>