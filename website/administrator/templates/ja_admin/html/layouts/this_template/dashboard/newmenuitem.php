<?php
defined('_JEXEC') or die('Restricted Access');

//Get All Groups
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$userGrps = $model->getItems();

//Get Menu items by Group Access
$url = $displayData['url'];
$displayData['acl'] = array();
foreach ($userGrps as $userGrp) {
	if(JAccess::checkGroup((int)$userGrp->id,'core.login.admin') || $userGrp->id == '8') {
		if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
			preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
			$asset = $matches[2][0];
			switch ($asset) {
				case 'com_cpanel' :
					break;
				case 'com_config' :
				case 'com_admin' :
					if (JAccess::checkGroup((int) $userGrp->id, 'core.admin', $asset) || $userGrp->id == 8) {
						$displayData['acl'][] = $userGrp->id;
					}
					break;
				default :
					if ($asset == 'com_categories') {
						if (preg_match('/(&extension=)(.+)/', $url)) {
							preg_match_all('/(&extension=)(.+)/', $url, $matches);
							$asset = $matches[2][0];
						}
					}
					if (JAccess::checkGroup((int) $userGrp->id, 'core.manage', $asset) || $userGrp->id == 8) {
						if (preg_match('/(edit)(&.+)?/', $url)) {
							if (JAccess::checkGroup((int) $userGrp->id, 'core.edit', $asset) || $userGrp->id == 8) {
								$displayData['acl'][] = $userGrp->id;
							}
						} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
							if (JAccess::checkGroup((int) $userGrp->id, 'core.create', $asset) || $userGrp->id == 8) {
								$displayData['acl'][] = $userGrp->id;
							}
						} else if (preg_match('/(\.delete&?)/', $url)) {
							if (JAccess::checkGroup((int) $userGrp->id, 'core.delete', $asset) || $userGrp->id == 8) {
								$displayData['acl'][] = $userGrp->id;
							}
						} else {
							$displayData['acl'][] = $userGrp->id;
						}
					}
					break;
			}
		} else {
			$displayData['acl'][] = $userGrp->id;
		}
	}
}

if (count($displayData['acl']) > 1) {
	array_unshift($displayData['acl'], 'all');
}

$path = JPATH_ROOT. '/media/'._ADMIN_TPL;
$fileConfigs = $path.'/configs.json';
$menus = array();

// Get Menu Items form file
$menufile = JPATH_ADMINISTRATOR . '/templates/'._ADMIN_TPL.'/admin/assets/links/menus.json';
if (JFile::exists($menufile)) {
	$menus['systems'] = json_decode(@file_get_contents($menufile), true);
}

// Get MenuTypes
$menuTypes = ModMenuHelper::getMenus();
if (count($menuTypes)) {
	foreach ($menuTypes as $menuType) {
		$item = array();
		$item['timestamp'] = 'mnt-'.$menuType->id;
		$item['title'] = $menuType->title;
		$item['url'] = 'index.php?option=com_menus&view=items&menutype=' . $menuType->menutype;
		$item['icon'] = '';
		$item['parent'] = 'menus';
		$item['level'] = 1;
		$menus['systems']['menus']['items'][] = $item;
		
		//Add new menu item
		$itemAdd = array();
		$itemAdd['timestamp'] = 'mnt-'.$menuType->id.'-add';
		$itemAdd['title'] = JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU_ITEM');
		$itemAdd['url'] = 'index.php?option=com_menus&view=item&layout=edit&menutype=' . $menuType->menutype;
		$itemAdd['icon'] = '';
		$itemAdd['parent'] = 'mnt-'.$menuType->id;
		$itemAdd['level'] = $item['level'] + 1;
		$menus['systems']['menus']['items'][] = $itemAdd;
	}
}

// Get Component Menu Items
$components = ModMenuHelper::getComponents(true);
if (count($components)) {
	$menus['systems']['components']['items'] = array();
	foreach ($components as $component) {
		$item = array();
		$item['timestamp'] = 'cpn-'.$component->id;
		$item['title'] = $component->title;
		$item['url'] = $component->link;
		$item['parent'] = 'components';
		$item['level'] = 2;
		$menus['systems']['components']['items'][] = $item;
		if (count($component->submenu) > 0) {
			foreach($component->submenu as $sub) {
				$item = array();
				$item['timestamp'] = 'cpn-'.$sub->id;
				$item['title'] = $sub->title;
				$item['url'] = $sub->link;
				$item['parent'] = 'cpn-'.$sub->parent_id;
				$item['level'] = 3;
				$menus['systems']['components']['items'][] = $item;
			}
		}
	}
}

// Get custom menu items
if (JFile::exists($fileConfigs)) {
	$customMenuFile = $path . '/links/custommenus.json';
	if (JFile::exists($customMenuFile)) {
		$custom_menus = json_decode(@file_get_contents($customMenuFile), true);
		if (count($custom_menus)) {
			foreach ($custom_menus as $menu) {
				$menu['timestamp'] = isset($menu['id']) ? $menu['id'] : $menu['timestamp'];
				if ($menu['parent'] == 'root') {
					$menu_name = preg_replace('/\s+/','', $menu['title']);
					$menus['systems'][$menu_name] = array();
					$menus['systems'][$menu_name]['id'] = $menu['timestamp'];
					$menus['systems'][$menu_name]['title'] = $menu['title'];
					$menus['systems'][$menu_name]['image'] = $menu['image'];
					$menus['systems'][$menu_name]['icon'] = $menu['icon'];
					$menus['systems'][$menu_name]['type'] = 'custom';
					$menus['systems'][$menu_name]['items'] = array();
				} else {
					foreach ($menus['systems'] as $group => $menuitem) {
						if ($menu['parent'] == $group) {
							$menu['type'] = 'custom';
							$menus['systems'][$group]['items'][] = $menu;
						} else {
							if (count($menus['systems'][$group]['items']) > 0) {
								foreach ($menus['systems'][$group]['items'] as $item) {
									if ($menu['parent'] == $item['timestamp']) {
										$menu['type'] = 'custom';
										$menus['systems'][$group]['items'][] = $menu;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

if (JFile::exists($fileConfigs)) {
	$configs = json_decode(@file_get_contents($fileConfigs), true);
	if (isset($configs['menus'])) {
		$menuConfigs = $configs['menus'];
		// Get all Components while already have the configs file
		$listComponents = array();
		foreach ($menuConfigs['components']['items'] as $cpn) {
			$listComponents[] = $cpn['id'];
		}
		
		foreach ($menus['systems']['components']['items'] as $it) {
			if (!in_array($it['timestamp'], $listComponents)) {
				$newCpn = array();
				$newCpn['id'] = $it['timestamp'];
				if (in_array('all',$it['acl'])) {
					$newCpn['acl'] = array('all');
				} else {
					$newCpn['acl'] = $it['acl'];
				}
				$menuConfigs['components']['items'][] = $newCpn;
			}
		}
	}
}
?>
<?php if ($displayData['parent'] == 'root') : ?>
	<li id="<?php echo preg_replace('/\s+/','',$displayData['title']).'-'.$displayData['id'] ?>" class="group-quicklinks">
		<div class="group-title">
			<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
			<a data-toggle="collapse" href="#menu-<?php echo $displayData['id'] ?>" aria-expanded="true" aria-controls="menu-<?php echo $displayData['id'] ?>">
				<?php if (isset($displayData['image']) && $displayData['image'] != '') : ?>
					<span class="mn-icon">
						<img style="max-width:30px;max-height:30px;" src="../<?php echo $displayData['image'];?>" />
					</span>
				<?php elseif (isset($displayData['icon']) && $displayData['icon'] != '') : ?>
					<span class="mn-icon">
						<i class="<?php echo $displayData['icon'] ?>"></i>
					</span>
				<?php endif; ?>
				<?php echo ucfirst($displayData['title']); ?>
			</a>
			<div class="mn-show-on">
				<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></h6>
				<select id="<?php echo preg_replace('/\s+/','',$displayData['title']).'-'.$displayData['id'] ?>" name="<?php echo preg_replace('/\s+/','',$displayData['menu-title']).'_'.$displayData['id'] ?>" class="usergr-list" multiple="true">
					<option value="all" selected="true">All</option>
					<?php foreach ($userGrps as $userGrp) :
						if(JAccess::checkGroup((int)$userGrp->id,'core.login.admin') || $userGrp->id == '8') : ?>
							<option value="<?php echo $userGrp->id ?>" disabled="true">
								<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
							</option>
					<?php	endif;
					 endforeach; ?>
				</select>
			</div>
			<?php if (isset($displayData['type']) && $displayData['type'] == 'custom') : ?>
				<div class="mn-group-edit">
					<a href="javascript:void(0);" 
							data-timestamp="<?php echo $displayData['id']?>"
							data-title="<?php echo $displayData['title'] ?>"
							data-icons="<?php echo $displayData['icon'] ?>"
							data-image="<?php echo $displayData['image'] ?>"
						<i class="fa fa-edit"></i>
					</a>
					<button class="delete-mn-group"><i class="fa fa-trash"></i></button>
					<div id="edit-mn-<?php echo $displayData['id'] ?>" class="edit-mn-group">
						<ul>
							<li>
								<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
								<input name="menu-title" value="<?php echo $displayData['title'] ?>" type="text" />
							</li>
							<li>
								<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
								<input name="menu-icon" type="text" value="<?php echo $displayData['icon'] ?>" />
							</li>
							<li>
								<div class="link-media">
									<input id="image_url" type="text" name="menu-image" value="<?php echo $displayData['image'] ?>" />
									<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
								</div>
							</li>
							<li>
								<button class="btn btn-primary btn-mn-group-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_SAVE'); ?></button>
								<a class="btn btn-cancel" href="javascript:void(0);">
									<i class="fa fa-close"></i>
									<?php echo JText::_('JCANCEL'); ?>
								</a>
							</li>
						</ul>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<ul class="links collapse" id="menu-<?php echo $displayData['id'] ?>"></ul>
	</li>
<?php else : ?>
	<li id="<?php if(isset($displayData['id'])) echo $displayData['id']; else echo $displayData['timestamp']; ?>" level="<?php echo $displayData['level'] ?>" parent="<?php echo $displayData['parents'] ?>" class="ja-menu-item <?php if ($displayData['level'] > 2) : ?>item-child<?php endif; ?>">
		<div class="mn-item-inner">
			<span class="handle"><i class="fa fa-ellipsis-v" style="cursor: move"></i></span>
			<div class="mn-item-title" <?php if ($displayData['level'] > 3) : ?>style="margin-left:<?php echo (($displayData['level'] - 2)*18) ?>px;"<?php endif;?>>
				<h4>
					<span class="leaf"><?php if ($displayData['level'] == 2) : ?>--<?php elseif ($displayData['level'] > 2) : ?> &#9482; -- <?php endif; ?></span> 
					<?php if (isset($displayData['image']) && $displayData['image'] != '') : ?>
						<span class="mn-icon">
							<img style="max-width:30px;max-height:30px;" src="../<?php echo $displayData['image'] ?>" />
						</span>
					<?php elseif (isset($displayData['icon']) && $displayData['icon'] != '') : ?>
						<span class="mn-icon">
							<i class="<?php echo $displayData['icon'] ?>"></i>
						</span>
					<?php endif; ?>
					<?php echo ucfirst(JText::_($displayData['title'])); ?>
				</h4>
				<small><span class="leaf">&#9482; --</span> <?php echo $displayData['url'] ?></small>
			</div>
			<div class="mn-show-on">
				<h6><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:</h6>
				<select name="jads_menu_<?php if(isset($displayData['id'])) echo $displayData['id']; else echo $displayData['timestamp']; ?>" class="usergr-list" multiple="true">
					<?php if (in_array('all', $displayData['acl'])) : ?>
							<option value="all" selected="true">All</option>
					<?php endif;
					foreach ($userGrps as $userGrp) : 
						if (in_array($userGrp->id, $displayData['acl'])) : ?>
							<option value="<?php echo $userGrp->id ?>">
								<?php for($i = 0; $i<$userGrp->level; $i++) : ?>-<?php endfor; echo $userGrp->title; ?>
							</option>
					<?php	endif;
					endforeach; ?>
				</select>
			</div>
			<?php if (isset($displayData['type']) && $displayData['type'] == 'custom') : ?>
				<div class="mn-item-actions">
					<button class="btn-edit-menu-item"
							data-timestamp="<?php if(isset($displayData['id'])) echo $displayData['id']; else echo $displayData['timestamp'];?>"
							data-title="<?php echo $displayData['title'] ?>"
							data-url = "<?php echo $displayData['url'] ?>"
							data-icons="<?php echo $displayData['icon'] ?>"
							data-image="<?php echo $displayData['image'] ?>">
						<i class="fa fa-edit"></i>
					</button>
					<button class="delete-menu-item" data-title="<?php echo $displayData['title'] ?>"><i class="fa fa-trash"></i></button>
					<div id="edit-menu-item-<?php if(isset($displayData['id'])) echo $displayData['id']; else echo $displayData['timestamp']; ?>" class="mn-edit-item collapse">
						<ul>
							<li class="edit-mn-title">
								<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE') ?>:</label>
								<input name="menu-title" value="<?php echo $displayData['title'] ?>" type="text" />
							</li>
							<li class="edit-mn-link">
								<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL') ?>:</label>
								<input name="menu-url" type="text" value="<?php echo $displayData['url'] ?>" />
							</li>
							<li class="edit-mn-icon">
								<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON') ?>:</label>
								<input name="menu-icon" type="text" value="<?php echo $displayData['icon'] ?>" />
							</li>
							<li>
								<div class="link-media">
									<input id="image_url" type="text" name="menu-image" value="<?php echo $displayData['image'] ?>" />
									<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
								</div>
							</li>
							<li>
								<button class="btn btn-primary btn-mn-item-edit"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_MENU_ITEM_SAVE'); ?></button>
								<a class="btn btn-cancel" href="javascript:void(0);">
									<i class="fa fa-close"></i>
									<?php echo JText::_('JCANCEL'); ?>
								</a>
							</li>
						</ul>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</li>
<?php endif; ?>
<script>
	jQuery(document).ready(function($) {
		$.getScript('<?php echo JUri::base() . 'templates/'. _ADMIN_TPL . '/js/quicklink.js'; ?>', function() {});
	});
</script>