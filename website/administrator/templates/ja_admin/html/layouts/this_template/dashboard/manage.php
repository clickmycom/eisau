<?php
require_once (JPATH_ADMINISTRATOR . '/templates/'. _ADMIN_TPL . '/helper.php');

if (!class_exists('JAdminCssMenu')) {
	require (JPATH_ADMINISTRATOR . '/modules/mod_menu/menu.php');
}

$helper = new BKTMPHelper();
$lang = JFactory::getLanguage();
$base_dir = JPATH_ADMINISTRATOR;
$language_tag = $lang->getTag();
$reload = true;

//Load templates language
$lang->load('tpl_'._ADMIN_TPL, $base_dir, $language_tag, $reload);

// Get all admin Group Users
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$groups = $model->getItems();
if (count($groups)) {
	foreach ($groups as $key => $group) {
		if(!JAccess::checkGroup((int)$group->id,'core.login.admin') && $group->id != '8') {
			unset($groups[$key]);
		}
	}
}

// Get list of Modules.
$modules_admin = BKTMPHelper::getModules('1');

// Get module description
jimport('joomla.filesystem.file');
foreach($modules_admin as $module) {
	//Load module language
	$lang->load($module->module, $base_dir, $language_tag, $reload);
	$file = JPATH_ADMINISTRATOR . '/modules/'.$module->module.'/'.$module->module.'.xml';
	if (JFile::exists($file)) {
		$module->description = JText::_(simplexml_load_file($file)->description);
	} else {
		$module->description = '';
	}
}
?>

<p class="manager-dashboard-desc"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MANAGER_DESC'); ?></p>
<div id="message"></div>
<ul class="nav nav-tabs" role="tablist">
	<li id="db-modules" role="presentation" class="active"><a href="#modules" aria-controls="modules" role="tab" data-toggle="tab"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES'); ?></a></li>
	<li id="db-quicklinks" role="presentation"><a href="#quicklinks" aria-controls="quicklinks" role="tab" data-toggle="tab"><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS'); ?></a></li>
	<li id="db-menus" role="presentation"><a href="#menus" aria-controls="menus" role="tab" data-toggle="tab"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MENUS'); ?></a></li>
</ul>
<div class="tab-content">
	<!-- Modules -->
	<div role="tabpanel" class="tab-pane fade in active" id="modules">
		<table id="moduleList" class="table table-striped">
			<thead>
				<tr>
					<th class="title" width="20%"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES_TITLE'); ?></th>
					<th class="left"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES_POSITION'); ?></th>
					<th width="10%" class="nowrap hidden-phone"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES_TYPE'); ?></th>
					<th width="10%" class="nowrap hidden-phone"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES_ACCESS'); ?></th>
					<th width="50%" class="nowrap hidden-phone"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MODULES_SHOW_ON'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($modules_admin as $module) : ?>
				<tr sortable-group-id="cpanel" class="row0">
					<td><?php echo $module->title; ?></td>
					<td><?php echo $module->position; ?></td>
					<td class="left">
						<?php echo $module->name; ?>
					</td>
					<td class="left">
					<?php echo $module->access_level; ?>
					</td>
					<td class="left">
						<select name="<?php echo $module->module.$module->id ?>" class="usergr-list" multiple="true">
							<option value="all"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?></option>
							<?php foreach($groups as $group) : ?>
								<option value="<?php echo $group->id ?>">
									<?php if ($module->module == 'mod_k2_quickicons' || $module->module == 'mod_k2_stats') { 
										if (!JAccess::checkGroup($group->id, 'core.manage', 'com_k2') && $group->id != '8') {
											continue;
										}
									} ?>
									<?php for($i = 0; $i<$group->level; $i++) : ?>-<?php endfor; echo $group->title; ?>
								</option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div><!-- End Modules -->
	
	<!-- Qick links -->
	<div role="tabpanel" class="tab-pane fade" id="quicklinks">
		<div id="dashboard-quicklinks"></div>
	</div> <!-- End Quick links -->
	<!-- Menu items -->
	<div role="tabpanel" class="tab-pane fade" id="menus">
		<div id="dashboard-menus"></div>
	</div><!-- End Menu items -->
</div>
<!-- Modal to insert image icon -->
<div id="jform-image-icon" class="modal hide fade" tabindex="-1">
	<div class="modal-header" style="height:50px">
		<h3><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS_SELECT_IMAGE'); ?></h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer" style="height:70px;">
		<button class="btn btn-cancel-image"><?php echo JText::_('JCANCEL');?></button>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('.usergr-list').chosen();
		$('.usergr-list').chosen().change(function() {
			if ($(this).val()) {
				if ($.inArray('all', $(this).val()) == -1) {
					$(this).find('option[value="all"]').attr('disabled', true);
				} else {
					$(this).find('option').attr('disabled', true);
					$(this).find('option[value="all"]').attr('disabled', false);
				}
			} else {
				$(this).find('option').attr('disabled', false);
			}
			$(this).trigger("liszt:updated");
		});
		
		// Load the configurations from the configs file
		loadConfigs();
		
		// Method to load configurations
		function loadConfigs () {
			$.ajax({
				type: 'POST',
				url : 'index.php?athajax=dashboard&action=loadconfigs',
				success : function(data) {
					loadMenuItem();
					if (data != '') {
						var configs = $.parseJSON(data);
						var modConfigs = configs.modules, linksConfigs = configs.quicklinks, menuConfigs = configs.menus;
						// Modules configs
						if ($.type(modConfigs) == 'object') {
							$.each(modConfigs, function(mod,value){
								for (var i=0; i<value.length; i++) {
									$('select[name="'+mod+'"]').find('option[value="'+value[i]+'"]').attr('selected', true);
									if ($.inArray('all', value) == -1) {
										$('select[name="'+mod+'"]').find('option[value="all"]').attr('disabled', true);
									} else {
										$('select[name="'+mod+'"]').find('option').attr('disabled', true);
										$('select[name="'+mod+'"]').find('option[value="all"]').attr('disabled', false);
									}
									$('.usergr-list').trigger("liszt:updated");
								}
							});
						} else {
							var selects = $('#modules').find('select');
							selects.each(function() {
								$(this).find('option[value="all"]').attr('selected', true);
								$(this).find('option').attr('disabled', true);
								$(this).find('option[value="all"]').attr('disabled', false);
								$(this).trigger("liszt:updated");
							});
						}
					} else {
						$('select').find('option').attr('disabled',true);
						$('select').find('option[value="all"]').attr('disabled',false);
						$('select').find('option[value="all"]').attr('selected',true);
						$('select.profile').find('option').attr('disabled',false);
						$('.usergr-list').trigger("liszt:updated");
					}
					loadQuicklinks();
				}
			});
		}
		
		function loadMenuItem() {
			$.ajax({
				type: 'POST',
				url : 'index.php?athajax=dashboard&action=loadmenuitems',
				success : function(data) {
					$('#dashboard-menus').html(data);
          var ScriptURL ='<?php echo JUri::base() . 'templates/'. _ADMIN_TPL . '/js/quicklink.js'; ?>';
					$.getScript(ScriptURL, function() {});
				}
			});
		}
		
		// Method to load the quicklinks configurations
		function loadQuicklinks() {
			var groups = <?php echo json_encode($groups) ?>;
			$.ajax({
				type : 'POST',
				data : {'groups' : groups},
				url : 'index.php?athajax=dashboard&action=loadquicklinks',
				success : function(data) {
					$('#dashboard-quicklinks').html(data);
					$('#dashboard-manage .radio.btn-group').radioToBtnGroup();
					var ScriptURL ='<?php echo JUri::base() . 'templates/'. _ADMIN_TPL . '/js/quicklink.js'; ?>';
					$.getScript(ScriptURL, function() {});
				}
			});
		}
	});
</script>