<?php
JHtml::_('bootstrap.framework');

$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . 'templates/' . _ADMIN_TPL . '/js/dashboard.js');
$doc->addStylesheet(JUri::base() . 'templates/' . _ADMIN_TPL . '/css/dashboard.css');

JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$groups = $model->getItems();
?>
<!-- Only display dashboard selection for the Super User -->
<?php if(JFactory::getUser()->get('isRoot')) : ?>
<div class="dashboard-home">
  <span class="btn btn-default btn-dashboard-manage"><i class="icon-edit"></i>Manage</span>

	<div class="dashboard-toolbox">
    <h3><?php echo JText::_('TPL_ADMIN_YOUR_DASHBOARD'); ?></h3>
		<select id="dashboard" class="profile">
			<?php foreach ($groups as $group) : ?>
				<?php if(JAccess::checkGroup((int)$group->id,'core.login.admin') || $group->id == '8') : ?>
				<option value="<?php echo $group->id ?>">
					<?php for ($i=0; $i<$group->level; $i++) echo '-'; ?>
					<?php echo $group->title ?>
				</option>
				<?php endif; ?>
			<?php endforeach ?>
		</select>
	</div>
</div> 

<!-- Modal -->
<div id="dashboard-manage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="dashboard-title" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><?php echo JText::_('TPL_ADMIN_DASHBOARD_MANAGER'); ?></h3>
  </div>
  <div class="modal-body">
    <p>One fine body…</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary btn-apply-profile"><?php echo JText::_('JAPPLY'); ?></button>
		<button class="btn btn-success btn-save-profile"><?php echo JText::_('JSAVE'); ?></button>
		<button class="btn btn-ds-close" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('JTOOLBAR_CLOSE'); ?></button>
  </div>
</div>
<script>
	jQuery(document).ready(function($) {
		$(window).on('load', function() {
			$('#dashboard').find('option[value="<?php echo JFactory::getApplication()->input->get('dashboard') ? : '8' ?>"]').attr('selected',true);
		});
		$('.btn-addlink-close').on('click', function(){
			$('#dashboard-manage').css('display','block');
		});
	});
</script>
<?php endif; ?>