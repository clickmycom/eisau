<?php 
jimport('joomla.filsystem.file');
jimport('joomla.filsystem.folder');

// Load language
$lang = JFactory::getLanguage();
$lang->load('tpl_'._ADMIN_TPL, JPATH_ADMINISTRATOR, $lang->getTag(), true);

// Get Admin menu items to quicklinks
$helper = new BKTMPHelper();
$app = JFactory::getApplication();
$user = JFactory::getUser();
$userGroups = $user->get('groups');
$groups = $displayData['groups'];
$links =array();
$path = JPATH_ROOT. '/media/'._ADMIN_TPL;
$qlPath = JPATH_ADMINISTRATOR . '/templates/' . _ADMIN_TPL . '/admin/assets/links/quicklinks.json';

$cfFile = $path . '/configs.json';
if (JFile::exists($cfFile)){
	$configs = json_decode(@file_get_contents($cfFile), true);
	if (isset($configs['quicklinks'])) {
		$qlConfigs = $configs['quicklinks'];
	}
}

$folder = JPATH_ROOT . '/media/' . _ADMIN_TPL . '/quicklinks';
if (!JFolder::exists($folder)) {
	JFolder::create($folder);
}

if (JFile::exists($qlPath)) {
	$quicklinks = json_decode(file_get_contents($qlPath), true);
}

if (!isset($quicklinks['customlinks'])) {
	$quicklinks['customlinks'] = array();
}

$components = $helper->getComponents();
$cusLinks = $helper->getComponentLinks($components, $quicklinks);
$quicklinks['systemlinks'] = array_merge($quicklinks['systemlinks'], $cusLinks);

// Quicklinks to display
$links = array_merge($quicklinks['systemlinks'], $quicklinks['customlinks']);

// Get links which customized by user to system links
$customPath = $path . '/links/customlinks.json';
if (JFile::exists($customPath)) {
	$custom = json_decode(file_get_contents($customPath), true);
	if (count($custom) > 0) {
		foreach($custom as $value) {
			$checklink = 0;
			foreach ($links as $k => $link) {
				if ($link['timeStamp'] == $value['timeStamp']) {
					$links[$k] = $value;
					break;
				} else {
					$checklink++;
				}
			}
			if ($checklink == count($links)) {
				$links[] = $value;
			}
		}
	}
}

// Get links by User Group Access
foreach ($links as $key => $link) {
	$url = (isset($link['link'])) ? $link['link'] : $link['url'];
	$links[$key]['acl'] = array();
	if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
		preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
		$asset = $matches[2][0];
		switch ($asset) {
			case 'com_cpanel' :
				break;
			case 'com_config' :
			case 'com_admin' :
				foreach ($groups as $gr) {
					if (JAccess::checkGroup((int) $gr['id'], 'core.admin', $asset) || $gr['id'] == '8') {
						$links[$key]['acl'][] = $gr['id'];
					}
				}
				break;
			default :
				if ($asset == 'com_categories') {
					if (preg_match('/(&extension=)(.+)/', $url)) {
						preg_match_all('/(&extension=)(.+)/', $url, $matches);
						$asset = $matches[2][0];
					}
				}
				foreach ($groups as $gr) {
					if (JAccess::checkGroup((int) $gr['id'], 'core.manage', $asset) || $gr['id'] == '8') {
						if (preg_match('/(edit)(&.+)?/', $url)) {
							if (JAccess::checkGroup((int) $gr['id'], 'core.edit', $asset) || $gr['id'] == '8') {
								$links[$key]['acl'][] = $gr['id'];
							}
						} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
							if (JAccess::checkGroup((int) $gr['id'], 'core.create', $asset) || $gr['id'] == '8') {
								$links[$key]['acl'][] = $gr['id'];
							}
						} else if (preg_match('/(\.delete&?)/', $url)) {
							if (JAccess::checkGroup((int) $gr['id'], 'core.delete', $asset) || $gr['id'] == '8') {
								$links[$key]['acl'][] = $gr['id'];
							}
						} else {
							$links[$key]['acl'][] = $gr['id'];
						}
					}
				}
				break;
		}
	} else {
		foreach ($groups as $gr) {
			$links[$key]['acl'][] = $gr['id'];
		}
	}
	if (count($links[$key]['acl']) > 1) {
		array_unshift($links[$key]['acl'], 'all');
	}
}
?>
<div id="quicklink-wrap">
	<div id="group-en">
		<div class="group-header">
			<h3><span><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS_ENABLE_GROUP'); ?></span></h3>
			<button class="btn btn-success" data-toggle="collapse" data-target="#addNewGroup"><i class="icon-new icon-white"></i><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ADD_NEW_GROUP'); ?></button>
		</div>

		<!-- Add new Group -->
		<div id="addNewGroup" class="collapse">
			<ul class="add-group-form">
				<li>
					<label for="group-name"><?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_ADD_NAME'); ?></label>
					<input type="text" value="" name="add-group-name" />
				</li>
				<li>
					<span class="btn btn-success btn-group-addnew"><i class="icon-new icon-white"></i><?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_ADD'); ?></a>
				</li>
				<li>
					<a class="btn btn-cancel" href="javascript:void(0);">
						<i class="fa fa-close"></i>
						<?php echo JText::_('JCANCEL'); ?>
					</a>
				</li>
			</ul>
		</div><!-- End Add Group -->

		<div class="group-body">
			<ul id="ql-group" class="connectedGroup">
				<?php 
				if (isset($qlConfigs) && count($qlConfigs) > 0) : 
					foreach($qlConfigs as $gr => $grlinks) :
				?>
						<li id="<?php echo $grlinks['id'] ?>" class="group-quicklink">
							<div class="group-title">
								<input type="hidden" id="grl-<?php echo $grlinks['id'] ?>" value="<?php echo $grlinks['id'] ?>">
								<h4><?php echo ucfirst($gr); ?></h4>
								<a class="btn btn-edit-gr" href="#" data-acl='<?php echo json_encode($grlinks['acl']); ?>' data-title="<?php echo $gr; ?>" data-toggle="collapse"><i class="fa fa-edit"></i></a>
								<button class="btn btn-remove-gr"><i class="fa fa-trash"></i></button>
								<span><i class="fa fa-angle-left"></i><i class="fa fa-angle-right"></i></span>
							</div>
							<div id="editGroup" class="collapse">
								<ul>
									<li>
										<label for="group-name"><?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_ADD_NAME'); ?></label>
										<input type="text" name="edit-group-name" value="<?php echo trim($gr); ?>" />
									</li>
									<li>
										<div>
											<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON'); ?>:
											<select name="<?php echo $gr.'-'.$grlinks['id'] ?>" class="usergr-list" multiple="true">
												<?php if (is_array($grlinks['acl'])) : 
														if (in_array('all', $grlinks['acl'])) : ?>
														<option value="all" selected="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?></option>
														<?php foreach($groups as $group) : ?>
															<option value="<?php echo $group['id'] ?>" disabled="true">
																<?php for($i = 0; $i<$group['level']; $i++) : ?>-<?php endfor; echo $group['title']; ?>
															</option>
														<?php endforeach; 
														else :
														?>
														<option value="all" disabled="true"><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?></option>
														<?php foreach($groups as $group) : ?>
															<option value="<?php echo $group['id'] ?>" <?php if (in_array($group['id'],$grlinks['acl'])) : ?>selected="true"<?php endif; ?>>
																<?php for($i = 0; $i<$group['level']; $i++) : ?>-<?php endfor; echo $group['title']; ?>
															</option>
												<?php endforeach;
														endif; 
													else : ?>
														<option value="all" ><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL'); ?></option>
															<?php foreach($groups as $group) : ?>
																<option value="<?php echo $group['id'] ?>">
																	<?php for($i = 0; $i<$group['level']; $i++) : ?>-<?php endfor; echo $group['title']; ?>
																</option>
															<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
									</li>
									<li>
										<span class="btn btn-success btn-apply-gr"><i class="icon-new icon-white"></i><?php echo JText::_('TPL_ADMIN_DASHBOARD_GROUP_SAVE'); ?></a>
									</li>
									<li>
										<a class="btn btn-cancel-gr" href="javascript:void(0);" data-acl='<?php echo json_encode($grlinks['acl']); ?>' data-grname="<?php echo $gr; ?>"><?php echo JText::_('JCANCEL'); ?></a>
									</li>
								</ul>
							</div>
							<div class="gr-show-on">
								<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:
								<h6>
								<?php 
								if (isset($grlinks['acl'])) {
									foreach ($grlinks['acl'] as $acl) {
										foreach ($groups as $gr) {
											if ($acl == $gr['id']) { ?>
												<input type="hidden" name="acl-<?php echo $grlinks['id'] ?>" value="<?php echo $gr['id'] ?>" />
												<?php echo $gr['title'].',';
											} elseif ($acl == 'all') { ?>
												<input type="hidden" name="acl-<?php echo $grlinks['id'] ?>" value="all" />
												<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL')?>
											<?php break;
											}
										}
									}
								}
								else {
									echo 'Disabled';
								}
								?>
								</h6>
							</div>
							<ul class="gr-ql-sortable connectedSortable links <?php if(!isset($grlinks['items']) || (isset($grlinks['items']) && (!is_array($grlinks['items']) || count($grlinks['items']) == 0))) : ?>ui-placeholder<?php endif; ?>" <?php if(!isset($grlinks['items']) || (isset($grlinks['items']) && (!is_array($grlinks['items']) || count($grlinks['items']) == 0))) : ?>style="min-height:50px;"<?php endif; ?>>
								<?php
								if (isset($grlinks['items']) && is_array($grlinks['items']) && count($grlinks['items']) > 0) :
									foreach($grlinks['items'] as $value) : 
										foreach ($links as $k => $link) :
											if ($link['timeStamp'] == $value['timestamp']) :
								?>
												<li id="<?php echo $link['timeStamp']; ?>" class="link-item">
													<!-- Image Icon -->
													<span class="handle"><i class="fa fa-ellipsis-v"></i></span>
													<span class="ql-icon <?php if ((!isset($link['image']) && !isset($link['icon'])) ||
																													(isset($link['image']) && $link['image'] == '' && $link['icon'] == '')
																												) : ?>hidden<?php endif; ?>">
														<?php if (isset($link['image']) && $link['image'] != '') : ?>
															<img src="../<?php echo $link['image']; ?>" />
														<?php else : ?>
															<span class="<?php echo $link['icon']; ?>"></span>
														<?php endif; ?>
													</span><!-- End Image Icon -->

													<div class="link-title">
														<h5><?php echo ucfirst($link['title']); ?></h5>
														<span><?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?></span>
													</div>
													<div class="ql-show-on">
														<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:
														<strong>
															<?php
															if (isset($value['acl'])) {
																if (in_array('all', $value['acl'])) {
																	echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL');
																} else {
																	foreach ($value['acl'] as $acl) {
																		foreach ($groups as $gr) {
																			if ($acl == $gr['id']) { ?>
																				<input type="hidden" name="acl-<?php echo $value['timestamp'] ?>" value="<?php echo $gr['id'] ?>" />
																				<?php echo $gr['title'].',';
																			}
																		}
																	}
																}
															}
															else {
																echo 'Disabled';
															}
															?>
														</strong>
													</div>
													<a class="btn-edit-link" href="javascript:void(0);" data-toggle="collapse" data-target="#ql-<?php echo $link['timeStamp']; ?>" 
														data-timestamp="<?php echo $link['timeStamp']; ?>" 
														data-title="<?php echo $link['title']; ?>" 
														data-url="<?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?>" 
														data-img="<?php echo $link['icon']; ?>" 
														data-image="<?php if (isset($link['image'])) echo $link['image']; else echo ''; ?>"
														data-acl='<?php if (isset($value['acl'])) echo json_encode($value['acl']); else echo ''; ?>'>
															<span class="fa fa-edit"></span>
													</a>
													<?php if(isset($link['url'])) : ?>
														<a class="btn-delete-customlink" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><span class="fa fa-trash"></span></a>
													<?php endif; ?>
													<div id="ql-<?php echo $link['timeStamp']; ?>" class="collapse edit-link">
														<ul>
															<li><input type="hidden" name="link-timeStamp" value="<?php echo $link['timeStamp']; ?>"/></li>
															<li>
																<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE'); ?></label>
																<input type="text" name="link-title" size="30" value="<?php echo $link['title']; ?>" />
															</li>
															<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL'); ?></label><input type="text" name="link-url" size="40" value="<?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?>" <?php if (isset($link['link'])) : ?> readonly="true"<?php endif;?>/></li>
															<li>
																<div class="gr-access-settings">
																	<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></label>
																	<select name="acl-<?php echo $link['timeStamp']; ?>" class="usergr-list" multiple="true">
																		<?php
																		if (isset($value['acl'])) :
																			if (in_array('all', $value['acl'])) :
																		?>
																				<option value="all" selected="true">All</option>
																				<?php 
																				foreach ($link['acl'] as $acl) :
																					foreach ($groups as $ugr) :
																						if ($acl != 'all' && $acl == $ugr['id']):
																				?>
																							<option value="<?php echo $acl ?>" disabled="true">
																								<?php for($i = 0; $i < $ugr['level']; $i++) : echo '-'; endfor; echo $ugr['title'] ?>
																							</option>
																			<?php
																						endif;
																					endforeach;
																				endforeach;
																			else : ?>
																				<option value="all" disabled="true">All</option>
																				<?php 
																				foreach ($link['acl'] as $acl) :
																					foreach ($groups as $ugr) :
																						if ($acl != 'all' && $acl == $ugr['id']):
																				?>
																							<option value="<?php echo $acl ?>" <?php if (in_array($acl, $value['acl'])) : ?>selected="true"<?php endif; ?>>
																								<?php for($i = 0; $i < $ugr['level']; $i++) : echo '-'; endfor; echo $ugr['title'] ?>
																							</option>
																			<?php
																						endif;
																					endforeach;
																				endforeach;
																			endif;
																		else:
																			foreach ($link['acl'] as $acl) :
																				if ($acl == 'all') :
																		?>
																					<option value="all">All</option>
																				<?php 
																				else: 
																					foreach ($groups as $ugr) :
																						if ($acl == $ugr['id']) :
																				?>
																							<option value="<?php echo $acl ?>">
																								<?php for($i = 0; $i < $ugr['level']; $i++) : echo '-'; endfor; echo $ugr['title'] ?>
																							</option>
																		<?php 
																						endif;
																					endforeach;
																				endif;
																			endforeach;
																		endif; 
																		?>
																	</select>
																</div>
															</li>
															<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON'); ?></label><input type="text" name="link-icon" value="<?php echo $link['icon']; ?>" /></li>
															<li>
																<div class="link-media">
																	<input id="image_url" type="text" name="link-image" value="<?php if (isset($link['image'])) : echo $link['image']; else : echo ''; endif; ?>" />
																	<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
																</div>
															</li>
															<li><a class="btn btn-apply" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp'] ?>"><i class="fa fa-floppy-o"></i><?php echo JText::_('JAPPLY'); ?></a></li>
															<li><a class="btn btn-cancel" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><i class="fa fa-close"></i><?php echo JText::_('JCANCEL') ?></a></li>
														</ul>
													</div>
												</li>
								<?php 
												unset($links[$k]);
											endif;
										endforeach;
									endforeach;
								endif;
								?>
							</ul>
						</li>
				<?php
					endforeach;
				endif;
				?>
			</ul>
		</div>
	</div>
	
	<!--Disable Quicklinks -->
	<div id="group-dis">
		<div class="group-header">
			<h3><span><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS_UNGROUP'); ?></span></h3>
			<button class="btn btn-success" data-toggle="collapse" data-target="#addLinkForm"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ADD_NEW_QUICKLINK'); ?></button>	
			<div class="input-append searchLink">
				<span class="add-on">
					<i title="Filter" class="fa fa-filter"></i>
				</span>
				<input type="text" id="searchLink" value="" class="input" placeholder="<?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS_SEARCH'); ?>" />
			</div>
		</div>
		<!--Add custom links -->
		<ul id="addLinkForm" class="add-link-form collapse">
			<input type="hidden" name="link-timeStamp" />
			<li>
				<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE'); ?></label>
				<input type="text" name="link-title" size="30" />
			</li>
			<li>
				<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL'); ?></label>
				<input type="text" name="link-url" size="40" />
			</li>
			<li>
				<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON'); ?></label>
				<input type="text" name="link-icon" />
			</li>
			<li>
				<div class="link-media">
					<input id="image_url" type="text" name="link-image" value="" />
					<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
				</div>
			</li>
			<li><button class="btn btn-primary btn-addlink"><i class="fa fa-plus-circle"></i> <?php echo JText::_('TPL_ADMIN_ADD'); ?></button></li>
			<li>
				<a class="btn btn-cancel" href="javascript:void(0);">
					<i class="fa fa-close"></i>
					<?php echo JText::_('JCANCEL'); ?>
				</a>
			</li>
		</ul>
		<!-- End Add Custom links -->
		<div class="group-body">
		<ul id="ungroup-links" class="connectedUngroup">
			<?php 
			foreach ($links as $link) :
				if (!isset($link['group']) || $link['group'] == '0') :
			?>
					<li id="<?php echo $link['timeStamp']; ?>" class="link-item">
						<span class="handle"><i class="fa fa-ellipsis-v"></i></span>
						<!-- Image Icon -->
						<span class="ql-icon <?php if ((!isset($link['image']) && !isset($link['icon'])) ||
																						(isset($link['image']) && $link['image'] == '' && $link['icon'] == '')
																					) : ?>hidden<?php endif; ?>">
							<?php if (isset($link['image']) && $link['image'] != '') : ?>
								<img src="../<?php echo $link['image']; ?>" />
							<?php else : ?>
								<span class="<?php echo $link['icon']; ?>"></span>
							<?php endif; ?>
						</span><!-- End Image Icon -->

						<div class="link-title">
							<h5><?php echo ucfirst($link['title']); ?></h5>
							<span><?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?></span>
						</div>
						<div class="ql-show-on" style="display:none">
							<?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?>:
							<strong>
								<?php if (in_array('all', $link['acl'])) {
									echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ALL'); 
								} else {
									foreach ($link['acl'] as $acl) {
										foreach ($groups as $gr) {
											if ($acl == $gr['id'])  
												echo ucfirst($gr['title']);
										}
									}
								} ?>
							</strong>
						</div>
						
						<?php if(isset($link['url'])) : ?>
							<a class="btn-delete-customlink" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><span class="fa fa-trash"></span></a>
						<?php endif; ?>

						<a class="btn-edit-link" href="javascript:void(0);" 
							data-parent="#ungroup-links"  data-toggle="collapse" 
							data-target="#ql-<?php echo $link['timeStamp']; ?>" 
							data-timestamp="<?php echo $link['timeStamp']; ?>" 
							data-title="<?php echo $link['title']; ?>" 
							data-url="<?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?>" 
							data-img="<?php echo $link['icon']; ?>" 
							data-image="<?php if (isset($link['image'])) echo $link['image']; else echo ''; ?>"
							data-acl='<?php if (in_array('all', $link['acl'])) : echo json_encode(array('all')); else: echo json_encode($link['acl']); endif; ?>'
							>
								<span class="fa fa-edit"></span>
						</a>

						<div id="ql-<?php echo $link['timeStamp']; ?>" class="collapse edit-link">
							<ul>
								<li><input type="hidden" name="link-timeStamp" value="<?php echo $link['timeStamp']; ?>"/></li>
								<li>
									<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_TITLE'); ?></label>
									<input type="text" name="link-title" size="30" value="<?php echo $link['title']; ?>" />
								</li>
								<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_URL'); ?></label><input type="text" name="link-url" size="40" value="<?php if (isset($link['url'])) : echo $link['url']; else : echo $link['link']; endif; ?>" <?php if (isset($link['link'])) : ?> readonly="true"<?php endif;?>/></li>
								<li>
									<div class="gr-access-settings" style="display:none;">
										<label><?php echo JText::_('TPL_ADMIN_DASHBOARD_SHOW_ON') ?></label>
										<select name="acl-<?php echo $link['timeStamp']; ?>" class="usergr-list" multiple="true">
											<?php if (in_array('all', $link['acl'])) : 
												foreach ($link['acl'] as $acl) :
													if ($acl == 'all') : ?>
														<option value="<?php echo $acl ?>" selected="true">All</option>
													<?php
													else :
														foreach ($groups as $gr) : 
															if ($acl == $gr['id']) :	
														?>
																<option value="<?php echo $acl ?>" disabled="true">
																	<?php for($i = 0; $i < $gr['level']; $i++) : echo '-'; endfor; echo $gr['title'] ?>
																</option>
														<?php
															endif;
														endforeach;
													endif;
												endforeach;
											else :
												foreach ($link['acl'] as $acl) : 
													foreach ($groups as $gr) : 
														if ($acl == $gr['id']) :	
													?>
															<option value="<?php echo $acl ?>" selected="true">
																<?php for($i = 0; $i < $gr['level']; $i++) : echo '-'; endfor; echo $gr['title'] ?>
															</option>
													<?php
														endif;
													endforeach;
												endforeach;
											endif; ?>
										</select>
									</div>
								</li>
								<li><label><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINK_ICON'); ?></label><input type="text" name="link-icon" value="<?php echo $link['icon']; ?>" /></li>
								<li>
									<div class="link-media">
										<input id="image_url" type="text" name="link-image" value="<?php if (isset($link['image'])) : echo $link['image']; else : echo ''; endif; ?>" />
										<a class="btn btn-image" href="javascript:void(0);" data-toggle="modal" data-target="#jform-image-icon"><i class="fa fa-image"></i></a>
									</div>
								</li>
								<li><a class="btn btn-apply" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp'] ?>"><i class="fa fa-floppy-o"></i>Save</a></li>
								<li><a class="btn btn-cancel" href="javascript:void(0);" data-timestamp="<?php echo $link['timeStamp']; ?>"><i class="fa fa-close"></i>Cancel</a></li>
							</ul>
						</div>
					</li>
			<?php
				endif;
			endforeach;
			?>
		</ul>
		</div>
	</div>
</div>