<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/* @var $menu JAAdminCssMenu */

$path = JPATH_ROOT. '/media/'._ADMIN_TPL;
$shownew = (boolean) $params->get('shownew', 1);
$showhelp = $params->get('showhelp', 1);
$user = JFactory::getUser();
$lang = JFactory::getLanguage();
$userGroups = $user->get('groups');
$dashboard = $app->input->get('dashboard');

$recovery = (boolean) $params->get('recovery', 0);
$rootClass = $recovery ? 'class:' : null;

// Is com_fields installed and enabled?
$comFieldsEnabled = JComponentHelper::isInstalled('com_fields') && JComponentHelper::isEnabled('com_fields');

// Get Menu Items form file
$menufile = JPATH_ADMINISTRATOR . '/templates/'._ADMIN_TPL.'/admin/assets/links/menus.json';
if (JFile::exists($menufile)) {
	$menuItems = json_decode(@file_get_contents($menufile), true);
}

// Get custom menu items
$customMenuFile = $path . '/links/custommenus.json';
if (JFile::exists($customMenuFile)) {
	$custom_menus = json_decode(@file_get_contents($customMenuFile), true);
	if (count($custom_menus)) {
		foreach ($custom_menus as $csmenu) {
			$csmenu['timestamp'] = isset($csmenu['id']) ? $csmenu['id'] : $csmenu['timestamp'];
			$csmenu['title'] = $csmenu['title'];
			$csmenu['url'] = $csmenu['url'];
			if ($csmenu['parent'] == 'root') {
				$gr_name = preg_replace('/\s+/','',$csmenu['title']);
				$menuItems[$gr_name] = array();
				$menuItems[$gr_name]['id'] = $csmenu['timestamp'];
				$menuItems[$gr_name]['title'] = $csmenu['title'];
				$menuItems[$gr_name]['icon'] = $csmenu['icon'];
				$menuItems[$gr_name]['image'] = $csmenu['image'];
				$menuItems[$gr_name]['items'] = array();
			} else {
				foreach ($menuItems as $group => $menuitem) {
					if ($csmenu['parent'] == $group) {
						$menuItems[$group]['items'][] = $csmenu;
					} else {
						foreach ($menuItems[$group]['items'] as $item) {
							if ($csmenu['parent'] == $item['timestamp']) {
								$menuItems[$group]['items'][] = $csmenu;
							}
						}
					}
				}
			}
		}
	}
}

//Check Menu items by Group Access
foreach ($menuItems as $group => $menuItem) {
	if (count($menuItem['items'])) {
		foreach ($menuItem['items'] as $key => $item) {
			$url = $item['url'];
			if ($dashboard) {
				if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
					preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
					$asset = $matches[2][0];
					switch($asset) {
						case 'com_cpanel' :
							break;
						case 'com_config' :
						case 'com_admin' :
							if (!JAccess::checkGroup($dashboard, 'core.admin', $asset) || $dashboard != 8) {
								unset($menuItems[$group]['items'][$key]);
							}
							break;
						default :
							if ($asset == 'com_categories') {
								if (preg_match('/(&extension=)(.+)/', $url)) {
									preg_match_all('/(&extension=)(.+)/', $url, $matches);
									$asset = $matches[2][0];
								}
							}
							if (JAccess::checkGroup($dashboard, 'core.manage', $asset) || $dashboard == 8) {
								if (preg_match('/(edit)(&.+)?/', $url)) {
									if (!JAccess::checkGroup($dashboard, 'core.edit', $asset) && $dashboard != 8) {
										unset($menuItems[$group]['items'][$key]);
									}
								} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
									if (!JAccess::checkGroup($dashboard, 'core.create', $asset) && $dashboard != 8) {
										unset($menuItems[$group]['items'][$key]);
									}
								} else if (preg_match('/(\.delete&?)/', $url)) {
									if (!JAccess::checkGroup($dashboard, 'core.delete', $asset) && $dashboard != 8) {
										unset($menuItems[$group]['items'][$key]);
									}
								}
							} else {
								unset($menuItems[$group]['items'][$key]);
							}
							break;
					}
				}
			} else if (!in_array('8', $userGroups)) {
				if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
					preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
					$asset = $matches[2][0];
					switch($asset) {
						case 'com_cpanel' :
							break;
						case 'com_config' :
						case 'com_admin' :
							$count = 0;
							foreach ($userGroups as $gr) {
								if (JAccess::checkGroup((int) $gr, 'core.admin', $asset)) {
									$count++;
								}
							}
							if ($count === 0) {
								unset($menuItems[$group]['items'][$key]);
							}
							break;
						default :
							if ($asset == 'com_categories') {
								if (preg_match('/(&extension=)(.+)/', $url)) {
									preg_match_all('/(&extension=)(.+)/', $url, $matches);
									$asset = $matches[2][0];
								}
							}
							$count = 0;
							foreach ($userGroups as $gr) {
								if (JAccess::checkGroup((int) $gr, 'core.manage', $asset)) {
									$count++;
								}
							}
							if ($count != 0) {
								if (preg_match('/(edit)(&.+)?/', $url)) {
									$check = 0;
									foreach ($userGroups as $gr) {
										if (JAccess::checkGroup((int) $gr, 'core.edit', $asset)) {
											$check++;
										}
									}
									if ($check === 0) {
										unset($menuItems[$group]['items'][$key]);
									}
								} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
									$check = 0;
									foreach ($userGroups as $gr) {
										if (JAccess::checkGroup((int) $gr, 'core.create', $asset)) {
											$check++;
										}
									}
									if ($check === 0) {
										unset($menuItems[$group]['items'][$key]);
									}
								} else if (preg_match('/(\.delete&?)/', $url)) {
									$check = 0;
									foreach ($userGroups as $gr) {
										if (JAccess::checkGroup((int) $gr, 'core.delete', $asset)) {
											$check++;
										}
									}
									if ($check === 0) {
										unset($menuItems[$group]['items'][$key]);
									}
								}
							} else {
								unset($menuItems[$group]['items'][$key]);
							}
							break;
					}
				}
			}
		}
	}
}

//Load configuration from file
$fileConfigs = $path.'/configs.json';
if (JFile::exists($fileConfigs)) {
	$configs = json_decode(@file_get_contents($fileConfigs), true);
	if (isset($configs['menus'])) {
		$menuConfigs = $configs['menus'];
	}
}

if (isset($menuConfigs)) {
	foreach ($menuConfigs as $group => $menus) {
		if (isset($menuConfigs[$group]['items'])) {
			foreach ($menuConfigs[$group]['items'] as $k => $item) {
				if (!isset($item['acl']) || !is_array($item['acl']))
					$menuConfigs[$group]['items'][$k]['acl'] = array();
			}
		}
		if (isset($menus['acl']) && is_array($menus['acl'])) {
			if (in_array('all', $menus['acl'])) {
				$statusGr = true;
			} else {
				if ($dashboard) {
					if (in_array($dashboard, $menus['acl'])) {
						$statusGr = true;
					} else {
						$statusGr = false;
					}
				} else {
					if (array_intersect($userGroups, $menus['acl'])) {
						$statusGr = true;
					} else {
						$statusGr = false;
					}
				}
			}
		} else {
			$statusGr = false;
		}
		
		if ($statusGr) {
			$count = 0;
			if (isset($menus['items'])) {
				foreach ($menus['items'] as $item) {
					if ($dashboard) {
						if (is_array($item['acl']) && (in_array($dashboard, $item['acl']) || in_array('all', $item['acl']))) {
							$count++;
						}
					} else {
						if (is_array($item['acl']) && (array_intersect($userGroups, $item['acl']) || in_array('all', $item['acl']))) {
							$count++;
						}
					}
				}
				
				if ($count == 0) {
					$statusGr = false;
				}
			}
		}
		$menuConfigs[$group]['show'] = $statusGr;
	}
	
	foreach ($menuConfigs as $group => $menus) {
		if ($group != 'components') {
			if ($menus['show']) {
				if (isset($menuItems[$group]['image']) && $menuItems[$group]['image'] != '') {
					$rootTitle = '<img style="max-width:30px;max-height:30px;" src="../'.$menuItems[$group]['image'].'" /> <span class="leaf"></span>';
				} else if (isset($menuItems[$group]['icon']) && $menuItems[$group]['icon'] != '') {
					$rootTitle = '<i class="'.$menuItems[$group]['icon'].'"></i> <span class="leaf"></span>';
				} else {
					$rootTitle = '';
				}
				
				if ($rootTitle == '') {
				  if (isset($menuItems[$group]['title'])) {
					$rootTitle .= '<span class="icon '.$group.'"></span></span class="menu-title">'.ucfirst(JText::_($menuItems[$group]['title'])) . '</span>';
				  } else {
					$rootTitle .= '<span class="icon '.$group.'"></span><span class="menu-title">'.ucfirst(JText::_($group)) . '</span>';
				  }
				} else {
				  if (isset($menuItems[$group]['title'])) {
					$rootTitle .= '<span class="menu-title">' . ucfirst(JText::_($menuItems[$group]['title'])) . '</span>';
				  } else {
					$rootTitle .= '<span class="menu-title">' . ucfirst(JText::_($group)) . '</span>';
				  }
				}
				
				$menu->addChild(new JAMenuNode($rootTitle, '#'), true);
			
				if (isset ($menus['items'])) {
					foreach ($menus['items'] as $item) {
						foreach($menuItems[$group]['items'] as $menuItem) {
							if ($dashboard) {
								if (in_array($dashboard, $item['acl']) || in_array('all', $item['acl'])) {
									if ($menuItem['timestamp'] == $item['id']) {
									//if ($menuItem['parent'] == $group) {
										if (isset($menuItem['image']) && $menuItem['image'] != '') {
											$nodeTitle = '<img style="max-width:30px;max-height:30px;" style="max-width:30px;max-height:30px;" src="../'.$menuItem['image'].'" /> <span class="leaf"></span>'.ucfirst(JText::_($menuItem['title']));
										} elseif (isset($menuItem['icon']) && $menuItem['icon'] != '') {
											$nodeTitle = '<span class="'.$menuItem['icon'].'"></span> <span class="leaf"></span>'.ucfirst(JText::_($menuItem['title']));
										} else {
											$nodeTitle = ucfirst(JText::_($menuItem['title']));
										}
										$node = new JAMenuNode($nodeTitle, $menuItem['url'],'class:'.$group, null, null,'');
										$currentNode = $menu->get('_current');
										if ($menuItem['parent'] == $group) {
											if ($currentNode->id != '') {
												$i = 1;
												do {
													$menu->getParent();
													$_current = $menu->get('_current');
													$menu->set('_current',$_current);
													$i++;
												} while( $i < $currentNode->level);
												$menu->addChild($node,true);
												$currentNode = $menu->get('_current');
												$currentNode->set('id', $menuItem['timestamp']);
												$currentNode->level = $menuItem['level'];
											} else {
												$menu->addChild($node,true);
												$currentNode = $menu->get('_current');
												$currentNode->set('id', $menuItem['timestamp']);
												$currentNode->level = $menuItem['level'];
											}
										} else {
											$checkParent = 0;
											$parentShow = false;
											foreach($menuItems[$group]['items'] as $mnit) {
												if ($menuItem['parent'] == $mnit['timestamp']) {
													foreach ($menus['items'] as $mn) {
														if ($mn['id'] == $mnit['timestamp']) {
															if (in_array($dashboard, $mn['acl']) || in_array('all', $mn['acl'])) {
																$parentShow = true;
															}
														}
													}
													break;
												} else {
													$checkParent++;
												}
											}
											if ($checkParent == count($menuItems[$group]['items'])) {
												$parentShow = false;
											}
											if ($parentShow) {
												if (isset($currentNode->level) && $menuItem['level'] == $currentNode->level) {
													$menu->getParent();
													$menu->addChild($node, true);
													$currentNode = $menu->get('_current');
													$currentNode->set('id', $menuItem['timestamp']);
													$currentNode->level = $menuItem['level'];
												} else {
													if (isset($currentNode->level) && $menuItem['level'] < $currentNode->level) {
														$subLevel = $currentNode->level - $menuItem['level'];
														$i = 0;
														do {
															$menu->getParent();
															$_current = $menu->get('_current');
															$menu->set('_current',$_current);
															$i++;
														} while ($i <= $subLevel);
													}
													$menu->addChild($node, true);
													$currentNode = $menu->get('_current');
													$currentNode->set('id', $menuItem['timestamp']);
													$currentNode->level = $menuItem['level'];
												}
											}
										}
									}
								}
							} else {
								if (array_intersect($userGroups, $item['acl']) || in_array('all', $item['acl'])) {
									if ($menuItem['timestamp'] == $item['id']) {
										//if ($menuItem['parent'] == $group) {
										if (isset($menuItem['image']) && $menuItem['image'] != '') {
											$nodeTitle = '<img style="max-width:30px;max-height:30px;" style="max-width:30px;max-height:30px;" src="../'.$menuItem['image'].'" /> <span class="leaf"></span>'.ucfirst(JText::_($menuItem['title']));
										} elseif (isset($menuItem['icon']) && $menuItem['icon'] != '') {
											$nodeTitle = '<span class="'.$menuItem['icon'].'"></span> <span class="leaf"></span>'.ucfirst(JText::_($menuItem['title']));
										} else {
											$nodeTitle = ucfirst(JText::_($menuItem['title']));
										}
										$node = new JAMenuNode($nodeTitle, $menuItem['url'],'class:'.$group, null, null,'');
										$currentNode = $menu->get('_current');
										if ($menuItem['parent'] == $group) {
											if ($currentNode->id != '') {
												$i = 1;
												do {
													$menu->getParent();
													$_current = $menu->get('_current');
													$menu->set('_current',$_current);
													$i++;
												} while( $i < $currentNode->level);
												$menu->addChild($node,true);
												$currentNode = $menu->get('_current');
												$currentNode->set('id', $menuItem['timestamp']);
												$currentNode->level = $menuItem['level'];
											} else {
												$menu->addChild($node,true);
												$currentNode = $menu->get('_current');
												$currentNode->set('id', $menuItem['timestamp']);
												$currentNode->level = $menuItem['level'];
											}
										} else {
											$checkParent = 0;
											$parentShow = false;
											foreach($menuItems[$group]['items'] as $mnit) {
												if ($menuItem['parent'] == $mnit['timestamp']) {
													foreach ($menus['items'] as $mn) {
														if ($mn['id'] == $mnit['timestamp']) {
															if (array_intersect($userGroups, $mn['acl']) || in_array('all', $mn['acl'])) {
																$parentShow = true;
															}
														}
													}
													break;
												} else {
													$checkParent++;
												}
											}
											if ($checkParent == count($menuItems[$group]['items'])) {
												$parentShow = false;
											}
											if ($parentShow) {
												if (isset($currentNode->level) && $menuItem['level'] == $currentNode->level) {
													$menu->getParent();
													$menu->addChild($node, true);
													$currentNode = $menu->get('_current');
													$currentNode->set('id', $menuItem['timestamp']);
													$currentNode->level = $menuItem['level'];
												} else {
													if (isset($currentNode->level) && $menuItem['level'] < $currentNode->level) {
														$subLevel = $currentNode->level - $menuItem['level'];
														$i = 0;
														do {
															$menu->getParent();
															$_current = $menu->get('_current');
															$menu->set('_current',$_current);
															$i++;
														} while ($i <= $subLevel);
													}
													$menu->addChild($node, true);
													$currentNode = $menu->get('_current');
													$currentNode->set('id', $menuItem['timestamp']);
													$currentNode->level = $menuItem['level'];
												}
											}
										}
									}
								}
							}
						}
					}
				}
			
				if ($group == 'menus') {
					$currentNode = $menu->get('_current');
					if (isset($currentNode->level) && $currentNode->level > 2) {
						$i = 1;
						do {
							$menu->getParent();
							$_current = $menu->get('_current');
							$menu->set('_current',$_current);
							$i++;
						} while( $i < $currentNode->level);
					}
					$menu->addSeparator();

					// Menu Types
					$menuTypes = JAModMenuHelper::getMenus();
					$menuTypes = JArrayHelper::sortObjects($menuTypes, 'title', 1, false);

					foreach ($menuTypes as $menuType)
					{
						$alt = '*' . $menuType->sef . '*';

						if ($menuType->home == 0)
						{
							$titleicon = '';
						}
						elseif ($menuType->home == 1 && $menuType->language == '*')
						{
							$titleicon = ' <span class="icon-home"></span>';
						}
						elseif ($menuType->home > 1)
						{
							$titleicon = ' <span>'
								. JHtml::_('image', 'mod_languages/icon-16-language.png', $menuType->home, array('title' => JText::_('MOD_MENU_HOME_MULTIPLE')), true)
								. '</span>';
						}
						else
						{
							$image = JHtml::_('image', 'mod_languages/' . $menuType->image . '.gif', null, null, true, true);

							if (!$image)
							{
								$image = JHtml::_('image', 'mod_languages/icon-16-language.png', $alt, array('title' => $menuType->title_native), true);
							}
							else
							{
								$image = JHtml::_('image', 'mod_languages/' . $menuType->image . '.gif', $alt, array('title' => $menuType->title_native), true);
							}

							$titleicon = ' <span>' . $image . '</span>';
						}
						
						foreach($menus['items'] as $item) {
							if ($dashboard) {
								if (in_array($dashboard, $item['acl']) || in_array('all', $item['acl'])) {
									if ('mnt-'.$menuType->id == $item['id']) {
										$node = new JAMenuNode(
															$menuType->title, 'index.php?option=com_menus&view=items&menutype=' . $menuType->menutype, 'class:menu', null, null, $titleicon
														);
										foreach($menus['items'] as $it) {
											if ('mnt-'.$menuType->id.'-add' == $it['id']) {
												if (in_array($dashboard, $it['acl']) || in_array('all', $it['acl'])) {
													$jamenunode = new JAMenuNode(
																								JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU_ITEM'), 'index.php?option=com_menus&view=item&layout=edit&menutype=' . $menuType->menutype,
																							'class:newarticle');
													$node->addChild($jamenunode);
												}
											}
										}
										$jamenunode = new JAMenuNode(ucfirst(JText::_($group)), '#');
										$node->setParent($jamenunode);
										$menu->addChild($node);
									}
								}
							} else {
								if (array_intersect($userGroups, $item['acl']) || in_array('all', $item['acl'])) {
									if ('mnt-'.$menuType->id == $item['id']) {
										$node = new JAMenuNode(
															$menuType->title, 'index.php?option=com_menus&view=items&menutype=' . $menuType->menutype, 'class:menu', null, null, $titleicon
														);
										foreach($menus['items'] as $it) {
											if ('mnt-'.$menuType->id.'-add' == $it['id']) {
												if (array_intersect($userGroups, $item['acl']) || in_array('all', $item['acl'])) {
													$jamenunode = new JAMenuNode(JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU_ITEM'), 'index.php?option=com_menus&view=item&layout=edit&menutype=' . $menuType->menutype,'class:newarticle');
													$node->addChild($jamenunode);
												}
											}
										}
										$jamenunode = new JAMenuNode(ucfirst(JText::_($group)), '#');
										$node->setParent($jamenunode);
										$menu->addChild($node);
									}
								}
							}
						}
					}
				}
				$menu->reset();
			}
		} else {
			/*
			* Components Submenu
			*/

			// Get the authorised components and sub-menus.
			$components = JAModMenuHelper::getComponents(true);

			// Check if there are any components, otherwise, don't render the menu
			if ($components)
			{
				// Get all Components while already have the configs file
				foreach ($menus['items'] as $cpn) {
					$listComponents[] = $cpn['id'];
				}
				
				foreach ($components as $it) {
					if (!in_array('cpn-'.$it->id, $listComponents)) {
						$newCpn = array();
						$newCpn['id'] = 'cpn-'.$it->id;
						$newCpn['acl'] = array('all');
						$menus['items'][] = $newCpn;
						$listComponents[] = $newCpn['id'];
					}
					
					if (!empty($it->submenu)) {
						foreach ($it->submenu as $sub) {
							if (!in_array('cpn-'.$sub->id, $listComponents)) {
								$newSub = array();
								$newSub['id'] = 'cpn-'.$sub->id;
								$newSub['acl'] = array('all');
								$menus['items'][] = $newSub;
								$listComponents[] = $newSub['id'];
							}
						}
					}
				}
				if ($menus['show']) {
					$rootTitle = '';
					if (isset($menuItems[$group]['title'])) {
						$rootTitle .= '<span class="icon '.$group.'"></span><span class="menu-title">' .ucfirst(JText::_($menuItems[$group]['title'])) . '</span>';
					} else {
						$rootTitle .= '<span class="icon '.$group.'"></span><span class="menu-title">'.ucfirst(JText::_($group)). '</span>';
					}
					
					$menu->addChild(new JAMenuNode($rootTitle, '#'), true);
					foreach ($menus['items'] as $item) {
						foreach ($components as &$component)
						{
							if (!empty($component->submenu))
							{
								if ($dashboard) {
									if (in_array($dashboard, $item['acl']) || (in_array('all', $item['acl']) && JAccess::checkGroup($dashboard, 'core.manage', $component->element)) || $dashboard == '8') {
										if ('cpn-'.$component->id == $item['id']) {
											$node = new JAMenuNode($component->text, $component->link, $component->img, NUll, NUll, '');
											foreach ($menus['items'] as $it) {
												foreach ($component->submenu as $sub) {
													if (in_array($dashboard, $it['acl']) || in_array('all', $it['acl'])) {
														if ('cpn-'.$sub->id == $it['id']) {
															$jamenunode = new JAMenuNode($sub->text, $sub->link, $sub->img, NUll, NUll, '');
															$node->addChild($jamenunode);
														}
													}
												}
											}
											$jamenunode = new JAMenuNode(ucfirst(JText::_($group)), '#');
											$node->setParent($jamenunode);
											$menu->addChild($node);
										}
									}
								} else {
									$z = 0;
									foreach ($userGroups as $userGroup) {
										if (JAccess::checkGroup($userGroup, 'core.manage', $component->element) || $userGroup == '8') {
											$z++;
										}
									}
									
									if (array_intersect($userGroups, $item['acl']) || (in_array('all', $item['acl']) && $z > 0)) {
										if ('cpn-'.$component->id == $item['id']) {
											$node = new JAMenuNode($component->text, $component->link, $component->img, NUll, NUll, '');
											foreach ($menus['items'] as $it) {
												foreach ($component->submenu as $sub) {
													if (array_intersect($userGroups, $it['acl']) || in_array('all', $it['acl'])) {
														if ('cpn-'.$sub->id == $it['id']) {
															$jamenunode = new JAMenuNode($sub->text, $sub->link, $sub->img, NUll, NUll, '');
															$node->addChild($jamenunode);
														}
													}
												}
											}
											$jamenunode = new JAMenuNode(ucfirst(JText::_($group)), '#');
											$node->setParent($jamenunode);
											$menu->addChild($node);
										}
									}
								}
								// This component has a db driven submenu.
							}
							else
							{
								if ($dashboard) {
									if (in_array($dashboard, $item['acl']) || (in_array('all', $item['acl']) && JAccess::checkGroup($dashboard, 'core.manage', $component->element)) || $dashboard == '8') {
										if ('cpn-'.$component->id == $item['id']) {
											$menu->addChild(new JAMenuNode($component->text, $component->link, $component->img, NUll, NUll, ''));
										}
									}
								} else {
									$z = 0;
									foreach ($userGroups as $userGroup) {
										if (JAccess::checkGroup($userGroup, 'core.manage', $component->element) || $userGroup == '8') {
											$z++;
										}
									}
									if (array_intersect($userGroups, $item['acl']) || (in_array('all', $item['acl']) && $z > 0)) {
										if ('cpn-'.$component->id == $item['id']) {
											$menu->addChild(new JAMenuNode($component->text, $component->link, $component->img, NUll, NUll, ''));
										}
									}
								}
							}
						}
					}
					$menu->getParent();
				}
			}
		}
	}
} else {
	
	/*
	* Default Admin menu of joomla if does not find the configs file.
	*/
	/*
 * Site Submenu
 */
	$menu->addChild(new JAMenuNode(JText::_('Dashboard'), 'index.php', 'cpanel', true , '', ''));
 
	$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_SYSTEM'), '#'), true);
	$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_CONTROL_PANEL'), 'index.php', 'class:cpanel'));

	if ($user->authorise('core.admin'))
	{
		$menu->addSeparator();
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_CONFIGURATION'), 'index.php?option=com_config', 'class:config'));
	}

	if ($user->authorise('core.manage', 'com_checkin'))
	{
		$menu->addSeparator();
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_GLOBAL_CHECKIN'), 'index.php?option=com_checkin', 'class:checkin'));
	}

	if ($user->authorise('core.manage', 'com_cache'))
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_CLEAR_CACHE'), 'index.php?option=com_cache', 'class:clear'));
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_PURGE_EXPIRED_CACHE'), 'index.php?option=com_cache&view=purge', 'class:purge'));
	}

	if ($user->authorise('core.admin'))
	{
		$menu->addSeparator();
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_SYSTEM_INFORMATION'), 'index.php?option=com_admin&view=sysinfo', 'class:info'));
	}

	$menu->getParent();

	/*
	 * Users Submenu
	 */
	if ($user->authorise('core.manage', 'com_users'))
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_USERS'), '#'), true);
		$createUser = $shownew && $user->authorise('core.create', 'com_users');
		$createGrp  = $user->authorise('core.admin', 'com_users');

		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_USER_MANAGER'), 'index.php?option=com_users&view=users', 'class:user'), $createUser);

		if ($createUser)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_ADD_USER'), 'index.php?option=com_users&task=user.add', 'class:newarticle'));
			$menu->getParent();
		}

		if ($createGrp)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_GROUPS'), 'index.php?option=com_users&view=groups', 'class:groups'), $createUser);

			if ($createUser)
			{
				$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_ADD_GROUP'), 'index.php?option=com_users&task=group.add', 'class:newarticle'));
				$menu->getParent();
			}

			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_LEVELS'), 'index.php?option=com_users&view=levels', 'class:levels'), $createUser);

			if ($createUser)
			{
				$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_ADD_LEVEL'), 'index.php?option=com_users&task=level.add', 'class:newarticle'));
				$menu->getParent();
			}
		}

		$menu->addSeparator();
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_NOTES'), 'index.php?option=com_users&view=notes', 'class:user-note'), $createUser);

		if ($createUser)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_USERS_ADD_NOTE'), 'index.php?option=com_users&task=note.add', 'class:newarticle'));
			$menu->getParent();
		}

		$menu->addChild(
			new JAMenuNode(
				JText::_('MOD_MENU_COM_USERS_NOTE_CATEGORIES'), 'index.php?option=com_categories&view=categories&extension=com_users', 'class:category'),
			$createUser
		);

		if ($createUser)
		{
			$menu->addChild(
				new JAMenuNode(
					JText::_('MOD_MENU_COM_CONTENT_NEW_CATEGORY'), 'index.php?option=com_categories&task=category.add&extension=com_users',
					'class:newarticle'
				)
			);
			$menu->getParent();
		}

		if (JFactory::getApplication()->get('massmailoff') != 1)
		{
			$menu->addSeparator();
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_MASS_MAIL_USERS'), 'index.php?option=com_users&view=mail', 'class:massmail'));
		}

		$menu->getParent();
	}

	/*
	 * Menus Submenu
	 */
	if ($user->authorise('core.manage', 'com_menus'))
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_MENUS'), '#'), true);
		$createMenu = $shownew && $user->authorise('core.create', 'com_menus');

		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_MENU_MANAGER'), 'index.php?option=com_menus&view=menus', 'class:menumgr'), $createMenu);

		if ($createMenu)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU'), 'index.php?option=com_menus&view=menu&layout=edit', 'class:newarticle'));
			$menu->getParent();
		}

		$menu->addSeparator();

		// Menu Types
		$menuTypes = JAModMenuHelper::getMenus();
		$menuTypes = JArrayHelper::sortObjects($menuTypes, 'title', 1, false);

		foreach ($menuTypes as $menuType)
		{
			$alt = '*' . $menuType->sef . '*';

			if ($menuType->home == 0)
			{
				$titleicon = '';
			}
			elseif ($menuType->home == 1 && $menuType->language == '*')
			{
				$titleicon = ' <span class="icon-home"></span>';
			}
			elseif ($menuType->home > 1)
			{
				$titleicon = ' <span>'
					. JHtml::_('image', 'mod_languages/icon-16-language.png', $menuType->home, array('title' => JText::_('MOD_MENU_HOME_MULTIPLE')), true)
					. '</span>';
			}
			else
			{
				$image = JHtml::_('image', 'mod_languages/' . $menuType->image . '.gif', null, null, true, true);

				if (!$image)
				{
					$image = JHtml::_('image', 'mod_languages/icon-16-language.png', $alt, array('title' => $menuType->title_native), true);
				}
				else
				{
					$image = JHtml::_('image', 'mod_languages/' . $menuType->image . '.gif', $alt, array('title' => $menuType->title_native), true);
				}

				$titleicon = ' <span>' . $image . '</span>';
			}

			$menu->addChild(
				new JAMenuNode(
					$menuType->title, 'index.php?option=com_menus&view=items&menutype=' . $menuType->menutype, 'class:menu', null, null, $titleicon
				),
				$createMenu
			);

			if ($createMenu)
			{
				$menu->addChild(
					new JAMenuNode(
						JText::_('MOD_MENU_MENU_MANAGER_NEW_MENU_ITEM'), 'index.php?option=com_menus&view=item&layout=edit&menutype=' . $menuType->menutype,
						'class:newarticle')
				);
				$menu->getParent();
			}
		}

		$menu->getParent();
	}

	/*
	 * Content Submenu
	 */
	if ($user->authorise('core.manage', 'com_content'))
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_CONTENT'), '#'), true);
		$createContent = $shownew && $user->authorise('core.create', 'com_content');
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_CONTENT_ARTICLE_MANAGER'), 'index.php?option=com_content', 'class:article'), $createContent);

		if ($createContent)
		{
			$menu->addChild(
				new JAMenuNode(JText::_('MOD_MENU_COM_CONTENT_NEW_ARTICLE'), 'index.php?option=com_content&task=article.add', 'class:newarticle')
			);
			$menu->getParent();
		}

		$menu->addChild(
			new JAMenuNode(
				JText::_('MOD_MENU_COM_CONTENT_CATEGORY_MANAGER'), 'index.php?option=com_categories&extension=com_content', 'class:category'),
			$createContent
		);

		if ($createContent)
		{
			$menu->addChild(
				new JAMenuNode(JText::_('MOD_MENU_COM_CONTENT_NEW_CATEGORY'), 'index.php?option=com_categories&task=category.add&extension=com_content', 'class:newarticle')
			);
			$menu->getParent();
		}
		
		if ($comFieldsEnabled && JComponentHelper::getParams('com_users')->get('custom_fields_enable', '1'))
		{
			$menu->addChild(
					new JAMenuNode(
							JText::_('MOD_MENU_FIELDS'), 'index.php?option=com_fields&context=com_users.user', 'class:fields')
					);
			$menu->addChild(
					new JAMenuNode(
							JText::_('MOD_MENU_FIELDS_GROUP'), 'index.php?option=com_fields&view=groups&context=com_users.user', 'class:category')
					);
		}

		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_CONTENT_FEATURED'), 'index.php?option=com_content&view=featured', 'class:featured'));

		if ($user->authorise('core.manage', 'com_media'))
		{
			$menu->addSeparator();
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_MEDIA_MANAGER'), 'index.php?option=com_media', 'class:media'));
		}

		$menu->getParent();
	}

	/*
	 * Components Submenu
	 */

	// Get the authorised components and sub-menus.
	$components = JAModMenuHelper::getComponents(true);

	// Check if there are any components, otherwise, don't render the menu
	if ($components)
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COMPONENTS'), '#'), true);

		foreach ($components as &$component)
		{
			if (!empty($component->submenu))
			{
				// This component has a db driven submenu.
				$menu->addChild(new JAMenuNode($component->text, $component->link, $component->img), true);

				foreach ($component->submenu as $sub)
				{
					$menu->addChild(new JAMenuNode($sub->text, $sub->link, $sub->img));
				}

				$menu->getParent();
			}
			else
			{
				$menu->addChild(new JAMenuNode($component->text, $component->link, $component->img));
			}
		}

		$menu->getParent();
	}

	/*
	 * Extensions Submenu
	 */
	$im = $user->authorise('core.manage', 'com_installer');
	$mm = $user->authorise('core.manage', 'com_modules');
	$pm = $user->authorise('core.manage', 'com_plugins');
	$tm = $user->authorise('core.manage', 'com_templates');
	$lm = $user->authorise('core.manage', 'com_languages');

	if ($im || $mm || $pm || $tm || $lm)
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_EXTENSIONS'), '#'), true);

		if ($im)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_EXTENSION_MANAGER'), 'index.php?option=com_installer', 'class:install'), $im);

			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_INSTALL'), 'index.php?option=com_installer', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_UPDATE'), 'index.php?option=com_installer&view=update', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_MANAGE'), 'index.php?option=com_installer&view=manage', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_DISCOVER'), 'index.php?option=com_installer&view=discover', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_DATABASE'), 'index.php?option=com_installer&view=database', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_WARNINGS'), 'index.php?option=com_installer&view=warnings', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_LANGUAGES'), 'index.php?option=com_installer&view=languages', 'class:install'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_INSTALLER_SUBMENU_UPDATESITES'), 'index.php?option=com_installer&view=updatesites', 'class:install'));
			$menu->getParent();
		}

		if ($im && ($mm || $pm || $tm || $lm))
		{
			$menu->addSeparator();
		}

		if ($mm)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_MODULE_MANAGER'), 'index.php?option=com_modules', 'class:module'));
		}

		if ($pm)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_PLUGIN_MANAGER'), 'index.php?option=com_plugins', 'class:plugin'));
		}

		if ($tm)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_TEMPLATE_MANAGER'), 'index.php?option=com_templates', 'class:themes'));
		}

		if ($lm)
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_EXTENSIONS_LANGUAGE_MANAGER'), 'index.php?option=com_languages', 'class:language'), $lm);

			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_LANGUAGES_SUBMENU_INSTALLED'), 'index.php?option=com_languages&view=installed', 'class:language'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_LANGUAGES_SUBMENU_CONTENT'), 'index.php?option=com_languages&view=languages', 'class:language'));
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_COM_LANGUAGES_SUBMENU_OVERRIDES'), 'index.php?option=com_languages&view=overrides', 'class:language'));
			$menu->getParent();
		}

		$menu->getParent();
	}

	/*
	 * Help Submenu
	 */
	if ($showhelp == 1)
	{
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP'), '#'), true);
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_JOOMLA'), 'index.php?option=com_admin&view=help', 'class:help'));
		$menu->addSeparator();

		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_SUPPORT_OFFICIAL_FORUM'), 'http://forum.joomla.org', 'class:help-forum', false, '_blank'));

		if ($forum_url = $params->get('forum_url'))
		{
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_SUPPORT_CUSTOM_FORUM'), $forum_url, 'class:help-forum', false, '_blank'));
		}

		$debug = $lang->setDebug(false);

		if ($lang->hasKey('MOD_MENU_HELP_SUPPORT_OFFICIAL_LANGUAGE_FORUM_VALUE') && JText::_('MOD_MENU_HELP_SUPPORT_OFFICIAL_LANGUAGE_FORUM_VALUE') != '')
		{
			$forum_url = 'http://forum.joomla.org/viewforum.php?f=' . (int) JText::_('MOD_MENU_HELP_SUPPORT_OFFICIAL_LANGUAGE_FORUM_VALUE');
			$lang->setDebug($debug);
			$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_SUPPORT_OFFICIAL_LANGUAGE_FORUM'), $forum_url, 'class:help-forum', false, '_blank'));
		}

		$lang->setDebug($debug);
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_DOCUMENTATION'), 'https://docs.joomla.org', 'class:help-docs', false, '_blank'));
		$menu->addSeparator();

		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_EXTENSIONS'), 'http://extensions.joomla.org', 'class:help-jed', false, '_blank'));
		$menu->addChild(
			new JAMenuNode(JText::_('MOD_MENU_HELP_TRANSLATIONS'), 'http://community.joomla.org/translations.html', 'class:help-trans', false, '_blank')
		);
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_RESOURCES'), 'http://resources.joomla.org', 'class:help-jrd', false, '_blank'));
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_COMMUNITY'), 'http://community.joomla.org', 'class:help-community', false, '_blank'));
		$menu->addChild(
			new JAMenuNode(JText::_('MOD_MENU_HELP_SECURITY'), 'https://developer.joomla.org/security-centre.html', 'class:help-security', false, '_blank')
		);
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_DEVELOPER'), 'https://developer.joomla.org', 'class:help-dev', false, '_blank'));
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_XCHANGE'), 'https://joomla.stackexchange.com', 'class:help-dev', false, '_blank'));
		$menu->addChild(new JAMenuNode(JText::_('MOD_MENU_HELP_SHOP'), 'https://shop.joomla.org', 'class:help-shop', false, '_blank'));
		$menu->getParent();
	}
}


