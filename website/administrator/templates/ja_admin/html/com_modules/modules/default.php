<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_modules
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// populate viewStyle from request
$app = JFactory::getApplication();

$listStyle = $app->getUserStateFromRequest ('modules.listStyle', 'listStyle', 'grid');
$app->setUserState('modules.listStyle', $listStyle);

$viewFile = __DIR__ . '/default_' . $listStyle . '.php';
if (!is_file($viewFile)) {
	$viewFile = __DIR__ . '/default_grid.php';
} 
include ($viewFile);
