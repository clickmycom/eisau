<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_modules
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once (JPATH_ADMINISTRATOR . '/templates/'. _ADMIN_TPL . '/helper.php');

$app = JFactory::getApplication();
$input = $app->input;
$client_id = (isset($this->activeFilters['client_id']) && $this->activeFilters['client_id'] == 0 ) ? 'site' : 'administrator';

//Get all active position on the template
jimport('joomla.filesystem.file');
if ($client_id == 'administrator') {
	$file = JPATH_ADMINISTRATOR . '/templates/'._ADMIN_TPL.'/templateDetails.xml';
	if (JFile::exists($file)) {
		$TPLxml = simplexml_load_string(JFile::read($file));
		$activePositions = $TPLxml->positions->position;
	}
} elseif ($client_id == 'site'){
	$templateSite = BKTMPHelper::getTemplateSite();
	$file = JPATH_SITE . '/templates/' . $templateSite . '/templateDetails.xml';
	if (JFile::exists($file)) {
		$TPLxml = simplexml_load_string(JFile::read($file));
		$activePositions = $TPLxml->positions->position;
	}
}

if ($this->state->get('list.limit') != 0) {
	// redirect with new list limit
	$redirectUrl = 'index.php?option=com_modules&list[limit]=0';
	JFactory::getApplication()->redirect($redirectUrl);
}

// Display all module which its position is none.
foreach($this->items as $item) {
	if($item->position == '') $item->position = 'none';
}

$user		= JFactory::getUser();
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$trashed	= $this->state->get('filter.state') == -2 ? true : false;
$canChange	= $user->authorise('core.edit.state', 'com_modules');
$saveOrder	= ($listOrder == 'ordering');
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_modules&task=modules.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'moduleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
?>
<form action="<?php echo JRoute::_('index.php?option=com_modules'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<?php
		// Search tools bar and filters
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('COM_MODULES_MSG_MANAGE_NO_MODULES'); ?>
			</div>
		<?php else : ?>
		<div class="modules-grid sortable-wrap">
			<?php 
				$arr = array();
				$arrPos = array();
				foreach ($this->items as $i => $item) : 
					$ordering   = ($listOrder == 'ordering');
					$canCreate  = $user->authorise('core.create',     'com_modules');
					$canEdit	= $user->authorise('core.edit',		  'com_modules.module.' . $item->id);
					$canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $user->get('id')|| $item->checked_out == 0;
					$canChange  = $user->authorise('core.edit.state', 'com_modules.module.' . $item->id) && $canCheckin;
					
					if ($item->position) :

						$select = '';
						if ($item->enabled > 0): 
									$select = JHtml::_('grid.id', $i, $item->id);
						endif;

						$status = '';
						if ($item->enabled > 0) :
								$status = JHtml::_('jgrid.published', $item->published, $i, 'modules.', $canChange, 'cb', $item->publish_up, $item->publish_down); 
						endif; 

						$iconClass = '';
						if (!$canChange)
						{
							$iconClass = ' inactive';
						}
						elseif (!$saveOrder)
						{
							$iconClass = ' inactive tip-top hasTooltip" title="' . JHtml::tooltipText('JORDERINGDISABLED');
						}
						?>
						
						<?php if ($canChange && $saveOrder) : ?>
							<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering; ?>" class="width-20 text-area-order" />
						<?php endif;

						$itemLink = '';
						if ($canEdit) :
							if (!$item->checked_out || $item->checked_out == $user->id) {
								$itemLink = '<a class="btn-edit-module" href="javascript:void(0);" data-toggle="modal" data-target="#jabktmp-module" data-url="'.JUri::base() .'index.php?option=com_modules&task=module.edit&id=' . (int) $item->id.'" data-module="'. (int) $item->id .'" title="Edit The Module"><span class="icon-edit" title="Edit Module"></span></a>';
							} else {
								$itemLink = '<a href="'.JRoute::_('index.php?option=com_modules&task=module.edit&id=' . (int) $item->id).'" title="Edit The Module"><span class="icon-edit" title="Edit Module"></span></a>';
								if ($item->checked_out && $canChange) {
									$itemLink .= JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'modules.', $canCheckin);
								}
							}
						else:
							$itemLink = '';
						endif;
						
						$arr[$item->position][] = '<div class="btn btn-block module-name sortable-item" data-modid=" ' . $item->id . '"><span class="item-select">'.$select.'</span><span class="sortable-handler'.$iconClass.'"><i class="fa fa-arrows"></i></span><span class="item-status">'.$status.'</span><span class="item-name">'.$item->title.'</span><span class="item-edit">'.$itemLink.'</span></div>';
						$arrPos[] = $item->position;
					else :
						$arr['none'][] = '';
					endif;
	      endforeach;
				
				// Display the Positions haven't got any module was assigned
				if (isset($activePositions)) {
					foreach ($activePositions as $key => $position) {
						if (!in_array((string)$position, $arrPos)) {
							$arr[(string)$position][] = '';
						}
					}
				}
				
	      foreach ($arr AS $k => $ar) {
	      	// $modules = '<div class="sortable-list item-list" data-position="' . $k . '">' . implode(' ', $ar) . '</div>';
	      	?>
	      	<div class="position-warpper">
	      		<div class="position-inner">
	      			<div class="position-name">
	      				<h3>
									<?php echo $k ?>
								</h3>
		      			<a href="javascript:void(0); " data-toggle="modal" data-target="#jabktmp-module" data-position="<?php echo $k; ?>" title="Add more" class="btn btn-small btn-success btn-addmore-module"><span class="icon-plus icon-white"></span>Add more</a>
	      			</div>
	      			<div class="global-actions"><input type="checkbox" data-modules="<?php echo count($ar);?>" name="checkall-toggle" onclick="checkall(this);" class="hasTooltip" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" /><?php echo JText::_('JGLOBAL_CHECK_ALL'); ?></div>
	      			<div class="sortable-list item-list" data-position="<?php echo $k ?>">
		      			<?php echo implode(' ', $ar) ?>
	      			</div>
	      		</div>
	      	</div>
	      	<?php
	      } ?>
	  </div>
	  <?php endif; 	?>

		<?php // Load the batch processing form. ?>
		<?php if ($user->authorise('core.create', 'com_modules')
			&& $user->authorise('core.edit', 'com_modules')
			&& $user->authorise('core.edit.state', 'com_modules')) : ?>
			<?php echo JHtml::_(
				'bootstrap.renderModal',
				'collapseModal',
				array(
					'title' => JText::_('COM_MODULES_BATCH_OPTIONS'),
					'footer' => $this->loadTemplate('batch_footer')
				),
				$this->loadTemplate('batch_body')
			); ?>
		<?php endif; ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<!-- Modal create/edit module -->
<div id="jabktmp-module" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="height:60px;">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
					<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
					<p>Loading...</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-save" onClick="Joomla.submitbutton('module.apply')"><?php echo JText::_('TPL_ADMIN_SAVE_CHANGE');?></button>
				<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal" onClick="Joomla.submitbutton('module.cancel')"><?php echo JText::_('JCANCEL'); ?></button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal -->

<script>
jQuery(document).ready(function($) {
	// Add more module
	$('.btn-addmore-module').on('click', function() {
		var position = $(this).data('position');
		var action = $(this).data('name');
		$('.btn-save').css('display','none');
		var loadurl = '<?php echo JRoute::_('index.php?option=com_modules&view=select&tmpl=component') ?>';
		
		$('#jabktmp-module').find('.modal-body').html('<iframe height="100%" scrolling="no" id="ja-md-creat" name="modalModule" src="'+loadurl+'"></iframe>');
		
		$('#ja-md-creat').on('load', function() {
			var listModuleInner = window.modalModule.document.getElementById('new-modules-list');
			var urlNewModule = $(listModuleInner).find('a');
			urlNewModule.each(function() {
				var newURL = $(this).attr('href') + '&tmpl=component';
				$(this).attr('href', newURL);
			});
			
			$(listModuleInner).find('a').on('click', function() {
				$('#ja-md-creat').load(function() {
					$('.btn-save').css('display','inline-block');
					window.modalModule.document.getElementById('module-form').action += '&tmpl=component';

					var mP = (window.modalModule.document.getElementsByClassName('chzn-custom-value')[0]);
					$(mP).next().remove();
					$(mP).val(position);
					$(mP).chosen();
					
				});

			Joomla.submitbutton = function (task) {
				Joomla.submitform(task, window.modalModule.document.getElementById('module-form'));
			};
			});
		});
	});
	
	//Edit Module
	$('.btn-edit-module').on('click', function() {
		loadurl = $(this).data('url');
		$('#jabktmp-module').find('.modal-body').html('<iframe height="100%" scrolling="no" id="ja-md-edit" name="modalModule" src="'+loadurl+'&tmpl=component"></iframe>');
		$('#jabktmp-module').find('.modal-title').html('<?php echo JTEXT::_('TPL_ADMIN_MODULE_EDIT'); ?>');
		$('#ja-md-edit').on('load', function() {
			if (window.modalModule.document.getElementById('module-form')) {
				window.modalModule.document.getElementById('module-form').action += '&tmpl=component';
			}
		});

		Joomla.submitbutton = function (task) {
			Joomla.submitform(task, window.modalModule.document.getElementById('module-form'));
		};
	});
	
	// Reload when close modal
	$('#jabktmp-module').on('hide.bs.modal', function() {
		window.location.href = window.location.href;
		$('body').css('overflow','inherit');
	});
	
	$('#jabktmp-module').on('shown.bs.modal', function() {
		$('body').css('overflow','hidden');
	});
});

// Checkall Modules in the position
function checkall(e) {
	jQuery(e).parent().next().find('input:checkbox').attr('checked', e.checked);
	var $values = parseInt(jQuery('input[name="boxchecked"]').val());
	if (e.checked) {
		if ($values != 0) {
			jQuery('input[name="boxchecked"]').attr('value', $values + parseInt(jQuery(e).data('modules')));
		} else {
			jQuery('input[name="boxchecked"]').attr('value', jQuery(e).data('modules'));
		}
	} else {
		if ($values > parseInt(jQuery(e).data('modules'))) {
			jQuery('input[name="boxchecked"]').attr('value', $values - parseInt(jQuery(e).data('modules')));
		} else {
			jQuery('input[name="boxchecked"]').attr('value', '0');
		}
	}
}
</script>