<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
require_once(dirname(dirname(__FILE__)).'../../../helper.php');
use Joomla\Registry\Registry;

JHtml::_('behavior.framework', true);
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$doc->addScript(JUri::root() . 'media/jui/js/chosen.jquery.min.js');
$doc->addStyleSheet(JUri::root().'media/jui/css/chosen.css');

$helper = new BKTMPHelper();
$user = JFactory::getUser();
$app = JFactory::getApplication();

// Get the dashboard configurations
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
$path = JPATH_ROOT. '/media/'._ADMIN_TPL;
$fileConfigs = $path.'/configs.json';
$userGroups = $user->get('groups');
$dashboard = $app->input->get('dashboard');

if (JFile::exists($fileConfigs)) {
	$configs = json_decode(@file_get_contents($fileConfigs), true);
} else {
	$configs = array();
	$configs['modules'] = array();
	$configs['quicklinks'] = array();
	$profilesFolder = $path. '/profiles';
	if (JFolder::exists($profilesFolder)) {
		$profiles = JFolder::files($profilesFolder);
		if (count($profiles)) {
			foreach ($profiles as $profile) {
				$file = $profilesFolder . '/'.$profile;
				if (JFile::exists($file)) {
					$oldConfigs = json_decode(@file_get_contents($file), true);
					// Old configs modules
					foreach ($oldConfigs['modules'] as $mod => $value) {
						if (!isset($configs['modules'][$mod])) {
							$configs['modules'][$mod] = array();
						}
						if ($value == '1') {
							$configs['modules'][$mod][] = $oldConfigs['general']['profile_id'];
						}
					}
					
					// old configs quick links
					if (count($oldConfigs['quicklinks'])) {
						$oldKey = 0;
						foreach ($oldConfigs['quicklinks'] as $group => $ql) {
							$configs['quicklinks'][$group] = array();
							$configs['quicklinks'][$group]['id'] = 'old-'.$oldConfigs['general']['profile_id'].'-'.$oldKey;
							$oldKey++;
							$configs['quicklinks'][$group]['acl'] = array($oldConfigs['general']['profile_id']);
							$configs['quicklinks'][$group]['items'] = array();
							if (count($ql)) {
								foreach ($ql as $key => $item) {
									if ($item != '') {
										$link = array();
										$link['timestamp'] = $item;
										$link['acl'] = array($oldConfigs['general']['profile_id']);
										$configs['quicklinks'][$group]['items'][] = $link;
									}
								}
							}
						}
					}
				}
			}
		}
		$newName = $path. '/oldversion_profiles';
		rename($profilesFolder, $newName);
		
		$buffer = json_encode($configs);
		$configsFile = $path. '/configs.json';
		JFile::makeSafe($fileConfigs);
		JFile::write($fileConfigs, $buffer);
	}
}

// Get quicklinks to Display
$quicklinks = array();
$links = array();

// Menu Item Links
$qlPath = JPATH_ADMINISTRATOR . '/templates/' . _ADMIN_TPL . '/admin/assets/links/quicklinks.json';
if (JFile::exists($qlPath)) {
	$links = json_decode(file_get_contents($qlPath), true);
	
	// Components Link
	$components = $helper->getComponents();
	if ($components){
		foreach ($components as $component) {
			if (count($component->submenu) > 0 ) {
				foreach ($component->submenu as $sub) {
					$item = array();
					$item['timeStamp'] = $sub->id;
					$item['name'] = strtolower($sub->title);
					$item['title'] = $sub->path;
					$item['link'] = $sub->link;
					$item['icon'] = str_replace('class:','', $sub->img);
					$item['image'] = '';
					$links['systemlinks'][] = $item;
				}
			} else {
				$item = array();
				$item['timeStamp'] = $component->id;
				$item['name'] = strtolower($component->title);
				$item['title'] = $component->path;
				$item['link'] = $component->link;
				$item['icon'] = str_replace('class:','', $component->img);
				$item['image'] = '';
				$links['systemlinks'][] = $item;
			}
		}
	}
	
	$links = array_merge($links['systemlinks'], $links['customlinks']);
}

// Get Links which optimized by user
$customPath = JPATH_SITE . '/media/' . _ADMIN_TPL . '/links/customlinks.json';
if (JFile::exists($customPath)) {
	$csLinks = json_decode(file_get_contents($customPath), true);
	foreach ($csLinks as $cslink) {
		$n = 0;
		foreach ($links as $k => $link) {
			if ($link['timeStamp'] == $cslink['timeStamp']) {
				$links[$k]['title'] = $cslink['title'];
				$links[$k]['icon'] = $cslink['icon'];
				$links[$k]['image'] = $cslink['image'];
			} else {
				$n++;
			}
		}
		if ($n == count($links)) {
			$links[] = $cslink;
		}
	}
}

// Get links by User Group Access
	// Get all admin Group Users
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_users/models', 'UsersModel');
$model = JModelLegacy::getInstance('Groups', 'UsersModel', array('ignore_request' => true));
$groups = $model->getItems();
if (count($groups)) {
	foreach ($groups as $key => $group) {
		if(!JAccess::checkGroup((int)$group->id,'core.login.admin') && $group->id != '8') {
			unset($groups[$key]);
		}
	}
}

foreach ($links as $key => $link) {
	$url = (isset($link['link'])) ? $link['link'] : $link['url'];
	$links[$key]['acl'] = array();
	if (preg_match('/(option=)(com_[a-z0-9]+)/', $url)) {
		preg_match_all('/(option=)(com_[a-z0-9]+)/', $url, $matches);
		$asset = $matches[2][0];
		switch ($asset) {
			case 'com_cpanel' :
				break;
			case 'com_config' :
			case 'com_admin' :
				foreach ($groups as $gr) {
					if (JAccess::checkGroup((int) $gr->id, 'core.admin', $asset) || $gr->id == '8') {
						$links[$key]['acl'][] = $gr->id;
					}
				}
				break;
			default :
				if ($asset == 'com_categories') {
					if (preg_match('/(&extension=)(.+)/', $url)) {
						preg_match_all('/(&extension=)(.+)/', $url, $matches);
						$asset = $matches[2][0];
					}
				}
				foreach ($groups as $gr) {
					if (JAccess::checkGroup((int) $gr->id, 'core.manage', $asset) || $gr->id == '8') {
						if (preg_match('/(edit)(&.+)?/', $url)) {
							if (JAccess::checkGroup((int) $gr->id, 'core.edit', $asset) || $gr->id == '8') {
								$links[$key]['acl'][] = $gr->id;
							}
						} else if (preg_match('/(\.add)$/', $url) || preg_match('/(\.add&)(.+)?/', $url)) {
							if (JAccess::checkGroup((int) $gr->id, 'core.create', $asset) || $gr->id == '8') {
								$links[$key]['acl'][] = $gr->id;
							}
						} else if (preg_match('/(\.delete&?)/', $url)) {
							if (JAccess::checkGroup((int) $gr->id, 'core.delete', $asset) || $gr->id == '8') {
								$links[$key]['acl'][] = $gr->id;
							}
						} else {
							$links[$key]['acl'][] = $gr->id;
						}
					}
				}
				break;
		}
	} else {
		foreach ($groups as $gr) {
			$links[$key]['acl'][] = $gr->id;
		}
	}
	if (count($links[$key]['acl']) > 1) {
		array_unshift($links[$key]['acl'], 'all');
	}
}

if (isset($configs) && count($configs['quicklinks']) > 0) {
	foreach ($configs['quicklinks'] as $gr => $qls) {
		$quicklinks[$gr] = array();
		$quicklinks[$gr]['links'] = array();
		$quicklinks[$gr]['acl'] = isset($qls['acl']) ? $qls['acl'] : '';
		if (isset($qls['items'])) {
			foreach ($qls['items'] as $ql) {
				if (isset($ql['acl'])) {
					foreach($links as $link) {
						if ($dashboard) {
							if (in_array($dashboard, $ql['acl']) || (in_array('all', $ql['acl']) && in_array($dashboard, $link['acl']))) {
								if($ql['timestamp'] == $link['timeStamp']) {
									$quicklinks[$gr]['links'][] = $link;
								}
							}
						} elseif ((in_array('all', $ql['acl']) && count(array_intersect($userGroups, $link['acl']))) || count(array_intersect($userGroups, $ql['acl']))) {
							if($ql['timestamp'] == $link['timeStamp']) {
								$quicklinks[$gr]['links'][] = $link;
							}
						}
					}
				}
			}
		}
	}
}

foreach($quicklinks as $gr => $qls) {
	foreach ($qls['links'] as $k => $ql) {
		if(!isset($ql['name'])){}
		if(preg_match('/\/.+/', $ql['title'])) {
			$quicklinks[$gr]['links'][$k]['name'] = preg_replace('/.+\//', '', $ql['title']);
		} else {
			$quicklinks[$gr]['links'][$k]['name'] = $ql['title'];
		}
	}
}

// Get links which customized by user
$customPath = $path . '/links/customlinks.json';
if (JFile::exists($customPath)) {
	$csLinks = json_decode(file_get_contents($customPath), true);
	if (count($csLinks) > 0) {
		foreach ($quicklinks as $gr => $qls) {
			foreach ($qls['links'] as $k => $v) {
				foreach ($csLinks as $link) {
					if(preg_match('/\/.+/', $link['title'])) {
						$link['name'] = preg_replace('/.+\//', '', $link['title']);
					} else {
						$link['name'] = $link['title'];
					} 
					if ($link['timeStamp'] == $v['timeStamp']) {
						$quicklinks[$gr]['links'][$k] = $link;
					}
				}
			}
		}
	}
}
?>

<?php /* $iconmodules = JModuleHelper::getModules('icon');
	if ($iconmodules) : 
	$iconmodulesNew = array();
	if (isset($dsConfigs) && isset($dsConfigs['modules'])) {
		foreach($iconmodules as $iconmodule) {
			foreach($dsConfigs['modules'] as $cf => $value ) {
				if($cf == $iconmodule->module.$iconmodule->id && $value == '1') {
					$iconmodulesNew[] = $iconmodule;
				}
			}
		}
	} else {
		$iconmodulesNew = $iconmodules;
	}
?>
<div class="cpanel-links">
	<?php
	// Display the submenu position modules
	foreach ($iconmodulesNew as $iconmodule)
	{
		echo JModuleHelper::renderModule($iconmodule);
	}
	?>
</div>
<?php endif; */ ?>

<?php if ($user->authorise('core.manage', 'com_postinstall') && $this->postinstall_message_count) : ?>
<div class="alert alert-info">
	<h4><?php echo JText::_('COM_CPANEL_MESSAGES_TITLE'); ?></h4>
	<p><?php echo JText::_('COM_CPANEL_MESSAGES_BODY_NOCLOSE'); ?></p>
	<p><?php echo JText::_('COM_CPANEL_MESSAGES_BODYMORE_NOCLOSE'); ?></p>
	<p>
		<a href="index.php?option=com_postinstall&amp;eid=700" class="btn btn-primary">
			<?php echo JText::_('COM_CPANEL_MESSAGES_REVIEW'); ?>
		</a>
	</p>
</div>
<?php endif; ?>

<?php echo JLayoutHelper::render ('this_template.dashboard.home', array()) ?>
<!-- Quicklinks -->

<?php if (count($quicklinks) > 0) : 
	$checkql = 0;
	foreach ($quicklinks as $groupql) {
		if ($dashboard) {
			if (in_array($dashboard, $groupql['acl']) || in_array('all', $groupql['acl'])) {
				$checkql++;
			}
		} else {
			if (count(array_intersect($userGroups, $groupql['acl'])) > 0 || in_array('all', $groupql['acl'])) {
				$checkql++;
			}
		}
	}
	if ($checkql > 0) :
?>
	<div class="dashboard-links">
		<h3><?php echo JText::_('TPL_ADMIN_DASHBOARD_QUICKLINKS'); ?></h3>
		<div class="dashboard-links-wrap">
		<?php 
		foreach($quicklinks as $gr => $qls) :
			if (is_array($qls['acl'])) :
				if ($dashboard) :
					if (in_array($dashboard, $qls['acl']) || in_array('all',$qls['acl'])) : 
						if (!is_array($qls['links']) || count($qls['links']) == 0) :
		?>
						<div class="group">
							<h3><span><?php echo trim(ucfirst($gr)); ?></span></h3>
							<p>
								<?php echo JText::_('TPL_ADMIN_QUICKLINKS_NO_LINKS'); ?>
							</p>
						</div>
					<?php else : ?>
						<div class="group">
							<h3><span><?php echo trim(ucfirst($gr)); ?></span></h3>
							<ul>
							<?php
							foreach($qls['links'] as $quicklink) :	
							?>
								<li>
									<a href="<?php if (isset($quicklink['url'])) : echo $quicklink['url']; else : echo $quicklink['link']; endif; ?>" title="<?php echo $quicklink['title']; ?>">
										<?php if (isset($quicklink['image']) && $quicklink['image'] != '') : ?>
											<img src="../<?php echo $quicklink['image']; ?>" alt="<?php echo preg_replace('/.+\//','', $quicklink['image']); ?>" />
										<?php elseif (isset($quicklink['icon']) && $quicklink['icon'] != '') : ?>
											<i class="<?php echo $quicklink['icon']; ?>"></i>
										<?php endif; ?>
											<span><strong><?php echo ucfirst($quicklink['name']); ?></strong></span>
									</a>
								</li>
							<?php endforeach;?>
							</ul>
						</div>
				<?php	
						endif;
					endif;
				elseif (count(array_intersect($userGroups, $qls['acl'])) > 0 || in_array('all',$qls['acl'])) :
					if (!is_array($qls['links']) || count($qls['links']) == 0) :
		?>
						<div class="group">
							<h3><span><?php echo trim(ucfirst($gr)); ?></span></h3>
							<p>
								<?php echo JText::_('TPL_ADMIN_QUICKLINKS_NO_LINKS'); ?>
							</p>
						</div>
					<?php else : ?>
						<div class="group">
							<h3><span><?php echo trim(ucfirst($gr)); ?></span></h3>
							<ul>
							<?php
							foreach($qls['links'] as $quicklink) :	
							?>
								<li>
									<a href="<?php if (isset($quicklink['url'])) : echo $quicklink['url']; else : echo $quicklink['link']; endif; ?>" title="<?php echo $quicklink['title']; ?>">
										<?php if (isset($quicklink['image']) && $quicklink['image'] != '') : ?>
											<img src="../<?php echo $quicklink['image']; ?>" alt="<?php echo preg_replace('/.+\//','', $quicklink['image']); ?>" />
										<?php elseif (isset($quicklink['icon']) && $quicklink['icon'] != '') : ?>
											<i class="<?php echo $quicklink['icon']; ?>"></i>
										<?php endif; ?>
											<span><strong><?php echo ucfirst($quicklink['name']); ?></strong></span>
									</a>
								</li>
							<?php endforeach;?>
							</ul>
						</div>
				 <?php	
					endif;
				endif;
			endif;
		endforeach; 
		?>
		</div>
	</div> 
<?php endif; endif;?><!-- End Quicklinks -->

<div class="sortable-wrap row-fluid">

	<!-- cpanel top -->
	<div class="sortable-list list-top cpanel-top" data-position="cpanel">
	<?php $modules = JModuleHelper::getModules('cpanel');
	if ($modules) : 
		$shownableModules = array();
		if (JFile::exists($fileConfigs)) {
			$configs = json_decode(@file_get_contents($fileConfigs), true);
			if (isset($configs['modules'])) {
				$modConfigs = $configs['modules'];
			}
		}
		if (isset($modConfigs) && count($modConfigs)) {
			foreach($modules as $module) {
				$key = $module->module.$module->id;
				// check module is showable
				if (isset($modConfigs[$key]) && is_array($modConfigs[$key])) {
					if (in_array('all', $modConfigs[$key])) {
						$shownableModules[] = $module;
					} else {
						if ($dashboard) {
							if (in_array($dashboard, $modConfigs[$key])) {
								$shownableModules[] = $module;
							}
						} else {
							if (count(array_intersect($userGroups, $modConfigs[$key]))) {
								$shownableModules[] = $module;
							}
						}
					}
				}		
			}
			foreach($shownableModules as $key => $module) {
				if ($module->module == 'mod_k2_quickicons' || $module->module == 'mod_k2_stats') {
					if ($dashboard) {
						if (!JAccess::checkGroup($dashboard, 'core.manage', 'com_k2') && $dashboard != 8) {
							unset($shownableModules[$key]);
						}
					} else {
						$count = 0;
						foreach ($userGroups as $gr) {
							if (JAccess::checkGroup($gr, 'core.manage', 'com_k2') || $gr == '8') {
								$count++;
							}
						}
						if ($count == 0) {
							unset($shownableModules[$key]);
						}
					}
				}
			}
		} else {
			$shownableModules = $modules;
		}
		?>
		<?php foreach ($shownableModules as $module): ?>
		<div class="sortable-item cpanel-top-module" data-modid="<?php echo $module->id ?>">
			<?php echo JModuleHelper::renderModule($module, array('style' => 'dragdrop')); ?>
		</div>
		<?php endforeach ?>
	<?php endif; ?>
	</div>

	<!-- cpanel left -->
	<div class="sortable-list list-left span6" data-position="cpanel-left">
	<?php $modules = JModuleHelper::getModules('cpanel-left');
	if ($modules) : 
		$shownableModules = array();
		if (JFile::exists($fileConfigs)) {
			$configs = json_decode(@file_get_contents($fileConfigs), true);
			if (isset($configs['modules'])) {
				$modConfigs = $configs['modules'];
			}
		}
		if (isset($modConfigs) && count($modConfigs)) {
			foreach($modules as $module) {
				$key = $module->module.$module->id;
				// check module is showable
				if (isset($modConfigs[$key]) && is_array($modConfigs[$key])) {
					if (in_array('all', $modConfigs[$key])) {
						$shownableModules[] = $module;
					} else {
						if ($dashboard) {
							if (in_array($dashboard, $modConfigs[$key])) {
								$shownableModules[] = $module;
							}
						} else {
							if (count(array_intersect($userGroups, $modConfigs[$key]))) {
								$shownableModules[] = $module;
							}
						}
					}
				}		
			}
			
			foreach($shownableModules as $key => $module) {
				if ($module->module == 'mod_k2_quickicons' || $module->module == 'mod_k2_stats') {
					if ($dashboard) {
						if (!JAccess::checkGroup($dashboard, 'core.manage', 'com_k2') && $dashboard != '8') {
							unset($shownableModules[$key]);
						}
					} else {
						$count = 0;
						foreach ($userGroups as $gr) {
							if (JAccess::checkGroup($gr, 'core.manage', 'com_k2') || $gr == '8') {
								$count++;
							}
						}
						if ($count == 0) {
							unset($shownableModules[$key]);
						}
					}
				}
			}
		} else {
			$shownableModules = $modules;
		}
		?>
		<?php foreach ($shownableModules as $module): ?>
		<div class="sortable-item cpanel-top-module" data-modid="<?php echo $module->id ?>">
			<?php echo JModuleHelper::renderModule($module, array('style' => 'dragdrop')); ?>
		</div>
		<?php endforeach ?>
	<?php endif; ?>
	</div>

	<!-- cpanel right -->
	<div class="sortable-list list-right span6" data-position="cpanel-right">
	<?php $modules = JModuleHelper::getModules('cpanel-right');
	if ($modules) : 
		$shownableModules = array();
		if (JFile::exists($fileConfigs)) {
			$configs = json_decode(@file_get_contents($fileConfigs), true);
			if (isset($configs['modules'])) {
				$modConfigs = $configs['modules'];
			}
		}
		if (isset($modConfigs) && count($modConfigs)) {
			foreach($modules as $module) {
				$key = $module->module.$module->id;
				// check module is showable
				if (isset($modConfigs[$key]) && is_array($modConfigs[$key])) {
					if (in_array('all', $modConfigs[$key])) {
						$shownableModules[] = $module;
					} else {
						if ($dashboard) {
							if (in_array($dashboard, $modConfigs[$key])) {
								$shownableModules[] = $module;
							}
						} else {
							if (count(array_intersect($userGroups, $modConfigs[$key]))) {
								$shownableModules[] = $module;
							}
						}
					}
				}		
			}
			foreach($shownableModules as $key => $module) {
				if ($module->module == 'mod_k2_quickicons' || $module->module == 'mod_k2_stats') {
					if ($dashboard) {
						if (!JAccess::checkGroup($dashboard, 'core.manage', 'com_k2') && $dashboard != '8') {
							unset($shownableModules[$key]);
						}
					} else {
						$count = 0;
						foreach ($userGroups as $gr) {
							if (JAccess::checkGroup($gr, 'core.manage', 'com_k2') || $gr == '8') {
								$count++;
							}
						}
						if ($count == 0) {
							unset($shownableModules[$key]);
						}
					}
				}
			}
		} else {
			$shownableModules = $modules;
		}
		?>
		<?php foreach ($shownableModules as $module): ?>
		<div class="sortable-item cpanel-top-module" data-modid="<?php echo $module->id ?>">
			<?php echo JModuleHelper::renderModule($module, array('style' => 'dragdrop')); ?>
		</div>
		<?php endforeach ?>
	<?php endif; ?>
	</div>

</div>