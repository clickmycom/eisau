<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.0
 */

defined('_JEXEC') or die;

$helper = require (__DIR__ . '/helper.php');
$helper->init($this);

$app             = JFactory::getApplication();
$input           = $app->input;
$option          = $input->get('option', '');
$cpanel          = ($option === 'com_cpanel');
$user            = JFactory::getUser();
$sitename = htmlspecialchars($app->get('sitename', ''), ENT_QUOTES, 'UTF-8');
$messages = $helper->getMessages();
$components = $helper->getComponents();

// Add css custom for 3rd Extensions
if (count($components)) {
	foreach ($components as $component) {
		$cssFile = JPATH_ADMINISTRATOR . '/templates/' . $this->template . '/css/extras/' . $component->element . '.css';
		if (JFile::exists($cssFile)) {
			$this->addStyleSheet(str_replace(JPATH_ADMINISTRATOR, $this->baseurl,$cssFile));
		}
	}
}

// Get menu position
$menuPosition = $this->params->get('styleMenuPosition');
$styleLogoSmImage = $this->params->get('styleLogoSmImage','');

// Get Quick Links View
$quicklinkView = $this->params->get('styleQuickLinks');


// Get quick link layout
switch ($quicklinkView) {
	case 0:
    $qlLayout = "quicklinks-grid";
    break;
  case 1:
    $qlLayout = "quicklinks-list";
    break;
  case 2:
    $qlLayout = "flayout";
    break;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.js');
// $this->addScript($this->baseurl . '/templates/' . $this->template . '/js/dragula/dragula.js');
//$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/masonry/masonry.pkgd.min.js');
// jquery UI
JHtml::_('jquery.ui', array('core', 'sortable'));

$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.min.css');
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/dragula/dragula.css');
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template' . ($this->direction == 'rtl' ? '-rtl' : '') . '.css');
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/ui.css');

// CUSTOM CSS
if (is_file(__DIR__ . '/css/custom.css')) {
  $this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/custom.css');
}

// temporary assign value, need verify later
$showSubmenu = false;
$this->submenumodules = JModuleHelper::getModules('submenu');

foreach ($this->submenumodules as $submenumodule)
{
	$output = JModuleHelper::renderModule($submenumodule);

	if (strlen($output))
	{
		$showSubmenu = true;
		break;
	}
}

$statusFixed = true;

// Hide/Show the Admin Menu
$userGroups = $user->get('groups');
$isRoot = $user->get('isRoot');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
$file = JPATH_ROOT. '/media/'._ADMIN_TPL.'/configs.json';
$dashboard = $app->input->get('dashboard');

if (JFile::exists($file)) {
	$configs = json_decode(@file_get_contents($file), true);
	if (isset($configs['modules'])) {
		$modConfigs = $configs['modules'];
	}
}

$showAdminMenu = true;
if (isset($modConfigs)) {
	foreach ($modConfigs as $mod => $values) {
		if (preg_match('/^mod_menu/', $mod)) {
			if (!in_array('all', $values)) {
				if ($dashboard) {
					if (!in_array($dashboard,$values)){
						$showAdminMenu = false;
					}
				} else {
					if (count(array_intersect($userGroups, $values)) == 0) {
						$showAdminMenu = false;
					}
				}
			}
		}
		break;
	}
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<jdoc:include type="head" />

	<!-- Custom color style -->
	<?php $helper->addCustomStyle(); ?>

	<!--[if lt IE 9]>
	<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="admin <?php $helper->_('option') ?> view-<?php $helper->_('view') ?> <?php echo $qlLayout; ?> <?php echo $menuPosition?'navbar-horizontal':'navbar-vertical'; ?>">
<!-- Header -->
<header class="header">
	<div class="container-logo">
		<a class="admin-logo <?php echo $styleLogoSmImage ? 'admin-logo-sm' : '' ; ?>" href="index.php" title="<?php $helper->_('siteName') ?>"></a>
	</div>

	<ul class="nav nav-user<?php echo ($this->direction == 'rtl') ? ' pull-left' : ' pull-right'; ?>">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<span class="icon-user"></span>
        <strong><?php echo $user->name; ?></strong>
        <span class="fa fa-angle-down"></span>
      </a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?option=com_admin&amp;task=profile.edit&amp;id=<?php echo $user->id; ?>"><?php echo JText::_('TPL_ADMIN_EDIT_ACCOUNT'); ?></a>
				</li>
				<li class="divider"></li>
				<li class="">
					<a href="<?php echo JRoute::_('index.php?option=com_login&task=logout&' . JSession::getFormToken() . '=1'); ?>"><?php echo JText::_('TPL_ADMIN_LOGOUT'); ?></a>
				</li>
			</ul>
		</li>
	</ul>

	<?php if ($user->authorise('core.manage', 'com_messages')) : ?>
		<a class="pm-message pull-right" href="index.php?option=com_messages&view=messages" title="<?php echo JText::sprintf('TPL_JA_ADMIN_PRIVATE_MESSAGE', $sitename); ?>">
			<i class="fa fa-envelope"></i>
			<span class="pm-number <?php if($messages == 0): echo 'all-readed'; endif; ?>"><?php echo $messages; ?></span>
		</a>
	<?php endif; ?>

	<!-- View front-end -->
	<a class="site-preview pull-right" href="<?php echo JUri::root(); ?>" title="<?php echo JText::sprintf('TPL_JA_ADMIN_PREVIEW', $sitename); ?>" target="_blank">
		<i class="fa fa-desktop"></i>
	</a>
	<!-- // View front-end -->

	<div class="search">
		<div class="search-box">
			<button class="btn"><i class="fa fa-search"></i></button>
			<input type="search" autocomplete="off" placeholder="Search ..." size="16" class="form-control search-query" maxlength="200" id="search-searchword" name="searchword">
		</div>
		<div class="search-result" id="search-result">
		</div>
	</div>

	<?php if ($this->countModules('comments')) : ?>
  <a href="#" class="comments pull-right"><span class="fa fa-comments"></span></a>
  <?php endif; ?>
  
	<div class="container-title">
		<jdoc:include type="modules" name="title" />
	</div>
</header>

<div class="admin-container-fluid">

	<!-- Left Navigation -->
	<?php if ($showAdminMenu) : ?>
		<nav class="sidebar clearfix">
			<div class="sidebar-title">
				<span><?php echo JText::_('TPL_ADMIN_ADMIN_MENU'); ?></span>
				<i class="fa fa-bars"></i>
			</div>
		
			<jdoc:include type="modules" name="menu" style="none" />
			<!--/.nav-collapse -->
		</nav>
	<?php endif; ?>

	<!-- Main Content -->
	<div class="main-content <?php if (!$showAdminMenu) { echo "layout-full"; } ?>">
		<?php if (!$cpanel) : ?>
			<!-- Subheader -->
			<div class="subhead">
        <div class="container-fluid">
          <div class="row-fluid">
            <div class="span12">
              <jdoc:include type="modules" name="toolbar" style="no" />
            </div>
          </div>
        </div>
      </div>
		<?php else : ?>
			<div style="margin-bottom: 40px"></div>
		<?php endif; ?>
		<!-- container-fluid -->
		<div class="container-fluid container-main">
			<section id="content">
				<!-- Begin Content -->
				<jdoc:include type="modules" name="top" style="xhtml" />

				<div class="row-fluid">
					<div class="span12">
						<?php if ($showSubmenu) : ?>
						<div class="jsubmenu">
							<jdoc:include type="modules" name="submenu" style="none" />
						</div>
						<?php endif; ?>
						<div class="jcomponent">
							<jdoc:include type="message" />
							<jdoc:include type="component" />
						</div>
					</div>
				</div>

					<?php if ($this->countModules('bottom')) : ?>
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					<?php endif; ?>
					<!-- End Content -->
			</section>

			<?php if (!$this->countModules('status') || (!$statusFixed && $this->countModules('status'))) : ?>
				<footer class="footer">
					<p align="center">
						<jdoc:include type="modules" name="footer" style="none" />
						&copy; <?php $helper->_('siteName') ?> <?php echo date('Y'); ?></p>
				</footer>
			<?php endif; ?>
		</div>

		<?php if ($this->countModules('status')) : ?>
			<!-- Begin Status Module -->
			<div id="status" class="navbar navbar-fixed-bottom hidden-phone">
				<div class="btn-toolbar">
					<div class="btn-group pull-right">
						<jdoc:include type="modules" name="footer" style="none" />
						&copy; <?php echo date('Y'); ?> <?php $helper->_('siteName') ?>
					</div>
					<jdoc:include type="modules" name="status" style="none" />
				</div>
			</div>
			<!-- End Status Module -->
		<?php endif; ?>
	</div>
</div>
<jdoc:include type="modules" name="debug" style="none" />
<script>
	jQuery(document).ready(function($) {
		jQuery("a[class^='dropdown-toggle menu-icon-16-djimageslider']").text('Slider');
		if ($('#j-sidebar-container').length > 0) {
			$('body').addClass('has-j-sidebar');
		}
		
		//Highlight the dashboard menu when it's actived
		if ($(location).attr('href') == '<?php echo JUri::root() . 'administrator/index.php'; ?>' || $(location).attr('href') == '<?php echo JUri::root() . 'administrator/';?>') {
			$($('ul#menu').find('li')[0]).addClass('active');
		} else {
			$($('ul#menu').find('li')[0]).removeClass('active');
		}
		
		// Remove hover event on the menu
		$('li.dropdown').on('click', function(){
			$('li.dropdown').on('hover', function(){
				if ($('body').hasClass('navbar-vertical')){
					$('#menu').removeClass('nav-hover');
				}
			});
		});
	});
</script>
</body>
</html>