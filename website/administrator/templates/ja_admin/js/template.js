/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.0
 */

(function($) {
    $(document).ready(function() {
        $('*[rel=tooltip]').tooltip();

        // Turn radios into btn-group
        $.fn.extend({
            radioToBtnGroup: function () {
                var $radiogroups = this;
                $radiogroups.find('label').addClass('btn').off('click').on('click', function () {
                    var label = $(this);
                    var input = $('#' + label.attr('for'));

                    if (!input.prop('checked')) {
                        label.closest('.btn-group').find('label').removeClass('active btn-success btn-danger btn-primary');
                        if (input.val() == '') {
                            label.addClass('active btn-primary');
                        } else if (input.val() == 0) {
                            label.addClass('active btn-danger');
                        } else {
                            label.addClass('active btn-success');
                        }
                        input.prop('checked', true);
                        input.trigger('change');
                    }
                });

                $radiogroups.find('input[checked=checked]').each(function() {
                    if ($(this).val() == '') {
                        $('label[for=' + $(this).attr('id') + ']').addClass('active btn-primary');
                    } else if ($(this).val() == 0) {
                        $('label[for=' + $(this).attr('id') + ']').addClass('active btn-danger');
                    } else {
                        $('label[for=' + $(this).attr('id') + ']').addClass('active btn-success');
                    }
                });

                return $radiogroups;
            }
        });
        
        $('.radio.btn-group').radioToBtnGroup();


        // add color classes to chosen field based on value
        $('select[class^="chzn-color"], select[class*=" chzn-color"]').on('liszt:ready', function() {
            var select = $(this);
            var cls = this.className.replace(/^.(chzn-color[a-z0-9-_]*)$.*/, '\1');
            var container = select.next('.chzn-container').find('.chzn-single');
            container.addClass(cls).attr('rel', 'value_' + select.val());
            select.on('change click', function() {
                container.attr('rel', 'value_' + select.val());
            });

        });

        /**
         * USED IN: All views with toolbar and sticky bar enabled
         */
        var navTop;
        var isFixed = false;

        if (window.isisStickyToolbar == 1) {
            processScrollInit();
            processScroll();

            $(window).on('resize', processScrollInit);
            $(window).on('scroll', processScroll);
        }

        function processScrollInit() {
            if ($('.subhead').length) {
                navTop = $('.subhead').length && $('.subhead').offset().top - window.isisOffsetTop;

                // Fix the container top
                $(".container-main").css("top", $('.subhead').height() + $('nav.navbar').height());

                // Only apply the scrollspy when the toolbar is not collapsed
                if (document.body.clientWidth > 480) {
                    $('.subhead-collapse').height($('.subhead').height());
                    $('.subhead').scrollspy({
                        offset: {
                            top: $('.subhead').offset().top - $('nav.navbar').height()
                        }
                    });
                }
            }
        }

        function processScroll() {
            if ($('.subhead').length) {
                var scrollTop = $(window).scrollTop();
                if (scrollTop >= navTop && !isFixed) {
                    isFixed = true;
                    $('.subhead').addClass('subhead-fixed');

                    // Fix the container top
                    $(".container-main").css("top", $('.subhead').height() + $('nav.navbar').height());
                } else if (scrollTop <= navTop && isFixed) {
                    isFixed = false;
                    $('.subhead').removeClass('subhead-fixed');
                }
            }
        }

        /**
         * USED IN: All list views to hide/show the sidebar
         */
        window.toggleSidebar = function(force) {
            var context = 'jsidebar';

            var $sidebar = $('#j-sidebar-container'),
                $main = $('#j-main-container'),
                $message = $('#system-message-container'),
                $debug = $('#system-debug'),
                $toggleSidebarIcon = $('#j-toggle-sidebar-icon'),
                $toggleButtonWrapper = $('#j-toggle-button-wrapper'),
                $toggleButton = $('#j-toggle-sidebar-button'),
                $sidebarToggle = $('#j-toggle-sidebar');

            var openIcon = 'icon-arrow-left-2',
                closedIcon = 'icon-arrow-right-2';

            var $visible = $sidebarToggle.is(":visible");

            if (jQuery(document.querySelector("html")).attr('dir') == 'rtl') {
                openIcon = 'icon-arrow-right-2';
                closedIcon = 'icon-arrow-left-2';
            }

            var isComponent = $('body').hasClass('component');

            $sidebar.removeClass('span2').addClass('j-sidebar-container');
            $message.addClass('j-toggle-main');
            $main.addClass('j-toggle-main');
            if (!isComponent) {
                $debug.addClass('j-toggle-main');
            }

            var mainHeight = $main.outerHeight() + 30,
                sidebarHeight = $sidebar.outerHeight(),
                bodyWidth = $('body').outerWidth(),
                sidebarWidth = $sidebar.outerWidth(),
                contentWidth = $('#content').outerWidth(),
                contentWidthRelative = contentWidth / bodyWidth * 100,
                mainWidthRelative = (contentWidth - sidebarWidth) / bodyWidth * 100;

            if (force) {
                // Load the value from localStorage
                if (typeof(Storage) !== "undefined") {
                    $visible = localStorage.getItem(context);
                }

                // Need to convert the value to a boolean
                $visible = ($visible == 'true');
            } else {
                $message.addClass('j-toggle-transition');
                $sidebar.addClass('j-toggle-transition');
                $toggleButtonWrapper.addClass('j-toggle-transition');
                $main.addClass('j-toggle-transition');
                if (!isComponent) {
                    $debug.addClass('j-toggle-transition');
                }
            }

            if ($visible) {
                $sidebarToggle.hide();
                $sidebar.removeClass('j-sidebar-visible').addClass('j-sidebar-hidden');
                $toggleButtonWrapper.removeClass('j-toggle-visible').addClass('j-toggle-hidden');
                $toggleSidebarIcon.removeClass('j-toggle-visible').addClass('j-toggle-hidden');
                $message.removeClass('span10').addClass('span12');
                $main.removeClass('span10').addClass('span12 expanded');
                $toggleSidebarIcon.removeClass(openIcon).addClass(closedIcon);
                $toggleButton.attr('data-original-title', Joomla.JText._('JTOGGLE_SHOW_SIDEBAR'));
                $sidebar.attr('aria-hidden', true);
                $sidebar.find('a').attr('tabindex', '-1');
                $sidebar.find(':input').attr('tabindex', '-1');

                if (!isComponent) {
                    $debug.css('width', contentWidthRelative + '%');
                }

                if (typeof(Storage) !== "undefined") {
                    // Set the last selection in localStorage
                    localStorage.setItem(context, true);
                }
            } else {
                $sidebarToggle.show();
                $sidebar.removeClass('j-sidebar-hidden').addClass('j-sidebar-visible');
                $toggleButtonWrapper.removeClass('j-toggle-hidden').addClass('j-toggle-visible');
                $toggleSidebarIcon.removeClass('j-toggle-hidden').addClass('j-toggle-visible');
                $message.removeClass('span12').addClass('span10');
                $main.removeClass('span12 expanded').addClass('span10');
                $toggleSidebarIcon.removeClass(closedIcon).addClass(openIcon);
                $toggleButton.attr('data-original-title', Joomla.JText._('JTOGGLE_HIDE_SIDEBAR'));
                $sidebar.removeAttr('aria-hidden');
                $sidebar.find('a').removeAttr('tabindex');
                $sidebar.find(':input').removeAttr('tabindex');

                if (!isComponent && bodyWidth > 768 && mainHeight < sidebarHeight) {
                    $debug.css('width', mainWidthRelative + '%');
                } else if (!isComponent) {
                    $debug.css('width', contentWidthRelative + '%');
                }

                if (typeof(Storage) !== "undefined") {
                    // Set the last selection in localStorage
                    localStorage.setItem(context, false);
                }
            }
        }

        // Add scroll to announcements
        if ($(".nav-announcements .open > .dropdown-menu").length) {
            $(".nav-announcements .open > .dropdown-menu").mCustomScrollbar();
        }

        // Add class to Sidebar
        $(".admin-container-fluid > .sidebar .sidebar-title .fa").click(function() {
            $('.admin-container-fluid > .sidebar').toggleClass('sidebar-close');

            if ($('.sidebar-close').length) {
                $('.sidebar #menu > li.dropdown').click(function() {
                    $('.admin-container-fluid > .sidebar').removeClass('sidebar-close');
                });
            }
        });

        // Fix the main content top when has subhead
        if ($('.subhead').length) {
            $(".main-content").css("padding-top", $('.sidebar-nav').height() + 40);
        }


        /******************
         * Draggable & Sortable
         ******************/
        $('.sortable-wrap').each (function () {
            var $list = $(this).find('.sortable-list');
            $list.sortable({
                connectWith: $list,
                placeHolder: 'placeholder'
            })
        });


        /******************
         * Update module position
         ******************/
        (function() {
            var $module = null;
            $( ".sortable-wrap .sortable-list" ).on( "sortupdate", function( event, ui ) {
                $module = ui.item;
            } );

            $( ".sortable-wrap .sortable-list" ).on( "sortstop", function( event, ui ) {
                if ($module != null) {
                    // submit the change to server
                    var modid = $module.data('modid'),
                        pos = $module.parent().data('position'),
                        order = $module.index();                        
                    $module = null;

                    // submit change
                    if (modid && pos) {
                        $.post('index.php?athajax=modules', {
                            action: 'reposition',
                            id: modid,
                            position: pos,
                            order: order
                        });
                    }
                }
            } );

        }) ();



        /******************
         * Site search
         ******************/
        (function(){ 
            var $search = $('#search-searchword'),
                $result = $('#search-result'),
                searchType = '';

            $search.keyup(function() {
                clearTimeout($search.data('timer'));
                var wait = setTimeout(search, 500);
                $(this).data('timer', wait);
            });

            function search() {
                var data = {
                    athajax: 'search',
                    q: "" + $search.val() + ""
                };
                if (searchType) data.type = searchType;
                $.get("index.php", data, function(data) {
                    if (data.length > 0) {
                        $result.html(data).show();
                        // bind event for More
                        $result.find('.btn-search-more').off('click').on('click', function() {
                            // toggle searchType
                            if (searchType) {
                                searchType = null;
                            } else {
                                searchType = $(this).data('type');
                            }
                            // call search to update result
                            search();
                        });
                    }
                });
            }

            // blur
            $('body').on('click', function(e){
                if (!$(e.target).closest('.search').length) {
                    $result.hide();
                }
            })

            // Focus
            $("#search-searchword").focus(function(){
               $('.header .search').addClass("search-focus");
            }).blur(function(){
                $('.header .search').removeClass("search-focus");
            })
        })();


        /******************
         * ACTIVE MAIN MENU
         ******************/
        (function() {
            var UrlParam = function(name){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results==null){
                   return null;
                }
                else{
                   return results[1] || 0;
                }
            };

            var option = UrlParam('option'),
                view = UrlParam('view'),
                regOption = new RegExp('option=' + option),
                regView = new RegExp('view=' + view),
                optionItems = [],
                viewItems = [];

            // detect active menu item
            $('#menu a').each (function (){
                var $item = $(this),
                    url = $item.attr('href');
                if (url && url.match (regOption)) {
                    optionItems.push($item);
                    if (url.match (regView)) {
                        viewItems.push($item);
                    }
                }
            });
            var $active = viewItems.length ? viewItems.pop() : optionItems.pop();
            if ($active) {
                $active.parent().addClass('active');
                // add active for parent
                $active.parents('li').addClass('active');
            }
        })();


        /*************************
         * FIX VIRTICAL MENU HEIGHT
         *************************/
        (function(){            
            var $sidebar = $('.sidebar').first();
            if (!$sidebar.length || $sidebar.css('position') != 'fixed') return;

            var statusHeight = $('#status').outerHeight(),                
                sidebarHeight = $sidebar.height(),
                sidebarTop = parseInt($sidebar.css('top')); 
            // Calculate menu height
            var NavHeight = function () {
                return $sidebar.prop('scrollHeight');
            };

            var UpdateSidebarTop = function () {                
                var sHeight = NavHeight();

                if (sHeight > sidebarHeight) {
                    var delta = sHeight - sidebarHeight,
                        st = $(this).scrollTop();
                    if (delta > st) delta = st;
                    $sidebar.css('top', sidebarTop - delta);                        
                } else {
                    $sidebar.removeAttr('style');
                }
            };

            var UpdateContentHeight = function (skipUpdateTop) {
                setTimeout(function() {
                    $('.main-content').css('min-height', NavHeight() + statusHeight);
                    if (!skipUpdateTop) UpdateSidebarTop();
                }, 50);
            };

            $('li.dropdown').on('click', function () {
                UpdateContentHeight();
            });

            $(window).on('click', function () {
                UpdateContentHeight();
            });

            $('li.dropdown-submenu').on('hover', function () {
                UpdateContentHeight(true);
            });

            var lastScrollTop = 0;
            $(window).scroll(function(event){
                UpdateSidebarTop();
            });
        })();

        /*************************
         * FIX MENU RESPONSIVE
         *************************/
        (function(){
            var $window = $(window);

            bindNavMenu();

            // Bind event listener
            $window.resize(function(){
                bindNavMenu();
            });

            function bindNavMenu() {
                if ($window.width() < 1200) {
                    $('.navbar-vertical .admin-container-fluid > .sidebar, .navbar-horizontal .admin-container-fluid > .sidebar').addClass('sidebar-close');

                    if ($('.sidebar-close').length) {
                        $('.sidebar #menu > li.dropdown').click(function() {
                            $('.admin-container-fluid > .sidebar').removeClass('sidebar-close');
                        });
                    }
                    
                } else {
                    $('.navbar-vertical .admin-container-fluid > .sidebar, .navbar-horizontal .admin-container-fluid > .sidebar').removeClass('sidebar-close');
                }
            }
        })();
    });
})(jQuery);