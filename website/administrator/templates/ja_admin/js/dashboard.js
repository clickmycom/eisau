jQuery(document).ready(function($){
	// manage button click
	$('.btn-dashboard-manage').on('click', function () {
		// get ajax content, then call popup modal
		var $this = $(this);
		if (!$this.data('form-loaded')) {
			$.get('index.php?athajax=dashboard', function (data) {
				$('#dashboard-manage .modal-body').html(data);
				$this.data('form-loaded', true);
				// show modal
				$('#dashboard-manage').modal();
				// init radio
				$('#dashboard-manage .radio.btn-group').radioToBtnGroup();
			});
		} else {
			$('#dashboard-manage').modal();
		}
	});
	
	// Save configurations not close the modal.
	$('.btn-apply-profile').on('click', function() {
		saveConfigs('apply');
	});
	
	// Save changed configurations and close the modal.
	$('.btn-save-profile').on('click', function() {
		saveConfigs('save');
	});
	
	var saveConfigs = function (action) {
		//Get params to save
		var params = {};
		params['modules'] = {};
		params['quicklinks'] = {};
		
		// Module configs
		$('select').each(function() {
			var name = $(this).attr('name');
			if (name && name.match(/^mod_/)) {
				params['modules'][name] = $(this).val();
			}
		});
		
		//Quicklinks configs
		var li = $('#group-en').find('li.group-quicklink');
		li.each(function() {
			var $this = $(this);
			var group = $.trim($this.find('h4').text());
			params['quicklinks'][group] = {};
			params['quicklinks'][group]['id'] = $this.attr('id');
			params['quicklinks'][group]['acl'] = $this.find('#editGroup ul li select.usergr-list').val();
			params['quicklinks'][group]['items'] = [];
			$this.find('ul li.link-item').each(function(key, value) {
				var item = {};
				item['acl'] = $(value).find('ul li .gr-access-settings .usergr-list').val();
				if ($(value).attr('id')) {
					item['timestamp'] = $(value).attr('id');
				}
				params['quicklinks'][group]['items'].push(item);
			});
		});
		$.each(params['quicklinks'], function(key, value) {
			if ($.map(value, function(){ return 1;}).length == 0) {
				params['quicklinks'][key] = null;
			}
		});
		
		//Menu items configs
		params['menus'] = {};
		var selector = $('#dashboard-menus').find('.group-title .mn-show-on').find('select'); 
		
		selector.each(function() {
			var menuGroup = $(this).attr('id');
			if (menuGroup.match(/\-[0-9].*/)) {
				var name = menuGroup.replace(/\-[0-9].*/,'');
				params['menus'][name] = {};
				var groupID = menuGroup.match(/[0-9].*/);
				if (groupID) {
					params['menus'][name]['id'] = groupID[0];
				}
				params['menus'][name]['acl'] = $(this).val();
				params['menus'][name]['items'] = [];
				var mnli = $($(this).parents('.group-title')[0]).next().find('li.ja-menu-item');
				mnli.each(function() {
					var item = {};
					item['id'] = $(this).attr('id');
					item['acl'] = $(this).find('select').val();
					params['menus'][name]['items'].push(item);
				});
			}
		});
		
		// Save Configs and display message
		$.ajax({
			type: 'POST',
			data: params,
			url: 'index.php?athajax=dashboard&action=saveconfigs',
			success: function(data) {
				if (action == 'apply') {
					$('#message').html('');
					setTimeout(function() {
						$('#message').html(data);
					},1000);
					
					$('#newGroup').each(function() {
						if ($(this).hasClass('ui-placeholder')) {
							$(this).removeClass('ui-placeholder');
						}
					});
					// Scroll to the top after save
					$('#dashboard-manage .modal-body').animate({scrollTop:0});
					
					// Close Btn
					$('.btn-ds-close').on('click', function(){
						var url = window.location.href;
						window.location.href = url;
					});
				} else if (action == 'save') {
					var url = window.location.href;
					$('#dashboard-manage').modal('hide');
					window.location.href = url;
				}
			}
		});
	}
	
	//Reloade page when select dashboard
	$('#dashboard').change(function() {
		window.location.href='index.php?dashboard='+$(this).find('option:selected').val();
	});
});