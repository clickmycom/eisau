<?php
$db = JFactory::getDBO();
$post = JFactory::getApplication()->input->post;
$modid = $post->getInt('id');
$order = $post->getInt('order');
$position = $post->get('position');

// load current module
$query = 'SELECT * FROM #__modules WHERE id=' . $db->quote($modid);
$db->setQuery($query);
$module = $db->loadObject();

if (!$module) return;

if ($position != $module->position) {
	// reposition
	$query = 'UPDATE #__modules SET position=' . $db->quote($position) . ' WHERE id=' . $db->quote($modid);
	$db->setQuery($query);
	$db->execute();
}

// update order
// Get all other modules in the position
$query = 'SELECT * FROM #__modules WHERE position=' . $db->quote($position) . ' AND id != ' . $db->quote($modid) . ' ORDER BY ordering';
$db->setQuery($query);
$modules = $db->loadObjectList();
// insert the current module
array_splice($modules, $order, 0, array($module));
foreach ($modules as $ordering => $module) {
	$query = 'UPDATE #__modules SET ordering=' . ($ordering+1) . ' WHERE id=' . $db->quote($module->id);
	$db->setQuery($query);
	$db->execute();
}
