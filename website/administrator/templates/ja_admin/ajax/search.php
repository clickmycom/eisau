<?php
$app = JFactory::getApplication();
$input = $app->input;
$db = JFactory::getDBO();
$keyword = trim($input->getString('q'));
$searchType = $input->getString('type');
$limit = $searchType ? 20 : 5;
$query_limit = ' LIMIT 0,' . $limit;
$list = array();


// Search in plugins
///////////////////////////
if (empty($searchType) || $searchType == 'plugin') {
	$query = 'SELECT * FROM #__extensions WHERE type=' . $db->quote('plugin');
	$db->setQuery($query);
	$plugins = $db->loadObjectList();
	// translate
	$lang = JFactory::getLanguage();

	foreach ($plugins as &$item)
	{
		$source = JPATH_PLUGINS . '/' . $item->folder . '/' . $item->element;
		$extension = 'plg_' . $item->folder . '_' . $item->element;
		$lang->load($extension . '.sys', JPATH_ADMINISTRATOR, null, false, true)
			|| $lang->load($extension . '.sys', $source, null, false, true);
		$item->name = JText::_($item->name);
	}

	// filter plugins with search phase
	$escapedSearchString = str_replace(' ', '.*', preg_quote(trim($keyword), '/'));
	foreach ($plugins as $i => $item)
	{
		if (!preg_match("/$escapedSearchString/i", $item->name))
		{
			unset($plugins[$i]);
		}
	}

	// put result into list
	if (!empty($plugins)) {
		$i = 0;
		$items = array();
		foreach ($plugins as $item) {
			if (++$i > $limit) break;
			$item->title = $item->name;
			$item->id = $item->extension_id;
			$item->link = 'index.php?option=com_plugins&task=plugin.edit&extension_id=' . $item->id;
			if (!isset($items[$item->folder])) $items[$item->folder] = array();
			$items[$item->folder][] = $item;
		}
		$list['plugin'] = $items;
	}
}

// Search in Contents
///////////////////////////////
if (empty($searchType) || $searchType == 'content') {
	$query = 'SELECT a.*, c.title AS category FROM #__content a INNER JOIN #__categories c ON a.catid=c.id WHERE a.title LIKE ' . $db->quote('%' . $keyword . '%') . $query_limit;
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	// put result into list
	if (!empty($rows)) {
		$i = 0;
		$items = array();
		foreach ($rows as $item) {
			$item->link = 'index.php?option=com_content&task=article.edit&id=' . $item->id;
			if (!isset($items[$item->category])) $items[$item->category] = array();
			$items[$item->category][] = $item;
		}
		$list['content'] = $items;
	}
}

// Search in Categories
///////////////////////////////
if (empty($searchType) || $searchType == 'category') {
	$query = 'SELECT * FROM #__categories WHERE title LIKE ' . $db->quote('%' . $keyword . '%') . $query_limit;
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	// put result into list
	if (!empty($rows)) {
		$i = 0;
		$items = array();
		foreach ($rows as $item) {
			$item->link = 'index.php?option=com_categories&task=category.edit&id=' . $item->id . '&extension=' . $item->extension;
			if (!isset($items[$item->extension])) $items[$item->extension] = array();
			$items[$item->extension][] = $item;
		}
		$list['category'] = $items;
	}
}

// Search in modules
///////////////////////////////
if (empty($searchType) || $searchType == 'module') {
	$query = 'SELECT * FROM #__modules WHERE title LIKE ' . $db->quote('%' . $keyword . '%') . $query_limit;
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	// put result into list
	if (!empty($rows)) {
		$i = 0;
		$items = array();
		foreach ($rows as $item) {
			$item->link = 'index.php?option=com_modules&task=module.edit&id=' . $item->id;
			if (!isset($items[$item->position])) $items[$item->position] = array();
			$items[$item->position][] = $item;
		}
		$list['module'] = $items;
	}
}

// Search in menus
///////////////////////////////
if (empty($searchType) || $searchType == 'menu') {
	$query = 'SELECT * FROM #__menu WHERE title LIKE ' . $db->quote('%' . $keyword . '%') . $query_limit;
	$db->setQuery($query);
	$rows = $db->loadObjectList();
	// put result into list
	if (!empty($rows)) {
		$i = 0;
		$items = array();
		foreach ($rows as $item) {
			$item->link = 'index.php?option=com_menus&task=item.edit&id=' . $item->id;
			if (!isset($items[$item->menutype])) $items[$item->menutype] = array();
			$items[$item->menutype][] = $item;
		}
		$list['menu'] = $items;
	}
}

echo JLayoutHelper::render ('this_template.ajax.search', array('list' => $list, 'keyword' => $keyword, 'searchType' => $searchType));
