<?php
defined('_JEXEC') or die;
use Joomla\Registry\Registry;

if (!class_exists('BKTMPHelper')) {
	class BKTMPHelper {
		private $params = null;
		private $template = null;
		private $style_id = 0;
		private $data = array();
		private $isHome = false;
		private $defaultParams = null;

		public function __construct () {
		}

		public function loadStyle ($id) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true)
						->select ('*')
						->from('#__template_styles')
						->where('id=' . $id);
			$db->setQuery($query);
			$tpl = $db->loadObject();
			if ($tpl) {
				$this->params = new Registry;
				$this->params->loadString ($tpl->params);
				$this->style_id = $tpl->id;
				$this->template = $tpl->template;
				$this->isHome = $tpl->home;

			// parse tplhelper json
			$this->data['tplhelper'] = json_decode($this->getParam('tplhelper'), true);

			// base url
			$this->params->set ('baseUrl', JUri::base(true) . '/');
			$this->params->set ('rootUrl', JUri::root(true) . '/');
				
			}
		}

		public function loadStyleId () {
			$style_id = JFactory::getUser()->getParam('admin_style');
			if (!$style_id) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(true)
							->select ('id')
							->from('#__template_styles')
							->where('client_id=1')
							->where('template=' . $db->quote($this->template));
				$db->setQuery($query);
				$style_id = $db->loadResult();
			}
			return $style_id;
		}

		public function loadDefaultStyleParams() {
			if ($this->isHome) {
				$this->defaultParams = $this->params;
				return;
			}

			$db = JFactory::getDBO();
			$query = $db->getQuery(true)
						->select ('*')
						->from('#__template_styles')
						->where('home=1')
						->where('client_id=0');
			$db->setQuery($query);
			$tpl = $db->loadObject();
			if ($tpl) {
				$this->defaultParams = new Registry;
				$this->defaultParams->loadString ($tpl->params);
			}
		}

		public function init ($template = null, $styleid = 0) {
			if ($template !== null) {
				$this->params = $template->params;
				$this->template = $template->template;
			}

			// load style id
			$this->style_id = $styleid ? $styleid : $this->loadStyleId();

			$app = JFactory::getApplication();
			$doc = JFactory::getDocument();
			$config = JFactory::getConfig();
			
			$this->data['option']   = $app->input->getCmd('option', '');
			$this->data['view']     = $app->input->getCmd('view', '');			
			$this->data['siteName'] = $config->get('sitename');

			// base url
			$this->params->set ('baseUrl', JUri::base(true) . '/');
			$this->params->set ('rootUrl', JUri::root(true) . '/');

			// call after save to generate custom css
			//$this->afterSave();
		}

		public function get ($name, $default = null) {		
			return isset ($this->data[$name]) ? $this->data[$name] : $this->getParam($name, $default);
		}

		public function getParam ($name, $default = null) {
			$val = $this->params->get ($name, $default);
			if ($val == 'use-default' && !$this->isHome) {
				if (!$this->defaultParams) $this->loadDefaultStyleParams();
				$val = $this->defaultParams->get ($name, $default);
			}
			if ($val == 'use-default') $val = $default;
			return $val;
		}

		public function setParam ($name, $value) {
			$this->params->set ($name, $value);
		}

		public function _ ($name, $glue = ' ', $separator = '') {
			$val = $this->get($name);
			if (is_array($val)) $val = implode($glue, $val);
			echo $val ? $separator . $val : '';
		}

		public function is ($name) {
			return (bool) $this->get($name);
		}

		public function has ($name) {
			return $this->is($name);
		}

		public function hasContainer ($section) {
			return ($this->getParam('layoutContainer_' . $section) == 1 ||
				($this->getParam('layoutContainer_' . $section, '') === '' && $this->getParam('layoutContainer', 1) == 1));
		}

		public function _container_open ($section) {
			if ($this->hasContainer($section)) {
				echo '<div class="container">';
			} else {
				echo '<div class="no-container">';
			}

		}

		public function _container_close ($section) {
			//if ($this->hasContainer($section)) {
				echo '</div>';
			//}
		}

		public function _bg ($section) {
			$background = $this->getParam('layoutBackground_' . $section) ? ' style="background-image:url(' . $this->getParam('layoutBackground_' . $section) . ');"' : '';
			echo $background;
		}

		public function _row_class ($section) {
			if ($this->hasContainer($section)) {
				echo 'row';
			} else {
				echo 'row-fluid';
			}
		}

		public function getStyleId () {
			return $this->style_id;
		}

		public function groupNeedUpdate ($group) {
			$needUpdate = false;
			// check if change in params, then save
			$tplhelper = $this->get('tplhelper');
			if (is_string($tplhelper)) $tplhelper = json_decode($tplhelper, true);
			if (is_array($tplhelper) && isset($tplhelper[$group]) && $tplhelper[$group] == 1) $needUpdate = true;

			return $needUpdate;
		}

		/**
		 * Save settings into file to allow rollback
		 */
		public function saveRevision ($prefix, $group) {			
			$path = JPATH_ROOT . $this->getMediaLocation() . '/revisions/' . $group . '/';
			$needUpdate = $this->groupNeedUpdate($group);
			if (!is_dir($path)) {
				// create folder
				jimport('joomla.filesystem.folder');
				if (!JFolder::create ($path)) return false;
				$needUpdate = true;
			}
			// check if change in params, then save
			if (!$needUpdate) return false;

			// export all var with $prefix and store in a file
			$lines = array();
			foreach ($this->params->toArray() as $name => $value) {
				// $value = json_encode($value);
				if (is_array($value)) $value = implode(',', $value);
				if (strpos ($name, $prefix) === 0) {
					$lines[] = $name . ': ' . preg_replace('/[\r\n]+/', '\\n', $value);
				}
			}

			// save to file
			$filename = $this->getStyleId();
			$filename .= '-' . JFactory::getDate()->format('Y.m.d-h.i.s') . '.nvp';
			jimport('joomla.filesystem.file');
			file_put_contents($path . $filename, implode("\n", $lines));

			return true;
		}		

		public function getCustomCssPath ($url = false) {
			$path = $this->getMediaLocation() . '/css/custom-styles/' . $this->getStyleId() . '.css';
			$custom_css_file = JPATH_ROOT . $path;
			$custom_css_url = JUri::root(true) . $path . (is_file($custom_css_file) ? '?v=' . filemtime($custom_css_file) : '');	
			return $url ? $custom_css_url : $custom_css_file;
		}

		public function getMediaLocation () {
			return '/media/' . $this->template;
		}

		public function saveCustomStyle () {
			// check custom css file existed or need update
			$custom_css_file = $this->getCustomCssPath();
			if (!is_file($custom_css_file) || $this->groupNeedUpdate('advanced')) {
				$custom_css = $this->buildCustomStyle();
				// write to file
				jimport('joomla.filesystem.file');
				return JFile::write ($custom_css_file, $custom_css);
			}
		}

		public function clearTplStatus () {
			return;
			// update the tplhelper value to prevent change file later
			$needUpdate = false;
			$tplhelper = $this->get('tplhelper');
			if (is_string($tplhelper)) $tplhelper = json_decode($tplhelper, true);
			if (!is_array($tplhelper)) return;
			foreach ($tplhelper as $group => $val) {
				if ($val) $needUpdate = true;
			}
			if (!$needUpdate) return ;

			$tplParams = $this->params;
			$tplParams->set('tplhelper', '');

			$db = JFactory::getDBO();
			$query = $db->getQuery(true)
						->update('#__template_styles')
						->set('params = ' . $db->quote(json_encode($tplParams->toArray())));
			if (JFactory::getApplication()->isAdmin()) {
				$query->where('template=' . $db->quote($this->template));
			} else {
				$query->where('id=' . $this->getStyleId());
			}

			$db->setQuery($query);
			$db->execute();
		}

		/**
		 * Compile settings into custom style css 
		 */
		public function buildCustomStyle () {
			static $custom_style_css = null;
			if ($custom_style_css !== null) return $custom_style_css;

			$custom_css_tpl_file = __DIR__ .'/css/custom-styles.tpl.css';
			if (!is_file($custom_css_tpl_file)) return; // no custom style file

			$custom_style_css = file_get_contents ($custom_css_tpl_file);
			// replace condition pattern
			$arr = preg_split('/\/\* \?([^\s]+) \*\//m', $custom_style_css, -1, PREG_SPLIT_DELIM_CAPTURE);
			$i = 0;
			$chucks = array();
			$chucks[] = $arr[0];
			while ($i < count($arr)-2) {
				// first chuck, no precess
			  	$checkVar = $arr[++$i];
			  	//$tmp = explode(':', $checkVar, 2);
			  	$tmp = preg_split('/(!?)(=|~)/', $checkVar, 2, PREG_SPLIT_DELIM_CAPTURE);
					  	
			  	$checkVar = $tmp[0];
			  	$checkValue = $checkFalse = $checkMatch = null;			  	
			  	if (count($tmp)) {
			  		$checkFalse = $tmp[1] == '!' ? true : false;
			  		$checkMatch = $tmp[2] == '~' ? true : false;
			  		$checkValue = $tmp[3];
			  	}
			  	// $checkValue = count($tmp) > 1 ? $tmp[1] : null;
			    $checkArr = preg_split('/\/\* \/' . $checkVar . ' \*\//', $arr[++$i]);
			    $checkStr = count($checkArr) > 1 ? $checkArr[0] : null;
			  	if ($checkStr) {
			  		// check if match with value then add, else ignore the block
			  		$matched = false;
			  		if ($this->getParam($checkVar) !== null) {
			  			if ($checkValue === null) {
			  				$matched = true;
			  			} else {
			  				$value = $this->getParam($checkVar);
			  				if ($checkMatch) {
			  					$checkValue = str_replace('/', '\\/', $checkValue);
			  					$matched = (bool)preg_match('/' . $checkValue . '/i', $value);
			  				} else {
			  					$matched = $value == $checkValue;
			  				}

			  				if ($checkFalse) $matched = !$matched;
			  			}
			  		}

			  		// if (($checkValue !== null && $this->getParam($checkVar) == $checkValue) || ($checkValue === null && $this->getParam($checkVar))) {
			  		if ($matched) {
			  			$chucks[] = $checkStr;
			  		}
			  		// push no process style
			  		if (count($checkArr) > 1) $chucks[] = $checkArr[1];
			  	} else {
			  		// wrong pattern, just ignore the separator
			  		$chucks[] = $arr[$i];
			  	}
			}
			
			if (count($chucks) > 1)
				$custom_style_css = implode('', $chucks);

			// replace variables
			$lines = explode("\n", $custom_style_css);
			$olines = array();
			foreach ($lines as $line) {
				$oline = $line;
				if (preg_match_all ('/\{?__([0-9a-zA-Z_]+)\}?/', $line, $matches)) {
					$replace = array();
					foreach ($matches[1] as $name) {
						$val = $this->get($name, null);

						if (!$val) {
							$oline = null;
							continue;
						}
						$replace[] = $val;
					}
					$oline = str_replace($matches[0], $replace, $oline);
				}
				if ($oline) $olines[] = $oline;
			}
			$custom_style_css = implode("\n", $olines);

	        return $custom_style_css;
		}

		/**
		 * Auto detect after template save to compile custom css and store settings revision
		 */
		public function afterSave () {
			// Check and save config revision, build custom css file
			$this->saveRevision ('style', 'advanced');
			$this->saveCustomStyle();
			$this->clearTplStatus();
		}

		/**
		 * Add custom style css
		 */
		public function addCustomStyle () {
			$html = '';
			$google_fonts = preg_split('/\n/', $this->getParam('styleGoogleFonts'));
			foreach ($google_fonts as $font) {
				$font = trim($font);
				if(!$font) continue;
				$html .= '<link href="https://fonts.googleapis.com/css?family=' . ($font) . '" rel="stylesheet" type="text/css" >' . "\n";
			}

			if (is_file($this->getCustomCssPath())) {
				$html .= '<link id="custom-style-css" href="' . $this->getCustomCssPath(true) . '" rel="stylesheet" type="text/css" >' . "\n";
			} else {
				$html .= '<style type="text/css" id="custom-style-css">' . "\n";
				$html .= $this->buildCustomStyle() . "\n";
				$html .= '</style>' . "\n";
			}

			echo $html;
		}


		/**
		 * Render a spotlight
		 */
		public function spotlight ($pos, $options = array()) {			
			if ($this->getParam('layoutEnable_' . $pos)) {			
			
				$position = $this->getParam('layoutPos_' . $pos);
				$posWidth = $this->getParam('layoutWidth_' . $pos);

				$modules = JModuleHelper::getModules($position);

				if (!count($modules)) return;

				$widths = array();
				$posWidth = preg_replace ('/\s/', '', $posWidth);
				if (preg_match('/^[0-9,:-]+$/', $posWidth)) {
					$widths = preg_split('/[,:-]/', $posWidth);
				} else {
					if ($posWidth == 'none') {
						$widths[] = '';
					} else {
						// auto width
						$col = count($modules);
						$width = $col < 6 ? floor (12 / $col) : 2;
						$lastWidth = $col < 6 ? 12 - $width * ($col - 1) : 2;
						$widths = $col > 1 ? array_fill(0, $col-1, $width) : array();
						$widths[] = $lastWidth;
					}
				}

				// pust into columns
				$columns = array();
				$mod = round(count ($modules) / count($widths));
				$c = 0;
				for ( $i =0; $i < count($widths); $i++) {
					$col = new stdclass();
					$col->width = $widths[$i] ? 'span' . $widths[$i] : '';
					$col->html = '';
					while($c < count($modules) && $c < ($i+1) * $mod) {
						$col->html .= JModuleHelper::renderModule($modules[$c], array('style'=>'JAxhtml')) . "\n";
						$c++;
					}
					$columns[] = $col;
				}
				$options['key'] = $pos;
				$options['id'] = $this->getParam('layoutName_' . $pos) ? $this->getParam('layoutName_' . $pos) : $pos;
				$class = isset($options['class']) ? $options['class'] . ' section ' : 'section ';
				$class .= is_array($this->getParam('layoutClass_' . $pos)) ? implode(' ', $this->getParam('layoutClass_' . $pos)) : $this->getParam('layoutClass_' . $pos);
				$options['class'] = $class ? ' class="' . trim($class) . '"' : '';
				$options['row'] = $posWidth !== 'none';
				
				// display the column
				echo JLayoutHelper::render ('simpli.position', array('columns' => $columns, 'options' => $options, 'helper' => $this));
			}
		}
		
		/*
		* Get the Profile
		*/
		public function getProfile($id) {
			$path = JPATH_ROOT . '/media/'._ADMIN_TPL.'/profiles';
			if (JFolder::exists($path)) {
				$profiles = JFolder::files($path);
				foreach ($profiles as $profile) {
					if($id == str_replace('.json', '', $profile)){
						$pFile = $profile;
						break;
					}
				}
			}
			if (isset($pFile)) {
				$data = json_decode(file_get_contents($path.'/'.$pFile),true);
				return $data;
			} else {
				return false;
			}
		}

		public function getMessages() {
			$user = JFactory::getUser();
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('message_id')->from($db->quoteName('#__messages'))->where('user_id_to = '. (int) $user->get('id') . ' AND state = 0');
			$db->setQuery($query);
			$messages = $db->loadAssocList();
			return count($messages);

		}
		
		public static function getComponents($authCheck = true)
		{
			$lang   = JFactory::getLanguage();
			$user   = JFactory::getUser();
			$db     = JFactory::getDbo();
			$query  = $db->getQuery(true);
			$result = array();

			// Prepare the query.
			$query->select('m.id, m.title, m.path, m.alias, m.link, m.parent_id, m.img, e.element')
				->from('#__menu AS m');

			// Filter on the enabled states.
			$query->join('LEFT', '#__extensions AS e ON m.component_id = e.extension_id')
				->where('m.client_id = 1')
				->where('e.enabled = 1')
				->where('m.id > 1');

			// Order by lft.
			$query->order('m.lft');

			$db->setQuery($query);

			// Component list
			try
			{
				$components = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$components = array();
				JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');
			}

			// Parse the list of extensions.
			foreach ($components as &$component)
			{
				// Trim the menu link.
				$component->link = trim($component->link);

				if ($component->parent_id == 1)
				{
					// Only add this top level if it is authorised and enabled.
					if ($authCheck == false || ($authCheck && $user->authorise('core.manage', $component->element)))
					{
						// Root level.
						$result[$component->id] = $component;

						if (!isset($result[$component->id]->submenu))
						{
							$result[$component->id]->submenu = array();
						}

						// If the root menu link is empty, add it in.
						if (empty($component->link))
						{
							$component->link = 'index.php?option=' . $component->element;
						}

						if (!empty($component->element))
						{
							// Load the core file then
							// Load extension-local file.
							$lang->load($component->element . '.sys', JPATH_BASE, null, false, true)
						||	$lang->load($component->element . '.sys', JPATH_ADMINISTRATOR . '/components/' . $component->element, null, false, true);
						}

						$component->text = $lang->hasKey($component->title) ? JText::_($component->title) : $component->alias;
					}
				}
				else
				{
					// Sub-menu level.
					if (isset($result[$component->parent_id]))
					{
						// Add the submenu link if it is defined.
						if (isset($result[$component->parent_id]->submenu) && !empty($component->link))
						{
							$component->text                          = $lang->hasKey($component->title) ? JText::_($component->title) : $component->alias;
							$result[$component->parent_id]->submenu[] = &$component;
						}
					}
				}
			}

			$result = JArrayHelper::sortObjects($result, 'text', 1, false, true);
			
			return $result;
		}
		
		public function getComponentLinks($components = array(), $quicklinks) {
			$slId = array();
			if (count($quicklinks['systemlinks'] > 0)) {
				foreach ($quicklinks['systemlinks'] as $link) {
					$slId[] = $link['timeStamp'];
				}
			}
			
			$links = array();
			if (count($components)) {
				$cpl = array();
				foreach ($components as $component) {
					if (count($component->submenu) > 0 ) {
						foreach ($component->submenu as $sub) {
							if (!in_array($sub->id, $slId)) {
								$item = array();
								$item['timeStamp'] = $sub->id;
								$item['name'] = strtolower($sub->title);
								$item['title'] = $sub->path;
								$item['link'] = $sub->link;
								$item['icon'] = str_replace('class:','', $sub->img);
								$links[] = $item;
							}
						}
					} else {
						if (!in_array($component->id, $slId)) {
							$item = array();
							$item['timeStamp'] = $component->id;
							$item['name'] = strtolower($component->title);
							$item['title'] = $component->path;
							$item['link'] = $component->link;
							$item['icon'] = str_replace('class:','', $component->img);
							$links[] = $item;
						}
					}
				}
			}
			
			return $links;
		}
		
		/*
		* Method to get all modules in the administrator
		* return	array		list of modules
		*/
		public static function getModules($clientId='1') {
			$app = JFactory::getApplication();

			// Create a new query object.
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Select the required fields.
			$query->select(
					'a.id, a.title, a.note, a.position, a.module, a.language,' .
						'a.checked_out, a.checked_out_time, a.published AS published, e.enabled AS enabled, a.access, a.ordering, a.publish_up, a.publish_down'
			);

			// From modules table.
			$query->from($db->quoteName('#__modules', 'a'));

			// Join over the language
			$query->select($db->quoteName('l.title', 'language_title'))
				->select($db->quoteName('l.image', 'language_image'))
				->join('LEFT', $db->quoteName('#__languages', 'l') . ' ON ' . $db->quoteName('l.lang_code') . ' = ' . $db->quoteName('a.language'));

			// Join over the users for the checked out user.
			$query->select($db->quoteName('uc.name', 'editor'))
				->join('LEFT', $db->quoteName('#__users', 'uc') . ' ON ' . $db->quoteName('uc.id') . ' = ' . $db->quoteName('a.checked_out'));

			// Join over the asset groups.
			$query->select($db->quoteName('ag.title', 'access_level'))
				->join('LEFT', $db->quoteName('#__viewlevels', 'ag') . ' ON ' . $db->quoteName('ag.id') . ' = ' . $db->quoteName('a.access'));

			// Join over the module menus
			$query->select('MIN(mm.menuid) AS pages')
				->join('LEFT', $db->quoteName('#__modules_menu', 'mm') . ' ON ' . $db->quoteName('mm.moduleid') . ' = ' . $db->quoteName('a.id'));

			// Join over the extensions
			$query->select($db->quoteName('e.name', 'name'))
				->join('LEFT', $db->quoteName('#__extensions', 'e') . ' ON ' . $db->quoteName('e.element') . ' = ' . $db->quoteName('a.module'));

			// Group (careful with PostgreSQL)
			$query->group(
					'a.id, a.title, a.note, a.position, a.module, a.language, a.checked_out, ' .
						'a.checked_out_time, a.published, a.access, a.ordering, l.title, l.image, uc.name, ag.title, e.name, ' .
						'l.lang_code, uc.id, ag.id, mm.moduleid, e.element, a.publish_up, a.publish_down, e.enabled'
				);

			// Filter by client.
			$query->where($db->quoteName('a.client_id') . ' = ' . (int) $clientId . ' AND ' . $db->quoteName('e.client_id') . ' = ' . (int) $clientId);
			$query->where($db->quoteName('a.published') . ' IN (0, 1)');
			// Filter by position.
			$query->where ('position IN ("cpanel", "cpanel-left", "cpanel-right", "menu", "icon")');
			
			$db->setQuery($query);
			$modules = $db->loadObjectList();
			
			return $modules;
		}
		
		/*
		* Method to get all the menu items in the Administrator from Menus table
		* return array of list Menu Item object
		*/
		public function getMenus($client = '1') {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('mn.*')
						->from($db->quoteName('#__menu','mn'))
						//->join('INNER', $db->quoteName('#__extensions','ex') . ' ON '.$db->quoteName('mn.component_id').'='.$db->quoteName('ex.extension_id'))
						->where('mn.client_id = '.(int) $client);
			$db->setQuery($query);
			$results = $db->loadObjectList();
			
			return $results;
		}
		
		/*
		* Method to get default site template
		* return Name of template was assigned to default site
		*/
		public static function getTemplateSite() {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('template')->from($db->quoteName('#__template_styles'))->where('client_id = 0 && home = 1');
			$db->setQuery($query);
			$template = $db->loadResult();
			return $template;
		}
	}
}	

return new BKTMPHelper();