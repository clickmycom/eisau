<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$helper = require (__DIR__ . '/helper.php');
$helper->init($this);

$app             = JFactory::getApplication();
$input           = $app->input;
$option          = $input->get('option', '');
$cpanel          = ($option === 'com_cpanel');
$user            = JFactory::getUser();
$color_is_light	 = false;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.js');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/dragula/dragula.js');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/masonry/masonry.pkgd.min.js');
$this->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/mCustomScrollbar/jquery.mCustomScrollbar.min.css');
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/dragula/dragula.css');
$this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template' . ($this->direction == 'rtl' ? '-rtl' : '') . '.css');

// CUSTOM CSS
if (is_file(__DIR__ . '/css/custom.css')) {
  $this->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/custom.css');
}

// Get background image name
$pageBg = $this->params->get('styleLoginBgImage');


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<jdoc:include type="head" />
	<?php $helper->addCustomStyle(); ?>
	<!--[if lt IE 9]>
		<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="site <?php $helper->_('option') ?> view-<?php $helper->_('view') ?> <?php if($pageBg) { echo 'bg-img'; } ?>" <?php if( $this->params->get('styleLoginBgImage')): ?> style="background-image: url(<?php echo JUri::root(true) ."/". $this->params->get('styleLoginBgImage'); ?>);"<?php endif ?>>

	<!-- Container -->
	<div class="container">
		<div id="content">

			<!-- Begin Content -->
			<div id="element-box" class="login">
				<div class="brand">
					<a href="#" class="login-logo" title="<?php $helper->_('siteName') ?>"></a>
				</div>

				<p class="message">Please fill <strong>username</strong> and <strong>password</strong> to login.</p>
				<!-- <h1 class="center"><?php echo JText::_('MOD_LOGIN'); ?></h1> -->
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
			<noscript>
				<?php echo JText::_('JGLOBAL_WARNJAVASCRIPT'); ?>
			</noscript>
			<!-- End Content -->
		</div>
	</div>
	<div class="navbar navbar-fixed-bottom hidden-phone">
		<p class="pull-right">
			&copy; <?php echo date('Y'); ?> <?php $helper->_('siteName') ?>
		</p>
		<a class="login-joomla hasTooltip" href="<?php echo $this->params->get('styleLoginLogoSmLink'); ?>" target="_blank" title="<?php echo $this->params->get('styleLoginLogoSmText'); ?>">
			<span class="icon-joomla"></span>
		</a>
		<a href="<?php echo JUri::root(); ?>" target="_blank" class="pull-left"><span class="icon-out-2"></span> <?php echo JText::_('COM_LOGIN_RETURN_TO_SITE_HOME_PAGE'); ?></a>
	</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
