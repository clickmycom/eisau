<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;
?>

<?php
if (file_exists(JPATH_SITE.'/components/com_jsn/helpers/helper.php')) {
						include_once(JPATH_SITE.'/components/com_jsn/helpers/helper.php');
						
						$user 		= JsnHelper::getUser($displayData['item']->created_by);
						$avatarUrl	= $user->avatar_mini;
						$name		= $user->name;
						$html .= '<img src="'.$avatarUrl.'" alt="'.$user->name.'" style="padding: 15px 15px 15px 0px;" class="rs_avatar"  />';
					} else {
						$html .= '<img src="'.JURI::root().'components/com_rseventspro/assets/images/user.png" style="padding: 15px 15px 15px 0px;" alt="Tanawat Kanrai" class="rs_avatar" width="64" height="64" />';
					}

?>

<dd class="createdby autor_wat" itemprop="author" itemscope itemtype="https://schema.org/Person" >

	<?php $author = ($displayData['item']->created_by_alias ? $displayData['item']->created_by_alias : $displayData['item']->author); ?>
	<?php $author = '<span itemprop="name">' . $author . '</span>'; ?>
	<?php if (!empty($displayData['item']->contact_link ) && $displayData['params']->get('link_author') == true) : ?>
		<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', JHtml::_('link', $displayData['item']->contact_link, $author, array('itemprop' => 'url'))); ?>
	<?php else :?>
		<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', $author); ?>
	<?php endif; ?>
	<br>
	<?=$html?>
</dd>
