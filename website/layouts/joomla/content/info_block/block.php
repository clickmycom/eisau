<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$blockPosition = $displayData['params']->get('info_block_position', 0);

?>
	<dl class="article-info muted hamomo">

		<?php if ($displayData['position'] == 'above' && ($blockPosition == 0 || $blockPosition == 2)
				|| $displayData['position'] == 'below' && ($blockPosition == 1)
				) : ?>

			<dt class="article-info-term">
				<?php if ($displayData['params']->get('info_block_show_title', 1)) : ?>
					<?php echo JText::_('COM_CONTENT_ARTICLE_INFO'); ?>
				<?php endif; ?>
			</dt>

			<?php if ($displayData['params']->get('show_author') && !empty($displayData['item']->author )) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.author', $displayData); ?>
			<?php endif; ?>

			<?php if ($displayData['params']->get('show_parent_category') && !empty($displayData['item']->parent_slug)) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.parent_category', $displayData); ?>
			<?php endif; ?>

			<?php if ($displayData['params']->get('show_category')) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.category', $displayData); ?>
			<?php endif; ?>

			<?php if ($displayData['params']->get('show_publish_date')) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.publish_date', $displayData); ?>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ($displayData['position'] == 'above' && ($blockPosition == 0)
				|| $displayData['position'] == 'below' && ($blockPosition == 1 || $blockPosition == 2)
				) : ?>
			<?php if ($displayData['params']->get('show_create_date')) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.create_date', $displayData); ?>
			<?php endif; ?>

			<?php if ($displayData['params']->get('show_modify_date')) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.modify_date', $displayData); ?>
			<?php endif; ?>

			<?php if ($displayData['params']->get('show_hits')) : ?>
				<?php echo JLayoutHelper::render('joomla.content.info_block.hits', $displayData); ?>
			<?php endif; ?>
		<?php endif; ?>
	</dl>




<style>
	@media (min-width: 1200px){
		.row-fluid .span3 {
    	width: 22.2%;
}
	
	.yahoo_a{
		padding: 20px;
	    margin-left: 0px !important;
	    margin-top: 4px !important;
	}
	table{
		width:100% !important;
			
	}
	table > thead > tr > td > p{
		display: table-cell !important;
		padding:10px;
		
	}
	
	table > tbody > tr > td > p{
		display: table-cell !important;
		padding:10px;

	}

.thumbnail_a.thumbnail_big {
    margin-bottom: 0;
	}
	.thumbnail_a {
    padding: 0;
    margin-bottom: 20px;
}
.thumbnail_a {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: border .2s ease-in-out;
    -o-transition: border .2s ease-in-out;
    transition: border .2s ease-in-out;
}
.thumbnail_big .thumbnail__caption {
    padding: 18.77px 9px;
}
.thumbnail_a .caption {
    padding: 15px 9px;
    color: #333;
}

.thumbnail_big img {
    border-radius: 6px !important;
    padding: 0px !important;
}
.thumbnail_small img {
	border-radius: 6px !important;
    padding: 0px !important;
}

.thumbnail__caption {
    padding: 9px;
    color: #333;
}
@media (min-width: 768px){
.col-sm-6 {
    width: 45.7%;padding-left: 19px;
 }
.col-sm-3 {
    width: 24.6%;padding-left: 19px;
 }
}


@media (min-width: 1200px){
	.row-fluid .span9 {
	    width: 76.468085% !important;
	}
}


.blog img {
    border-radius: 7px !important;
    padding: 0px !important;
    width: 250px;
    height: 177px;
}
.special__desc {
  font-size: 14px;
  line-height: 1.4;
  font-family: 'Roboto', sans-serif;
  margin-bottom: 0;
}
/* 3.6. END Special */

div[class^='blognewsletters-new-00000'] p,h2{
	display:none;
}

div[class^='bloglast_news0abade'] p[class^='Heading2'] ,h2{
	display:block;
}

.page-header > h2[itemprop^='name'] {
    color: #EC3B33 !important;
    font-weight: 700 !important;
    font-size: 18px;
    margin-top: 4px;
    word-wrap: break-word;
    font-family: "Source Sans Pro",sans-serif;
}

.page-header h2{
	    margin-top: 0px;
	   
}

#sidebar{
	padding: 18px;
}

.tk00_hr{
	border-bottom: 1px solid #eee !important;
}
div[class^='items-row cols-1 row-9 row-fluid clearfix'] > div.tk00_hr{
	border-bottom: 0px solid #eee;
}
@media screen and (max-width: 767px){
	input#mod-search-searchword{
		width: 93%;
	}
}
@media screen and (max-width: 768px){
	.pull-left.item-image{
		padding-left: 0;
		padding-right: 0;
		padding-bottom: 2%;
		margin: auto;
		width: 100%;
        max-width: 100%;

	}
	.blog img{
		width: 100%;
		height: auto; 

	}
}
@media screen and (max-width: 480px){

	.pull-left.item-image{
		padding-left: 0;
		padding-right: 0;
		padding-bottom: 2%;
		margin: auto;
		width: 100%;
        max-width: 100%;

	}
	.blog img{
		width: 100%;
		height: auto;

	}
	
}

p {
    margin-bottom: 10px !important;
    margin-top: 0px;
     font-family: Helvetica,Arial,sans-serif;
    -ms-text-size-adjust: 100%;
    /* -webkit-text-size-adjust: 100%; */
    /* text-align: left; */
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
    line-height: 1.538461538;
    /* direction: ltr; */
    font-size: 15px;
    color: #777;

}
p {
    word-wrap: break-word;
}

</style>                           
          
