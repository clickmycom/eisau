<?php
defined('_JEXEC') or die('Restricted Access');
$protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
// Auth call to Google.
if (!$client->getAccessToken()) : 
	echo JLayoutHelper::render($module->module.'.authorize', array('client'=>$client,'scriptUri'=>$scriptUri),dirname(__DIR__) . '/layouts');
elseif (!isset($profileId['error'])) : ?>
	<div id="ja_ga_stats" class="<?php echo $params->get('moduleclass_sfx','') ?> row-fluid">
    <div class="ga-container">
      <div class="row">
    		<?php if ($params->get('active_users','1') == '1') : ?>
    			<div id="ja_active_users" class="span4">
    				<span class="ja-ga-title"><?php echo JText::_('JA_GA_TMPL_ACTIVE_USERS_TITLE');?></span>
    				<span class="ja-ga-number" id="active-users"></span>
    				<span class="ja-ga-time"><?php echo JText::_('JA_GA_NOW')?></span>
    			</div>
    		<?php endif; ?>
    		<?php if ($params->get('pageview', '1') == '1') : ?>
    			<div id="ja_page_views" class="span4">
    				<span class="ja-ga-title"><?php echo JText::_('JA_GA_TMPL_PAGE_VIEWS_TITLE');?></span>
    				<span class="ja-ga-number"><?php echo number_format($pageViews)?></span>
    				<span class="ja-ga-time"><?php echo $time ?></span>
    			</div>
    		<?php endif; ?>
    		<?php if ($params->get('bounce_rate','1') == '1') : ?>
    			<div id="ja_bounce_rates" class="span4">
    				<span class="ja-ga-title"><?php echo JText::_('JA_GA_TMPL_BOUNCE_RATE_TITLE');?></span>
    				<span class="ja-ga-number"><?php echo round($bounceRate,2)?>%</span>
    				<span class="ja-ga-time"><?php echo $time ?></span>
    			</div>
    		<?php endif; ?>
      </div>
    </div>
    <div class="ja_ga_foot">
      <div class="ga-container">
        <a href="<?php echo $scriptUri.'?ja_refresh=1' ?>"><?php echo JText::_('JA_GA_REFRESH')?></a>
      </div>
    </div>
  </div>
	<script type="text/javascript">
		(function($) {
			getActiveUsers();
			function getActiveUsers() {
				var current = $('#active-users').text();
				var time_request = <?php echo $params->get('time_getuser','10'); ?>*1000;
				console.log(time_request);
				$.get('<?php echo $fetch_users_url; ?>', function(data) {
					if (data.rows) {
            var result = 0;
						<?php if ($isHome) : ?>
              result = data.rows[0][0];
            <?php else : ?>
              var siteUrl = '<?php echo $protocol.'://'.$_SERVER['HTTP_HOST']; ?>',
                  currentPage = '<?php echo $currentPage ?>';
              $.each(data.rows, function(key,value) {
                if (currentPage.slice(-1) == '/') {
                  if (value[0].slice(-1) != '/') {
                    value[0] += '/';
                  }
                } else {
                  if (value[0].slice(-1) == '/') {
                    currentPage += '/';
                  }
                }
                if (siteUrl+value[0] == currentPage) {
                  result  = value[1];
                }
              });
            <?php endif; ?>
            if (result != current) {
              $('#active-users').html('<span style="color:#ff0000;">'+result+'</span>');
              setTimeout(function() {
                $('#active-users span').css('color','#096');
              }, 2000);
            }
					}
				});
        
				setTimeout(function() {
					getActiveUsers();
				},time_request)
			}
		})(jQuery);
	</script>
<?php else : ?>
	<p><?php echo JText::_('JA_GA_ERROR_NOTICE')?>, please
		<a href="<?php echo $scriptUri.'?ja_refresh=1' ?>"><?php echo JText::_('JA_GA_REFRESH')?></a>
	</p>
<?php endif;