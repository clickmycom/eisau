<?php
/**
 * @package Sj Basic News
 * @version 2.5
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 * 
 */
defined('_JEXEC') or die;

class BasicNewsReader extends SjReader{		
	public function getList(&$params){	
		$arrcustom_url=array();	
		$arrcustom_url = $this->_getCustomUrl($params['custom_url']);
		$categories = array();
		$list=array();			
		$user 		=&	JFactory::getUser();
		$app 		=&	JFactory::getApplication();
		$db   		=&	JFactory::getDBO();
		$jnow		=& 	JFactory::getDate();
		$now		= 	$jnow->toMySQL();
		$nullDate	=	$db->getNullDate();
		$noauth		= !	$app->getParams()->get('show_noauth');
		$aid = $user->get ( 'aid', 0 );
		$aid = 1;
		if (!isset($params['source']) || empty($params['source'])){
			$this->errors = "No selected or all selected is unpublished.";
		return array();
		}
		$category_ids = is_array($params['source']) ? $params['source'] : array($params['source']);		
		if ($noauth){
			$accessids 	= $user->getAuthorisedViewLevels();
			$access_set = implode(",", $accessids);
			$authorize	= "AND c.access IN ($access_set)";
		} else {
			$authorize 	= "";
		}						
		if ( isset($params['source_filter']) ){
				// frontpage filter.
			switch ($params['source_filter']){
					default:
					case '0':
						$join_filter = "";
						break;
					case '1':
						$join_filter = "AND a.featured=0";
						break;
					case '2':
						$join_filter = "AND a.featured=1";
						break;
				}
			}				
			$query =
				"
				SELECT
					a.id, a.title, a.introtext, a.fulltext,
					a.created, a.modified, a.hits, a.alias, 
					DATEDIFF(NOW(), a.created) as datediff,
					c.id AS category_id, c.title as cat_name, c.access as cat_access
					
				FROM #__content AS a
					INNER JOIN #__categories AS c ON c.id = a.catid					
					
				WHERE
					c.id IN (" . implode(",", $category_ids) . ")
					$authorize
					AND a.state IN (1)
					AND ( a.publish_up = {$db->quote($nullDate)} OR a.publish_up <= {$db->quote($now)} )
					AND ( a.publish_down = {$db->quote($nullDate)} OR a.publish_down >= {$db->quote($now)} )
					AND a.language IN ( {$db->quote(JFactory::getLanguage()->getTag())} , {$db->quote('*')} )
					$join_filter
				";
			if ( isset($params['source_order_by']) ){
				switch ($params['source_order_by']){
					default:
					case 'ordering':
						$query .= " ORDER BY a.ordering";
						break;
					case 'hits':
						$query .= " ORDER BY a.hits DESC";
						break;
					case 'created':
						$query .= " ORDER BY a.created DESC";
						break;
					case 'modified':
						$query .= " ORDER BY a.modified DESC";
						break;
					case 'title':
						$query .= " ORDER BY a.title";
						break;
					case 'random':
						$query .= " ORDER BY rand()";
						break;
				}
			}							   
			$query .=  $params['source_limit'] ? ' LIMIT ' . $params['source_limit'] : '';	
			$db->setQuery($query);
			$rows = $db->loadObjectList();		
			include_once JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php';
			if(!empty($rows)) {
			foreach ($rows as $key=>$item){				
				$temp = array();
				if($item->cat_access <= $aid ) {                       
					$item->category_link = JRoute::_(ContentHelperRoute::getCategoryRoute($item->category_id));												
				}								
				if (!isset($item->cat_name)){								
					$item->cat_name =$item->cattitle;
				}
					$item->image = YTools::extractImages($item->introtext);
				if (empty($item->image[0]) ||  !file_exists($item->image[0])){
						$item->image[0] = 'modules/'.Ytools::getModule()->module . '/assets/images/nophoto.gif';
					}		
				if($item->cat_access <= $aid ) {
					if(array_key_exists($item->id, $arrcustom_url)){
						$item->link = $arrcustom_url[$item->id];
					}else{
						$item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->id, $item->category_id));
					}
				}	
											
					if ($params['item_desc_keephtml'] == 0){
						$item->desc = strip_tags($item->introtext);
					}	else{
						$item->desc=$item->introtext;
					}
					$temp['id'] = $item->id;
					$temp['link'] = $item->category_link;					
					$temp['cattitle']= $item->cat_name;	
					$temp['title'] = $item->title;		
					$temp['link'] = $item->link;	 
					$temp['image'] = $item->image[0];						
					$temp['desc'] = $item->desc;
					$temp['created']=	$item->created;					
					$list[$key] = $temp;					
					}
			/*  	Ytools::dump($list);  */
			return $list;
			}
		}
	private function _getCustomUrl($custom_url) {     
    	$arrUrl = array();
        $tmp = explode("\n", trim($custom_url));            
        foreach( $tmp as $strTmp){
        	$pos = strpos($strTmp, ":");
            if($pos >=0){
            	$tmpKey = substr($strTmp, 0, $pos);
                $key = trim($tmpKey);
                $tmpLink = substr($strTmp, $pos+1, strlen($strTmp)-$pos);
                $haveHttp =  strpos(trim($tmpLink), "http://");
                if($haveHttp<0 || ($haveHttp !== false) ){
                    $link = trim($tmpLink);
                }else{
                    $link = "http://" . trim($tmpLink);
                }
                $arrUrl[$key] = $link;
            }  
        }            
        return $arrUrl;
	}
}