<?php
/**
* @package Sj Basic News
* @version 3.0
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* @copyright (c) 2012 YouTech Company. All Rights Reserved.
* @author YouTech Company http://www.smartaddons.com
*
*/
defined('_JEXEC') or die;?>
<?php 
	$options=$params->toObject();
	$image_config = array(
    	'output_width'  => $params->get('item_image_width'),
    	'output_height' => $params->get('item_image_height'),
    	'function'		=> $params->get('item_image_function'),
    	'background'	=> $params->get('item_image_background')
    );
?>
<?php
	if (!empty($list)) { ?>
	<?php if ( !empty($options->pretext)){ ?>
         <div class="bsn-pretext"><?php echo $options->pretext; ?></div>
    <?php } ?>
	<!-- <div class="bsn-wrap"> -->
		
		
<style>
	.item_news_home > div:last-child > div{
		border-bottom:none !important;
		padding: 30px 0px 0px 0px!important;
	}
</style>


<section class="kindergarden">
    <div class="kindergardenFacilities" id="manood_tk" style="background-color: #fff !important; background-image: url('templates/protostar/css/_bg-1.jpg'); background-size: cover; background-position: 50% 100%; padding: 3%;">
        <div class="container">
            <div class="row" style="margin-left: 0px !important;">
                <div class="contentFacilities">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1461047932461">
                        <div class="">
                            <div class="vc_column-inner ">
                            	 <div >
                                        <h1 class="section-title dark_title"><span class="notViewed " style="color: #58595b; font-size: 54px; font-weight: 800;">EISAU</span> <span class="notViewed " style="color: #ee3928; font-size: 54px; font-weight: 800;">NEWS</span></h1>
                                    </div>
                                <div class="wpb_wrapper item_news_home">
                            
                                                                            	
                                                                            	
    <?php
    	$array_latest_news = array();
		$array_newsletters = array();
		foreach ($list as $items){
			if($items->catid == 17 || $items->catitle == 'Eisau News'){
				if(count($array_latest_news) == 0){
					array_push($array_latest_news,$items);
				}
			}else if($items->catid == 20 || $items->catitle == 'Newsletters'){		
				if(count($array_newsletters) == 0){
					array_push($array_newsletters,$items);	
				}
			}
		}
		
    ?>                                                                        	
                    
            
                             	                                                        	
	 <?php 
	 	$count = 0; 
		$count++; 
		 if($count == count($list)){
			 $iditem = 'last-item';
		 }else if($count == 1){
			 $iditem = ' first-item';
		 }else{
			 $iditem = '';
		 }
	?>
					 
	 <!-- /////////////////////////
	 Section Latest News
	 ///////////////////////////// -->
	 <?php
	 	foreach ($array_latest_news as $items) {
	 ?>
	  <div class="<?php echo $module->id; ?> post <?php if($params->get('showline')){ echo 'showlinebottom'.$iditem; } ?>">
	   							<div class="notViewed wpb_column vc_column_container " style="float: left; border-bottom: 3px solid; line-height: 43px; border-color: #ff0000 #0000ff;    padding: 30px 0px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-3" >
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_wrapper" style="text-align: center;">
                                                                <?php if ($options->item_image_display==1 ){?>
                                                                        <a title="<?php echo $items->title?>" target="<?php echo $options->item_link_target; ?>" href="<?php echo $items->link?>">
                                                                            <img class="_img_news size-full wp-image-3729 aligncenter" style="border-radius: 10px;" src="<?php echo Ytools::resize($items->image, $image_config);?>" title="<?php echo $items->title ;?>" alt="<?php echo $items->link?>" width="<?php echo $params->get('item_image_width');?>" height="<?php echo $params->get('item_image_height');?>" /> </a>
                                                                        </a>                                                                  
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="" class="_news_eiasu wpb_column vc_column_container vc_col-sm-12" style="width: 64%;max-height: 245px;">
                                                        <div class="vc_column-inner vc_custom_1445429978052">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_top-to-bottom top-to-bottom wpb_start_animation animated">
                                                                    <div class="wpb_wrapper">
                                                                        <h2 class="_txt_news" style="margin-bottom: 10px; text-align: left; font-size: 32px; margin-top: 0px; padding-top: 0px;">
                                                                            <span style="color: #ff0000;">
                                                                               <a title="<?php echo $items->title?>" target="<?php echo $options->item_link_target; ?>" href="<?php echo $items->link?>"> 
                                                                               		<strong style="color: #ee3928;">Latest News : </strong><strong style="font-size: 32px;"><?php echo YTools::truncate($items->title,$options->item_title_max_characs);?></strong>
                                                                               </a>
                                                                            </span>
                                                                        </h2>

                                                                        <?php if ($options->item_desc_display == 1 ){?>

                                                                        <p style="margin-left: 0rem; ">
                                                                            <span class="_txt_news_dec" style="color: #808080; font-size: 20px; letter-spacing: 1px; line-height: 32px;">
                                                                               <?php echo Ytools::truncate($items->description,$options->item_desc_max_characs);?>
                                                                            </span>
                                                                        </p>
                                                                        <?php } ?>

                                                                        <?php if( $options->item_date_display==1 || $options->cat_title_display==1 ){?>
                                                                            <p>
                                                                           <?php if($options->cat_title_display==1) {?>
                                                                                <span class="cattitle"><?php echo $items->catitle; ?> </span>
                                                                           <?php } ?>
                                                                           <?php if ($options->item_date_display == 1):?>
                                                                                <span class="basic-date"><?php echo $items->created?></span>
                                                                            <?php endif; ?>
                                                                           </p>        
                                                                        <?php } ?>
                                                                        <a class="je_dropbtn active" href="<?php echo $items->link?>" target="<?php echo $options->item_link_target; ?>" ><span class="je_dropbtn-inner text_login_top" style="background: transparent; color: #ee3928; font-weight: 600; float: right; border-color: #ee3928; line-height: 23px;">Read more</span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                 </div>         
	  </div> 
	  <?php
	  	} // end section Latest News
	  ?>
	 <!-- /////////////////////////
	 	Section Newsletters
	 ///////////////////////////// -->
	  <?php
	 	foreach ($array_newsletters as $items) {
	 		$myArray = json_decode($items->urls, true);	
	 ?>
	 		<div class="<?php echo $module->id; ?> post <?php if($params->get('showline')){ echo 'showlinebottom'.$iditem; } ?>">
	   							<div class="notViewed wpb_column vc_column_container " style="float: left; border-bottom: 3px solid; line-height: 43px; border-color: #ff0000 #0000ff;    padding: 30px 0px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-3" >
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_wrapper" style="text-align: center;">
                                                                <?php if ($options->item_image_display==1 ){?>
                                                                	
                                                                		
																		<h3>
																	
                                                                        <a title="<?php echo $items->title?>" target="_blank"  href="<?=$myArray['urla']?>">
                                                                            <img class="_img_news size-full wp-image-3729 aligncenter" style="border-radius: 10px;" src="<?php echo Ytools::resize($items->image, $image_config);?>" title="<?php echo $items->title ;?>" alt="<?php echo $items->link?>" width="<?php echo $params->get('item_image_width');?>" height="<?php echo $params->get('item_image_height');?>" /> </a>
                                                                        </a>                                                                  
                                                                <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="" class="_news_eiasu wpb_column vc_column_container vc_col-sm-12" style="width: 64%; margin-top: 1.1%;max-height: 245px;">
                                                        <div class="vc_column-inner vc_custom_1445429978052">
                                                            <div class="wpb_wrapper">
                                                                <div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_top-to-bottom top-to-bottom wpb_start_animation animated">
                                                                    <div class="wpb_wrapper">
                                                                        <h2 class="_txt_news" style="margin-bottom: 10px; text-align: left; font-size: 32px; margin-top: 0px; padding-top: 0px;">
                                                                            <span style="color: #ff0000;">
                                                                            	<!-- <pre>
                                                                            		<?php
                                                                            			print_r($items);
                                                                            		?>
                                                                            	</pre> -->
                                                                            	<?php
                                                                            		if(strip_tags($items->fulltext) == '' || strip_tags($items->fulltext) == null){
                                                                            	?>
                                                                            			 <a title="<?php echo $items->title?>" target="_blank"   href="<?=$myArray['urla']?>"> 
                                                                               				<strong style="color: #ee3928;">Newsletters : </strong><strong style="font-size: 32px;"><?php echo YTools::truncate(strip_tags($items->title),$options->item_title_max_characs);?></strong>
                                                                   						</a>
                                                                            	<?php		
                                                                            		}else{
                                                                            	?>
                                                                            			<a title="<?php echo $items->title?>" target="_blank"  href="<?=$myArray['urla']?>"> 
                                                                               				<strong style="color: #ee3928;">Newsletters : </strong><strong style="font-size: 32px;"><?php echo YTools::truncate(strip_tags($items->introtext),$options->item_title_max_characs);?></strong>
                                                                   						</a>
                                                                            	<?php		
                                                                            		}
                                                                            	?>
                                                                               
                                                                            </span>
                                                                        </h2>

                                                                        <?php if ($options->item_desc_display == 1 ){?>

																				<?php
                                                                            		if(strip_tags($items->fulltext) == '' || strip_tags($items->fulltext) == null){
                                                                            	?>
                                                                            			<p style="margin-left: 0rem;">
				                                                                            <span class="_txt_news_dec" style="color: #808080; font-size: 20px; letter-spacing: 1px; line-height: 32px;">
				                                                                                <?php echo Ytools::truncate(strip_tags($items->introtext),$options->item_desc_max_characs);?>
				                                                                            </span>
				                                                                        </p>
                                                                            	<?php		
                                                                            		}else{
                                                                            	?>
                                                                            			<p style="margin-left: 0rem;">
				                                                                            <span class="_txt_news_dec" style="color: #808080; font-size: 20px; letter-spacing: 1px; line-height: 32px;">
				                                                                                <?php echo Ytools::truncate(strip_tags($items->fulltext),$options->item_desc_max_characs);?>
				                                                                            </span>
				                                                                        </p>
                                                                            	<?php		
                                                                            		}
                                                                            	?>
                                                                            	
                                                                        <?php } ?>

                                                                        <?php if( $options->item_date_display==1 || $options->cat_title_display==1 ){?>
                                                                            <p>
                                                                           <?php if($options->cat_title_display==1) {?>
                                                                                <span class="cattitle"><?php echo $items->catitle; ?> </span>
                                                                           <?php } ?>
                                                                           <?php if ($options->item_date_display == 1):?>
                                                                                <span class="basic-date"><?php echo $items->created?></span>
                                                                            <?php endif; ?>
                                                                           </p>        
                                                                        <?php } ?>
                                                                        <a class="je_dropbtn active" href="<?=$myArray['urla']?>" target="_blank" ><span class="je_dropbtn-inner text_login_top" style="background: transparent; color: #ee3928; font-weight: 600; float: right; border-color: #ee3928; line-height: 23px; margin-top: 21px;">Read more</span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                 </div>
                                    
                             
	  </div>
	  <?php
	  	} // end section Newsletters
	  ?>
	 
	 
	  	
	  	
	  	
	  
	   							</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
	<?php if ( !empty($options->posttext)){ ?>
         <div class="bsn-posttext"><?php echo $options->posttext; ?></div>
    <?php } ?>
<?php  } else { ?>
<p>Has no connect to show!</p>
<?php } ?>


