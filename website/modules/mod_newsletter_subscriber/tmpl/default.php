<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<section class="kindergarden " style="background-color: #1279bf;">
    <div class="kindergardenFacilities" style="background-color: #1279bf;">
        <div class="container">
            <div class="row" style="margin-left: 0px !important;">
                <div class="contentFacilities">
                	 <div class="form-inline" style="text-align: center;">
                        <h1 class="form-group text_head_subscribe" style="margin-top: 0;text-align: center; font-weight: 800;text-transform: uppercase;" data-position="left"><span style="color: #ffffff;">Subscribe to our newsletter</span></h1>
                        <div class="form-inline" style="text-align: center; color: #ffffff;  padding-top: 0;">
                        	<h2 style="text-align: center; color: #ffffff;" id="txt_sub_to_new">Subscribe to Eisau's newsletter. Insert your email address below.</h2>
                        </div>
                    </div>
					<div class="modns">
					  <?php if ($myError != "") { print '<div style="text-align:center;    padding-bottom: 14px;" class="modns_error">'.$myError.'</div>'; } ?>
					  <form method="post" name="form-id-subscrib" id="form-id-subscrib" <?php print $action; ?>>
					    <?php if ($params->get('pre_text', '') != '') { ?>
					      <div class="modnsintro"><?php print $params->get('pre_text', ''); ?></div>
					    <?php } ?>
					
					    <div class="row">
						      <?php if ($anti_spam_position == 0) { // Anti-Spam Before ?>
						        <div class="input-group"><?php print $anti_spam_field; ?></div>
						      <?php } ?>
								
							<div class="col-sm-12 col-xs-12">	
							  <div class="input-group col-sm-6 col-sm-offset-3 col-xs-12" id="textbox-responsive">
						  			<input style="    margin-bottom: 15px;"
						        	   class=" inputbox form-control <?php print $mod_class_suffix;?>" type="email"
						               name="m_email<?php print $unique_id; ?>" id="m_email<?php print $unique_id; ?>"
						               placeholder="<?php print $emailPlaceholder;?>"
						               size="<?php print $emailWidth; ?>" <?php print $email_value;?>/>
						      </div>
						      
						      <div class="input-group input-group col-sm-6 col-sm-offset-3 col-xs-12">
						      			<input 
						        	   class=" inputbox form-control <?php print $mod_class_suffix;?>" type="text"
						               name="m_name<?php print $unique_id; ?>" id="m_name<?php print $unique_id; ?>"
						               placeholder="<?php print $namePlaceholder;?>"
						               size="<?php print $nameWidth; ?>" <?php print $name_value;?>/>
						      </div>
						    </div>
						
						      
						      <?php if ($anti_spam_position == 1) { // Anti-Spam After ?>
						        <div class="input-group"><?php print $anti_spam_field; ?></div>
						      <?php } ?>
						
						<div class="input-group col-sm-12 col-xs-12" id="button_sub_news">
 <div class="input-group col-sm-6 col-sm-offset-3 col-xs-12" style=" text-align: center;   left: 1%;" >
									    <span onclick="document.forms['form-id-subscrib'].submit();" class="je_dropbtn-inner" style="border-radius: 5px;border-width: 1px;border-style: solid solid solid solid;border-color: #fff;text-transform: uppercase;cursor: pointer;background: #f1f1f1 !important;color: #424242;font-weight: 600;padding: 11px !important;">Subscribe&nbsp;&nbsp;
											<i class="fa fa-caret-right" aria-hidden="true"></i>
										</span>

											    <i class="fa fa-envelope" id="" aria-hidden="true" style="color: #f1f1f1;margin-left: 11px;font-size: 51.7px;/* margin-top: -0.9px !important; */position: relative;top: 12px;"></i>
											</div>
                        		</div>
					
					    </div>
					    <input type="hidden" name="modns_uniqid" value="<?php print $unique_id; ?>"/>
					  </form>
					</div>
				</div>
            <!--end contentFacilit -->
        </div>
    </div>
</section>
<style type="text/css">
@media screen and (max-width: 767px){
	.col-xs-12{
      width: 100% ;

	}
}

@media screen and (max-width: 416px){
		.text_head_subscribe{
			line-height: 35px;
		}
	}
@media screen and (max-width: 414px){
	h2#txt_sub_to_new {
    	padding-top: 20px;
		}
}
@media screen and (max-width: 360px){
	h2#txt_sub_to_new{
		padding-bottom: 20px;
	}

}


</style>
