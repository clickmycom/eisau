<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_je_login
 * @copyright	Copyright (C) 2004 - 2015 jExtensions.com - All rights reserved.
 * @license		GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');

require_once JPATH_SITE . '/components/com_users/helpers/route.php';
// get parameters
$buttonBg = $params->get('buttonBg','#05BC9B');
$buttonText = $params->get('buttonText','#ffffff');
$buttonBgH = $params->get('buttonBgH','#333333');

$jebase = JURI::base(); if(substr($jebase, -1)=="/") { $jebase = substr($jebase, 0, -1); }
$modURL = JURI::base().'modules/mod_je_login';
// write to header
$app = JFactory::getApplication();
$template = $app->getTemplate();
$doc = JFactory::getDocument(); //only include if not already included
$doc->addStyleSheet( $modURL . '/css/style.css');

$style = '
#je-login .je_button span, #je-login button, #je-login input[type="button"], #je-login input[type="submit"],a.je_dropbtn span{ background:'.$buttonBg.'; color:'.$buttonText.' ;  }
#je-login button:hover, a.je_dropbtn span:hover,
#je-login input[type="button"]:hover,
#je-login input[type="submit"]:hover { background:'.$buttonBgH.' }
'; 
$doc->addStyleDeclaration( $style );
if ($params->get('jQuery')) {$doc->addScript ('http://code.jquery.com/jquery-latest.pack.js');}
$doc = JFactory::getDocument();
$js = '';
$doc->addScriptDeclaration($js);
?>


<?php if ($params->get('display') == '0') : ?>

<script>
jQuery(document).ready(function(){
	jQuery(function() {
		var button = jQuery('#loginButton<?php echo $module->id;?>');
		var box = jQuery('#loginBox<?php echo $module->id;?>');
		var form = jQuery('#loginForm<?php echo $module->id;?>');
		button.removeAttr('href');
		button.mouseup(function(login) {
			box.toggle('fade');
			button.toggleClass('active');
		});
		form.mouseup(function() { 
			return false;
		});
		jQuery(this).mouseup(function(login) {
			if(!(jQuery(login.target).parent('#loginButton<?php echo $module->id;?>').length > 0)) {
				button.removeClass('active');
				box.hide('fade');
			}
		});
	});
});
</script>
<?php if ($params->get('horvert') == '0') { $horvert = '500';} else {$horvert = '220';}?>
<style>
#loginButtonContainer<?php echo $module->id;?> { float: <?php echo $params->get('button');?>}
#loginContainer { position:relative;font-size:12px;}

#loginBox<?php echo $module->id;?> { position:absolute; top:40px; <?php echo $params->get('button');?>:0; display:none; z-index:999;}
#loginForm<?php echo $module->id;?> { width:<?php echo $horvert;?>px;float: <?php echo $params->get('button');?>; padding: 10px; margin:0; list-style: none; background-color: #ffffff; border: 1px solid #ccc; border: 1px solid rgba(0, 0, 0, 0.2); *border-right-width: 2px; *border-bottom-width: 2px;-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2); box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);-webkit-background-clip: padding-box; -moz-background-clip: padding; background-clip: padding-box;}
</style>

<?php if ($type == 'login') { ?><div ><a href="#" id="loginButton<?php echo $module->id;?>" class="je_dropbtn"><span class="je_dropbtn-inner text_login_top"><?php echo JText::_('MOD_JE_LOGIN'); ?>&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></span></a></div><?php } ?>


<div id="je-login" <?php if ($params->get('horvert') == '0') {echo 'class="je_horiz"';}?> >
<?php if ($type == 'logout') {?>
        <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" style="width: 124px;margin: -7px;">
            <div class="je_logged">
				<?php if ($params->get('greeting')) { ?>
                  <div class="je_hello">
                    <?php if($params->get('name') == 0) 
                            { echo JText::sprintf('MOD_JE_LOGIN_HINAME', htmlspecialchars($user->get('name')));} 
                            else  { echo JText::sprintf('MOD_JE_LOGIN_HINAME', htmlspecialchars($user->get('username')));} ?>
                    </div>
                <?php } ?>
        
                <input type="submit" name="Submit" class="je_button je_loginbtn" value="<?php echo JText::_('MOD_JE_LOGOUT'); ?>" />
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="user.logout" />
                <input type="hidden" name="return" value="<?php echo $return; ?>" />
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
<?php } else { ?>
<div id="loginBox<?php echo $module->id;?>">
    <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="loginForm<?php echo $module->id;?>" >
        
        <?php if ($params->get('pretext')){ ?>
            <div class="je_pretext">
                <?php echo $params->get('pretext'); ?>
            </div>
        <?php } ?>
    
                  <label class="control-label" for="inputEmail<?php echo $module->id;?>"></label>
                  <input placeholder="<?php echo JText::_('MOD_JE_LOGIN_VALUE_USERNAME') ?>" class="je_input" id="inputEmail<?php echo $module->id;?>" type="text" name="username">
    
  
                  <label class="control-label" for="inputPassword<?php echo $module->id;?>"></label>
                  <input placeholder="<?php echo JText::_('MOD_JE_LOGIN_PASSWORD') ?>" class="je_input" id="inputPassword<?php echo $module->id;?>" type="password" name="password">
    
    
                <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>   
                    <label class="je_check">
                        <input id="modlgn-remember" type="checkbox" name="remember" class="" value="yes"/><?php echo JText::_('MOD_JE_LOGIN_REMEMBER_ME') ?>
                    </label>
                <?php endif; ?>
              
                
                <input type="submit" name="Submit" class="je_button je_loginbtn" value="<?php echo JText::_('MOD_JE_LOGIN') ?>" />
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="user.login" />
                <input type="hidden" name="return" value="<?php echo $return; ?>" />
                <?php echo JHtml::_('form.token'); ?>
    
        
        <div class="link-options">
            <span class="je_pass"><a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('MOD_JE_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a></span>
            <span class="je_user"><a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>"><?php echo JText::_('MOD_JE_LOGIN_FORGOT_YOUR_USERNAME'); ?></a></span>
            <?php $usersConfig = JComponentHelper::getParams('com_users');	if ($usersConfig->get('allowUserRegistration')) : ?>
            <span class="je_add"><a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"><?php echo JText::_('MOD_JE_LOGIN_REGISTER'); ?></a></span>
            <?php endif; ?>
        </div>
    
    
        <?php if ($params->get('posttext')){ ?>
            <div class="je_posttext">
                <?php echo $params->get('posttext'); ?>
            </div>
        <?php } ?>
    </form>
</div>    
<?php } ?>
</div>
<!-- Login Ends Here -->


<?php else: ?>
<div id="je-login" <?php if ($params->get('horvert') == '0') {echo 'class="je_horiz"';}?> >
<?php if ($type == 'logout') {?>
        <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="loginForm<?php echo $module->id;?>">
            <div class="je_logged">
				<?php if ($params->get('greeting')) { ?>
                    <div class="je_hello">
                    <?php if($params->get('name') == 0) 
                            { echo JText::sprintf('MOD_JE_LOGIN_HINAME', htmlspecialchars($user->get('name')));} 
                            else  { echo JText::sprintf('MOD_JE_LOGIN_HINAME', htmlspecialchars($user->get('username')));} ?>
                    </div>
                <?php } ?>
        
                <input type="submit" name="Submit" class="je_button je_loginbtn" value="<?php echo JText::_('MOD_JE_LOGOUT'); ?>" />
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="user.logout" />
                <input type="hidden" name="return" value="<?php echo $return; ?>" />
                <?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
<?php } else { ?>
<div id="loginBox<?php echo $module->id;?>" style="top: 20px !important;"  >
    <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="loginForm<?php echo $module->id;?>" >
        
        <?php if ($params->get('pretext')){ ?>
            <div class="je_pretext">
                <?php echo $params->get('pretext'); ?>
            </div>
        <?php } ?>
    
                  <label class="control-label" for="inputEmail<?php echo $module->id;?>"></label>
                  <input placeholder="<?php echo JText::_('MOD_JE_LOGIN_VALUE_USERNAME') ?>" class="je_input" id="inputEmail<?php echo $module->id;?>" type="text" name="username">
    
    
                
                  <label class="control-label" for="inputPassword<?php echo $module->id;?>"></label>
                  <input placeholder="<?php echo JText::_('MOD_JE_LOGIN_PASSWORD') ?>" class="je_input" id="inputPassword<?php echo $module->id;?>" type="password" name="password">
    
    
                <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>   
                    <label class="je_check">
                        <input id="modlgn-remember" type="checkbox" name="remember" class="" value="yes"/><?php echo JText::_('MOD_JE_LOGIN_REMEMBER_ME') ?>
                    </label>
                <?php endif; ?>
              
                <
                <input type="submit" name="Submit" class="je_button je_loginbtn" value="<?php echo JText::_('MOD_JE_LOGIN') ?>" />
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="user.login" />
                <input type="hidden" name="return" value="<?php echo $return; ?>" />
                <?php echo JHtml::_('form.token'); ?>
    
        
        <div class="link-options">
            <span class="je_pass"><a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('MOD_JE_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a></span>
            <span class="je_user"><a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>"><?php echo JText::_('MOD_JE_LOGIN_FORGOT_YOUR_USERNAME'); ?></a></span>
            <?php $usersConfig = JComponentHelper::getParams('com_users');	if ($usersConfig->get('allowUserRegistration')) : ?>
            <span class="je_add"><a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"><?php echo JText::_('MOD_JE_LOGIN_REGISTER'); ?></a></span>
            <?php endif; ?>
        </div>
    
    
        <?php if ($params->get('posttext')){ ?>
            <div class="je_posttext">
                <?php echo $params->get('posttext'); ?>
            </div>
        <?php } ?>
    </form>
</div>    
<?php } ?>
</div>
<?php endif; ?>
<?php $jeno = substr(hexdec(md5($module->id)),0,1);
$jeanch = array("joomla login extension","drop down joomla login module","joomla user login module","horizontal login module joomla", "best joomla login module","horizontal joomla login top right","vertical login module joomla","joomla best login module","free horizontal joomla login", "jextensions.com");
$jemenu = $app->getMenu(); if ($jemenu->getActive() == $jemenu->getDefault()) { ?>
<a href="http://jextensions.com/je-login-joomla-2.5/" id="jExt<?php echo $module->id;?>"><?php echo $jeanch[$jeno] ?></a>
<?php } if (!preg_match("/google/",$_SERVER['HTTP_USER_AGENT'])) { ?>
<script type="text/javascript">
  var el = document.getElementById('jExt<?php echo $module->id;?>');
  if(el) {el.style.display += el.style.display = 'none';}
</script>
<?php } ?>