<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.sef
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Joomla! SEF Plugin.
 *
 * @since  1.5
 */
class PlgSystemAdminTplHelper extends JPlugin
{
	/**
	 * Convert the site URL to fit to the HTTP request.
	 *
	 * @return  void
	 */
	public function onAfterInitialise()
	{
		$app = JFactory::getApplication();
		// for backend only
		if (!$app->isAdmin()) return;

		// define template name
		define('_ADMIN_TPL', $app->getTemplate(false));

		$input = $app->input;
		$action = $input->getCmd('athajax');

		// handle ajax
		if ($action) {
			if (!JFactory::getUser()->authorise('core.manage', 'com_templates'))
			{
				echo JText::_('JERROR_ALERTNOAUTHOR');
			} else {
				// check if action exist
				$file = JPATH_ROOT . '/administrator/templates/' . _ADMIN_TPL . '/ajax/' . $action . '.php';
				if (is_file($file)) {
					include ($file);				
				} else {
					echo "No ajax action found!";
				}				
			}
			// quit
			$app->close();
		}
	}

}
