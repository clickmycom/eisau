<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


?>
<?php
// for a single variable
$session = JFactory::getSession();

$cars_value=array();
$bolg_value=array();

if($session->get('this_category_search') == "Eisau News"){
		foreach ($this->results as $result){
			if($result->section == "Eisau News"){
				array_push($cars_value,$result);
			}
				
		}
}
if($session->get('this_category_search') == "Blog_Search"){
		foreach ($this->results as $result){
			if($result->section == "People Interested in Education" || $result->section == "For Suppliers to Schools"){
				array_push($bolg_value,$result);
			}
		}
}

if(!empty($cars_value)){
?>
				
				<?php foreach ($cars_value as $result) : ?>
					<dt class="result-title">
						<!-- <?php echo $this->pagination->limitstart + $result->count . '. ';?> -->
						<?php if ($result->href) :?>
							<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
								<?php echo $this->escape($result->title);?>
							</a>
						<?php else:?>
							<?php echo $this->escape($result->title);?>
						<?php endif; ?>
					</dt>
					<?php if ($result->section) : ?>
						<dd class="result-category">
							<span class="small<?php echo $this->pageclass_sfx; ?>">
								(<?php echo $this->escape($result->section); ?>)
							</span>
						</dd>
					<?php endif; ?>
					<dd class="result-text">
						<?php echo $result->text; ?>
					</dd>
					<?php if ($this->params->get('show_date')) : ?>
						<dd class="result-created<?php echo $this->pageclass_sfx; ?>">
							<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
						</dd>
					<?php endif; ?>
				<?php endforeach; ?>
				
				
				
<?php	
}else if(!empty($bolg_value)){
?>
	
	<?php foreach ($bolg_value as $result) : ?>
					<dt class="result-title">
						<!-- <?php echo $this->pagination->limitstart + $result->count . '. ';?> -->
						<?php if ($result->href) :?>
							<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
								<?php echo $this->escape($result->title);?>
							</a>
						<?php else:?>
							<?php echo $this->escape($result->title);?>
						<?php endif; ?>
					</dt>
					<?php if ($result->section) : ?>
						<dd class="result-category">
							<span class="small<?php echo $this->pageclass_sfx; ?>">
								(<?php echo $this->escape($result->section); ?>)
							</span>
						</dd>
					<?php endif; ?>
					<dd class="result-text">
						<?php echo $result->text; ?>
					</dd>
					<?php if ($this->params->get('show_date')) : ?>
						<dd class="result-created<?php echo $this->pageclass_sfx; ?>">
							<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
						</dd>
					<?php endif; ?>
	<?php endforeach; ?>
			

<?php	
}else{
?>
<dl class="search-results<?php echo $this->pageclass_sfx; ?>">
<?php foreach ($this->results as $result) : ?>
	<dt class="result-title">
		<!-- <?php echo $this->pagination->limitstart + $result->count . '. ';?> -->
		<?php if ($result->href) :?>
			<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
				<?php echo $this->escape($result->title);?>
			</a>
		<?php else:?>
			<?php echo $this->escape($result->title);?>
		<?php endif; ?>
	</dt>
	<?php if ($result->section) : ?>
		<dd class="result-category">
			<span class="small<?php echo $this->pageclass_sfx; ?>">
				(<?php echo $this->escape($result->section); ?>)
			</span>
		</dd>
	<?php endif; ?>
	<dd class="result-text">
		<?php echo $result->text; ?>
	</dd>
	<?php if ($this->params->get('show_date')) : ?>
		<dd class="result-created<?php echo $this->pageclass_sfx; ?>">
			<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
		</dd>
	<?php endif; ?>
	
<?php endforeach; ?>
</dl>

<div class="pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
<?php	
}
?>

<?php
	$session->clear('this_category_search');
?>

