<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
?>

<style>
#system-message-container{
	    width: 97%;
}
	input[type="text"], input[type="email"],input[type="password"],select,.form-control{
		width:auto !important;
		    height: 19px;
    padding: 6px 12px;
    font-size: 15px;
        background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 0;
	}
</style>

<div class="remind<?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>

	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=remind.remind'); ?>" method="post" class="form-validate form-horizontal ">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
		<fieldset>
			<p><?php echo JText::_($fieldset->label); ?></p>
			<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</fieldset>
		<?php endforeach; ?>
		<br>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JSUBMIT'); ?></button>
				
			</div>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
<style type="text/css">
	input[type="text"], input[type="email"], input[type="password"], select, .form-control{
			height: 34px;
	}
    .form-horizontal .controls{
    	margin-left: 120px;
    }
	.form-horizontal .control-group{
		margin-bottom: 0;
	}
	@media (max-width: 480px){
    .form-horizontal .controls{
    	margin-left: 0;
      }
	}
	@media (max-width: 380px){
	input[type="text"], input[type="email"], input[type="password"], select, .form-control{
		width: 100% !important;
	}
	}
</style>
