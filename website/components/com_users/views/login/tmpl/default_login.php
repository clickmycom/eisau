

<?php /** * @package Joomla.Site * @subpackage com_users * * @copyright Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved. * @license GNU General Public License version 2 or later; see LICENSE.txt */ defined( '_JEXEC') or die; JHtml::_( 'behavior.keepalive'); JHtml::_( 'behavior.formvalidator'); ?>

<div style="padding-top:2%;">
    <h3 class="login_h3" style="    text-align: center;
    font-weight: 900;
    color: #1d68a8; ">LOGIN</h3>
    <h4 style="text-align: center;padding: 18px; ">In order to create a list of profiles you must either sign in or create a free account</h4>

    <div id="home" class="tab-pane fade in active">

        <div class="col-lg-12 login<?php echo $this->pageclass_sfx; ?>">

            <?php if ($this->params->get('show_page_heading')) : ?>
            <div class="page-header">
                <h1>
            <?php echo $this->escape($this->params->get('page_heading')); ?>
          </h1>
            </div>
            <?php endif; ?>

            <?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
            <div class="login-description">
                <?php endif; ?>

                <?php if ($this->params->get('logindescription_show') == 1) : ?>
                <?php echo $this->params->get('login_description'); ?>
                <?php endif; ?>

                <?php if (($this->params->get('login_image') != '')) :?>
                <img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JText::_('COM_USERS_LOGIN_IMAGE_ALT')?>" />
                <?php endif; ?>

                <?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
            </div>
            <?php endif; ?>

            <form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate form-horizontal ">

                <?php $i=0 ; ?>
                <?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
                <?php if (!$field->hidden) : ?>

                <div class="control-group">
                    <div class="control-label col-sm-4">
                        <label id="check_bo_re" style="color: #000;margin-right: 8px;font-size: 15px;    padding: 0px 13px 0px 13px;">
                            <?php if($i==0){ echo "Email Address/ Username <span class='star'>&nbsp;*</span>"; }else{ echo $field->label; } ?>

                        </label>
                    </div>
                    <div class="controls col-sm-6 col-xs-12" style="margin-left:0;">
                        <?php echo $field->input; ?>
                    </div>
                </div>
                <?php $i++; ?>
                <?php endif; ?>
                <?php endforeach; ?>

                <?php if ($this->tfa): ?>
                <div class="control-group">
                    <div class="control-label col-sm-4">
                        <?php echo $this->form->getField('secretkey')->label; ?>
                    </div>
                    <div class="controls col-sm-6 col-xs-12" style="margin-left:0;">
                        <?php echo $this->form->getField('secretkey')->input; ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php if (JPluginHelper::isEnabled( 'system', 'remember')) : ?>
                <div class="control-group">
                    <div class="control-label col-sm-4" id="txt_checkbox">
                        <label id="check_bo_re" style="color: #000;margin-right: 8px;font-size: 15px;padding: 0px 13px 13px 13px;">
                            <?php echo JText::_( 'COM_USERS_LOGIN_REMEMBER_ME') ?>
                        </label>
                    </div>
                    <div class="controls col-sm-6 col-xs-12" id="icon_remem" >
                        <input id="remember" type="checkbox" name="remember" class="inputbox"  value="yes" />
                    </div>
                </div>
                <?php endif; ?>

               
                <div class="control-group">

                    <div class="control-label col-sm-4 col-xs-3" >
                        <label id="check_bo_re" style="color: #000;margin-right: 8px;margin-left: 50px;margin-top: -5px;    font-size: 15px;padding: 0px 13px 13px 13px;">
                            
                        </label>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12" >
                        <button type="submit" class="btn make_bottom col-md-3 col-xs-12">
                            <i class="fa fa-sign-in" aria-hidden="true"></i> <?php echo JText::_( 'JLOGIN'); ?>
                        </button>
                    </div>
                </div>

                <?php if ($this->params->get('login_redirect_url')) : ?>
                <input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
                <?php else : ?>
                <input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_menuitem', $this->form->getValue('return'))); ?>" />
                <?php endif; ?>
                <?php echo JHtml::_( 'form.token'); ?>


            </form>

        </div>

    </div>

</div>

        <div class="control-label col-sm-4 col-xs-4">
            <label id="check_bo_re" style="color: #000;margin-right: 8px;margin-left: 50px;margin-top: -5px;    font-size: 15px;padding: 0px 13px 13px 13px;">
                
            </label>
        </div>

        <div class="control-label col-sm-3 col-lg-2 col-xs-6 col-lg-offset-0 col-xs-offset-3"  id="txt_user_login" style="padding-bottom: 1%;padding-top: 1.5%;">
            <a style="" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
                <?php echo JText::_( 'COM_USERS_LOGIN_RESET'); ?>
            </a>
        </div>
    
        <div class="control-label col-sm-3 col-lg-2 col-xs-6 col-lg-offset-0 col-xs-offset-4" style="padding-bottom: 1%;padding-top: 1.5%;">
            <a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
                <?php echo JText::_( 'COM_USERS_LOGIN_REMIND'); ?>
            </a>
        </div>
        <div class="control-label col-sm-3 col-lg-2 col-xs-6 col-lg-offset-0 col-xs-offset-4" id="not_reg" style="padding-top: 1.5%;padding-bottom: 4%;">
            <a href="<?php echo JUri::root(true); ?>/index.php/schools/register">Not Registered?</a>
        </div>
       

<style type="text/css">
	

    #username-lbl {
        font-size: 15px;
        padding-top: 6px;
    }
    #password-lbl {
        font-size: 15px;
        padding-top: 6px;
    }
    .login-banner {
        background: #006eb3;
        background: -moz-linear-gradient(top, #006eb3 0, #005e99 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #006eb3), color-stop(100%, #005e99));
        background: -webkit-linear-gradient(top, #006eb3 0, #005e99 100%);
        background: -o-linear-gradient(top, #006eb3 0, #005e99 100%);
        background: -ms-linear-gradient(top, #006eb3 0, #005e99 100%);
        background: linear-gradient(to bottom, #006eb3 0, #005e99 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#006eb3', endColorstr='#005e99', GradientType=0);
    }
    .tabs a.current {
        background: rgba(255, 255, 255, .3);
        color: #666;
        cursor: default;
    }
    .tabs a {
        display: block;
        padding: 20px;
        background: rgba(255, 255, 255, .3);
        border-radius: 4px 27px 0 0;
        -moz-border-radius: 4px 27px 0 0;
        margin: 20px 6px 0 0;
        color: #fff;
        font-size: 21px;
    }
    .tabs {
        position: absolute;
        : 20px;
        margin-top: 6px;
        margin-bottom: 0;
    }
    .tabs li {
        float: left;
        margin-top: 46px;
    }
    .tabs a {
        display: block;
        padding: 20px;
        background: rgba(255, 255, 255, .3);
        border-radius: 4px 27px 0 0;
        -moz-border-radius: 4px 27px 0 0;
        margin: 20px 6px 0 0;
        color: #fff;
        font-size: 21px
    }
    .tabs a.current {
        background: rgba(255, 255, 255, .3);
        color: #fff;
        cursor: default
    }
    .tabs a.current:focus,
    .tabs a.current:hover {
        color: #666
    }
    .tabs a:focus,
    .tabs a:hover {
        background: #fff;
        color: #007ecc;
        text-decoration: none
    }
    .tabs .nav > li > a:hover,
    .nav > li > a:focus {
        text-decoration: none;
        background-color: #fff !important;
    }
    .login-banner {
        height: 130px
    }
    .modify_login {
        background-color: #404040 !important;
    }
    .login_h3 {
        font-size: 26px;
        color: #000;
    }
    .tab_active {
        background: #fff !important;
        color: #666 !important;
    }
    #username,
    #password,
    .f-pass,
    .f-pass2 {
        font-size: 17px;
        line-height: 46px;
        height: 30px;
        
        background-color: #f8f9fa;
        margin-bottom: 6px;
        border: 1px solid #5b5b5b;
        color: #0c0c0c;
    }
    #username {
        /*background: url("https://www.tppwholesale.com.au/images/icons/f-name.png") no-repeat scroll 0px 17px  #464646!important;*/
        /*background: url('../../../images/form/support_man.png') no-repeat scroll 16px 10px #f1f2f2 !important;*/
        
        background-size: 22px !important;
    }
    #password {
        /*background: url('../../../images/form/key_alt.png') no-repeat scroll 17px 11px #f1f2f2 !important;*/
        
        background-size: 21px !important;
    }
    .make_bottom {

        border-radius: 20px;
        -moz-border-radius: 4px 30px 30px 4px;
        padding: 10px 21px 10px 16px;
        color: #fff;
        font-size: 17px;
        font-weight: 400;
        clear: both;
        float: left;
        border: 0;
        cursor: pointer;
        background: #1279bf;
        border-width: 0;
        color: #fff;
    }

    .make_bottom:hover{
         background: #ee3928;
    }

    .f_k a {
        color: #FF5722 !important;
    }
    .form-horizontal .control-group {
        margin-bottom: 7px !important;
    }

    .yahoo_a {
        padding: 20px 0px 20px 20px !important;
    }
    .yahoo {
        background-size: cover;
        background-position: 50% 99%;
        background-image: url('../templates/protostar/css/_bg-1.jpg') !important;
    }
    #system-message{
            padding: 3%;
    padding-bottom: 0;

    }

    .form-horizontal .controls {
        margin-left: 0px;
    }


    @media(min-width: 771px){
        #txt_user_login{
            margin-left: 0;
        }
    }

    @media (max-width: 768px){
        body {
            padding-left: 0px;
            padding-right: 0px;
        }
         #txt_user_login{
            margin-left: 0;
        }
         #txt_checkbox{
            text-align: center;
        }
       

    }

    @media (max-width: 425px){
        #not_reg{
            padding-bottom:4%;
        }
       
        .loginbtn_login_tk{
            padding: 4%;
        }
        #txt_checkbox{
            text-align: center;
        }
        #icon_remem{
            text-align: center;
        }
    }

span.star {
    color: #ee3928;
}

#wel_not_home{
    display: none;
}
</style>



<script>
    jQuery(document).ready(function() {
        setTimeout(function(){ jQuery("#content").removeClass("yahoo_a col-lg-9").addClass("col-lg-12"); },100);

    });
</script>