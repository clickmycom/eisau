<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');
?>
<div class="blog<?php echo $this->pageclass_sfx; ?> col-md-8 col-lg-12 col-sm-8 col-xs-12" itemscope itemtype="https://schema.org/Blog">
	
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1> <?php echo $this->escape($this->params->get('page_heading')); ?> </h1>
		</div>
	<?php endif; ?>

	<?php if ($this->params->get('show_category_title', 1) or $this->params->get('page_subheading')) : ?>
		
		<h2> <?php echo $this->escape($this->params->get('page_subheading')); ?>
			<?php if ($this->params->get('show_category_title')) : ?>
				<span class="subheading-category"><?php echo $this->category->title; ?></span>
			<?php endif; ?>
		</h2>
	<?php endif; ?>

	<?php if ($this->params->get('show_cat_tags', 1) && !empty($this->category->tags->itemTags)) : ?>
		<?php $this->category->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
		<?php echo $this->category->tagLayout->render($this->category->tags->itemTags); ?>
	<?php endif; ?>

	<?php if ($this->params->get('show_description', 1) || $this->params->def('show_description_image', 1)) : ?>
		<div class="category-desc clearfix">
			<?php if ($this->params->get('show_description_image') && $this->category->getParams()->get('image')) : ?>
				<img src="<?php echo $this->category->getParams()->get('image'); ?>" alt="<?php echo htmlspecialchars($this->category->getParams()->get('image_alt'), ENT_COMPAT, 'UTF-8'); ?>"/>
			<?php endif; ?>
			<?php if ($this->params->get('show_description') && $this->category->description) : ?>
				<?php echo JHtml::_('content.prepare', $this->category->description, '', 'com_content.category'); ?>
			<?php endif; ?>
			<hr style="color:red">
		</div>
	<?php endif; ?>

	<?php if (empty($this->lead_items) && empty($this->link_items) && empty($this->intro_items)) : ?>
		<?php if ($this->params->get('show_no_articles', 1)) : ?>
			<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
		<?php endif; ?>
	<?php endif; ?>

	<?php $leadingcount = 0; ?>
	<?php
		//print_r($this->lead_items);
		//exit;
	?>
	
	
	<?php if (!empty($this->lead_items)) : ?>
		<div class="items-leading clearfix">
			<?php foreach ($this->lead_items as &$item) : ?>
				<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
					itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting">
					<?php
					$this->item = & $item;
					echo $this->loadTemplate('item');
					?>
				</div>
				<?php $leadingcount++; ?>
			<?php endforeach; ?>
		</div><!-- end items-leading -->
	<?php endif; ?>

	<?php
	$introcount = (count($this->intro_items));
	$counter = 0;
	?>

	<?php if (!empty($this->intro_items)) : ?>
		
		<?php foreach ($this->intro_items as $key => &$item) : ?>
			
			<?php $rowcount = ((int) $key % (int) $this->columns) + 1; ?>
			<?php if ($rowcount == 1) : ?>
				<?php $row = $counter / $this->columns; ?>
				<div class="tk00_hr items-row cols-<?php echo (int) $this->columns; ?> <?php echo 'row-' . $row; ?> row-fluid clearfix">
			<?php endif; ?>
			<div class=" col-md-12">
				<div class="item column-<?php echo $rowcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
					itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting" style="    padding-top: 12px;">
					<?php
					$this->item = & $item;
					?>
					
					<?php
						$myArray = json_decode($item->urls, true);
						if($myArray['urla'] != null ||$myArray['urla'] == '/'){
					?>
					<h3 class="naruto" style="margin: 0px;">
						<a href="<?=$myArray['urla']?>" alt="<?=$item->title?>" target="_blank"><?=$item->title?></a>
					</h3>
					<?php
						}
					?>
					<?php
					echo $this->loadTemplate('item');
					?>
				</div>
				<!-- end item -->
				<?php $counter++; ?>
			
			</div><!-- end span -->
			
			<?php if (($rowcount == $this->columns) or ($counter == $introcount)) : ?>
				</div><!-- end row -->
			<?php endif; ?>
			
		<?php endforeach; ?>
	<?php endif; ?>


	<?php 
		if (!empty($this->link_items)) : ?>
		<div class="items-more">
			<?php echo $this->loadTemplate('links'); ?>
		</div>
	<?php endif; ?>

	<?php if (!empty($this->children[$this->category->id]) && $this->maxLevel != 0) : ?>
		<div class="cat-children" style="display:none;">
			<?php if ($this->params->get('show_category_heading_title_text', 1) == 1) : ?>
				<h3> <?php echo JText::_('JGLOBAL_SUBCATEGORIES'); ?> </h3>
			<?php endif; ?>
			<?php echo $this->loadTemplate('children'); ?> </div>
	<?php endif; ?>
	<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
		<div class="pagination">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
			<?php endif; ?>
			<?php echo $this->pagination->getPagesLinks(); ?> </div>
	<?php endif; ?>
</div>



<style>
	@media (min-width: 1200px){
		.row-fluid .span3 {
    	width: 22.2%;
}
	
	.yahoo_a{
		padding: 20px;
	    margin-left: 0px !important;
	    margin-top: 4px !important;
	}
	table{
		width:100% !important;
			
	}
	table > thead > tr > td > p{
		display: table-cell !important;
		padding:10px;
		
	}
	
	table > tbody > tr > td > p{
		display: table-cell !important;
		padding:10px;

	}

.thumbnail_a.thumbnail_big {
    margin-bottom: 0;
	}
	.thumbnail_a {
    padding: 0;
    margin-bottom: 20px;
}
.thumbnail_a {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: border .2s ease-in-out;
    -o-transition: border .2s ease-in-out;
    transition: border .2s ease-in-out;
}
.thumbnail_big .thumbnail__caption {
    padding: 18.77px 9px;
}
.thumbnail_a .caption {
    padding: 15px 9px;
    color: #333;
}

.thumbnail_big img {
    border-radius: 6px !important;
    padding: 0px !important;
}
.thumbnail_small img {
	border-radius: 6px !important;
    padding: 0px !important;
}

.thumbnail__caption {
    padding: 9px;
    color: #333;
}
@media (min-width: 768px){
.col-sm-6 {
    width: 45.7%;padding-left: 19px;
 }
.col-sm-3 {
    width: 24.6%;padding-left: 19px;
 }
}


@media (min-width: 1200px){
	.row-fluid .span9 {
	    width: 76.468085% !important;
	}
}


.blog img {
    border-radius: 7px !important;
    padding: 0px !important;
    width: 250px;
    height: 177px;
}
.special__desc {
  font-size: 14px;
  line-height: 1.4;
  font-family: 'Roboto', sans-serif;
  margin-bottom: 0;
}
/* 3.6. END Special */

div[class^='blognewsletters-new-00000'] p,h2{
	display:none;
}

div[class^='bloglast_news0abade'] p[class^='Heading2'] ,h2{
	display:block;
}

.page-header > h2[itemprop^='name'] {
    color: #EC3B33 !important;
    font-weight: 700 !important;
    font-size: 18px;
    margin-top: 4px;
    word-wrap: break-word;
    font-family: "Source Sans Pro",sans-serif;
}

.page-header h2{
	    margin-top: 0px;
	   
}

#sidebar{
	padding: 18px;
}

.tk00_hr{
	border-bottom: 1px solid #eee !important;
}
div[class^='items-row cols-1 row-9 row-fluid clearfix'] > div.tk00_hr{
	border-bottom: 0px solid #eee;
}
@media screen and (max-width: 767px){
	input#mod-search-searchword{
		width: 93%;
	}
}
@media screen and (max-width: 768px){
	.pull-left.item-image{
		padding-left: 0;
		padding-right: 0;
		padding-bottom: 2%;
		margin: auto;
		width: 100%;
        max-width: 100%;

	}
	.blog img{
		width: 100%;
		height: auto; 

	}
}
@media screen and (max-width: 480px){

	.pull-left.item-image{
		padding-left: 0;
		padding-right: 0;
		padding-bottom: 2%;
		margin: auto;
		width: 100%;
        max-width: 100%;

	}
	.blog img{
		width: 100%;
		height: auto;
	}
	
}



</style>                           
          

<script>
jQuery(document).ready(function(){
    jQuery(".readmore .btn").text("Read more");
});
</script>          
