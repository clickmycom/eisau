<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$app       = JFactory::getApplication();
// Create shortcuts to some parameters.
$params = $this->item->params;
$images = json_decode($this->item->images);
$urls = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user = JFactory::getUser();
$info = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');
JHtml::_('bootstrap.tooltip');
jimport( 'joomla.methods' );
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$lang = JFactory::getLanguage();
$doc = JFactory::getDocument();
$meta_description = $doc->getMetaData("keywords");


?>


<div  class="22item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
    <meta itemprop="inLanguage"
          content="<?php echo ($this->item->language === '*') ? JFactory::getConfig()->get('language') : $this->item->language; ?>"/>
    <?php if ($this->params->get('show_page_heading')) : ?>
        <div class="page-header" class="new_article_header">
            <h1> <?php echo $this->escape($this->params->get('page_heading')); ?></h1>
        </div>
    <?php endif;
    if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && $this->item->paginationrelative) {
        echo $this->item->pagination;
    }
    ?>

    <?php // Todo Not that elegant would be nice to group the params ?>
    <?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
        || $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author')); ?>

    <?php if (!$useDefList && $this->print) : ?>
        <div id="pop-print" class="btn hidden-print">
            <?php echo JHtml::_('icon.print_screen', $this->item, $params); ?>
        </div>
        <div class="clearfix"></div>
    <?php endif; ?>
    <?php if ($params->get('show_title') || $params->get('show_author')) : ?>
        <div class="page-header">
            <?php if ($params->get('show_title')) : ?>
                <h1 itemprop="name" class="article-title-single new_article_header" >
                    <?php echo $this->escape($this->item->title); ?>
                </h1>
            <?php endif; ?>
            <?php if ($this->item->state == 0) : ?>
                <span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
            <?php endif; ?>
            <?php if (strtotime($this->item->publish_up) > strtotime(JFactory::getDate())) : ?>
                <span class="label label-warning"><?php echo JText::_('JNOTPUBLISHEDYET'); ?></span>
            <?php endif; ?>
            <?php if ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != JFactory::getDbo()->getNullDate()) : ?>
                <span class="label label-warning"><?php echo JText::_('JEXPIRED'); ?></span>
            <?php endif; ?>
        </div>
    <?php endif; ?>






    <?php
    $GLOBALS['title'] = $this->item->title;


    function getCategoryID()
    {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('id', 'parent_id', 'name', 'alias', 'published')));
        $query->from($db->quoteName('jxkdm_jbusinessdirectory_categories'));
        $query->where($db->quoteName('name') . " = " . $db->quote($GLOBALS['title']));
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results[0]->id;
    }

    function tanawat()
    {
        $id = getCategoryID();

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        //$query->select('*');
        $query->select($db->quoteName(array('id', 'parent_id', 'name', 'alias', 'published')));
        $query->from($db->quoteName('jxkdm_jbusinessdirectory_categories'));
        $query->where($db->quoteName('published') . " = " . '1');
        $query->where($db->quoteName('parent_id') . " = " . $db->quote($id));
        $query->order('name ASC');
        
        $db->setQuery($query);
        return $db->loadObjectList();
    }


    ?>


    <?php
    $show = 1;
    
    if ((!empty(tanawat())) && tanawat()[0]->alias != "root") {
                    
                $wat_sour_array_name = array();
                $wat_array = array();
                foreach (tanawat() as $key => $value) {
                    
                        array_push($wat_array,$value);
                        array_push($wat_sour_array_name,trim($value->name));
                    
                }
                
                
                $count = count($wat_array);
                $col_num = 2;
                $row_num = ceil($count/$col_num);
                
                
                //new Array
                $new_data = array();
                for($i=0;$i<$row_num;$i++){
                    $row = array();
                    $index = $i;
                    for($j=0;$j<$row_num+1;$j++){
                        $row[] = isset($wat_array[$index]) ? $wat_array[$index]: '';
                        $index += $row_num; 
                    }
                    $new_data[] = $row;
                }

    
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        ?>
        <style>
            .page-header{
                display:none !important;
            }
        </style>
        
        <?php
            if(!empty($meta_description)){
        ?>
            <h2 style="margin-top:0px;color:#eb3b32"><strong><?=$meta_description?></strong></h2><br>
        <?php       
            }
        ?>
        
        <table id="main_cat" title="<?=$meta_description?>">
            <thead>
            <tr>
                <th colspan="99" style="font-size: 14px;text-transform: uppercase;"><?php echo $this->escape($this->item->title); ?></th>
            </tr>
            <tr>

                <th style="text-align:center;"></th>
                <th style="padding: 0 0px 0 20px;"></th>
            </tr>
            </thead>
            <tbody>

            <?php
          $i=1; 
                $new_wat_array = array();
                foreach($new_data as $row){
                    if($i>10)
                        break;
                    //echo $i++==1?'':'|';
                    if ($i % 2 != 0) {
                        echo "<tr>";
                    }?>
                    <?php
                        
                        foreach ($row as $data) {
                            if(!empty($data->name)){

                            

                        echo "<td style='background-color:#eeeeee;'>";
                    ?>
                            <a class="categoryLink" style="color: #eb3b32;font-weight: 600;" title="<?php echo $data->name?>" alt="<?php echo $data->name?>" 
                                href="<?= $data->alias ?>">
                                <?php echo $data->name ?>
                            </a>
                    <?php
                    
                        echo "</td>";
                    }
                        }
                
                    ?>
                    
                    <?php
                    if ($i % 2 <> 0) {
                        echo "</tr>";
                    } 
                } 
        
        ?>


            </tbody>
        </table>

        <br>
        <style>
            .page-header h2 {
                display: none;
                padding-bottom: 1px !important;
            }
            input:focus{
        border-color: #e9322d;
    -webkit-box-shadow: 0 0 6px #f8b9b7;
    -moz-box-shadow: 0 0 6px #f8b9b7;
    box-shadow: 0 0 6px #f8b9b7;
        }
        </style>
        <?php
    }
    ?>





    <?php if (!$this->print) : ?>
        <?php if ($canEdit || $params->get('show_print_icon') || $params->get('show_email_icon')) : ?>
            <?php echo JLayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item, 'print' => false)); ?>
        <?php endif; ?>
    <?php else : ?>
        <?php if ($useDefList) : ?>
            <div id="pop-print" class="btn hidden-print">
                <?php echo JHtml::_('icon.print_screen', $this->item, $params); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>


    <?php if ($info == 0 && $params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
        <?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>

        <?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
    <?php endif; ?>



    <?php // Content is generated by content plugin event "onContentAfterTitle" ?>
    <?php if (!$params->get('show_intro')) : echo $this->item->event->afterDisplayTitle; endif; ?>
    <?php // Content is generated by content plugin event "onContentBeforeDisplay" ?>
    <?php echo $this->item->event->beforeDisplayContent; ?>

    <?php if (isset($urls) && ((!empty($urls->urls_position) && ($urls->urls_position == '0')) || ($params->get('urls_position') == '0' && empty($urls->urls_position)))
        || (empty($urls->urls_position) && (!$params->get('urls_position')))
    ) : ?>
        <?php echo $this->loadTemplate('links'); ?>
    <?php endif; ?>


    <?php if ($params->get('access-view')): ?>
        <?php if (isset($images->image_fulltext) && !empty($images->image_fulltext)) : ?>
            <?php $imgfloat = (empty($images->float_fulltext)) ? $params->get('float_fulltext') : $images->float_fulltext; ?>
            <div class="pull-<?php echo htmlspecialchars($imgfloat); ?> item-image"><img
                    <?php if ($images->image_fulltext_caption):
                        echo 'class="caption"' . ' title="' . htmlspecialchars($images->image_fulltext_caption) . '"';
                    endif; ?>
                        src="<?php echo htmlspecialchars($images->image_fulltext); ?>"
                        alt="<?php echo htmlspecialchars($images->image_fulltext_alt); ?>" itemprop="image"/></div>
        <?php endif; ?>
        <?php
        if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && !$this->item->paginationrelative):
            echo $this->item->pagination;
        endif;
        ?>
        <?php if (isset ($this->item->toc)) :
            echo $this->item->toc;
        endif; ?>

        
            <div itemprop="articleBody" class="articleBody">
                <?php echo $this->item->text; ?>
            </div>
       


        
<style>
    dl.article-info.muted.hamomo{
        display:block !important;
    }
</style>
       
        <!-- Details Autor -->
    
        <?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
      
        <hr>
        <p>
            <?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
            </p>
       
        <?php endif; ?>
        
        <!-- End Details Autor -->

        <?php if ($info == 1 || $info == 2) : ?>
            <?php if ($useDefList) : ?>
                <?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
            <?php endif; ?>
            <?php if ($params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
                <?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
                <?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php
        if (!empty($this->item->pagination) && $this->item->pagination && $this->item->paginationposition && !$this->item->paginationrelative):
            echo $this->item->pagination;
            ?>
        <?php endif; ?>
        <?php if (isset($urls) && ((!empty($urls->urls_position) && ($urls->urls_position == '1')) || ($params->get('urls_position') == '1'))) : ?>
            <?php echo $this->loadTemplate('links'); ?>
        <?php endif; ?>
        <?php // Optional teaser intro text for guests ?>
    <?php elseif ($params->get('show_noauth') == true && $user->get('guest')) : ?>
        <?php echo JLayoutHelper::render('joomla.content.intro_image', $this->item); ?>
        <?php echo JHtml::_('content.prepare', $this->item->introtext); ?>
        <?php // Optional link to let them register to see the whole article. ?>
        <?php if ($params->get('show_readmore') && $this->item->fulltext != null) : ?>
            <?php $menu = JFactory::getApplication()->getMenu(); ?>
            <?php $active = $menu->getActive(); ?>
            <?php $itemId = $active->id; ?>
            <?php $link = new JUri(JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false)); ?>
            <?php $link->setVar('return', base64_encode(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language))); ?>
            <p class="readmore">
                <a href="<?php echo $link; ?>" class="register">
                    <?php $attribs = json_decode($this->item->attribs); ?>
                    <?php
                    if ($attribs->alternative_readmore == null) :
                        echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
                    elseif ($readmore = $this->item->alternative_readmore) :
                        echo $readmore;
                        if ($params->get('show_readmore_title', 0) != 0) :
                            echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
                        endif;
                    elseif ($params->get('show_readmore_title', 0) == 0) :
                        echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
                    else :
                        echo JText::_('COM_CONTENT_READ_MORE');
                        echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
                    endif; ?>
                </a>
            </p>
        <?php endif; ?>
    <?php endif; ?>
    <?php
    if (!empty($this->item->pagination) && $this->item->pagination && $this->item->paginationposition && $this->item->paginationrelative) :
        echo $this->item->pagination;
        ?>
    <?php endif; ?>
    <?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
    <?php echo $this->item->event->afterDisplayContent; ?>
</div>


<style>
    div.img_ {
        margin: 5px;
        border: 1px solid #ccc;
        float: left;
        width: 257px;
        text-align: center;
        height: 151px;
        padding: 10px;
    }

    div.img_:hover {
        border: 1px solid #777;
    }

    div.img_ img {

        height: auto;
    }

    div.desc {
        padding: 15px;
        text-align: center;
    }

    #content {
        margin-bottom: 25px;
    }
    
    #txt_sub_artisladklsfj:first-letter {
        text-transform: uppercase !important;
    }
</style>




<?php
// $companies = getDB("jxkdm_jbusinessdirectory_companies","mainSubcategory");


$companies = getDB_All();

$i = 1;
if ($show == 1) {
    if (!empty($companies)) {
        if (strlen($this->item->text) > 1) {
            ?>
            <h2 style=" text-transform: lowercase !important;font-size: 15px; line-height: 1.4em; font-style: normal; font-weight: normal; font-family: Helvetica, sans-serif; color: #0077c0; letter-spacing: 0em; text-transform: none; margin: 0px; clear: both; background-color: transparent; padding: 15px 0px 0px;">
                    <span id="txt_sub_artisladklsfj" style="line-height: 1.8em; color: #f01305;">
                        <label style="display: inline;text-transform:initial;font-size:15px;">F</label>or a full list of
                        <?php
                        if(!empty($meta_description)){
                            echo $meta_description;
                        }else{
                            echo "school " . $this->escape($this->item->title);
                            echo " providers";
                        }
                        ?>, please see below.
                    </span>
            </h2>
            <br>
            <?php
        }
        ?>



<div class="col-lg-12" style="    border-bottom: 1px solid #1279bf;">
    <div class="row" style="background-color:#1279bf; margin-left: 0px;">
        <div class="col-lg-12" style="text-indent: 19px;padding: 15px 0px;font-size: 16px;color: #fff;font-size: 16px;font-weight: 500;margin-top: 8px;line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;margin-top: 10px;">
           <?php echo $this->escape($this->item->title); ?>
            <small style="float:right;padding-right: 23px;" class="result_total">Results: <?= count($companies); ?> items</small>
        </div>
    </div>



     <?php

            foreach ($companies as $index => $company_id) {
                foreach (getCompany() as $key => $company) {
                    if ($company_id->companyId == $company->id) {

    ?>

    <div class="row" style="background-color: #ebebeb;margin-left: 0;border-bottom: 1px solid #1279bf;border-left: 2px solid #1279bf;border-right: 2px solid #1279bf;">
        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 sec_show_de">
           
             <?php  if (!empty($company->logoLocation)) { ?>
                        <a href="<?php echo JBusinessUtil::getCompanyLink($company) ?>"><img
                                    style="padding:30px;height: 102px;border-radius:5px;"
                                    title="<?php echo $company->name ?>" alt="<?php echo $company->name ?>"
                                    src="<?php echo JURI::root() . PICTURES_PATH . $company->logoLocation ?>"/></a>
            <?php   }else { ?>
                        <img style="padding:30px;height: 102px;border-radius:5px;"  title="<?php echo $company->name ?>"
                             alt="<?php echo $company->name ?>"
                             src="<?php echo JURI::root() . PICTURES_PATH . '/no_image.jpg' ?>"/>
            <?php   }?>

        </div>
        <div class="col-lg-5 col-md-5 col-sm-7 col-xs-12 sec_show_de">
            <div style=" padding: 23px 0px; " class="p-b-0">
                <a style="font-size: 16px;font-weight: 600;"
                                   href="<?php echo JBusinessUtil::getCompanyLink($company) ?>">
                                   <span
                                            itemprop="name"> <?php echo $company->name ?> </span></a>
                               

                                <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;    margin-top: 10px;"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;
                                <a href="#" title="<?= $company->street_number ?> <?= $company->address ?> <?= $company->county ?>  <?= $company->postalCode ?>"><?= $company->street_number ?> <?= $company->address ?> <?= $company->county ?>  <?= $company->postalCode ?></a>                             
                                </p>

                                <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;:&nbsp;<a href="tel:<?= $company->phone ?>"><?= $company->phone ?></a>
                                </p>
                                <p style="    line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;margin-top: 10px;"><i class="fa fa-internet-explorer" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<a href="<?= $company->website ?>"><?= $company->website ?></a>
                                </p>
                                <p><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<a href="mailto:<?= $company->email ?>"><?= $company->email ?></a>
                                </p>
            </div>
        </div>



        <div class="col-lg-3 col-md-3 col-sm-7 col-xs-12" style="float:right;">
            <div class="btn_contact">
                <div id="company-contact<?=$company->id?>" style="display: none;">
                                <div id="dialog-container">
                                    <div class="titleBar">
                                        <span class="dialogTitle" id="dialogTitle"></span>
                                        <span title="Cancel" class="dialogCloseButton" onclick="jQuery.unblockUI();">
                                    <span title="Cancel" class="closeText">x</span>
                                        </span>
                                    </div>
                            
                                    <div class="dialogContent">
                                        <h3 class="title" style="color: #fff;font-size: 18px;"><i class="fa fa-envelope"></i>  Contact: <?= $company->name ?></h3>
                                        <div class="dialogContentBody" id="dialogContentBody">
                                        
                                            <form name="contactCompanyFrm<?=$company->id?>" id="contactCompanyFrm<?=$company->id?>" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory') ?>" method="post" onSubmit='return validateContactForm()''>
                                                <fieldset>
                                                    
                                                    
                                                    <div id="frmFirstNameC_error_msg<?=$company->id?>"  class="control-group" >
                                                      <label class="control-label" for="firstName">First name</label>
                                                      <div class="controls">
                                                       
                                                        <input type="text" name="firstName" id="firstName_<?=$company->id?>" size="50" required>
                                                        <span class="help-inline_frmFirstNameC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                      </div>
                                                    </div>
    
    
                                                    <div id="frmLastNameC_error_msg<?=$company->id?>"  class="control-group" >
                                                      <label class="control-label" for="lastName">Last name</label>
                                                      <div class="controls">
                                                            <input type="text" name="lastName" id="lastName_<?=$company->id?>" size="50" required>
                                                        <span class="help-inline_frmLastNameC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                      </div>
                                                    </div>
                                                  
                            
                                                    <div id="frmEmailC_error_msg<?=$company->id?>"  class="control-group" >
                                                      <label class="control-label" for="lastName">E-mail address</label>
                                                      <div class="controls">
                                                            <input type="text" name="email" id="frmEmailC_error_msg<?=$company->id?>" size="50" required>
                                                        <span class="help-inline_frmEmailC_error_msg<?=$company->id?>" style="display:none;">Please enter valid email address</span>
                                                      </div>
                                                    </div>
                                                    
                                                    
                                                    <div id="frmDescriptionC_error_msg<?=$company->id?>"  class="control-group" >
                                                      <label class="control-label" for="lastName">Message</label>
                                                      <div class="controls">
                                                        
                                                            <textarea class="form-control" rows="3" style="padding: 0;margin: 0px !important;height:100px;" name="description" id="description_<?=$company->id?>" required></textarea>
                                                        <span class="help-inline_frmDescriptionC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                      </div>
                                                    </div>
                                                    <div>
                                                        
                                                    </div>
                                                    
                                                   
                            
                                                    <div class="clearfix clear-left">
                                                        <div class="button-row col-md-12">
                                                            <button style="margin-bottom: 10px;" type="button" class="ui-dir-button" onclick="contactCompany(<?=$company->id?>)">
                                                                <span class="ui-button-text"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Message</span>
                                                            </button>
                            
                                                            <button style="margin-left:0px;" type="button" class="ui-dir-button ui-dir-button-grey" onclick="jQuery.unblockUI()">
                                                                <span class="ui-button-text">CANCEL</span>
                                                            </button>
                                                        </div>
                                                    </div>
                            
                                                    <?php echo JHTML::_( 'form.token' ); ?>
                                                    <input type='hidden' name='task' value='companies.contactCompany' />
                                                    <input type='hidden' name='userId' value="<?=$user->id?>" />
                                                    <input type="hidden" id="companyId" name="companyId" value="<?=$company->id?>" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <!-- showContactCompany($company->id) !-->
                            <!-- <a class="ui-dir-button" href='javascript:showContactCompany()'>> !-->
                            <a class="ui-dir-button" href='<?php echo JBusinessUtil::getCompanyLink($company) ?>'>
                                <span class="ui-button-text" style="font-size: 13px;">
                                    <i class="fa fa-envelope"></i>
                                    Contact this Provider
                                </span>
                            </a>
            </div>
        </div>
    </div>

<?php
            }
        }
    }
?>



</div>




        
        
        <?php

    }
}
?>





<?php
function getID()
{
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $wat = explode("/", $actual_link);
    $number = count($wat);
    $a = $number - 1;

    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select($db->quoteName(array('id', 'alias')));
    $query->from($db->quoteName('jxkdm_jbusinessdirectory_categories'));
    $query->where($db->quoteName('alias') . " = " . $db->quote($wat[$a]));
    $db->setQuery($query);
    $results = $db->loadObjectList();

    return $results[0]->id;
}

function getDB($from, $where)
{
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*');
    $query->from($db->quoteName($from));
    $query->where($db->quoteName($where) . " = " . $db->quote(getID()));
    $query->order('name ASC');

    $db->setQuery($query);
    return $db->loadObjectList();
}


function getCompany()
{
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*');
    $query->from($db->quoteName('jxkdm_jbusinessdirectory_companies'));
    $db->setQuery($query);
    $results = $db->loadObjectList();

    return $results;
}


function getDB_All()
{

    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*');
    $query->from($db->quoteName('jxkdm_jbusinessdirectory_company_category'));
    $query->where($db->quoteName('categoryId') . " = " . $db->quote(getID()));
    $query->order('RAND()');
    $db->setQuery($query);
    $results = $db->loadObjectList();

    return $results;

    // $db       = JFactory::getDbo();
    // $query    = $db->getQuery(true);
    // $sql = "SELECT * FROM  `jxkdm_jbusinessdirectory_company_category` INNER JOIN  `jxkdm_jbusinessdirectory_companies` ON  `jxkdm_jbusinessdirectory_company_category`.`categoryId` =  `jxkdm_jbusinessdirectory_companies`.`id` WHERE  `jxkdm_jbusinessdirectory_company_category`.`categoryId` = ".$db->quote(getID());
    // $db->setQuery($sql);
    // return $rows = $db->loadObjectList();


}


?>


<style>
    .articleBody h1 {
        display: none;
    }
    p {
    margin-bottom: 10px !important;
    margin-top: 0px;
}

    table {
        text-align: left;
        line-height: 40px;
        border-collapse: separate;
        border-spacing: 0;
        border: 2px solid #1279bf;
        width: 100%;
        background-color: #ebebeb;
        border-radius: .25rem;
    }

    thead tr:first-child {
        background: #1279bf;
        color: #fff;
        border: none;
    }

    th:first-child, td:first-child {
        padding: 0 0px 0 20px;
    }

    thead tr:last-child th {
        border-bottom: 3px solid #ddd;
    }

    tbody tr:last-child td {
        border: none;
    }

    tbody td {
        border-bottom: 1px solid #1279bf;
    }

    td:last-child {
        text-align: left;
        padding-right: 10px;
    }

    .button {
        color: #aaa;
        cursor: pointer;
        vertical-align: middle;
    }

    .edit:hover {
        color: #0a79df;
    }

    .delete:hover {
        color: #dc2a2a;
    }

    .new_article_header {
        margin: 2px 0px -4px !important;
        color: #f71800  !important;
        font-family: Arial  !important;
        font-weight: bold  !important;
        font-size: large  !important;
    }


hr {
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    height: 0;
    margin: 0;
    padding: 0px !important;
}


</style>



<script>

function showQuoteCompany(companyId){
    jQuery("#company-quote #companyId").val(companyId);
    jQuery.blockUI({ message: jQuery('#company-quote'), css: {width: 'auto',top: '10%', left:"0", position:"absolute"} });
    jQuery('.blockUI.blockMsg').center();
    jQuery('.blockOverlay').click(jQuery.unblockUI); 

}   

function showContactCompany(companyId){
    
    jQuery("#company-contact"+companyId+" #companyId").val(companyId);
    jQuery.blockUI({ message: jQuery("#company-contact"+companyId), css: {width: 'auto',top: '10%', left:"0", position:"absolute"} });
    jQuery('.blockUI.blockMsg').center();
    jQuery('.blockOverlay').click(jQuery.unblockUI); 

}   

function requestQuoteCompany(){
    var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=companies.requestQuoteCompanyAjax', false, -1); ?>";
    var postData="";
    postData +="&firstName="+jQuery("#company-quote #firstName").val();
    postData +="&lastName="+jQuery("#company-quote #lastName").val();
    postData +="&email="+jQuery("#company-quote #email").val();
    postData +="&description="+jQuery("#company-quote #description").val();
    postData +="&companyId="+jQuery("#company-quote #companyId").val();
    postData +="&category="+jQuery("#company-quote #category").val();
    postData +="&recaptcha_response_field="+jQuery("#company-quote #recaptcha_response_field").val();
    postData +="&g-recaptcha-response="+jQuery("#company-quote #g-recaptcha-response-1").val();
    
    jQuery.post(baseUrl, postData, processContactCompanyResult);
}

function contactCompany(id){
    
    
    if( validateContactForm(id) == true){
        var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=companies.contactCompanyAjax', false, -1); ?>";
        
        var postData="";
        postData +="&firstName="+jQuery("#company-contact"+id+" #firstName_"+id+"").val();
        postData +="&lastName="+jQuery("#company-contact"+id+" #lastName_"+id+"").val();
        postData +="&email="+jQuery("#company-contact"+id+" #email_"+id+"").val();
        postData +="&description="+jQuery("#company-contact"+id+" #description_"+id+"").val();
        postData +="&companyId="+jQuery("#company-contact"+id+" #companyId").val();
        postData +="&recaptcha_response_field="+jQuery("#captcha-div-contact #recaptcha_response_field").val();
        postData +="&g-recaptcha-response="+jQuery("#captcha-div-contact #g-recaptcha-response").val();
         console.log(postData);
        jQuery.post(baseUrl, postData, processContactCompanyResult);
    }
}


function processContactCompanyResult(responce){
    var xml = responce;
    jQuery(xml).find('answer').each(function()
    {
        if( jQuery(this).attr('error') == '1' ){
             jQuery.blockUI({ 
                    message: '<strong>An error has occurred!</strong><br/><br/><p>'+jQuery(this).attr('errorMessage')+'</p>'
                }); 
             setTimeout(jQuery.unblockUI, 2000); 
        }else{
             jQuery.blockUI({ 
                
                    message:"<h3>Business has been successfully contacted!</h3>"
                }); 
             setTimeout(jQuery.unblockUI, 2000); 
        }
    });
}

function validateContactForm(idx){

    var form  = document.getElementById("contactCompanyFrm"+idx);
    //var form = document.contactCompanyFrm;
    var isError = false;
    //console.log(x);
    
    jQuery(".error_msg").each(function(){
        jQuery(this).hide();
    });
    
    if( !validateField( form.elements['firstName'], 'string', false, null ) ){
        //console.debug("firstName");
        jQuery("#frmFirstNameC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmFirstNameC_error_msg"+idx).show();
        
        if(!isError)
            jQuery("#firstName_"+idx).focus();
        isError = true;
    }

    if( !validateField( form.elements['lastName'], 'string', false, null ) ){
        jQuery("#frmLastNameC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmLastNameC_error_msg"+idx).show();
        if(!isError)
            jQuery("#lastName_"+idx).focus();
        isError = true;
    }

    if( !validateField( form.elements['email'], 'email', false, null ) ){
        jQuery("#frmEmailC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmEmailC_error_msg"+idx).show();
        if(!isError)
            jQuery("#email_"+idx).focus();
        isError = true;
    }
    
    if( !validateField( form.elements['description'], 'string', false, null ) ){
        jQuery("#frmDescriptionC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmDescriptionC_error_msg"+idx).show();
        if(!isError)
            jQuery("#description_"+idx).focus();
        isError = true;
    }
    
    //console.debug(isError);
    return !isError;
}


</script>



<style type="text/css">
    .btn_contact{
        margin-top: 21%;
        padding: 9px;
    }
    .result_total{
         padding-top: 1px;

    }
    .p-b-0{
            padding-bottom: 0 !important;
           
        }

    @media (max-width: 991px){
        .btn_contact{
            margin-top: 0%;
            margin-left: 14px;
            padding-bottom: 23px;
        }
        
    }

    @media (max-width: 425px){
        .sec_show_de{
            text-align: center;
        }
        .btn_contact{
            text-align: center;
        }
    }

    @media (max-width: 806px){
        .result_total{
            padding-top: 25px;
        }
    }

    @media (max-width: 767px){
        .col-xs-12{
            width: 50%;
        }
    }

    @media (max-width: 640px){
        .col-xs-12{
            width: 100%;
            text-align: center;
        }

        .result_total{
            padding-top: 15px;
        }
        .p-b-0{
            padding-bottom: 0 !important;
            padding-top: 0 !important;
        }

    }


    

</style>