<?php

defined('_JEXEC') or die;

$alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
$allowed_symbols = "23456789abcdeghkmnpqsuvxyz";
$fontsdir = 'fonts';	
$length = 3;
$width = 100;
$height = 50;
$fluctuation_amplitude = 5;
$no_spaces = false;
$show_credits = false;
$credits = 'www.joomlatune.ru';
$foreground_color = array(180, 180, 180);
$background_color = array(246, 246, 246);
$jpeg_quality = 290;
?>