<?php
/**
 * JComments - Joomla Comment System
 *
 * @version 3.0
 * @package JComments
 * @author Sergey M. Litvinov (smart@joomlatune.ru)
 * @copyright (C) 2006-2013 by Sergey M. Litvinov (http://www.joomlatune.ru)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

/**
 * Comments form template
 */
class jtt_tpl_form extends JoomlaTuneTemplate
{
	function render() 
	{
		if ($this->getVar('comments-form-message', 0) == 1) {
			$this->getMessage( $this->getVar('comments-form-message-text') );
			return;
		}
		
		if ($this->getVar('comments-form-link', 0) == 1) {
			$this->getCommentsFormLink();
			return;
		}

		$this->getCommentsFormFull();
	}

	/*
	 *
	 * Displays full comments form (with smilies, bbcodes and other stuff)
	 * 
	 */
	function getCommentsFormFull()
	{
		$object_id = $this->getVar('comment-object_id');
		$object_group = $this->getVar('comment-object_group');

		$htmlBeforeForm = $this->getVar('comments-html-before-form');
		$htmlAfterForm = $this->getVar('comments-html-after-form');

		$htmlFormPrepend = $this->getVar('comments-form-html-prepend');
		$htmlFormAppend = $this->getVar('comments-form-html-append');

?>
<h4 style="    padding: 20px;background: #e9ebee;"><?php echo JText::_('FORM_HEADER'); ?></h4>
<?php
		if ($this->getVar( 'comments-form-policy', 0) == 1) {
?>
<div class="comments-policy"><?php echo $this->getVar( 'comments-policy' ); ?></div>
<?php
		}
?>
<?php echo $htmlBeforeForm; ?>
<style>

.make_bottom {
    background: url(https://www.tppwholesale.com.au/images/icons/arrow-thick.png) 100% 50% no-repeat #33b1ff;
    border-radius: 20px;
    -moz-border-radius: 4px 30px 30px 4px;
    padding: 10px 52px 10px 16px;
    color: #fff;
    font-size: 17px;
    font-weight: 400;
    clear: both;
    float: left;
    border: 0;
    cursor: pointer;
    background-color: #1279bf;
    border-width: 0;
    color: #fff;
}
.btn {
    display: inline-block;
    padding: 4px 12px;
    margin-bottom: 0;
    font-size: 13px;
    line-height: 18px;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    color: #333;
    text-shadow: 0 1px 1px rgba(255,255,255,0.75);
    background-color: #f5f5f5;
    background-image: -moz-linear-gradient(top,#fff,#e6e6e6);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#e6e6e6));
    background-image: -webkit-linear-gradient(top,#fff,#e6e6e6);
    background-image: -o-linear-gradient(top,#fff,#e6e6e6);
    background-image: linear-gradient(to bottom,#fff,#e6e6e6);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe5e5e5', GradientType=0);
    border-color: #e6e6e6 #e6e6e6 #bfbfbf;
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
    border: 1px solid #bbb;
    border-bottom-color: #a2a2a2;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
    box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
}
#comments-footer{
	border-top: 1px solid #f6f9fb !important;
}
#jc {
    clear: both;
        background: #f6f9fb;
}
.make_css_textbox_comment{
		display: block !important;
	    text-indent: 10px !important;
	    font-size: 14px !important;
	    line-height: 1.428571429 !important;
	    color: #555555 !important;
	    background-color: #fff !important;
	    background-image: none !important;
	    border: 1px solid #ccc !important;
	    border-radius: 4px !important;
	    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
	    box-shadow: none !important;
	    height: 33px !important;
	}
	#comments-form input{
		width: 100%;
	}
	#comments-form p{
		margin-right: 20px;
	}
	#comments-form textarea{
		width: 99% !important;
		max-width: 100% !important;
	}
	input#comments-form-captcha{
		width: 100% !important;
	}
	div#comments-form-send a{
		color: #fff !important;
		padding: 12px 25px;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
	#comments-form label{
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
	#comments-form .grippie{
		max-width: 100% !important;
		width: 99%;
	}
	#comments-form img.captcha{
		margin-bottom: 20px;
	}
</style>
<a id="addcomments" href="#addcomments"></a>
<form id="comments-form" name="comments-form" action="javascript:void(null);" style="    background: #f6f9fb;padding-top: 3px;">
<?php
		$this->getFormFields($htmlFormPrepend);
?>
<?php
		if ($this->getVar( 'comments-form-user-name', 1) == 1) {
			$text = ($this->getVar('comments-form-user-name-required', 1) == 0) ? JText::_('FORM_NAME') : JText::_('FORM_NAME_REQUIRED');
?>
<p>
	<span>
		<label for="comments-form-name"><!-- <?php echo $text; ?> -->Name *</label>
		<input id="comments-form-name" class="make_css_textbox_comment" type="text" name="name" value="" maxlength="<?php echo $this->getVar('comment-name-maxlength');?>" size="22" tabindex="1" />
	</span>
</p>
<?php
		}
		if ($this->getVar( 'comments-form-user-email', 1) == 1) {
			$text = ($this->getVar('comments-form-email-required', 1) == 0) ? JText::_('FORM_EMAIL') : JText::_('FORM_EMAIL_REQUIRED');
?>
<p>
	<span>
		<label for="comments-form-email">Email * (will not be published)</label>
		<input id="comments-form-email" class="make_css_textbox_comment" type="text" name="email" value="" size="22" tabindex="2" />
	</span>
</p>
<?php
		}
		if ($this->getVar('comments-form-user-homepage', 0) == 1) {
			$text = ($this->getVar('comments-form-homepage-required', 1) == 0) ? JText::_('FORM_HOMEPAGE') : JText::_('FORM_HOMEPAGE_REQUIRED');
?>

<p>
	<span>
		<label for="comments-form-homepage"><?php echo $text; ?></label>
		<input id="comments-form-homepage" class="make_css_textbox_comment" type="text" name="homepage" value="" size="22" tabindex="3" />
	</span>
</p>
<p><label>Comment *</label> 	</p>

<?php
		}
		if ($this->getVar('comments-form-title', 0) == 1) {
			$text = ($this->getVar('comments-form-title-required', 1) == 0) ? JText::_('FORM_TITLE') : JText::_('FORM_TITLE_REQUIRED');
?>
<p>
	<span>
		<label for="comments-form-title"><?php echo $text; ?></label>
		<input id="comments-form-title" type="text" name="title" value="" size="22" tabindex="4" />
	</span>
</p>
<?php
		}
?>
<p>
	
	<span>
		
		<textarea id="comments-form-comment" name="comment" cols="65" rows="8" tabindex="5"></textarea>
	</span>
</p>
<?php
		if ($this->getVar('comments-form-subscribe', 0) == 1) {
?>
<!-- <p>
	<span>
		<label for="comments-form-subscribe"><?php echo JText::_('FORM_SUBSCRIBE'); ?></label><br />
		<input class="checkbox" id="comments-form-subscribe" type="checkbox" name="subscribe" value="1" tabindex="5" />
	</span>
</p> -->
<?php
		}

		if ($this->getVar('comments-form-captcha', 0) == 1) {
			$html = $this->getVar('comments-form-captcha-html');
			if ($html != '') {
				echo $html;
			} else {
				$link = JCommentsFactory::getLink('captcha');
?>
<p>
	<span>
		<label>Please type the characters you see below</label>
		<input class="captcha" id="comments-form-captcha" type="text" name="captcha_refid" value="" size="5" tabindex="6" /><br />
		
		<img class="captcha" onclick="jcomments.clear('captcha');" id="comments-form-captcha-image" src="<?php echo $link; ?>" width="121" height="60" alt="<?php echo JText::_('FORM_CAPTCHA'); ?>" /><br />
		<span class="captcha" onclick="jcomments.clear('captcha');" style="font-style: normal;
    /*font-weight: bold;*/
    font-family: Helvetica, sans-serif;
    color: #FF0000;">Hard to read? Click here for a new code.</span><br />
    
	</span>
</p>
<?php
			}
		}
?>
<?php
		$this->getFormFields($htmlFormAppend);
?>
<div id="comments-form-buttons">
	<div class="btn-primary" id="comments-form-send"><div><a href="#" tabindex="7" onclick="jcomments.saveComment();return false;" title="<?php echo JText::_('FORM_SEND_HINT'); ?>"><?php echo JText::_('FORM_SEND'); ?></a></div></div>
	<div class="btn-primary" id="comments-form-cancel" style="display:none;"><div><a class="btn-primary make_bottom" href="#" tabindex="8" onclick="return false;" title="<?php echo JText::_('FORM_CANCEL'); ?>"><?php echo JText::_('FORM_CANCEL'); ?></a></div></div>
	<div style="clear:both;"></div>
</div>
<div>
	<input type="hidden" name="object_id" value="<?php echo $object_id; ?>" />
	<input type="hidden" name="object_group" value="<?php echo $object_group; ?>" />
</div>
</form>
<script type="text/javascript">
<!--
function JCommentsInitializeForm()
{
	var jcEditor = new JCommentsEditor('comments-form-comment', true);
<?php
		if ($this->getVar('comments-form-bbcode', 0) == 1) {
			$bbcodes = array( 'b'=> array(0 => JText::_('FORM_BBCODE_B'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT'))
					, 'i'=> array(0 => JText::_('FORM_BBCODE_I'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT'))
					, 'u'=> array(0 => JText::_('FORM_BBCODE_U'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT'))
					, 's'=> array(0 => JText::_('FORM_BBCODE_S'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT'))
					, 'img'=> array(0 => JText::_('FORM_BBCODE_IMG'), 1 => JText::_('BBCODE_HINT_ENTER_FULL_URL_TO_THE_IMAGE'))
					, 'url'=> array(0 => JText::_('FORM_BBCODE_URL'), 1 => JText::_('BBCODE_HINT_ENTER_FULL_URL'))
					, 'hide'=> array(0 => JText::_('FORM_BBCODE_HIDE'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT_TO_HIDE_IT_FROM_UNREGISTERED'))
					, 'quote'=> array(0 => JText::_('FORM_BBCODE_QUOTE'), 1 => JText::_('BBCODE_HINT_ENTER_TEXT_TO_QUOTE'))
					, 'list'=> array(0 => JText::_('FORM_BBCODE_LIST'), 1 => JText::_('BBCODE_HINT_ENTER_LIST_ITEM_TEXT'))
					);

			foreach($bbcodes as $k=>$v) {
				if ($this->getVar('comments-form-bbcode-' . $k , 0) == 1) {
					$title = trim(JCommentsText::jsEscape($v[0]));
					$text = trim(JCommentsText::jsEscape($v[1]));
?>
	jcEditor.addButton('<?php echo $k; ?>','<?php echo $title; ?>','<?php echo $text; ?>');
<?php
				}
			}
		}

		$customBBCodes = $this->getVar('comments-form-custombbcodes');
		if (count($customBBCodes)) {
			foreach($customBBCodes as $code) {
				if ($code->button_enabled) {
					$k = 'custombbcode' . $code->id;
					$title = trim(JCommentsText::jsEscape($code->button_title));
					$text = empty($code->button_prompt) ? JText::_('BBCODE_HINT_ENTER_TEXT') : JText::_($code->button_prompt);
					$open_tag = $code->button_open_tag;
					$close_tag = $code->button_close_tag;
					$icon = $code->button_image;
					$css = $code->button_css;
?>
	jcEditor.addButton('<?php echo $k; ?>','<?php echo $title; ?>','<?php echo $text; ?>','<?php echo $open_tag; ?>','<?php echo $close_tag; ?>','<?php echo $css; ?>','<?php echo $icon; ?>');
<?php
				}
			}
		}

		$smiles = $this->getVar( 'comment-form-smiles' );

		if (isset($smiles)) {
			if (is_array($smiles)&&count($smiles) > 0) {
?>
	jcEditor.initSmiles('<?php echo $this->getVar( "smilesurl" ); ?>');
<?php
				foreach ($smiles as $code => $icon) {
					$code = trim(JCommentsText::jsEscape($code));
					$icon = trim(JCommentsText::jsEscape($icon));
?>
	jcEditor.addSmile('<?php echo $code; ?>','<?php echo $icon; ?>');
<?php
				}
			}
		}
		if ($this->getVar( 'comments-form-showlength-counter', 0) == 1) {
?>
	jcEditor.addCounter(<?php echo $this->getVar('comment-maxlength'); ?>, '<?php echo JText::_('FORM_CHARSLEFT_PREFIX'); ?>', '<?php echo JText::_('FORM_CHARSLEFT_SUFFIX'); ?>', 'counter');
<?php
		}
?>
	jcomments.setForm(new JCommentsForm('comments-form', jcEditor));
}

<?php
	if ($this->getVar('comments-form-ajax', 0) == 1) {
?>
setTimeout(JCommentsInitializeForm, 100);
<?php
	} else {
?>
if (window.addEventListener) {window.addEventListener('load',JCommentsInitializeForm,false);}
else if (document.addEventListener){document.addEventListener('load',JCommentsInitializeForm,false);}
else if (window.attachEvent){window.attachEvent('onload',JCommentsInitializeForm);}
else {if (typeof window.onload=='function'){var oldload=window.onload;window.onload=function(){oldload();JCommentsInitializeForm();}} else window.onload=JCommentsInitializeForm;} 
<?php
	}
?>
//-->
</script>
<?php echo $htmlAfterForm; ?>
<?php
	}

	/*
	 *
	 * Displays link to show comments form
	 *
	 */
	function getCommentsFormLink()
	{
		$object_id = $this->getVar('comment-object_id');
		$object_group = $this->getVar('comment-object_group');
?>
<div id="comments-form-link">
<a id="addcomments" class="showform" href="#addcomments" onclick="jcomments.showForm(<?php echo $object_id; ?>,'<?php echo $object_group; ?>', 'comments-form-link'); return false;"><?php echo JText::_('FORM_HEADER'); ?></a>
</div>
<?php
	}

	/*
	 *
	 * Displays service message
	 *
	 */
	function getMessage( $text )
	{
		if ($text != '') {
?>
<a id="addcomments" href="#addcomments"></a>
<p class="message"><?php echo $text; ?></p>
<?php
		}
	}

	function getFormFields($fields)
	{
	        if (!empty($fields)) {
			$fields = is_array($fields) ? $fields : array($fields);
		
			foreach($fields as $field) {

				$labelElement = '';
				$inputElement = '';

				if (is_array($field)) {
					$labelElement = isset($field['label']) ? $field['label'] : '';
					$inputElement = isset($field['input']) ? $field['input'] : '';
				} else {
					$inputElement = $field;					
				}

				if (!empty($inputElement)) {
?>
<p>
	<span>
		<?php echo $inputElement; ?>
		<?php echo $labelElement; ?>
	</span>
</p>
<?php
	                       	}
			}
                }
	}
}