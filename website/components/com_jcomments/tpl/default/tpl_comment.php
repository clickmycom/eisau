<?php
/**
 * JComments - Joomla Comment System
 *
 * @version 3.0
 * @package JComments
 * @author Sergey M. Litvinov (smart@joomlatune.ru)
 * @copyright (C) 2006-2013 by Sergey M. Litvinov (http://www.joomlatune.ru)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

/**
 * Comment item template. Results of rendering used in tpl_list.php
 */
class jtt_tpl_comment extends JoomlaTuneTemplate
{
	function render()
	{
		$comment = $this->getVar('comment');

		if (isset($comment)) {
			if ($this->getVar('get_comment_vote', 0) == 1) {
				// return comment vote
			 	$this->getCommentVoteValue( $comment );
			} else if ($this->getVar('get_comment_body', 0) == 1) {
				// return only comment body (for example after quick edit)
				echo $comment->comment;
			} else {
				// return all comment item
?>

<style>

#comments-list {
    font-family: Helvetica,Arial,sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
    text-align: left;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
    line-height: 1.538461538;
    direction: ltr;
}

#comments-list .kt-comment-tools {
    margin: 0 0 16px;
    padding: 8px;
    background: #f7f7f7
}
#comments-list .kt-comment-tools .kt-comment-sorting a {
    color: #333
}
#comments-list .kt-comment-tools .kt-comment-sorting a.is-active {
    font-weight: 700;
    font-size: 12px
}
#comments-list .kt-comments-view-disallowed {
    text-align: center;
    background: #f7f7f7;
    color: #666;
    margin-bottom: 20px;
    padding: 40px
}
#comments-list .kt-comments-view-disallowed>i {
    font-size: 24px;
    display: block;
    margin-bottom: 20px
}
#comments-list .kt-comments {
    list-style: none;
    margin: 0 0 16px;
    padding: 0
}
#comments-list .kt-comments__item {
    list-style: none;
    margin: 0;
    padding: 16px;
}
#comments-list .kt-comments__item:first-child.is-parent,
#comments-list .kt-comments__item:first-child.is-parent.is-featured {
    border-top: 0
}
#comments-list .kt-comments__item .kt-comment-message {
    font-size: 15px;
    line-height: 24px
}
#comments-list .kt-comments__item.is-parent.is-featured {
    background-color: #f7fcff
}
#comments-list .kt-comments__item.is-parent.is-featured+div {
    border-color: #c7dbf2;
    border-top: 0
}
#comments-list .kt-comments__item.is-parent.is-featured+.is-child {
    margin-top: 16px
}
#comments-list .kt-comments__item.is-child {
    margin-bottom: 16px
}
#comments-list .kt-comments__item.is-child.is-featured {
    background-color: #f7fcff
}
#comments-list .kt-comments__item.is-featured .kt-featured-label-wrap {
    display: inline-block
}
#comments-list .kt-comments__item.is-featured .kt-unpin-comment {
    display: block
}
#comments-list .kt-comments__item.is-featured .kt-pin-comment {
    display: none
}
#comments-list .kt-comments__item.is-pending {
    background-color: #fffaf0
}
#comments-list .kt-comments__item .kt-unpin-comment {
    display: none
}
#comments-list .kt-comments__item .kt-pin-comment {
    display: block
}
#comments-list .kt-comments__item .kt-manage-tools {
    position: relative
}
#comments-list .kt-comments__item .kt-location-wrap i {
    color: #1565c0
}
#comments-list .kt-comments__item .kt-attachments {
    display: none
}
#comments-list .kt-comments__item .kt-attachments.has-attachments {
    display: block
}
#comments-list .kt-comments__item.is-edited .kt-edited-info {
    display: block
}
#comments-list .kt-comments__item .kt-edited-info {
    display: none;
    font-size: 90%;
    font-style: italic;
    opacity: .7;
    filter: alpha(opacity=70);
    margin: 0 0 1em
}
#comments-list .kt-comments__item .kt-likes-wrapper.is-liked .unlike-comment {
    display: inline-block
}
#comments-list .kt-comments__item .kt-likes-wrapper.is-liked .like-comment {
    display: none
}
#comments-list .kt-comments__item .kt-likes-wrapper .unlike-comment {
    display: none
}
#comments-list .kt-comments__item .kt-likes-wrapper .like-comment {
    display: inline-block
}
#comments-list .kt-comments__item .kt-likes-wrapper .kt-likes-counter {
    display: inline-block;
    width: 26px;
    font-weight: 700;
    text-align: center;
    background: #eee;
    border: 1px solid #ccc;
    line-height: 16px;
    border-radius: 8px;
    margin: 0 3px 0 0
}
#comments-list li.kt-featured-label-wrap {
    display: none
}
#comments-list .kt-comment.has-checkbox {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: -ms-flex;
    display: flex;
    padding: 8px
}
#comments-list .kt-comment.has-checkbox .o-checkbox {
    margin: 2px 16px 0 0
}
#comments-list .kt-comment.has-checkbox>div+div {
    -webkit-flex: 1;
    -ms-flex: 1;
    flex: 1
}
#comments-list .kt-comment-content__hd,
#comments-list .kt-comment-content__bd,
#comments-list .kt-comment-content__ft {
    position: relative
}
#comments-list .kt-comment-control .btn-control i {
    color: #888;
    font-size: 15px
}
#comments-list .kt-comment-content-action {
    font-size: 12px
}
#comments-list .kt-comment-content-action>div {
    display: inline-block
}
#comments-list .kt-comment-content-action>div+div {
    margin-left: 8px
}
#comments-list .kt-comment-content-action a {
    color: #888
}



#comments-list .o-flag {
    display: table;
    width: 100%;
}
#comments-list .o-flag__image {
    padding-right: 8px;
    width: 1%;
}
#comments-list .o-flag--top {
    vertical-align: top;
}
#comments-list .o-flag__image, #comments-list .o-flag__body {
    display: table-cell;
    vertical-align: middle;
}
#comments-list .o-flag__image {
    padding-right: 15px !important;
}
.sp-avatar.sp-avatar-round {
    border-radius: 100px;
}
.sp-avatar.sp-avatar-lg {
    width: 40px;
    height: 40px;
    line-height: 40px;
}
.sp-avatar-bg-1 {
    background-color: #ffcdd2;
}
.sp-avatar {
    display: inline-block;
    width: 24px;
    height: 24px;
    line-height: 24px;
    font-size: 16px;
    font-weight: normal;
    border-radius: 3px;
    text-align: center;
    color: #fff;
    overflow: hidden;
}
#comments-list .o-flag__body {
    width: 100%;
    padding-top: 6px;
}

#comments-list .g-list-inline {
    margin: 0;
    padding: 0;
    list-style: none;
    font-size: 0;
}

#comments-list .kt-comment-meta>li {
    font-size: 13px;
}
#comments-list .g-list-inline>li {
    display: inline-block;
    font-size: 13px;
}
#comments-list li {
    line-height: 1.538461538;
}
#comments-list a:not(.btn):link {
    text-decoration: none;
}
#comments-list .g-list-inline>li>a>span {
    font-weight: bold;
}
#comments-list .g-list-inline--delimited>li+li {
    margin-left: 4px;
}
#comments-list .kt-comment-meta>li {
    font-size: 13px;
}
#comments-list .g-list-inline>li {
    display: inline-block;
    font-size: 13px;
}
#comments-list .t-text--muted {
    color: #888 !important;
}
#comments-list li {
    line-height: 1.538461538;
}

#comments-list .kt-comments__item .kt-comment-message {
    font-size: 15px;
    line-height: 24px;
}
#comments-list .kt-comment-content-action {
    font-size: 12px;
}
#comments-list .g-list-inline {
    margin: 0;
    padding: 0;
    list-style: none;
    font-size: 0;
}
#comments-list .g-list-inline--delimited>li+li {
    margin-left: 4px;
}

#comments-list .kt-comments__item {
    background: #fff;
    border-radius: 3px;
    box-shadow: 0 0 6px rgba(0,0,0,0.05);
    -webkit-box-shadow: 0 0 6px rgba(0,0,0,0.05);
}

#comments-list .kt-comments__item {
    list-style: none;
    margin: 0;
    padding: 16px;
}
#comments-list .kt-comments__item.is-parent+div {
    margin-top: 16px;
}



.sp-avatar-bg-1 {
  background-color: #ffcdd2;
}
.sp-avatar-bg-1:hover {
  background-color: #ffcdd2;
}
.sp-avatar-bg-2 {
  background-color: #e1bee7;
}
.sp-avatar-bg-2:hover {
  background-color: #e1bee7;
}
.sp-avatar-bg-3 {
  background-color: #bbdefb;
}
.sp-avatar-bg-3:hover {
  background-color: #bbdefb;
}
.sp-avatar-bg-4 {
  background-color: #b2dfdb;
}
.sp-avatar-bg-4:hover {
  background-color: #b2dfdb;
}
.sp-avatar-bg-5 {
  background-color: #ffcc80;
}
.sp-avatar-bg-5:hover {
  background-color: #ffcc80;
}

.sp-avatar-bg-6 {
  background-color: #bec3cc;
}
.sp-avatar-bg-6:hover {
  background-color: #bec3cc;
}

.sp-avatar-bg-7 {
  background-color: #FFDBA5;
}
.sp-avatar-bg-7:hover {
  background-color: #FFDBA5;
}

.sp-avatar-bg-8 {
  background-color: #FFCAA5;
}
.sp-avatar-bg-8:hover {
  background-color: #FFCAA5;
}

.sp-avatar-bg-9 {
  background-color: #CFCBFF;
}
.sp-avatar-bg-9:hover {
  background-color: #CFCBFF;
}



</style>


<?php

				$comment_number = $this->getVar('comment-number', 1);
				$thisurl = $this->getVar('thisurl', '');

				$commentBoxIndentStyle = ($this->getVar('avatar') == 1) ? ' avatar-indent' : '';
?>



        <div class="kt-comments__item kmt-item kmt-comment-item-registered kt-group-2   is-parent " data-kt-comment-item="" data-id="2235" data-parentid="kmt-0" data-depth="0" itemscope="" itemtype="http://schema.org/Comment" style="">
        <div class="kt-comment">
            <a id="comment-2235"></a>
            <div class="o-flag">
                <div class="o-flag__image o-flag--top" style="    vertical-align: top;">
                	<?php
                    	$str = substr($comment->author,0,1);
						
                    		if((ord(strtoupper($str))>=65 && ord(strtoupper($str)) <=67)){
									$_this_class_bg = "sp-avatar-bg-1";
							}else if((ord(strtoupper($str))>=68 && ord(strtoupper($str)) <=70)){
									$_this_class_bg = "sp-avatar-bg-2";
							}else if((ord(strtoupper($str))>=71 && ord(strtoupper($str)) <=73)){
									$_this_class_bg = "sp-avatar-bg-3";
							}else if((ord(strtoupper($str))>=74 && ord(strtoupper($str)) <=76)){
									$_this_class_bg = "sp-avatar-bg-4";
							}else if((ord(strtoupper($str))>=77 && ord(strtoupper($str)) <=79)){
									$_this_class_bg = "sp-avatar-bg-5";
							}else if((ord(strtoupper($str))>=80 && ord(strtoupper($str)) <=82)){
									$_this_class_bg = "sp-avatar-bg-6";
							}else if((ord(strtoupper($str))>=83 && ord(strtoupper($str)) <=85)){
									$_this_class_bg = "sp-avatar-bg-7";
							}else if((ord(strtoupper($str))>=86 && ord(strtoupper($str)) <=88)){
									$_this_class_bg = "sp-avatar-bg-8";
							}else if((ord(strtoupper($str))>=89 && ord(strtoupper($str)) <=90)){
									$_this_class_bg = "sp-avatar-bg-9";
							}
                    	?>
                    	
                    <span class="sp-avatar  <?=$_this_class_bg?> sp-avatar-lg sp-avatar-round" style="text-transform: uppercase;">
                    	<?php echo substr($comment->author,0,1)?></span> 
                    	
                </div>
                <div class="o-flag__body">
                    <div class="kt-comment-content">
                        <div class="kt-comment-content__hd">
                            <ol class="g-list-inline g-list-inline--delimited kt-comment-meta">
                                <li data-breadcrumb="·">
                                    <a rel="nofollow" itemprop="creator" itemscope="" itemtype="#">
                                        <span itemprop="name">
											<?php				
													if ($this->getVar('comment-show-homepage') == 1) {
											?>
													<a class="author-homepage" href="<?php echo $comment->homepage; ?>" rel="nofollow" title="<?php echo $comment->author; ?>"><?php echo $comment->author; ?></a>
											<?php
													} else {
											?>
											<span class="comment-author"><?php echo $comment->author?></span>
											<?php
													}
											?>
										</span>
                                    </a>
                                </li>
                                <li class="t-text--muted" data-breadcrumb="·">
                                    <time itemprop="dateCreated" datetime="2017-03-27T13:00:06+06:00">
                                       <?php echo JCommentsText::formatDate($comment->date, JText::_('DATETIME_FORMAT')); ?>
                                    </time>
                                   
                                </li>
                                <li data-breadcrumb="·" class="kt-featured-label-wrap" data-kt-provide="tooltip" data-title="This is a featured comment" style="display: none">
                                    <div class="kt-featured-label">
                                        <i class="fa fa-star"></i>
                                    </div>
                                </li>
                            </ol>
                        </div>
                        <div class="kt-comment-content__bd" itemprop="text">
                            <ol class="g-list-inline g-list-inline--delimited kt-comment-meta-sub t-lg-mb--md">
                            </ol>
                            <div class="kt-comment-message" data-kt-comment-content="">
                                <p><div class="comment-body" id="comment-body-<?php echo $comment->id; ?>"><?php echo $comment->comment; ?></div></p>
                            </div>
                            <div class="kt-attachments " data-kt-attachment-wrapper="">
                                <div class="kt-attachments-title t-lg-mb--md">
                                    <b>
                                        <i class="fa fa-link"></i>&nbsp; Attachments </b>
                                </div>
                            </div>
                        </div>
                        <div class="kt-comment-content__ft">
                            <div class="kt-comment-content-action">
                                <ol class="g-list-inline g-list-inline--delimited kt-comment-meta">
                                	
                                	
                                    <li class="kt-reply-wrap" data-breadcrumb="·">
                                        <?php
											if ($this->getVar('button-quote') == 1) {
										?>
												<a href="#" onclick="jcomments.quoteComment(<?php echo $comment->id; ?>); return false;"><?php echo JText::_('BUTTON_QUOTE'); ?></a>
										<?php
											}
										?>
                                    </li>
                                    <!-- <li class="kt-report-wrap" data-breadcrumb="·">
                                        <a href="javascript:void(0);" data-kt-report="">Report</a>
                                    </li> -->
                                    <li class="kt-permalink-wrap" data-breadcrumb="·" style="display: none;">
										| <a class="comment-anchor" title="Permalink" href="<?php echo $thisurl; ?>#comment-<?php echo $comment->id; ?>" id="comment-<?php echo $comment->id; ?>">#<?php echo $comment_number; ?></a> 
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                
<!-- <div class="rbox">


<?php
				if ($this->getVar('avatar') == 1) {
?>
<div class="comment-avatar"><?php echo $comment->avatar; ?></div>
<?php
				}
?>
<div class="comment-box<?php echo $commentBoxIndentStyle; ?>">
<?php
				if ($this->getVar('comment-show-vote', 0) == 1) {
					$this->getCommentVote( $comment );
				}
?>
<a class="comment-anchor" href="<?php echo $thisurl; ?>#comment-<?php echo $comment->id; ?>" id="comment-<?php echo $comment->id; ?>">#<?php echo $comment_number; ?></a>
<?php
				if (($this->getVar('comment-show-title') > 0) && ($comment->title != '')) {
?>
<span class="comment-title"><?php echo $comment->title; ?></span> &mdash; 
<?php
				}
?>

<?php				
				if ($this->getVar('comment-show-homepage') == 1) {
?>
					<a class="author-homepage" href="<?php echo $comment->homepage; ?>" rel="nofollow" title="<?php echo $comment->author; ?>"><?php echo $comment->author; ?></a>
<?php
				} else {
?>
<span class="comment-author"><?php echo $comment->author?></span>
<?php
				}
?>

<?php
				if (($this->getVar('comment-show-email') > 0) && ($comment->email != '')) {
?>
<a class="comment-email" href="mailto:<?php echo $comment->email; ?>"><?php echo $comment->email; ?></a>
<?php
				}
?>
<span class="comment-date"><?php echo JCommentsText::formatDate($comment->date, JText::_('DATETIME_FORMAT')); ?></span>
<div class="comment-body" id="comment-body-<?php echo $comment->id; ?>"><?php echo $comment->comment; ?></div>
<?php
				if (($this->getVar('button-reply') == 1)
				|| ($this->getVar('button-quote') == 1)
				|| ($this->getVar('button-report') == 1)) {
?>
<span class="comments-buttons">
<?php
					if ($this->getVar('button-reply') == 1) {
?>
<a href="#" onclick="jcomments.showReply(<?php echo $comment->id; ?>); return false;"><?php echo JText::_('BUTTON_REPLY'); ?></a>
<?php
						if ($this->getVar('button-quote') == 1) {
?>
 | <a href="#" onclick="jcomments.showReply(<?php echo $comment->id; ?>,1); return false;"><?php echo JText::_('BUTTON_REPLY_WITH_QUOTE'); ?></a> | 
<?php
						}
					}
?>


					
					<?php
					if ($this->getVar('button-report') == 1) {
						if ($this->getVar('button-quote') == 1 || $this->getVar('button-reply') == 1) {
?>
 | 
<?php
						}
?>
<a href="#" onclick="jcomments.reportComment(<?php echo $comment->id; ?>); return false;"><?php echo JText::_('BUTTON_REPORT'); ?></a>
<?php
					}
?>
</span>
<?php
				}
?>
</div><div class="clear"></div>
<?php
				// show frontend moderation panel
				$this->getCommentAdministratorPanel( $comment );
?>
</div> -->
<?php
			}
		}
	}

	/*
	 *
	 * Displays comment's administration panel
	 *
	 */
	function getCommentAdministratorPanel( &$comment )
	{
		if ($this->getVar('comments-panel-visible', 0) == 1) {
?>
<p class="toolbar" id="comment-toolbar-<?php echo $comment->id; ?>">
<?php
			if ($this->getVar('button-edit') == 1) {
				$text = JText::_('BUTTON_EDIT');
?>
	<a class="toolbar-button-edit" href="#" onclick="jcomments.editComment(<?php echo $comment->id; ?>); return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-delete') == 1) {
				$text = JText::_('BUTTON_DELETE');
?>
	<a class="toolbar-button-delete" href="#" onclick="if (confirm('<?php echo JText::_('BUTTON_DELETE_CONIRM'); ?>')){jcomments.deleteComment(<?php echo $comment->id; ?>);}return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-publish') == 1) {
				$text = $comment->published ? JText::_('BUTTON_UNPUBLISH') : JText::_('BUTTON_PUBLISH');
				$class = $comment->published ? 'publish' : 'unpublish';
?>
	<a class="toolbar-button-<?php echo $class; ?>" href="#" onclick="jcomments.publishComment(<?php echo $comment->id; ?>);return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-ip') == 1) {
				$text = JText::_('BUTTON_IP') . ' ' . $comment->ip;
?>
	<a class="toolbar-button-ip" href="#" onclick="jcomments.go('http://www.ripe.net/perl/whois?searchtext=<?php echo $comment->ip; ?>');return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-ban') == 1) {
				$text = JText::_('BUTTON_BANIP');
?>
	<a class="toolbar-button-ban" href="#" onclick="jcomments.banIP(<?php echo $comment->id; ?>);return false;" title="<?php echo $text; ?>"></a>
<?php
			}
?>
</p>
<div class="clear"></div>
<?php
		}
	}

	function getCommentVote( &$comment )
	{
		$value = intval($comment->isgood) - intval($comment->ispoor);

		if ($value == 0 && $this->getVar('button-vote', 0) == 0) {
			return;
		}
?>
<span class="comments-vote">
	<span id="comment-vote-holder-<?php echo $comment->id; ?>">
<?php
		if ($this->getVar('button-vote', 0) == 1) {
?>
<a href="#" class="vote-good" title="<?php echo JText::_('BUTTON_VOTE_GOOD'); ?>" onclick="jcomments.voteComment(<?php echo $comment->id;?>, 1);return false;"></a><a href="#" class="vote-poor" title="<?php echo JText::_('BUTTON_VOTE_BAD'); ?>" onclick="jcomments.voteComment(<?php echo $comment->id;?>, -1);return false;"></a>
<?php
		}
		echo $this->getCommentVoteValue( $comment );
?>
	</span>
</span>
<?php
	}

	function getCommentVoteValue( &$comment )
	{
		$value = intval($comment->isgood - $comment->ispoor);

		if ($value == 0 && $this->getVar('button-vote', 0) == 0 && $this->getVar('get_comment_vote', 0) == 0) {
			// if current value is 0 and user has no rights to vote - hide 0
			return;
		}

		if ($value < 0) {
			$class = 'poor';
		} else if ($value > 0) {
			$class = 'good';
			$value = '+' . $value;
		} else {
			$class = 'none';
		}
?>
<span class="vote-<?php echo $class; ?>"><?php echo $value; ?></span>

<?php
	}
}
