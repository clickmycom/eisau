<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';

$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
$enableSEO = $appSettings->enable_seo;
$enablePackages = $appSettings->enable_packages;
$enableRatings = $appSettings->enable_ratings;
$enableNumbering = $appSettings->enable_numbering;
$user = JFactory::getUser();

$showData = !($user->id==0 && $appSettings->show_details_user == 1);




JHtml::_('behavior.caption');
JHtml::_('bootstrap.tooltip');
jimport( 'joomla.methods' );
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$lang = JFactory::getLanguage();
$doc = JFactory::getDocument();
$meta_description = $doc->getMetaData("keywords");
?>



<?php

if($this->searchkeyword != '' || $this->searchkeyword != null){
    ?>
    <h2 itemprop="name" class="article-title-single new_article_header" style=" display: none;       margin: -8px 0px 12px;color: #676765;padding-bottom: 6px;padding-top: 20px;font-family: 'Helvetica';font-size: 15px;padding-left: 9px;">
        <?php
        echo '<i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;';
        echo  "Search for keywords: "."<strong style='font-weight: 500;text-transform: lowercase;'> ".$this->searchkeyword."</strong>";
        ?>
    </h2>

    <?php

}else {
    ?>

    <h2 itemprop="name" class="article-title-single new_article_header" style="display: none;     margin: -8px 0px 12px;color: #676765;padding-bottom: 6px;padding-top: 20px;font-family: 'Helvetica';font-size: 15px;padding-left: 9px;">
        <?php
        if($this->category->name !=null || $this->category->name != ''){
            echo '<i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;';
            echo  "Search for:"."<strong style='font-weight: 500;text-transform: lowercase;'> ".$this->category->name."</strong>";
        }
        ?>
    </h2>
    <?php
}

?>

<div class="page-header">
    <h2 itemprop="name" class="article-title-single new_article_header" style="margin: 2px 0px -4px;">
        Search For Providers<hr></h2>
</div>

<p>The results of your search are below. To see further information including testimonials click on "View full profile". Contact details and links to company websites provided in the profile make them easily contactable.</p>

<p class="DirectoryResultCount" style="font-weight: bold; padding-top: 1em; float: left;">Your search returned 

    <?php
        $str_001 = explode("of",$this->pagination->getResultsCounter());
        print_r($str_001[1]);
    ?>
     matches.
</p>

<div id="results-container" <?php echo $this->appSettings->search_view_mode?'style="display: none"':'' ?> class="search-style-1">
















<div class="col-lg-12 " style="border-bottom: 1px solid #1279bf;" id="search_category">
    <div class="row" style="background-color:#1279bf; margin-left: 0px;">
        <div class="col-lg-12 " style="text-indent: 19px;padding: 15px 0px; font-size: 16px;color: #fff;font-size: 16px;font-weight: 500;">
            <small class="samll_item" style="float:right;"><?=$this->pagination->getResultsCounter(); ?> items</small>
        </div>
    </div>



    <?php
        if(!empty($this->companies)){
            foreach($this->companies as $index=>$company){
    ?>

        <div class="row" style="background-color: #ebebeb;margin-left: 0;border-bottom: 1px solid #1279bf;border-left: 2px solid #1279bf;border-right: 2px solid #1279bf;">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 sec_show_de">
                <a href="<?php echo JBusinessUtil::getCompanyLink($company)?>">
                            <?php if(!empty($company->logoLocation)){?>
                                <img style="height: 102px;border-radius: 5px;padding:30px;    padding-bottom: 0;" title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.$company->logoLocation ?>"/>
                            <?php }else{ ?>
                                <img style="height: 102px;border-radius: 5px;padding:30px;    padding-bottom: 0;" title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>"/>
                            <?php } ?>
                </a>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-7 col-xs-12 sec_show_de">
                <div style="padding: 23px 23px 0px 23px; ">
                   
                   <a style="font-size: 16px;font-weight: 600;" href="<?php echo JBusinessUtil::getCompanyLink($company)?>"
                        <span itemprop="name"> <?php echo $company->name?> </span>
                    </a>
                        <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;    margin-top: 10px;"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;
                                <a href="#" title="<?php echo JBusinessUtil::getAddressText($company) ?>"><?php echo JBusinessUtil::getAddressText($company) ?></a>                             
                        </p>

                        <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;
                            <?php if( $showData && (isset($company->packageFeatures) && in_array(PHONE, $company->packageFeatures) || !$enablePackages )){ ?>
                                <?php if(!empty($company->phone)) { ?>

                                    <strong><a href="tel:<?php  echo $company->phone; ?>"><?php  echo $company->phone; ?></a></strong>

                                <?php } ?>
                            <?php } ?>
                        </p>
                        <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-internet-explorer" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<a href="<?php echo $company->website?>"><?php echo $company->website?></a>
                        </p>

                        <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<a href="mailto:enquiries@lotusfilters.com.au" target="_top"><?php echo $company->email?></a>
                        </p>
                        <p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-folder-open-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;
                            <?php
                            $categoryIds = explode(',',$company->categoryIds);
                            $categoryNames = explode('#',$company->categoryNames);
                            $categoryAliases = explode('#',$company->categoryAliases);
                            for($i=0;$i<count($categoryIds);$i++){
                                ?>
                                <a rel="nofollow" href="<?php echo JBusinessUtil::getCategoryLink($categoryIds[$i], $categoryAliases[$i]) ?>"><?php echo $categoryNames[$i]?><?php echo $i<(count($categoryIds)-1)? ',&nbsp;':'' ?> </a>
                                <?php

                                if(count($categoryIds) > 1){
                                    echo " | ";
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </p>

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-7 col-xs-12" style="float: right;">
                <div class="btn_contact">
                    <div id="company-contact<?=$company->id?>" style="display: none;">
                                <div id="dialog-container">
                                    <div class="titleBar">
                                        <span class="dialogTitle" id="dialogTitle"></span>
                                        <span title="Cancel" class="dialogCloseButton" onclick="jQuery.unblockUI();">
                                    <span title="Cancel" class="closeText">x</span>
                                        </span>
                                    </div>
                                        
                                    <div class="dialogContent">
                                        <h3 class="title" style="color: #fff;font-size: 18px;"><i class="fa fa-envelope"></i>  Contact: <?= $company->name ?></h3>
                                        <div class="dialogContentBody" id="dialogContentBody">

                                            <div id="article_<?=$index?>" data-meta='name="contactCompanyFrm<?=$company->id?>" id="contactCompanyFrm<?=$company->id?>" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory') ?>" method="post" onSubmit="return validateContactForm()"'>

                                                <form name="contactCompanyFrm<?=$company->id?>" id="contactCompanyFrm<?=$company->id?>" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory') ?>" method="post" onSubmit="return validateContactForm()">
                                                    <fieldset>
                                                        <div id="frmFirstNameC_error_msg<?=$company->id?>"  class="control-group" >
                                                          <label class="control-label" for="firstName">First name</label>
                                                          <div class="controls">
                                                           
                                                            <input type="text" name="firstName" id="firstName_<?=$company->id?>" size="50" required>
                                                            <span class="help-inline_frmFirstNameC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                          </div>
                                                        </div>
        
                                                        <div id="frmLastNameC_error_msg<?=$company->id?>"  class="control-group" >
                                                          <label class="control-label" for="lastName">Last name</label>
                                                          <div class="controls">
                                                                <input type="text" name="lastName" id="lastName_<?=$company->id?>" size="50" required>
                                                            <span class="help-inline_frmLastNameC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                          </div>
                                                        </div>
                                                      
                                
                                                        <div id="frmEmailC_error_msg<?=$company->id?>"  class="control-group" >
                                                          <label class="control-label" for="lastName">E-mail address</label>
                                                          <div class="controls">
                                                                <input type="text" name="email" id="frmEmailC_error_msg<?=$company->id?>" size="50" required>
                                                            <span class="help-inline_frmEmailC_error_msg<?=$company->id?>" style="display:none;">Please enter valid email address</span>
                                                          </div>
                                                        </div>
                                                        
                                                        
                                                        <div id="frmDescriptionC_error_msg<?=$company->id?>"  class="control-group" >
                                                          <label class="control-label" for="lastName">Message</label>
                                                          <div class="controls">
                                                            
                                                                <textarea class="form-control" rows="3" style="height:100px;" name="description" id="description_<?=$company->id?>" required></textarea>
                                                            <span class="help-inline_frmDescriptionC_error_msg<?=$company->id?>" style="display:none;">This is a required field</span>
                                                          </div>
                                                        </div>
                                                        <div>
                                                            
                                                        </div>
                                                        
                                                       
                                                         <div class="clearfix clear-left">
                                                        <div class="button-row col-md-12 col-xs-12">
                                                            <button style="margin-bottom: 10px;" type="button" class="ui-dir-button" onclick="contactCompany(<?=$company->id?>)">
                                                                <span class="ui-button-text"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Message</span>
                                                            </button>
                            
                                                            <button style="margin-left:0px;" type="button" class="ui-dir-button ui-dir-button-grey" onclick="jQuery.unblockUI()">
                                                                <span class="ui-button-text">CANCEL</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    
                                
                                
                                                        <?php echo JHTML::_( 'form.token' ); ?>
                                                        <input type='hidden' name='task' value='companies.contactCompany' />
                                                        <input type='hidden' name='userId' value="<?=$user->id?>" />
                                                        <input type="hidden" id="companyId" name="companyId" value="<?=$company->id?>" />
                                                    </fieldset>
                                                    </form>
                                               
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <a class="ui-dir-button" href='javascript:showContactCompany(<?=$company->id?>)'>
                                <span class="ui-button-text" style="font-size: 13px;">
                                    <i class="fa fa-envelope"></i>
                                    Contact this Provider
                                </span>
                            </a>
                </div>
            </div>
        </div>

<?php
        }
    }
?>



</div>


    
</div>

<style>
    .search-style-1 .business-container, .list-intro .business-container{
        margin-left: 0px !important;
    }

    table {
        text-align: left;
        line-height: 40px;
        border-collapse: separate;
        border-spacing: 0;
        border: 2px solid #1279bf;
        width: 100%;
        background-color: #ebebeb;
        border-radius: .25rem;
    }

    thead tr:first-child {
        background: #1279bf;
        color: #fff;
        border: none;
    }

    th:first-child, td:first-child {
        /*padding: 0 0px 0 20px;*/
        padding: 0 25px 0 20px !important;
    }

    thead tr:last-child th {
        border-bottom: 3px solid #ddd;
    }

    tbody tr:last-child td {
        border: none;
    }

    tbody td {
        border-bottom: 1px solid #1279bf;
    }

    td:last-child {
        text-align: left;
        padding-right: 10px;
    }
    p {
        margin-bottom: 13px !important;
        margin-top: 0px;
    }

    #search-path{
        display: none;
    }
    #search-path ul{
        padding-bottom: 15px !important;
    }
    #search-filter > h3{
        background-color: #f2f2f2;
        text-transform: uppercase;
        border-radius: 4px;
        padding: 8px;
        text-align: center;
    }
    .search-category-box{
        padding-top: 13px;
        font-family: "Source Sans Pro",sans-serif !important;
    }
</style>

<script>

function showQuoteCompany(companyId){
    jQuery("#company-quote #companyId").val(companyId);
    jQuery.blockUI({ message: jQuery('#company-quote'), css: {width: 'auto',top: '10%', left:"0", position:"absolute"} });
    jQuery('.blockUI.blockMsg').center();
    jQuery('.blockOverlay').click(jQuery.unblockUI); 

}   

function showContactCompany(companyId){
    
    jQuery("#company-contact"+companyId+" #companyId").val(companyId);
    jQuery.blockUI({ message: jQuery("#company-contact"+companyId), css: {width: 'auto',top: '10%', left:"0", position:"absolute"} });
    jQuery('.blockUI.blockMsg').center();
    jQuery('.blockOverlay').click(jQuery.unblockUI); 

}   

function requestQuoteCompany(){
    var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=companies.requestQuoteCompanyAjax', false, -1); ?>";
    var postData="";
    postData +="&firstName="+jQuery("#company-quote #firstName").val();
    postData +="&lastName="+jQuery("#company-quote #lastName").val();
    postData +="&email="+jQuery("#company-quote #email").val();
    postData +="&description="+jQuery("#company-quote #description").val();
    postData +="&companyId="+jQuery("#company-quote #companyId").val();
    postData +="&category="+jQuery("#company-quote #category").val();
    postData +="&recaptcha_response_field="+jQuery("#company-quote #recaptcha_response_field").val();
    postData +="&g-recaptcha-response="+jQuery("#company-quote #g-recaptcha-response-1").val();
    
    jQuery.post(baseUrl, postData, processContactCompanyResult);
}

function contactCompany(id){
    
    
    if( validateContactForm(id) == true){
        var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=companies.contactCompanyAjax', false, -1); ?>";
        
        var postData="";
        postData +="&firstName="+jQuery("#company-contact"+id+" #firstName_"+id+"").val();
        postData +="&lastName="+jQuery("#company-contact"+id+" #lastName_"+id+"").val();
        postData +="&email="+jQuery("#company-contact"+id+" #email_"+id+"").val();
        postData +="&description="+jQuery("#company-contact"+id+" #description_"+id+"").val();
        postData +="&companyId="+jQuery("#company-contact"+id+" #companyId").val();
        postData +="&recaptcha_response_field="+jQuery("#captcha-div-contact #recaptcha_response_field").val();
        postData +="&g-recaptcha-response="+jQuery("#captcha-div-contact #g-recaptcha-response").val();
         console.log(postData);
        jQuery.post(baseUrl, postData, processContactCompanyResult);
    }
}


function processContactCompanyResult(responce){
    var xml = responce;
    jQuery(xml).find('answer').each(function()
    {
        if( jQuery(this).attr('error') == '1' ){
             jQuery.blockUI({ 
                    message: '<strong>An error has occurred!</strong><br/><br/><p>'+jQuery(this).attr('errorMessage')+'</p>'
                }); 
             setTimeout(jQuery.unblockUI, 2000); 
        }else{
             jQuery.blockUI({ 
                
                    message:"<h3>Business has been successfully contacted!</h3>"
                }); 
             setTimeout(jQuery.unblockUI, 2000); 
        }
    });
}

function validateContactForm(idx){

    var form  = document.getElementById("contactCompanyFrm"+idx);
    //var form = document.contactCompanyFrm;
    var isError = false;
    console.log(form);
    
    jQuery(".error_msg").each(function(){
        jQuery(this).hide();
    });
    
    if( !validateField( form.elements['firstName'], 'string', false, null ) ){
        //console.debug("firstName");
        jQuery("#frmFirstNameC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmFirstNameC_error_msg"+idx).show();
        
        if(!isError)
            jQuery("#firstName_"+idx).focus();
        isError = true;
    }

    if( !validateField( form.elements['lastName'], 'string', false, null ) ){
        jQuery("#frmLastNameC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmLastNameC_error_msg"+idx).show();
        if(!isError)
            jQuery("#lastName_"+idx).focus();
        isError = true;
    }

    if( !validateField( form.elements['email'], 'email', false, null ) ){
        jQuery("#frmEmailC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmEmailC_error_msg"+idx).show();
        if(!isError)
            jQuery("#email_"+idx).focus();
        isError = true;
    }
    
    if( !validateField( form.elements['description'], 'string', false, null ) ){
        jQuery("#frmDescriptionC_error_msg"+idx).addClass('error');
        jQuery(".help-inline_frmDescriptionC_error_msg"+idx).show();
        if(!isError)
            jQuery("#description_"+idx).focus();
        isError = true;
    }
    
    //console.debug(isError);
    return !isError;


}


</script>


<script type="text/javascript">
    jQuery(document).ready(function(){
        setTimeout(function(){
            var url = jQuery("#article_0").attr('data-meta');
            var newDiv = jQuery('<form '+url+'></form>');
            jQuery(newDiv).html( jQuery("#article_0 > fieldset").html() );
            jQuery("#article_0").replaceWith( newDiv );
        }, 2000);
    });
</script>


<style type="text/css">
    .btn_contact{
        margin-top: 21%;
        padding: 9px;
    }
    .samll_item{
        padding-right: 18px;
    }

    .pagination{
        text-align: center;
        padding-top: 6px;
    }

    @media (max-width: 991px){
        .btn_contact{
            margin-top: 0%;
            margin-left: 14px;
        }
    }

    @media (max-width: 425px){
        .sec_show_de{
            text-align: center;
        }
        .btn_contact{
            text-align: center;
        }
        .samll_item{
            padding-right: 25px;
            margin-top: -30px;
        }
    }

    @media (max-width: 806px){
        .result_total{
            padding-top: 25px;
        }
    }
    
    @media (max-width: 767px){
        .col-xs-12{
            width: 41.66666667%;
        }

        .btn_contact{
            margin-left: -100px;
        }

            
    }

    @media (max-width: 640px){
        .col-xs-12{
            width: 100%;
            text-align: center;
        }

         .btn_contact{
                margin-left: 7px;
        }
       

    }


    

</style>
