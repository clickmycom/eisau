ห<?php // no direct access
/**
* @copyright	Copyright (C) 2008-2009 CMSJunkie. All rights reserved.
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

defined('_JEXEC') or die('Restricted access');
?>
<h2 id="title-form">Provider Registration</h2>
<hr>

<div id="step-1" class="step-content" style="display: block;">
    <h3><strong><span style="color: #1279bf;">Publicise your business to Schools. List with Eisau. </span></strong></h3>
    <h3><strong><span style="color: #1279bf;">BUY NOW!  </span></strong></h3>
    <h3><strong><span style="color: #1279bf;">SEIZE THE OPPORTUNITY TO EXTEND YOUR REACH INTO SCHOOLS!</span></strong></h3>
    <div class="col-md-12 col-sm-12">
        <div class="quick-link-box">
            <div class="col-md-3 col-sm-6">
                <img src="images/wat_one.svg" class="col-md-1 col-sm-1" style="width: 45px;padding-bottom: 15px;padding-top: 15px;">
                <a class="col-md-8 col-xs-8" href="#" style="padding: 20px 7px 7px 16px;color: #000;">Have you already worked in a school?.</a>
            </div>

            <div class="col-md-3 col-sm-6">
                <img src="images/wat_two.svg" class="col-md-1 col-sm-1" style="width: 45px;padding-bottom: 15px;padding-top: 15px;">
                <a class="col-md-8 col-xs-8" href="#" style="padding: 20px 7px 7px 16px;color: #000;">Register and buy your listings.</a>
            </div>

            <div class="col-md-3 col-sm-6">
                <img src="images/wat_tee.svg" class="col-md-1 col-sm-1" style="width: 45px;padding-bottom: 15px;padding-top: 15px;">
                <a class="col-md-8 col-xs-8" style="padding: 20px 7px 7px 16px;color: #000;">Complete your business listings and have theme appear instantaneously on Eisau.</a>
            </div>

            <div class="col-md-3 col-sm-6">
                <img src="images/wat_for.svg" class="col-md-1 col-sm-1" style="width: 45px;padding-bottom: 15px;padding-top: 15px;">
                <a class="col-md-8 col-xs-8" href="#" style="padding: 19px 7px 7px 16px;color: #000;text-shadow: none;">Receive enquiries and business from schools.</a>
            </div>
        </div>
    </div>

    <br>
    <br>
    <p>We welcome registrations from those businesses which have previously supplied schools or pre-schools. Check the
        <span style="color: #ff0000;">
                    <strong>
                        <a style="color: #ff0000;" href="index.php/schools/industries-categories" target="_blank" rel="alternate">industries and categories</a>
                    </strong>
                </span>to see which ones suit your business. Need more information?
        <span style="color: #ff0000;">
                    <a style="color: #ff0000;" href="index.php/about-us/contact-us" target="_blank" rel="alternate">Contact us</a>
                </span>&nbsp;To find our more
        <span style="color: #ff0000;">
                    <strong>
                        <a style="color: #ff0000;" href="http://youtu.be/52Phb_OUSkM" target="_blank" rel="alternate">watch our video</a>
                    </strong>.
                </span>
    </p>
    <br>
    <br>
    <h3><span style="color: #1279bf;"><strong>Benefits to your company:</strong></span></h3>

    <table id="camera" style="background: #fff; padding-top: 10px;" border="0">
        <tbody>
            <tr>
                <td valign="top" width="350px">
                    <ul class="size13">
                        <li><strong>Let schools search for you at www.eisau.com.au</strong>
                        </li>
                        <li><strong>Gain the advantage of having one or more testimonials vouching for the quality of your goods or services</strong>
                        </li>
                        <li><strong>Flexibility and control over your own advertising- you describe what your business offers</strong>
                        </li>
                    </ul>
                </td>
                <td valign="top" width="350px">
                    <ul class="size13">
                        <li><strong>Effective cost efficient promotion of your company to all types of schools</strong>
                        </li>
                        <li><strong>Australia wide</strong>
                        </li>
                        <li><strong>Privileged access to a niche markets</strong>
                        </li>
                        <li><strong>Update your information as required</strong>
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    <p>  
        <span style="text-decoration: underline;">Note</span>: If your school contact is providing a verbal rather than written testimonial, insert the name of the referee, job title, school and phone number in the testimonial field.
    </p>
    <br>
    <br>
    <address class="h4">
        <span style="color: #0077c0;">
            <span style="text-decoration: underline;">
             <strong style="font-family: &quot;Source Sans Pro&quot;,sans-serif;font-size: 18px;">BUY NOW TO ENJOY THE BENEFITS OF MEMBERSHIP</strong>
            </span>
        </span>
    </address>
    
    <address>
        <strong style="color: #0077c0; line-height: 1.8em; font-style: normal; letter-spacing: 0em;">Membership ensures your business will be&nbsp;publicized&nbsp;to schools!&nbsp;</strong>
    </address>
    <ul>
        <li><strong style="letter-spacing: 0em; line-height: 1.8em; font-style: normal;"><span style="color: #0077c0;">$499 FOR 12 MONTHS - less than $42</span></strong></li>
        <li>
            <strong style="letter-spacing: 0em; line-height: 1.8em;">
                <span style="color: #0077c0;">
                    <strong>Additional Category listings - $179 FOR 12 MONTHS - less than $15 per month (normally $179 for 12 MONTHS)</strong>
                </span>
            </strong>
        </li>
    </ul>

    <strong style="letter-spacing: 0em; line-height: 1.8em; font-style: normal;">
        <span style="color: #0077c0;">
            <strong>(Quoted prices above exclude GST)&nbsp;</strong>
        </span>
    </strong>

    <br>
    <br>
    <br>
    <table id="productlisttable" class="productlisttable" border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td width="447" style="padding: 0 1px 1px 0; text-align: left; vertical-align: top;">
                    <span class="productlistitem" id="productlistitem1060640" style="float: left; width: 447px; margin: 0px; ">
                        <span class="productlistitemmid" style="float: left; width: 447px; ">
                            <span class="productlistimage" style="float: left; position: relative; padding: 0 0 3px 0; width: 447px; height: 36px;">
                                <a href="http://www.eisau.com.au/shop/join-membership-listing/membership/" style="display: block; position: relative;">
                                    <img src="http://www.eisau.com.au/uploads/62948/eisau1.jpg" data-original="http://www.eisau.com.au/uploads/62948/eisau1.jpg" width="129" height="36" class="lazy" alt="" title="" style="display: inline;">
                                </a>
                            </span>
                    
                            <span class="productlistdesc" style="float: left; width: 447px;">
                                <span style="display: block; padding-top: 0px;">
                                    <a class="productlistproductheadinglink" href="http://www.eisau.com.au/shop/join-membership-listing/membership/" style="display: block;">
                                        <div class="heading productlistproductheading">
                                            <span class="ellipsis_text" style="color:red;font-weight: 600;">Membership and First Listing</span>
                                        </div>
                                    </a>
                                </span>
                                <br>
                                <span>
                                    <p style="" id="displayprice1060640" class="v_container">
                                        <strong>
                                            <span class="price exchangerateprice" data-tws-price="548.90">548.90</span>
                                            <span class="gstlabel">inc GST</span>
                                        </strong>
                                        <br>
                                    </p>
                                </span> 

                                <div class="productlistaddtocartbutton" style="clear: both;">
                                    <br>
                                    <button style="background:#0077c0;" class="btn  btn_add_to_cart sw-btn-next" type="button" alt="Add To Cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add To Cart
                                    </button> 
                                </div>
                            </span>
                        </span>
                    </span>
                </td>
            </tr>
            <input type="hidden" name="BulkQuantity1060640" value="1" updatewhenoptionselected="1">
            <input type="hidden" name="ProductName_1060640" value="Membership and First Listing">
            <input type="hidden" name="ProductID_1060640" class="productid" id="1060640">
        </tbody>
    </table>
</div>

	








<br/>
<div id="orders-holder">

	<?php foreach($this->orders as $order){?>
		<div class="order-container">
			<div class="order-details">
				<h3> <?php echo ($order->order_id) ?></h3>
				<h4> <?php echo ($order->businessName) ?></h4>
				<?php echo $order->state == 0 ? JText::_("LNG_AWAITING_PAYMENT"):JText::_("LNG_PAID")  ?> | <?php echo JBusinessUtil::getPriceFormat($order->amount) ?> | <?php echo $order->created ?>
			</div>
			
			<div class="order-options">
				<?php if($order->state == 0){?>
					<button type="button" class="ui-dir-button" onclick="payOrder(<?php echo $order->id ?>)">
							<span class="ui-button-text"><?php echo JText::_("LNG_PAY_NOW")?></span>
					</button>
				<?php }?>
				<button type="button" class="ui-dir-button ui-dir-button-green" onclick="showInvoice(<?php echo $order->id; ?>)">
						<span class="ui-button-text"><?php echo JText::_("LNG_DETAILS")?></span>
				</button>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	<div class="clear"></div>
	
</div>

<form id="payment-form" name="payment-form" method="post" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=billingdetails&layout=edit') ?>"> 
	<input type="hidden" name="orderId" id="orderId" value="" /> 
</form>




<div id="invoice" class="invoice" style="display:none">
	<div id="dialog-container">
		<div class="titleBar">
			<span class="dialogTitle" id="dialogTitle"></span>
			<span  title="Cancel"  class="dialogCloseButton" onClick="jQuery.unblockUI();">
				<span title="Cancel" class="closeText">x</span>
			</span>
		</div>
		
		<div class="dialogContent">
			<iframe id="invoiceIfr" height="500" src="">
			
			</iframe>
		</div>
	</div>
</div>



<script>
// starting the script on page load
	jQuery(document).ready(function(){
	});		
	
	function showInvoice(invoice){
		var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=invoice&tmpl=component'); ?>";
		baseUrl = baseUrl + "&invoiceId="+invoice;
		jQuery("#invoiceIfr").attr("src",baseUrl);
		jQuery.blockUI({ message: jQuery('#invoice'), css: {width: 'auto', top: '5%', left:"0", position:"absolute"} });
		jQuery('.blockOverlay').click(jQuery.unblockUI); 
		jQuery('.blockUI.blockMsg').center();
	}

	function payOrder(orderId){
		jQuery("#orderId").val(orderId);
		jQuery("#payment-form").submit();
	}
</script>


<style>

.rsform-error{
border: 1px solid #822525;
}
.alert{
display:none;
}

div[class^='btn-group navbar-btn sw-btn-group-extra pull-right']{
 display:none;
}

div[class^='btn-group navbar-btn sw-btn-group pull-right']{
margin:0 !important;
}

.formNoError {
    color: #cf4d4d;
    font-size: 10px;
    font-weight: 700;
}

button.btn.btn-danger{
     background: #ee3928 !important;
}
button.btn.btn-danger:hover{
 background: #0077c0 !important;
}

div[class^='btn-group navbar-btn sw-btn-group-extra pull-right']{
    margin: 0 !important;
}

.form-horizontal .form-group {
    margin-right: 0 !important;
    margin-left: 0 !important;
}

div[class^='btn-group navbar-btn sw-btn-group-extra pull-right']{
float:left !important;
}


.btn-group>.btn:last-child:not(:first-child), .btn-group>.dropdown-toggle:not(:first-child){
background: #0077c0;
    color: #fff !important;
    text-shadow: none !important;
    border: none !important;
    font-size: 15px !important;
    border-radius: 8px !important;
    padding: 6px 23px !important;

}
div[class^='btn-group navbar-btn sw-btn-group pull-right']{
margin-left: 0 !important;
}

.btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    background: #0077c0;
    color: #fff !important;
    text-shadow: none !important;
    border: none !important;
    font-size: 15px !important;
    border-radius: 8px !important;
    padding: 6px 23px !important;
    margin: 0 10px 0 0px;
}

.btn:hover{
background:#ee3928 !important;
}

.btn_add_to_cart{
    background: #0077c0;
    color: #fff !important;
    text-shadow: none !important;
    border: none !important;
    font-size: 15px !important;
    border-radius: 8px !important;
    padding: 6px 23px !important;
}

.btn_add_to_cart:hover{
background:#000;
}

label#txt_i_agree {
    padding-left: 14px;

}
h2#title-form {
    margin-left: 0px;
    margin-right: 15px;
    color: #eb3b32;
    font-weight: bold;
}
.col-lg-3 {
padding-right: 0px !important;
 }

.well {
	display:none !important;
}
@media (min-width: 1200px){
.col-lg-3 {
    width: 25%;
   padding-left : 0 !important;
padding-right: 0px !important;
 }
}

#content {
    /* margin-top: 15px; */
    margin-bottom: 19px !important;
    margin-top: 0px !important;
    padding-right: 0;
}
h2 {
    font-size: 22px;
    line-height: 24px;
    color: #eb3b32;
    font-weight: 600;
}
p {
    margin-bottom: -8px !important;
    margin-top: 0px;
}
/* SmartWizard Basic CSS */
.sw-main {
    position: relative;
    display:block;
    margin:0;
    padding:0;
}
.sw-main .sw-container {
    display: block;
    margin: 0;
    padding: 0;
    overflow:hidden;
    position: relative;
}
.sw-main .step-content {
    display:none;
    position: relative;
    margin:0;
}
.sw-main .sw-toolbar{
    margin-left: 0;
}



/* Responsive CSS */
@media screen and (max-width: 768px) {
#detail_pages {
    padding: 0% !important;
}
.row {
    margin-left: 0px !important;
 margin-right: 0px !important;
}

label#txt_i_agree {
    padding-left: 0;
    padding-top: 30px;
    padding-bottom: 20px;
}

    .sw-theme-default > .nav-tabs > li {
        float: none !important;
    }


div[class^='sw-container tab-content']{
   height:980px;
}

.form-horizontal .form-group {
    margin:0px !important;
}

#pro_subpplied_to{
padding-right: 0 !important;
}

.form-horizontal .control-label {
    float: left;
    padding-top: 5px;
    text-align: left !important;
 }

}



/* Loader Animation
Courtesy: http://bootsnipp.com/snippets/featured/loading-button-effect-no-js
*/
@-webkit-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-moz-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-o-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
.sw-theme-default > ul.step-anchor > li.loading:before {
    content: '';
    display: inline-block;
    position: absolute;
    background: transparent;
    border-radius: 50%;
    box-sizing: border-box;
    border: 2px solid #fff;
    border-top-color: transparent;
    border-bottom-color: transparent;
    border-left-color: #4285f4;
    border-right-color: #4285f4;
    top: 50%;
    left: 50%;
    margin-top: -16px;
    margin-left: -16px;
    width: 32px;
    height: 32px;
    -webkit-animation: ld 1s ease-in-out infinite;
    -moz-animation:    ld 1s ease-in-out infinite;
    -o-animation:      ld 1s ease-in-out infinite;
    animation:         ld 1s ease-in-out infinite;
}

/* SmartWizard Theme: Arrows */
.sw-theme-arrows{
    border-radius: 5px;
}
.sw-theme-arrows > .sw-container {
    min-height: 200px;
}
.sw-theme-arrows .step-content {
       padding: 10px 10px;
    /* border: 0px solid #D4D4D4; */
   /* border: 1px solid #ddd; */
    background-color: #FFF;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
.sw-theme-arrows .sw-toolbar{
    padding-left: 10px;
    padding-right: 10px;
    margin-bottom: 0 !important;
    border-radius: 5px;
}
.sw-theme-arrows > .sw-toolbar-top{
display:none;
}

.btn-toolbar {
    margin-top: 10px !important;
}

.sw-theme-arrows > .sw-toolbar-bottom{
}
.sw-theme-arrows > ul.step-anchor{
     border: 0;
    /* border: 1px solid #ddd; */
    border-right: 1px solid #ddd;
    border-top: 1px solid #ddd;
    padding: 0px;
    background: #f5f5f5;
    border-radius: 5px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    list-style: none;
    overflow: hidden;
    margin-bottom: 0px;
}
.sw-theme-arrows > ul.step-anchor li+li:before {
	padding: 0;
}
.sw-theme-arrows > ul.step-anchor > li {
}

.sw-theme-arrows > ul.step-anchor > li > a, .sw-theme-arrows > ul.step-anchor > li > a:hover  {
	color: #bbb;
font-size: 16.1px;
    font-weight: 500;
	text-decoration: none;
	padding: 10px 0 10px 40px;
	position: relative;
	display: block;
    border: 0 !important;
    border-radius: 0;
    outline-style:none;
    background: #f5f5f5;
}
.sw-theme-arrows > ul.step-anchor > li > a:after {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	border-bottom: 50px solid transparent;
	border-left: 30px solid #f5f5f5;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	left: 100%;
	z-index: 2;
}
.sw-theme-arrows > ul.step-anchor > li > a:before {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid #ddd;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	margin-left: 1px;
	left: 100%;
	z-index: 1;
}
.sw-theme-arrows > ul.step-anchor > li:first-child > a {
	padding-left: 15px;
}
.sw-theme-arrows > ul.step-anchor > li > a:hover  {
    color: #bbb;
    text-decoration: none;
    outline-style:none;
    background: #f5f5f5;
    border-color: #f5f5f5;
}
.sw-theme-arrows > ul.step-anchor > li > a:hover:after {
    border-left-color: #f5f5f5;
}
.sw-theme-arrows > ul.step-anchor > li > a small{
}
.sw-theme-arrows > ul.step-anchor > li.clickable > a:hover {
    color: #4285F4 !important;
	background: #46b8da !important;
}
.sw-theme-arrows > ul.step-anchor > li.active > a {
    border-color: #1279bf !important;
    color: #fff !important;
	background: #1279bf !important;
}
.sw-theme-arrows > ul.step-anchor > li.active > a:after {
	border-left:30px solid #1279bf !important;
}
.sw-theme-arrows > ul.step-anchor > li.done > a {
    border-color:#56b48c !important;
    color: #fff !important;
    background: #56b48c !important;
}
.sw-theme-arrows > ul.step-anchor > li.done > a:after {
	    border-left: 30px solid #56b48c;
}
.sw-theme-arrows > ul.step-anchor > li.danger > a {
    border-color: #d9534f !important;
    color: #d9534f !important;
    background: #fff !important;
}
.sw-theme-arrows > ul.step-anchor > li.disabled > a, .sw-theme-arrows > ul.step-anchor > li.disabled > a:hover {
    color: #eee !important;
}

/* Responsive CSS */
@media screen and (max-width: 768px) {
    .sw-theme-arrows > ul.step-anchor{
        border: 0;
        background: #ddd !important;
    }
    .sw-theme-arrows > .nav-tabs > li {
        float: none !important;
        margin-bottom: 0;
    }
    .sw-theme-arrows > ul.step-anchor > li > a, .sw-theme-arrows > ul.step-anchor > li > a:hover {
        padding-left: 15px;
        margin-right: 0;
        margin-bottom: 1px;
    }
    .sw-theme-arrows > ul.step-anchor > li > a:after, .sw-theme-arrows > ul.step-anchor > li > a:before {
        display: none;
    }
}

/* Loader Animation
Courtesy: http://bootsnipp.com/snippets/featured/loading-button-effect-no-js
*/
@-webkit-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-moz-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-o-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
.sw-theme-arrows > ul.step-anchor > li.loading:before {
    content: '';
    display: inline-block;
    position: absolute;
    background: transparent;
    border-radius: 50%;
    box-sizing: border-box;
    border: 2px solid #fff;
    border-top-color: transparent;
    border-bottom-color: transparent;
    border-left-color: #4285f4;
    border-right-color: #4285f4;
    top: 50%;
    left: 50%;
    margin-top: -16px;
    margin-left: -16px;
    width: 32px;
    height: 32px;
    z-index: 99;
    -webkit-animation: ld 1s ease-in-out infinite;
    -moz-animation:    ld 1s ease-in-out infinite;
    -o-animation:      ld 1s ease-in-out infinite;
    animation:         ld 1s ease-in-out infinite;
}
/* Loader Animation
Courtesy: http://bootsnipp.com/snippets/featured/loading-button-effect-no-js
*/
@-webkit-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-moz-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@-o-keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
@keyframes ld {
  0%   { transform: rotate(0deg) scale(1); }
  50%  { transform: rotate(180deg) scale(1.1); }
  100% { transform: rotate(360deg) scale(1); }
}
.sw-theme-arrows > ul.step-anchor > li.loading:before {
    content: '';
    display: inline-block;
    position: absolute;
    background: transparent;
    border-radius: 50%;
    box-sizing: border-box;
    border: 2px solid #fff;
    border-top-color: transparent;
    border-bottom-color: transparent;
    border-left-color: #4285f4;
    border-right-color: #4285f4;
    top: 50%;
    left: 50%;
    margin-top: -16px;
    margin-left: -16px;
    width: 32px;
    height: 32px;
    z-index: 99;
    -webkit-animation: ld 1s ease-in-out infinite;
    -moz-animation:    ld 1s ease-in-out infinite;
    -o-animation:      ld 1s ease-in-out infinite;
    animation:         ld 1s ease-in-out infinite;
}



h1{
    color: #333 !important;
}

ul.step-anchor > li.active > a {
    border-color: #5bc0de !important;
    color: #fff !important;
    background: #5bc0de !important;
}

ul.step-anchor > li > a:before {
    content: " ";
    display: block;
    width: 0;
    height: 0;
    border-top: 50px solid transparent;
    border-bottom: 50px solid transparent;
    border-left: 30px solid #ddd;
    position: absolute;
    top: 50%;
    margin-top: -50px;
    margin-left: 1px;
    left: 100%;
    z-index: 1;
}

ul.step-anchor > li.active > a:after {
    border-left: 30px solid #5bc0de !important;
}

#wat____0043333 {
    margin-bottom: 18px !important;
    margin-top: 49px;
}
.btn_submit_001{
border-radius: 4px 30px 30px 4px;
    -moz-border-radius: 4px 30px 30px 4px;
    padding: 10px 52px 10px 16px;
    color: #fff;
    font-size: 17px;
    font-weight: 400;
    border: 0;
    cursor: pointer;
    vertical-align: top;
    background: #33b1ff;
    margin-top: 8px;
}

.progress {
 display:none !important;
}
.radio, .checkbox {
    min-height: 18px;
    padding-left: 10px !important;
    font-size: 15px !important;
}
.form-horizontal .control-label {
    float: left;

    padding-top: 5px;
    text-align: right;
    line-height: 22px;
padding-right: 15px !important;
}

fieldset {
    border: none !important;
    margin: 0 2px;
    padding: 1.35em .625em .75em !important;
}
.form-layout {
background-color: #404040;
padding: 20px;
}

.f-fname, .f-lname, .f-name {
    background-image: url(/images/icons/f-name.png);
}

input[type="text"], .form-control , .formResponsive input[type="password"] {

    background-color: #f8f9fa !important;
    height: 30px;
    padding-left: 55px;
    width: 40%;
}
input[type="text"], .form-control ,input[type="password"] {
    display: block;
    width: 100%;
    height: 34px !important;
    padding: 6px 12px;
    font-size: 15px;
    line-height: 1.369;
    color: #1a1a1a;
    background-color: #f8f9fa;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.formResponsive button[type="submit"]{
    border-radius: 4px 30px 30px 4px;
    -moz-border-radius: 4px 30px 30px 4px;
    padding: 10px 52px 10px 16px;
    color: #fff;
    font-size: 17px;
    font-weight: 400;
    border: 0;
    cursor: pointer;
    vertical-align: top;
    background: #33b1ff;
    margin-top: 8px;
}

 .formResponsive input[type="text"]{
    color: #333333 !important;
background-color: #f6f6f6 !important;
height: 30px;
padding-left: 55px;
    width: 40%;
}
.formControlLabel {
    color: #999 !important;
    font-size: 16px;
    margin-bottom: 4px!important;
    line-height: 22px !important;
}


h1 {
    font-size: 48px;
    margin-top: 30px;
    line-height: 43px;
    color: #e6e6e6;
font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
    font-weight: 200;
}

.wat{
       background: url(../../images/form/support_man.png) no-repeat scroll 16px 10px !important;
    background-size: 22px !important;
}

.email_icon{
   background: url(../../images/form/email.png) no-repeat scroll 16px 11px !important;
    background-size: 22px !important;
}

.phone_icon{
    background: url(../../images/form/phone.png) no-repeat scroll 15px 12px !important;
    background-size: 24px !important;
}

.pass_icon{
background: url(../../images/form/key_alt.png) no-repeat scroll 17px 11px !important;
    background-size: 21px !important;
}


.company_icon{
     background: url(../../images/form/my_briefcase.png) no-repeat scroll 17px 12px !important;
    background-size: 20px !important;
}

.home_icon{
        background: url(../../images/form/home.png) no-repeat scroll 16px 11px !important;
    background-size: 24px !important;
}

div[id^='aside']  > div:nth-child(3){
display:none !important; 
}

div[id^='aside']  > #wat____004{
display:none !important; 
}



.wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;

    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    border: 2px solid #e0e0e0;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
        width: 30px;
    height: 30px;
    line-height: 28px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #1279bf;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 19px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #eb3b32;
    border: 2px solid #c2c2c2;
    color: #fff;
    
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 33%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
        width: 31px;
    height: 30px;
    margin: 14px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 50px;
}

.wizard h3 {
    margin-top: 0;
}
.step1 .row {
    margin-bottom:10px;
}
.step_21 {
    border :1px solid #eee;
    border-radius:5px;
    padding:10px;
}
.step33 {
    border:1px solid #ccc;
    border-radius:5px;
    padding-left:10px;
    margin-bottom:10px;
}
.dropselectsec {
    width: 68%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.dropselectsec1 {
    width: 74%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.mar_ned {
    margin-bottom:10px;
}
.wdth {
    width:25%;
}
.birthdrop {
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    width: 16%;
    outline: 0;
    font-weight: normal;
}


/* according menu */
#accordion-container {
    font-size:13px
}
.accordion-header {
    font-size:13px;
	background:#ebebeb;
	margin:5px 0 0;
	padding:7px 20px;
	cursor:pointer;
	color:#fff;
	font-weight:400;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px
}
.unselect_img{
	width:18px;
	-webkit-user-select: none;  
	-moz-user-select: none;     
	-ms-user-select: none;      
	user-select: none; 
}
.active-header {
	-moz-border-radius:5px 5px 0 0;
	-webkit-border-radius:5px 5px 0 0;
	border-radius:5px 5px 0 0;
	background:#F53B27;
}
.active-header:after {
	content:"\f068";
	font-family:'FontAwesome';
	float:right;
	margin:5px;
	font-weight:400
}
.inactive-header {
	background:#333;
}
.inactive-header:after {
	content:"\f067";
	font-family:'FontAwesome';
	float:right;
	margin:4px 5px;
	font-weight:400
}
.accordion-content {
	display:none;
	padding:20px;
	background:#fff;
	border:1px solid #ccc;
	border-top:0;
	-moz-border-radius:0 0 5px 5px;
	-webkit-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px
}
.accordion-content a{
	text-decoration:none;
	color:#333;
}
.accordion-content td{
	border-bottom:1px solid #dcdcdc;
}



@media( max-width : 585px ) {
#detail_pages {
    padding: 3% !important;
}
    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 35px;
        height: 35px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}


.nav-tabs {
    border-bottom: 1px solid #fff;
}

#smartwizard>.sw-container>#step-2{
 height: auto !Important;
}
.container,.col-lg-3.col-md-3.col-sm-4.col-xs-12,.col-lg-2.col-md-3.col-sm-4.col-xs-12{
padding-left: 0;
padding-right: 0;
}

.wraper-j{
 margin:10px auto ;
display: inline-block;
}
p.content-box{
   display: block;
    padding-left: 10px;
    padding-right: 20px;
}

@media screen and (max-width: 991px){
     p.content-box{
     display:inline;
   }
}

@media screen and (max-width: 767px){
    .inbox{
       width: 100%;
       float: left;
      padding-bottom: 25px;

     }
    p.content-box{
    display: block;
    position: absolute;
    padding-left: 40px;
    margin-top: -15px;
   }
}

@media screen and (max-width: 550px){
.navbar .btn, .navbar .btn-group{
width: 100% !important;
}

button.btn.btn-default.sw-btn-prev{
 margin-top: 25px !important;
}

.btn-group>.btn:last-child:not(:first-child), .btn-group>.dropdown-toggle:not(:first-child){
margin-top: 4px;
}
}

@media screen and (max-width: 340px) and (min-width: 320px){
    .inbox{
        padding-top: 10px;
   }   
 p.content-box{
    display: block;
    position: absolute;
    margin-top: -18px;
    padding-left: 36px;
   }



}
@media screen and (min-width: 768px){
    .inbox{
       width: 50%;
       float: left;
       padding-bottom: 20px;
      
     }
 
}

@media screen and (min-width: 992px){
    .inbox{
       width: 25%;
       float: left;
display: -webkit-inline-box ;
     }
}
</style>