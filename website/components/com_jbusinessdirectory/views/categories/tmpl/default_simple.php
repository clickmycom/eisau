<style>
	.page-header h1.title{
		margin: 2px 0px -4px !important;
	    color: #f71800 !important;
	    font-family: Arial !important;
	    font-weight: bold !important;
	    font-size: large !important;
	    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
	}
	
	.page-header h1.title hr{
		    box-sizing: content-box;
    height: 0;
    margin: 0;
    padding: 0px !important;
	}
	
	#sidebar_can_remove{
		display:none;
	}
</style>


<div id="categories-container" class="categories-container">
	<div class="clear"></div>
	<div class="row-fluid">
	<?php $k = 0;?>
	<?php foreach($this->categories as $category){
		

		if(isset($category[0]->name)){	
			$k= $k+1;
			
	?>

		<table class="col-lg-12 main_cat" style="border: 2px solid #0077c1;    background: #eee;"><thead>
				
			<?php if(!empty($category[0]->imageLocation)){ ?>
				<div class="category-img-container">
					<img alt="" src="<?php echo JURI::root().PICTURES_PATH.$category[0]->imageLocation ?>">
				</div>
			<?php } ?>

			<tr colspan="5">
				<th colspan="20" style="background: #0077c1;">
				<?php echo $category[0]->name ?></th>
			</tr>
		</thead>
			
		<tbody>
			<?php
			
				$wat_array = array();
				foreach ($category["subCategories"] as $key => $value) {
						
						array_push($wat_array,$value);
				}
				
				$count = count($wat_array);
				$col_num = 2;
				$row_num = ceil($count/$col_num);
				
				
				//new Array
				$new_data = array();
				for($i=0;$i<$row_num;$i++){
					$row = array();
					$index = $i;
					for($j=0;$j<$row_num+1;$j++){
						$row[] = isset($wat_array[$index]) ? $wat_array[$index]: '';
						$index += $row_num;	
					}
					$new_data[] = $row;
				}
			?>
		
			
			<?php
			
				$i=1; 
				$new_wat_array = array();
				foreach($new_data as $row){
					if($i>10)
						break;
					//echo $i++==1?'':'|';
					if ($i % 2 != 0) {
						echo "<tr>";
					}?>
					<?php
						// ที่เอาออกมาจาก href = index.php/find-a-provider/<?=$category[0]->alias
						foreach ($row as $data) {
							if(!empty($data[0]->name)){

							
						echo "<td class='td_bus' style='background-color:#eeeeee; '>";
					?>
							<a class="categoryLink" style="color: #eb3b32;font-weight: 600;" title="<?php echo $data[0]->name?>" alt="<?php echo $data[0]->name?>" 
								href="<?=$data[0]->alias?>">
								<?php echo $data[0]->name ?>
							</a>
					<?php
					
						echo "</td>";
					}
					}
					?>
					
					<?php
					if ($i % 2 <> 0) {
						echo "</tr>";
					} 
				} 
		
		?>

		
		</tbody>	
		</table>
			
		
		
		<?php if($k%3==0){?>
			</div>
			<div class="row-fluid">
		<?php }?>
		
	<?php 
		}
	} 
	?>
	</div>
</div>

<div class="clear"></div>
<style type="text/css">
	

.td_bus{
	width: 50%;
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
table {
  text-align: left;
  line-height: 40px;
  border-collapse: separate;
  border-spacing: 0;
  border: 2px solid #ee3928;
  width: 100%;
      background-color: #fff;
  border-radius: .25rem;
  margin-bottom: 20px;
}

thead tr:first-child {
  background: #ee3928;
  color: #fff;
  border: none;
}

th:first-child, td:first-child { padding: 0 0px 0 20px; }

thead tr:last-child th { border-bottom: 3px solid #ddd; }

@media (max-width: 850px) {
	.td_bus{
		width: auto !important;
		
	}
	table tbody tr td {
    	display: block !important;
    	padding: 0 0px 0 20px !important;
	}


}

@media (max-width: 320px) {
	.td_bus {
    	width: auto !important;
    	font-size: 11px;
	}
}


tbody tr:last-child td { border: none; }
tbody td { border-bottom: 1px solid #ddd;}

td:last-child {
  text-align: left;
  padding-right: 10px;
}
</style>