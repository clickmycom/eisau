
<!-- box4 -->
	<?php
	if($attributeConfig["logo"]!=ATTRIBUTE_NOT_SHOW){?>
		<?php
		if(!$enablePackages || isset($this->item->package->features) && in_array(SHOW_COMPANY_LOGO,$this->item->package->features)) { ?>

			<div class="panel panel-default fie_wid">
				<div class="panel-heading"><h3><i class="fa fa-file-image-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ADD_LOGO');?> <?php $attributeConfig["logo"]=ATTRIBUTE_OPTIONAL?"(". JText::_('LNG_OPTIONAL').")":"" ?></h3> <small class="small_note"><?php echo JText::_('LNG_ADD_LOGO_TEXT');?></small>
				</div>


				<div class="package_content panel-body">

				<div class="box-info">
				<fieldset class="boxed ">
								<!-- /.box-header -->
								<!-- form start -->
						<div class="form-horizontal">
						    <div class="box-body">
						       
						           
						                <div class="form-upload">
						                    <label class="optional" for="logo">
						                        <?php echo JText::_( "LNG_SELECT_IMAGE_TYPE") ?>.</label>
						                    
						                    <input type="hidden" name="logoLocation" id="imageLocation" value="<?php echo $this->item->logoLocation?>">
						                    <input type="hidden" id="MAX_FILE_SIZE" value="2097152" name="MAX_FILE_SIZE">
						                    
						                    
						                    
						                    
						                    
						                    <input type="file" name="uploadLogo" id="imageUploader" size="50" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">

											<label for="imageUploader"> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
					
						                    
						                    
						                    
						                    
						                    <div class="clear"></div>
						                   
						                   
						                 
						                </div>
						                
						                <div class="info">
						                    <?php if($attributeConfig[ "logo"]==ATTRIBUTE_OPTIONAL){ ?>
						                    	
						                    <div class="info-box">
						                        <?php echo JText::_( 'LNG_ADD_LOGO_CONTINUE');?>
						                    </div>
						                    
						                    <?php } ?>
						                </div>
						                
						          
						            
						            <div class="picture-preview" id="picture-preview">
						                <?php if(!empty($this->item->logoLocation)){
						                	
											 echo "<img src='".JURI::root().PICTURES_PATH.$this->item->logoLocation."' />"; 
								
										?>
										
											   <a href="javascript:removeLogo()">
						                        	<?php echo JText::_( "LNG_REMOVE_LOGO")?>
						                    	</a>
										<?php
										
										} ?>
									
						            </div>
						            	
						           		
						            <div class="clear"></div>
						            <span class="error_msg" id="frmCompanyName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
						       
						    </div>
						</div>
					</fieldset>
				
				</div> </div>
			</div>
		<?php
		} ?>
	<?php
	} ?>
<!-- end box4 -->

<div class="panel panel-default fie_wid">
		<div class="panel-heading"><h3> <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_PICTURES');?></h3>
			<small class="small_note"><?php echo JText::_('LNG_COMPANY_PICTURE_INFORMATION_TEXT');?>.</small>
		</div>
		<div class="panel-body" style="padding-top: 0;">

	<?php
	if($attributeConfig["pictures"]!=ATTRIBUTE_NOT_SHOW) {
		if(!$enablePackages || isset($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features)) { ?>
			<fieldset class="boxed ">

				<TABLE class="admintable" align='center'  id='table_company_pictures' name='table_company_pictures'>
								<?php
								foreach( $this->item->pictures as $picture ) { ?>
									<TR>
											<TD align='left'>
												<textarea cols='50' rows='2' name='company_picture_info[]'  style="width: 100% !important;" id='company_picture_info'><?php echo $picture['company_picture_info']?></textarea>
											</TD>
											<td align='center' id='wat'>
												
												<input type='hidden' value='<?php echo $picture['company_picture_enable']?>' name='company_picture_enable[]' id='company_picture_enable'>
												<input type='hidden' value='<?php echo $picture['company_picture_path']?>' name='company_picture_path[]' id='company_picture_path'>
											</td>
											<td align='center'>
												<img class='btn_picture_delete'
													src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_options.gif"?>'
													onclick="
														if(!confirm('<?php echo JText::_('LNG_CONFIRM_DELETE_PICTURE',true)?>'))
															return;
														var row = jQuery(this).parents('tr:first');
														var row_idx = row.prevAll().length;
														jQuery('#crt_pos').val(row_idx);
														jQuery('#crt_path').val('<?php echo $picture['company_picture_path']?>');
														jQuery('#btn_removefile').click();"/>
											</td>
											<td align='center'>
												<img class='btn_picture_status'
													src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/".($picture['company_picture_enable'] ? 'checked' : 'unchecked').".gif"?>'
													onclick="
														var form = document.adminForm;
														var v_status = null;
														var pos = jQuery(this).closest('tr')[0].sectionRowIndex;

														if( form.elements['company_picture_enable[]'].length == null ) {
															v_status  = form.elements['company_picture_enable[]'];
														}
														else {
															v_status  = form.elements['company_picture_enable[]'][pos];
														}
														if( v_status.value == '1') {
															jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/unchecked.gif"?>');
															v_status.value ='0';
														}
														else {
															jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/checked.gif"?>');
															v_status.value ='1';
														}"/>
											</td>
											<td>
												<span class="span_up" onclick='var row = jQuery(this).parents("tr:first");  row.insertBefore(row.prev());'>
													<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/up-icon.png">
												</span>
												<span class="span_down"onclick='var row = jQuery(this).parents("tr:first"); row.insertAfter(row.next());'>
													<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/down-icon.png">
												</span>
											</td>
									</TR>
								<?php
								} ?>
				</TABLE>
							
							
				<input type='button' name='btn_removefile' id='btn_removefile' value='x' style='display:none'>
				<input type='hidden' name='crt_pos' id='crt_pos' value=''>
				<input type='hidden' name='crt_path' id='crt_path' value=''>
				<input type='hidden' name='images_included' value="1">
				<TABLE class='picture-table' align='left' border='0'>
				
					<TR id="add-pictures">
						
							
							<label class="optional" for="logo">
						                        <?php echo JText::_( "LNG_SELECT_IMAGE_TYPE") ?>.</label>
							<input type="file" name="uploadfile" id="multiImageUploader"  class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">

							<label for="multiImageUploader"> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
							<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
				
				
				
					</TR>
				</TABLE>
			</fieldset>
		<?php
		} ?>
	<?php
	} ?>
</div></div>