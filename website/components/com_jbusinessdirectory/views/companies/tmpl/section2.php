<div id="edit-tab2" class="edit-tab">
					<?php if($attributeConfig["category"]!=ATTRIBUTE_NOT_SHOW){?>
						<div class="panel panel-default fie_wid">
								<div class="panel-heading"> <h2> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_COMPANY_CATEGORIES');?></h2><small class="small_note"><?php echo JText::_('LNG_SELECT_CATEGORY');?></small> </div>
							<div class="package_content panel-body" style="display: grid;">

								<div class="box-info" style="padding: 15px;">

										<!-- /.box-header -->
										<!-- form start -->
										<div class="form-horizontal">
											<div class="box-body">
												<div class="form-box" >

													<div class="col-sm-5 col-xs-12 col-xs-offset-1 wat_0kakdk" style="margin-left: 4%;">
														<?php if($attributeConfig["category"] == ATTRIBUTE_MANDATORY){?>
															<div  class="form-detail req"></div>
														<?php }?>
														<label for="category"><?php echo JText::_('LNG_CATEGORY');?></label>
															<select name="selectedSubcategories[]" id="selectedSubcategories" class="inputbox input-medium categories_ids-select col-sm-12 col-xs-12" multiple="multiple" style="padding-top: 5px;padding-bottom: 5px;height: 38px;">
																<option value=""><?php echo JText::_('LNG_SELECT_CAT');?></option>
																<?php echo JHtml::_('select.options', $this->categoryOptions, 'value', 'text', $this->item->selCats);?>
															</select>
														<div class="clear"></div>
													</div>
													<div class="col-sm-5 col-xs-offset-1 col-xs-12 wat_kkkaaaa">
														<?php if($attributeConfig["category"] == ATTRIBUTE_MANDATORY){?>
															<div  class="form-detail req"></div>
														<?php }?>
														<label for="subcat_main_id"><?php echo JText::_('LNG_MAIN_SUBCATEGORY');?></label>
														<div class="clear"></div>
														<select class="inputbox input-medium categories_ids-select col-sm-12 col-xs-12 select <?php echo $attributeConfig["category"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" name="mainSubcategory" id="mainSubcategory" style="padding-top: 5px;padding-bottom: 5px;height: 38px;">
														<?php foreach( $this->item->selectedCategories as $selectedCategory){?>
																	<option value="<?php echo $selectedCategory->id ?>" <?php echo $selectedCategory->id == $this->item->mainSubcategory ? "selected":"" ; ?>><?php echo $selectedCategory->name ?></option>
															<?php } ?>
														</select>
														<div class="clear"></div>
														<span class="error_msg" id="frmMainSubcategory_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
													</div>

												</div>
											</div>
										</div>
								</div>
							</div>
							<!-- panel-body -->
						</div>
					<?php }?>

					<?php
					if($this->appSettings->limit_cities == 1) { ?>
						<div class="panel panel-default fie_wid">
							  	<div class="panel-heading">
									<strong>
										<h3>
											<i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ACTIVITY_CITIES');?>
										</h3>
									</strong>
									<small class="small_note"><?php echo JText::_('LNG_ACTIVITY_CITIES_INFO');?>.</small>
								</div>
								<div class="package_content panel-body">
									<div class="box-info" style="padding: 15px;">
									        <!-- /.box-header -->
									        <!-- form start -->
										<div class="form-horizontal">
											<div class="box-body">
									  			<div class="form-box" >
													<div class="form-box">
														<div class="detail_box_3">
															<label for="activity_cities"><?php echo JText::_('LNG_SELECT_ACTIVITY_CITY')?></label>
															<select multiple="multiple" id="activity_cities" class="input_sel select" name="activity_cities[]">
																<option  value=""> </option>
																<?php
																foreach( $this->item->cities as $city ) {
																	$selected = false;
																	foreach($this->item->activityCities as $acity) {
																		if($acity->city_id == $city->id)
																			$selected = true;
																	} ?>
																	<option <?php echo $selected ? "selected" : ""?> value='<?php echo $city->id ?>'>
																		<?php echo $city->name ?>
																	</option>
																<?php
																} ?>
															</select>
															<a href="javascript:checkAllActivityCities()" class="select-all"><?php echo JText::_('LNG_CHECK_ALL',true)?></a>
															<a href="javascript:uncheckAllActivityCities()" class="deselect-all"><?php echo JText::_('LNG_UNCHECK_ALL',true)?></a>
															<div class="clear"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						</div>
					<?php
					} ?>
</div>