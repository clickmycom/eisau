<div id="edit-tab3" class="edit-tab">
				<div class="panel panel-default fie_wid">
					<div class="panel-heading">
						<h3>  <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<stong><?php echo JText::_('LNG_COMPANY_LOCATION');?></stong></h3>
						<small class="small_note"><?php echo JText::_('LNG_COMPANY_LOCATION_TXT');?></small>&nbsp;&nbsp;&nbsp;
						<small class="small_note"><?php echo JText::_("LNG_ADDRESS_SUGESTION")?></small>
					</div>

					<div class="package_content panel-body">

						<div class="box-info" style="padding: 15px;">
							<div class="form-horizontal">
								<div class="box-body">
									<div class="form-box" >
										<label for="address_id"><?php echo JText::_('LNG_ADDRESS')?></label>
										<div class="detail_box">
											<input type="text" id="autocomplete" class="" placeholder="<?php echo JText::_("LNG_ENTER_ADDRESS") ?>" onFocus="" ></input>
											<div class="clear"></div>
										</div>

										<?php if($attributeConfig["street_number"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box">
												<?php if($attributeConfig["street_number"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="address_id"><?php echo JText::_('LNG_STREET_NUMBER')?></label>
												<input type="text" name="street_number" id="street_number" class="input_txt text-input <?php echo $attributeConfig["street_number"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->street_number ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmStreetNumber_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php } ?>

										<?php if($attributeConfig["address"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box">
												<?php if($attributeConfig["address"] == ATTRIBUTE_MANDATORY){?>
												<div  class="form-detail req"></div>
											<?php }?>
												<label for="address_id"><?php echo JText::_('LNG_ADDRESS')?></label>
												<input type="text" name="address" id="route" class="input_txt <?php echo $attributeConfig["address"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" value="<?php echo $this->item->address ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmAddress_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php } ?>

										<?php if($attributeConfig["country"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box">
												<?php if($attributeConfig["country"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="countryId"><?php echo JText::_('LNG_COUNTRY')?> </label>
												<div class="clear"></div>
												<select class="input_sel <?php echo $attributeConfig["country"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> select" name="countryId" id="country" >
														<option value=''></option>
														<?php
															foreach( $this->item->countries as $country ){
														?>
															<option <?php echo $this->item->countryId==$country->id? "selected" : ""?>
																value='<?php echo $country->id?>'
															><?php echo $country->country_name ?></option>
														<?php
															}
														?>
												</select>
												<div class="clear"></div>
												<span class="error_msg" id="frmCountry_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php } ?>

										<?php if($attributeConfig["city"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box">
												<?php if($attributeConfig["city"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="city_id"><?php echo JText::_('LNG_CITY')?> </label>
												<input class="input_txt <?php echo $attributeConfig["city"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" type="text" name="city" id="locality" value="<?php echo $this->item->city ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmCity_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php } ?>

										<?php if($attributeConfig["region"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box" id="districtContainer">
												<?php if($attributeConfig["region"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="district_id"><?php echo JText::_('LNG_COUNTY')?> </label>
												<input class="input_txt <?php echo $attributeConfig["region"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" type="text" name="county" id="administrative_area_level_1" value="<?php echo $this->item->county ?>" />
												<div class="clear"></div>
												<span class="error_msg" id="frmDistrict_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php } ?>

										<?php if($attributeConfig["postal_code"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box" id="districtContainer">
												<?php if($attributeConfig["postal_code"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="district_id"><?php echo JText::_('LNG_POSTAL_CODE')?> </label>
												<input class="input_sel input_txt  <?php echo $attributeConfig["postal_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="postalCode" id="postal_code" value="<?php echo $this->item->postalCode ?>" />
												<div class="clear"></div>
											</div>
										<?php } ?>

										<div class="detail_box" style="display: none;">

											<label for="longitude"><?php echo JText::_('LNG_PUBLISH_ONLY_CITY')?> </label>
											<input class="" type="checkbox" name="publish_only_city" id="publish_only_city" value="1" <?php echo $this->item->publish_only_city?"checked":"" ?>>
											<div class="clear"></div>
										</div>

										<?php if(!$enablePackages || isset($this->item->package->features) && in_array(GOOGLE_MAP,$this->item->package->features)){ ?>
											<?php if($attributeConfig["map"]!=ATTRIBUTE_NOT_SHOW){?>
												<div class="detail_box" id="districtContainer">
													
													<?php if($attributeConfig["map"] == ATTRIBUTE_MANDATORY){?>
														<div  class="form-detail req"></div>
													<?php }?>
													<label for="district_id"><?php echo JText::_('LNG_ACTIVITY_RADIUS')?> </label>
													<select name="activity_radius" id="activity_radius" class="inputbox validate[required]">
														<option value="0"  <?=$this->item->activity_radius==0?'selected':''?>>Range</option>
														<option value="5"  <?=$this->item->activity_radius==5?'selected':''?>>5 km</option>
														<option value="10" <?=$this->item->activity_radius==10?'selected':''?>>10 km</option>
														<option value="25" <?=$this->item->activity_radius==25?'selected':''?>>25 km</option>
														<option value="50" <?=$this->item->activity_radius==50?'selected':''?> >50 km</option>
														<option value="100" <?=$this->item->activity_radius==100?'selected':''?>>100 km</option>
														<option value="150" <?=$this->item->activity_radius==150?'selected':''?> >150 km</option>
													</select>

													<!--Make ____555 for create Select you can remove !-->
						<input class="input_sel" type="hidden" name="activity_radius____555" id="activity_radius___555" value="<?php echo $this->item->activity_radius ?>" />


													<div class="clear"></div>
												</div>

												<div class="detail_box">
													<?php if($attributeConfig["map"] == ATTRIBUTE_MANDATORY){?>
														<div  class="form-detail req"></div>
													<?php }?>
													<label for="latitude"><?php echo JText::_('LNG_LATITUDE')?> </label>
													<p class="small"><?php echo JText::_('LNG_MAP_INFO')?></p>
													<input class="input_txt <?php echo $attributeConfig["map"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="latitude" id="latitude" value="<?php echo $this->item->latitude ?>">
													<div class="clear"></div>
												</div>

												<div class="detail_box">
													<?php if($attributeConfig["map"] == ATTRIBUTE_MANDATORY){?>
														<div  class="form-detail req"></div>
													<?php }?>
													<label for="longitude"><?php echo JText::_('LNG_LONGITUDE')?> </label>
													<p class="small"><?php echo JText::_('LNG_MAP_INFO')?></p>
													<input class="input_txt <?php echo $attributeConfig["map"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="longitude" id="longitude" value="<?php echo $this->item->longitude ?>">
													<div class="clear"></div>
												</div>

												<div id="map-container">
													<div id="company_map">
													</div>
												</div>
											<?php }?>
										<?php } ?>
									</div>	
								</div>	
							</div>
						</div>


						<?php
						if($this->appSettings->show_secondary_locations == 1 && !$showSteps && !empty($this->item->id)) { ?>
							<div class="panel panel-default fie_wid">
									<div class="panel-heading">
										<strong><h3> <?php echo JText::_('LNG_COMPANY_SECONDARY_LOCATIONS');?></h3></strong>
										<small class="small_note"> <?php echo JText::_('LNG_COMPANY_SECONDARY_LOCATIONS_TXT');?>.</small>
									</div>
									<div class="package_content panel-body">

									<div class="box-info" style="padding: 15px;">

													<!-- /.box-header -->
													<!-- form start -->
									<div class="form-horizontal">
									<div class="box-body">
										<div class="form-box" >
								<div class="form-box" id="company-locations">
								<?php foreach ( $this->item->locations as $location){ ?>
									<div class="detail_box" id="location-box-<?php echo $location->id?>">
										<div id="location-<?php echo $location->id?>"><?php echo $location->name." - ".$location->street_number.", ".$location->address.", ".$location->city.", ".$location->county.", ".$location->country?></div>
										<a href="javascript:editLocation(<?php echo $location->id ?>)"><?php echo JText::_("LNG_EDIT") ?></a> | <a href="javascript:deleteLocation(<?php echo $location->id ?>)"><?php echo JText::_("LNG_DELETE") ?></a>
									</div>
								<?php } ?>
								</div>
								<div>
									<a href="javascript:editLocation(0)"><?php echo JText::_("LNG_ADD_NEW_LOCATION") ?></a>
								</div></div></div></div></div></div>
							</div>
						<?php
						} ?>
				</div>
				</div>
			</div>