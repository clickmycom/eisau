<div id="edit-tab4" class="edit-tab">
				<div class="panel panel-default fie_wid">
					<div class="panel-heading">
							<strong><h3> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_CONTACT_INFORMATION');?></h3></strong>
							<small class="small_note"> <?php echo JText::_('LNG_COMPANY_CONTACT_INFORMATION_TEXT');?>.</small>
					</div>
					<div class="package_content panel-body">

					<div class="box-info" style="padding: 15px;">

				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-box" >

						<?php
						if($attributeConfig["phone"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["phone"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="phone"><?php echo JText::_('LNG_TELEPHONE')?></label>
								<input type="text"	name="phone" id="phone" class="input_txt <?php echo $attributeConfig["phone"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input"
									value="<?php echo $this->item->phone ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>
						<?php
						if($attributeConfig["mobile_phone"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["mobile_phone"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="phone"><?php echo JText::_('LNG_MOBILE_PHONE')?></label>
								<input type="text"	name="mobile" id="mobile" class="input_txt <?php echo $attributeConfig["mobile_phone"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->mobile ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>
						<?php
						if($attributeConfig["email"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["email"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="email"><?php echo JText::_('LNG_EMAIL')?></label>
								<input type="text" name="email" id="email" class="input_txt <?php echo $attributeConfig["email"] == ATTRIBUTE_MANDATORY?"validate[required,custom[email]]":""?> text-input" value="<?php echo $this->item->email ?>">
								<div class="description">e.g. office@site.com</div>
								<div class="clear"></div>
								<span class="error_msg" id="frmEmail_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
							</div>
						<?php
						} ?>
						<?php
						if($attributeConfig["fax"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["fax"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="fax"><?php echo JText::_('LNG_FAX')?></label>
								<input type="text" name="fax" id="fax" class="input_txt <?php echo $attributeConfig["fax"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->fax ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>
					</div>	</div>	</div>


						</div>	</div>
				</div>



				<?php
				if($attributeConfig["contact_person"]!=ATTRIBUTE_NOT_SHOW) { ?>

					<div class="panel panel-default fie_wid">
						<div class="panel-heading">
								<strong><h3> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_CONTACT_PERSON_INFORMATION');?></strong></h3>
								<small class="small_note"> <?php echo JText::_('LNG_COMPANY_CONTACT_PERSON_INFORMATION_TEXT');?>.</small>
						</div>
						<div class="package_content panel-body">

						<div class="box-info" style="padding: 15px;">

					<div class="form-horizontal">
						<div class="box-body">
							<div class="form-box" >

						<div class="form-box">
							<div class="detail_box">
								<?php if($attributeConfig["contact_person"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="contact_name"><?php echo JText::_('LNG_NAME')?></label>
								<input type="text" name="contact_name" id="contact_name" class="input_txt <?php echo $attributeConfig["contact_person"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->contact->contact_name ?>">
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_email"><?php echo JText::_('LNG_EMAIL')?></label>
								<input type="text" name="contact_email" id="contact_email" class="input_txt text-input"
									value="<?php echo $this->item->contact->contact_email ?>">
									<div class="description">e.g. office@site.com</div>
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_phone"><?php echo JText::_('LNG_TELEPHONE')?></label>
								<input type="text" name="contact_phone" id="contact_phone" class="input_txt text-input"
									value="<?php echo $this->item->contact->contact_phone ?>">
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_fax"><?php echo JText::_('LNG_FAX')?></label>
								<input type="text" name="contact_fax" id="contact_fax" class="input_txt" value="<?php echo $this->item->contact->contact_fax ?>">
								<div class="clear"></div>
							</div>
						</div></div></div></div></div></div>
					</div>



				<?php
				} ?>
			</div>