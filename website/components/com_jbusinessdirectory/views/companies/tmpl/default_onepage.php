<?php /*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once 'header.php';
require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';
?>

<?php
require_once 'breadcrumbs.php';
?>

<style>
.company-menu{
	padding-top: 34px !important;
}

.img-thumbnail {
    display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
h1{
	color: #191919 !important;
	    font-weight: 400;
}
#one-page-container{
	font-size: 16px !important;
}
#aside{
display:none !important;
}
#contact-person-details{
	    line-height: 3;
}



.company-menu nav {
    font-size: 18px !important;
    line-height: 20px;
    padding: 15px 0px;
}
.animBlock[data-position="left"].viewed{
	font-size: 15px !important;
}
.one-page-container dt{
	font-size: 15px !important;
}

</style>

<div id="one-page-container" class="one-page-container col-md-9 col-lg-12">
	<div class="company-name">
		<!-- <h1>
			<?php  echo isset($this->company->name)?$this->company->name:"" ; ?>
		</h1> -->
	</div>
	<div class="clear"></div>





	<table>
		  <thead>
		    <tr >
		      <th colspan="5"><i class="fa fa-file-text-o" aria-hidden="true"></i>  <?php  echo isset($this->company->name)?$this->company->name:"" ; ?></th>
		    </tr>
		  </thead>
		  
	</table>

	<div class="row" style="margin-left: 0;padding: 15px;">
		<div class="col-sm-3 company_show_imgs col-xs-12" >
			<img  src="<?php echo JUri::root(true); ?>/media/com_jbusinessdirectory/pictures<?=$this->company->logoLocation?>">
		</div>
		<div class="col-lg-8 col-sm-8 col-xs-12" class="wat_de_com">
				<a style="font-size: 20px;font-weight: 600;" >
					<span itemprop="name"> <?php echo $this->company->name?> </span>
				</a>
				<p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;  padding-top: 15px;"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<?=$this->company->street_number?> <?=$this->company->address?> <?=$this->company->county?>  <?=$this->company->postalCode?></p>
				<p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;:&nbsp;<?=$this->company->phone?></p>
				<p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-internet-explorer" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<?=$this->company->website?></p>
				<p style="line-height: 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;:&nbsp;<?=$this->company->email?></p>
				<input type="hidden"  id="this_company_email" value="<?=$this->company->email?>">
				<div class="company-links" style="display:none;">
					<ul class="features-links">
						<li class="col-sm-3 col-xs-12 col-lg-2">
							<a class="website" title="<?php echo $this->company->name?> Website" target="_blank" rel="nofollow"  href="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=companies&task=companies.showCompanyWebsite&companyId='.$company->id) ?>"><?php echo JText::_('LNG_WEBSITE') ?></a>
						</li>
						<li class="col-sm-6 col-xs-12">
							<a class="email" href="javascript:showContactCompany()" ><?php echo JText::_('LNG_CONTACT_COMPANY'); ?></a>
						</li>
					</ul>
				</div>
		</div>
	</div>
	
	
	<div class="row-fluid" style="margin-bottom: 0;display: none;">
		<div id="company-info" class="company-info span8">
			
			<div class="company-info-container" >
				<div class="company-info-rating" itemscope itemtype="http://data-vocabulary.org/Review-aggregate" <?php echo !$appSettings->enable_ratings? 'style="display:none"':'' ?>>
				 	<span style="display:none" itemprop="itemreviewed"><?php echo $this->company->name?></span>
				 	 <span style="display:none" itemprop="rating" itemscope itemtype="http://data-vocabulary.org/Rating">
				      	<span itemprop="average"><?php echo $this->company->averageRating?></span>  <span itemprop="best">5</span>
				    </span>
		    		<span  style="display:none" itemprop="count"><?php echo count($this->reviews)?></span>

					<div class="company-info-average-rating">
						<div class="rating">
							<div id="rating-average" title="<?php echo $this->company->averageRating?>"></div>
						</div>
						<p class="rating-text">
						 <?php echo JText::_('LNG_AVG_OF') ?> <span id="average-rating-count"> <span id="rateNumber<?php echo $this->company->id?>" itemprop="votes"> <?php echo $this->ratingCount ?> </span> <?php echo JText::_('LNG_RATINGS') ?></span></p>
					</div>
					<div class="company-info-user-rating">
						<div class="rating">
							<div id="rating-user"></div>
						</div>
						<p class="rating-text">  <span id="average-rating-count" > <?php echo JText::_('LNG_YOUR_RATING') ?></span></p>
					</div>
				</div>

				<div class="company-info-review" <?php echo !$appSettings->enable_reviews? 'style="display:none"':'' ?>>
					<div style="display:none" class="login-awareness tooltip">
						<div class="arrow">�</div>
						<div class="inner-dialog">
							<a href="javascript:void(0)" class="close-button" onclick="jQuery(this).parent().parent().hide()"><?php echo JText::_('LNG_CLOSE') ?></a>
							<p>
							<strong><?php echo JText::_('LNG_INFO') ?></strong>
							</p>
							<p>
								<?php echo JText::_('LNG_YOU_HAVE_TO_BE_LOGGED_IN') ?>
							</p>
							<p>
								<a href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>"><?php echo JText::_('LNG_CLICK_LOGIN') ?></a>
							</p>

						</div>
					</div>
					<p class="review-count">
						<?php if(count($this->reviews)){ ?>
						 <a href="#reviews"><?php echo count($this->reviews)?> <?php echo JText::_('LNG_REVIEWS') ?></a>
							 <?php if(!$appSettings->enable_reviews_users || $user->id !=0) {?>
							&nbsp;|&nbsp;
							<a href="javascript:void(0)" onclick="addNewReview()"> <?php echo JText::_('LNG_WRITE_REVIEW') ?></a>
							<?php }?>
						<?php } else{ ?>
						<a href="javascript:void(0)" onclick="addNewReview()" <?php echo ($appSettings->enable_reviews_users &&  $user->id == 0) ? 'style="display:none"':'' ?>><?php echo JText::_('LNG_BE_THE_FIRST_TO_REVIEW') ?></a>
						<?php }?>
					</p>
				</div>

				<div>
					<div class="company-info-details" style="display: none;">
						<?php if(!empty($company->contact_name)){ ?>
							<div id="contact-person-details" class="contact-person-details open">
									<div onclick="jQuery('#contact-person-details').toggleClass('open')"><strong ><?php echo JText::_('LNG_CONTACT_PERSON') ?>: </strong> <?php echo $company->contact_name?> (+)</div>
									<ul>
										<?php if(!empty($company->contact_phone) || !empty($company->contact_fax)){?>
											<li><i class="dir-icon-phone"></i> <a href="tel:<?php  echo $this->company->phone; ?>"><?php echo $company->contact_phone?></a> &nbsp;&nbsp;  <?php echo $company->contact_fax?></li>
										<?php }?>
										<?php if(!empty($company->contact_email)){ ?>
											<li><i class="dir-icon-envelope"></i> <?php echo $company->contact_email?></li>
										<?php }?>
									</ul>
								</div>
						<?php } ?>
						<p>
							<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
								<i class="dir-icon-map-marker"></i> <?php echo JBusinessUtil::getAddressText($this->company) ?>
							</span>
						</p>

						<?php if( $showData && isset($this->package->features) && in_array(PHONE, $this->package->features) || !$appSettings->enable_packages ) { ?>
							<?php if(!empty($this->company->phone)) { ?>
								<span class="phone" itemprop="tel">
									<i class="dir-icon-phone"></i> <a href="tel:<?php  echo $this->company->phone; ?>"><?php  echo $this->company->phone; ?></a>
								</span>
								<br/>
							<?php } ?>

							<?php if(!empty($this->company->mobile)) { ?>
								<span class="phone" itemprop="tel">
									<i class="dir-icon-mobile-phone"></i> <a href="tel:<?php  echo $this->company->mobile; ?>"><?php  echo $this->company->mobile; ?></a>
								</span>
								<br/>
							<?php } ?>

							<?php if(!empty($this->company->fax)) {?>
								<span class="phone">
									<i class="dir-icon-fax"></i> <?php echo $this->company->fax?>
								</span>
								<br/>
							<?php } ?>
						<?php } ?>

						<?php if(!empty( $this->company->email) && $showData && $appSettings->show_email){?>
							<span itemprop="email">
								<i class="dir-icon-envelope"></i> <?php echo $this->company->email?>
							</span>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="company-links">
				<ul class="features-links">
					<?php if(($showData && isset($this->package->features) && in_array(WEBSITE_ADDRESS,$this->package->features) || !$appSettings->enable_packages) && !empty($company->website)){ ?>
						<li>
							<a class="website" title="<?php echo $this->company->name?> Website" target="_blank" rel="nofollow"  href="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=companies&task=companies.showCompanyWebsite&companyId='.$company->id) ?>"><?php echo JText::_('LNG_WEBSITE') ?></a>
						</li>
					<?php }?>
					<?php if((isset($this->package->features) && in_array(CONTACT_FORM,$this->package->features) || !$appSettings->enable_packages) && $showData && !empty($company->email)){ ?>
						<li>
							<a class="email" href="javascript:showContactCompany()" ><?php echo JText::_('LNG_CONTACT_COMPANY'); ?></a>
						</li>
					<?php } ?>
				</ul>

				<?php require_once 'listing_social_networks.php'; ?>
			</div>
		</div>
		<?php if($showData){?>
			<div class="company-map span4">
				<a href="javascript:showMap()" title="Show Map">
					<?php
						if((isset($this->package->features) && in_array(GOOGLE_MAP,$this->package->features) || !$appSettings->enable_packages )
										&& !empty($this->company->latitude) && !empty($this->company->longitude)){
							echo '<img src="https://maps.googleapis.com/maps/api/staticmap?center='.$this->company->latitude.','.$this->company->longitude.'&zoom=13&size=200x106&markers=color:blue|'.$this->company->latitude.','.$this->company->longitude.'&sensor=false">';
						}
					?>
				</a>

				<div style="display:none" class="login-awareness tooltip" id="claim-login-awarness">
								<div class="arrow">�</div>
								<div class="inner-dialog">
								<a href="javascript:void(0)" class="close-button" onclick="jQuery(this).parent().parent().hide()"><?php echo JText::_('LNG_CLOSE') ?></a>
								<p>
								<strong><?php echo JText::_('LNG_INFO') ?></strong>
								</p>
								<p>
									<?php echo JText::_('LNG_YOU_HAVE_TO_BE_LOGGED_IN') ?>
								</p>
								<p>
									<a href="<?php echo JRoute::_('index.php?option=com_users&view=login&return='.base64_encode($url)) ?>"><?php echo JText::_('LNG_CLICK_LOGIN') ?></a>
								</p>
								</div>
				</div>
				<div class="clear"></div>

				<?php if((!isset($this->company->userId) || $this->company->userId == 0) && $appSettings->claim_business){ ?>
				<div class="claim-container" id="claim-container">

					<a href="javascript:void(0)" onclick="claimCompany()">
						<div class="claim-btn">
							<?php echo JText::_('LNG_CLAIM_COMPANY')?>
						</div>
					</a>
				</div>
				<?php  } ?>
			</div>
		<?php } ?>
	</div>

	<div id="company-map-holder" style="display:none" class="company-cell">
		<div class="search-toggles">
			<span class="button-toggle">
				<a title="" class="" href="javascript:hideMap()"><?php echo JText::_("LNG_CLOSE_MAP")?></a>
			</span>
			<div class="clear"></div>
		</div>
		<h2><?php echo JText::_("LNG_BUSINESS_MAP_LOCATION")?></h2>

		<?php
			if(isset($this->company->latitude) && isset($this->company->longitude) && $this->company->latitude!='' && $this->company->longitude!='')
				require_once 'map.php';
		?>
	</div>
	<div class="clear"></div>

	<div class="company-menu">
		<nav>
			<a id="business-link" href="javascript:showDetails('company-business');" class="active"><?php echo JText::_("LNG_BUSINESS_DETAILS")?></a>
			<?php
				if((isset($this->package->features) && in_array(IMAGE_UPLOAD,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->pictures) && count($this->pictures)>0){
			?>
				<a id="gallery-link" href="javascript:showDetails('company-gallery');" class=""><?php echo JText::_("LNG_GALLERY")?></a>
			<?php } ?>

			<?php
				if((isset($this->package->features) && in_array(VIDEOS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->videos) && count( $this->videos)>0){
			?>
					<a id="videos-link" href="javascript:showDetails('company-videos');" class=""><?php echo JText::_("LNG_VIDEOS")?></a>
			<?php } ?>

			<?php
				if((isset($this->package->features) && in_array(COMPANY_OFFERS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->offers) && count($this->offers) && $appSettings->enable_offers){
			?>
					<a id="offers-link" href="javascript:showDetails('company-offers');" class=""><?php echo JText::_("LNG_OFFERS")?></a>
			<?php } ?>

			<?php
				if((isset($this->package->features) && in_array(COMPANY_EVENTS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->events) && count($this->events) && $appSettings->enable_events){
			?>
					<a id="events-link" href="javascript:showDetails('company-events');" class=""><?php echo JText::_("LNG_EVENTS")?></a>
			<?php } ?>

			<?php if($appSettings->enable_reviews){ ?>
				<a id="reviews-link" href="javascript:showDetails('company-reviews');" class="">Testimonials</a>
			<?php }?>

		</nav>
	</div>

	<div id="company-details" class="company-cell">
		<?php if(isset($this->company->slogan) && strlen($this->company->slogan)>2){?>
			<p class="business-slogan"><?php echo  $this->company->slogan ?> </p>
		<?php }?>

		<dl>
			<?php if(!empty($this->company->typeName)){?>
				<dt class="animBlock floatl viewed" data-position="left"><?php echo JText::_('LNG_TYPE')?>:</dt>
				<dd class="animBlock floatr viewed" data-position="right"><?php echo $this->company->typeName?></dd>
			<?php } ?>

			<?php if(!empty($this->company->categoryIds)){?>
				<dt><?php echo JText::_('LNG_CATEGORIES')?>:</dt>
				<dd><ul><?php
							$categoryIds = explode(',',$this->company->categoryIds);
							$categoryNames =  explode('#',$this->company->categoryNames);
							$categoryAliases =  explode('#',$this->company->categoryAliases);

							for($i=0;$i<count($categoryIds);$i++){
								?>
									<li><a rel="nofollow" href="<?php echo JBusinessUtil::getCategoryLink($categoryIds[$i],  $categoryAliases[$i]) ?>"><?php echo $categoryNames[$i]?><?php echo $i<(count($categoryIds)-1)? ',&nbsp;':'' ?></a> </li>
								<?php
							}
						?>
					</ul>
				</dd>
			<?php } ?>
			<?php if(!empty($this->company->description)){?>
				<dt class="animBlock floatl viewed" data-position="left"><?php echo JText::_("LNG_GENERAL_INFO")?></dt>
				<dd><?php echo $this->company->description;	?></dd>
			<?php }?>

			<?php if(!empty($this->company->locations)){ ?>
				<dt class="animBlock floatl viewed" data-position="left"><?php echo JText::_("LNG_COMPANY_LOCATIONS")?></dt>
				<dd><?php require_once 'locations.php';?></dd>
			<?php } ?>

			<?php if(!empty($this->company->business_hours)){ ?>
				<dt class="animBlock floatl viewed" data-position="left"><?php echo JText::_("LNG_OPENING_HOURS")?></dt>
				<dd><?php require_once 'business_hours.php'; ?>	</dd>
			<?php } ?>

			<?php if( $showData && isset($this->package->features) && in_array(ATTACHMENTS, $this->package->features) || !$appSettings->enable_packages ) { ?>
				
			<?php } ?>
		</dl>

		<div class="classification">
			<?php
				$packageFeatured = isset($this->package->features)?$this->package->features:null;
				$renderedContent = AttributeService::renderAttributesFront($this->companyAttributes,$appSettings->enable_packages, $packageFeatured);
				echo $renderedContent;
			?>
		</div>

	</div>
	<div class="clear"></div>

	<?php
	if((isset($this->package->features) && in_array(IMAGE_UPLOAD,$this->package->features) || !$appSettings->enable_packages)
			&& ((isset($this->pictures) && count($this->pictures)>0) || (isset($this->videos) && count($this->videos)>0)) ){
	?>
	<div id="company-gallery" class="company-cell">
		<h2><?php echo JText::_("LNG_GALLERY")?></h2>
		<?php require_once 'gallery_slider.php';?>
	</div>
	<div class="clear"></div>
	<?php }?>

	<?php
		if((isset($this->package->features) && in_array(VIDEOS,$this->package->features) || !$appSettings->enable_packages)
							&& isset($this->videos) && count( $this->videos)>0){
	?>
		<div id="company-videos" class="company-cell">
			<h2><?php echo JText::_("LNG_VIDEOS")?></h2>
			<?php  require_once 'companyvideos.php';?>
		</div>
	<?php }	?>

	<?php
	if((isset($this->package->features) && in_array(COMPANY_OFFERS,$this->package->features) || !$appSettings->enable_packages)
			&& isset($this->offers) && count($this->offers) && $appSettings->enable_offers){
	?>
		<div id="company-offers" class="company-cell">
			<h2><?php echo JText::_("LNG_COMPANY_OFFERS")?></h2>
			<?php require_once 'companyoffers.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>

	<?php
	if((isset($this->package->features) && in_array(COMPANY_EVENTS,$this->package->features) || !$appSettings->enable_packages)
			&& isset($this->events) && count($this->events) && $appSettings->enable_events){
	?>
		<div id="company-events" class="company-cell">
			<h2><?php echo JText::_("LNG_COMPANY_EVENTS")?></h2>
			<?php require_once 'events.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>

	<div class="animBlock floatl viewed company-cell" data-position="left" style="float:left;width: 100%;" >
		<h2 style=" text-transform: uppercase;">SHARE</h2>
			<?php require_once JPATH_COMPONENT_SITE."/include/social_share.php"?>
			<div><?php require_once 'listing_social_networks.php'; ?></div>
			
	</div>
	<div class="clear"></div>
		
	<?php
	if($appSettings->enable_reviews){
	?>
		<div id="company-reviews" class="animBlock floatl viewed company-cell" data-position="left" >
		<h2 style=" text-transform: uppercase;">BUSINESS Testimonials</h2>
		<?php require_once 'reviews.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>
	
	

	<form name="tabsForm" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory') ?>" id="tabsForm" method="post">
	 	 <input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
		 <input type="hidden" name="task" value="companies.displayCompany" />
		 <input type="hidden" name="tabId" id="tabId" value="<?php echo $this->tabId?>" />
		 <input type="hidden" name="view" value="companies" />
		 <input type="hidden" name="layout2" id="layout2" value="" />
		 <input type="hidden" name="companyId" value="<?php echo $this->company->id?>" />
		 <input type="hidden" name="controller"	value="<?php echo JRequest::getCmd('controller', 'J-BusinessDirectory')?>" />
	</form>
</div>




<script>

function showMap(){
	jQuery("#company-map-holder").show();
	loadScript();

}

function hideMap(){
	jQuery("#company-map-holder").hide();
}

function readMore(){
	jQuery("#general-info").removeClass("collapsed");
	jQuery(".read-more").hide();
}

function showDetails(identifier){
	var ids = ["company-details","company-gallery","company-videos" ,"company-offers","company-events","company-reviews"];
	var pos = ids.indexOf(identifier);

	jQuery(".company-menu a").each(function(){
		jQuery(this).removeClass("active");
	});

	jQuery("#"+identifier+"-link").addClass("active");

	for(var i=0;i<pos;i++){
		jQuery("#"+ids[i]).slideUp();
	}

	for(var i=pos;i<ids.length;i++){
		jQuery("#"+ids[i]).slideDown();
	}
}
</script>

<style>


div.company-links{
	display: none;
}

form#_tk_form_contact_provider{
	width: 100%;
}

form#_tk_form_contact_provider .formControlLabel{
	color: #fff !important;
}


#_tk_form_contact_provider {
    	display: block !important;
    	right: 0px;
}

.company_show_imgs{
	padding: 4% 4% 0% 4%;
}


@media screen and (max-width: 1199px) {
   #_tk_form_contact_provider {
    	display: block !important;
    	right: 0px;
	}


	#sidebar_can_remove{
		margin: 0;
		top: -31px;
    left: 8px;
	}
}

@media screen and (max-width: 991px) {

	#_tk_form_contact_provider > h2 {
    	border-top-right-radius: 5px;
	}
	#sidebar_can_remove{
		top: 0px;
    	left: 0px;
    	padding: 21px;
	}
	#_tk_form_contact_provider {
    	display: block !important;
    	padding-bottom: 25px;
	}
	.formResponsive select, 
	.formResponsive textarea, 
	.formResponsive input[type="text"], 
	.formResponsive input[type="number"], 
	.formResponsive input[type="email"], 
	.formResponsive input[type="tel"], 
	.formResponsive input[type="url"], 
	.formResponsive input[type="password"]{
		width: 96% !important;
		padding: 4px;
	    font-size: 13px;
	    line-height: 18px;
	    color: #555555;
	    box-sizing: content-box;
	    display: inline-block;
	}

}





@media screen and (max-width: 767px) {
	.company_show_imgs {
    	padding: 4% 0% 4% 0%;
    	text-align: center;
	}
	.wat_de_com{
		text-align: center;

	}
	#one-page-container .features-links li{
		border-left: none;
		padding: 1em 0 0 0;
	}
}


@media screen and (max-width: 490px) {
	.formResponsive select, 
	.formResponsive textarea, 
	.formResponsive input[type="text"], 
	.formResponsive input[type="number"], 
	.formResponsive input[type="email"], 
	.formResponsive input[type="tel"], 
	.formResponsive input[type="url"], 
	.formResponsive input[type="password"]{
		width: 91% !important;
		padding: 4px;
	    font-size: 13px;
	    line-height: 18px;
	    color: #555555;
	    box-sizing: content-box;
	    display: inline-block;
	}
}
@media screen and (max-width: 375px) {
	.formResponsive select, 
	.formResponsive textarea, 
	.formResponsive input[type="text"], 
	.formResponsive input[type="number"], 
	.formResponsive input[type="email"], 
	.formResponsive input[type="tel"], 
	.formResponsive input[type="url"], 
	.formResponsive input[type="password"]{
		width: 89% !important;
		
	}
}


.articleBody h1{
		display:none;
}
	
	
table {
  text-align: left;
  line-height: 40px;
  border-collapse: separate;
  border-spacing: 0;
  
  width: 100%;
      background-color: #fff;
  border-radius: .25rem;
}

thead tr:first-child {
  background: #ee3928;
  color: #fff;
  border: none;
}

th:first-child, td:first-child { padding: 0 0px 0 20px; }

thead tr:last-child th { border-bottom: 3px solid #ddd; }



tbody tr:last-child td { border: none; }
tbody td { border-bottom: 1px solid #ddd;}

td:last-child {
  text-align: left;
  padding-right: 10px;
}

.button {
  color: #aaa;
  cursor: pointer;
  vertical-align: middle;
}

.edit:hover {
  color: #0a79df;
}

.delete:hover {
  color: #dc2a2a;
}

.add-review{
	width: none !important;
	    border-radius: 7px;
}
#company-reviews{
	width:100% !important;
}

.user-rating {
	display:none;
}

#_tk_form_contact_provider {
                display: block !important;
}

</style>

<?php require_once 'company_util.php'; ?>

<script>
	jQuery(document).ready(function(){
	    jQuery('.add-review-link').text("Add Testimonials");
	    jQuery('.ui-button-text:first').text("Send Testimonials");
	});
</script>
