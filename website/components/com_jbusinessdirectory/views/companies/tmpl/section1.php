<div id="edit-tab1" class="edit-tab">
				<!-- box 1 -->
				<div class="panel panel-default fie_wid">
				<div class="panel-heading"> 
					<h2> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_COMPANY_DETAILS');?></h2> 
				</div>
					<div class="package_content panel-body" style="padding-top: 0px;">
						<div class="box-info" style="padding: 15px;">
							<div class="form-horizontal">
								<div class="box-body">
									<div class="form-box" >
										<div class="detail_box">
											<div  class="form-detail req"></div>
												<label for="name"><?php echo JText::_('LNG_COMPANY_NAME')?> </label>
													<input type="text"	name="name" id="name" class="input_txt validate[required] text-input" value="<?php echo $this->item->name ?>" onchange="checkCompanyName('<?php echo $this->item->name ?>',this.value)" maxlength="100">
														<div class="clear"></div>
												<span class="error_msg" id="company_exists_msg" style="display: none;"><?php echo JText::_('LNG_COMPANY_NAME_ALREADY_EXISTS')?></span>
												<span class="" id="claim_company_exists_msg" style="display: none;"><?php echo JText::_('LNG_CLAIM_COMPANY_EXISTS')?> <a id="claim-link" href=""><?php echo JText::_("LNG_HERE")?></a></span>
												<span class="error_msg" id="frmCompanyName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
										</div>
										<div class="detail_box">
												<label for="name"><?php echo JText::_('LNG_ALIAS')?> </label>
												<input type="text"	name="alias" id="alias"  placeholder="<?php echo JText::_('LNG_AUTO_GENERATE_FROM_NAME')?>" class="input_txt text-input" value="<?php echo $this->item->alias ?>">
												<div class="clear"></div>
										</div>
										<?php
										if($attributeConfig["comercial_name"]!=ATTRIBUTE_NOT_SHOW){?>
											<div class="detail_box">
												<?php if($attributeConfig["comercial_name"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="comercialName"><?php echo JText::_('LNG_COMPANY_COMERCIAL_NAME')?> </label>
												<input type="text"
													name="comercialName" id="comercialName" class="input_txt <?php echo $attributeConfig["comercial_name"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->comercialName ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmCompanyComercialName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["tax_code"]!=ATTRIBUTE_NOT_SHOW){ ?>
											<div class="detail_box" style="display:none">
												<?php if($attributeConfig["tax_code"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="taxCode"><?php echo JText::_('LNG_TAX_CODE')?> </label>
												<input type="text" name="taxCode" id="taxCode" class="input_txt <?php echo $attributeConfig["tax_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->taxCode ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmTaxCode_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["registration_code"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["registration_code"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="registrationCode"><?php echo JText::_('LNG_REGISTRATION_CODE')?> </label>
												<input type="text"
													name="registrationCode" id="registrationCode" class="input_txt <?php echo $attributeConfig["registration_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" 	value="<?php echo $this->item->registrationCode ?>">
												<div class="clear"></div>
												<span class="error_msg" id="frmRegistrationCode_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["website"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<?php
											if(!$enablePackages || isset($this->item->package->features) && in_array(WEBSITE_ADDRESS,$this->item->package->features)) { ?>
												<div class="detail_box">
													<?php if($attributeConfig["website"] == ATTRIBUTE_MANDATORY){?>
														<div  class="form-detail req"></div>
													<?php }?>
													<label for="website"><?php echo JText::_('LNG_WEBSITE')?> </label>
													<input type="text" name="website" id="website" value="<?php echo $this->item->website ?>"	class="input_txt <?php echo $attributeConfig["website"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>">
													<div class="clear"></div>
												</div>
											<?php
											} ?>
										<?php
										} ?>
										<?php
										if($attributeConfig["company_type"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["company_type"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="companyTypes"><?php echo JText::_('LNG_COMPANY_TYPE')?> </label>
												<select class="input_sel <?php echo $attributeConfig["company_type"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> select" name="typeId" id="companyTypes">
													<option  value=""> </option>
													<?php
													foreach( $this->item->types as $type ) { ?>
														<option <?php echo $this->item->typeId==$type->id? "selected" : ""?> value='<?php echo $type->id?>'><?php echo $type->name ?></option>
													<?php
													} ?>
												</select>
												<div class="clear"></div>
												<span class="error_msg" id="frmCompanyType_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["slogan"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["slogan"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="slogan"><?php echo JText::_("LNG_COMPANY_SLOGAN")?> &nbsp;&nbsp;&nbsp;</label>
												<p class="small"><?php echo JText::_("LNG_COMPANY_SLOGAN_INFO")?></p>
												<?php
												if($this->appSettings->enable_multilingual) {
													echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
													foreach($this->languages  as $k=>$lng ) {
														echo JHtml::_('tabs.panel', $lng, 'tab'.$k );
														$langContent = isset($this->translationsSlogan[$lng])?$this->translationsSlogan[$lng]:"";
														if($lng==JFactory::getLanguage()->getDefault() && empty($langContent)){
															$langContent = $this->item->slogan;
														}
														echo "<textarea id='slogan'.$lng' name='slogan_$lng' class='input_txt' cols='75' rows='5' maxLength='".COMPANY_SLOGAN_MAX_LENGHT."'>$langContent</textarea>";
														echo "<div class='clear'></div>";
													}
													echo JHtml::_('tabs.end');
												} else { ?>
													<textarea name="slogan" id="slogan" class="input_txt text-input <?php echo $attributeConfig["slogan"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>"  cols="75" rows="5"  maxLength="<?php echo COMPANY_SLOGAN_MAX_LENGHT?>"><?php echo $this->item->slogan ?></textarea>
												<?php
												} ?>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["short_description"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["short_description"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="description_id"><?php echo JText::_("LNG_COMPANY")." ".JText::_('LNG_SHORT_DESCRIPTION')?>  &nbsp;&nbsp;&nbsp;</label>
												<p class="small"><?php echo JText::_("LNG_COMPANY_SHORT_DESCR_INFO")?></p>
												<?php
												if($this->appSettings->enable_multilingual) {
													echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
													foreach( $this->languages  as $k=>$lng ) {
														echo JHtml::_('tabs.panel', $lng, 'tab'.$k );
														$langContent = isset($this->translations[$lng."_short"])?$this->translations[$lng."_short"]:"";
														if($lng==JFactory::getLanguage()->getDefault() && empty($langContent)) {
															$langContent = $this->item->short_description;
														}
														echo "<textarea id='short_description'.$lng' name='short_description_$lng' class='input_txt' cols='75' rows='5' maxLength='".COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT."'>$langContent</textarea>";
														echo "<div class='clear'></div>";
													}
													echo JHtml::_('tabs.end');
												} else { ?>
													<textarea name="short_description" id="short_description" class="input_txt <?php echo $attributeConfig["short_description"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input"  cols="75" rows="5"  maxLength="<?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?>" onkeyup="calculateLenghtShort();"><?php echo $this->item->short_description ?></textarea>
													<div class="clear"></div>
													<span class="error_msg" id="frmDescription_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
													<div class="description-counter">
														<input type="hidden" name="descriptionMaxLenghtShort" id="descriptionMaxLenghtShort" value="<?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?>" />
														<label for="decriptionCounterShort">(Max. <?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?> <?php JText::_('LNG_CHARACTRES')?>).</label>
														<?php echo JText::_('LNG_REMAINING')?>&nbsp;&nbsp;
													</div>
												<?php
												} ?>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["description"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["description"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="description_id"><?php echo JText::_("LNG_COMPANY")." ".JText::_('LNG_DESCRIPTION')?>  &nbsp;&nbsp;&nbsp;</label>
												<p class="small"><?php echo JText::_("LNG_COMPANY_DESCR_INFO")?></p>
												<?php
												if($this->appSettings->enable_multilingual) {
													echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
													foreach( $this->languages  as $k=>$lng ) {
														echo JHtml::_('tabs.panel', $lng, 'tab'.$k );
														$langContent = isset($this->translations[$lng])?$this->translations[$lng]:"";
														if($lng==JFactory::getLanguage()->getDefault() && empty($langContent)) {
															$langContent = $this->item->description;
														}
														if(!$enablePackages || isset($this->item->package->features) && in_array(HTML_DESCRIPTION,$this->item->package->features)) {
															$editor = JFactory::getEditor();
															echo $editor->display('description_'.$lng, $langContent, '95%', '200', '70', '10', false);
														} else {
															echo "<textarea id='description'.$lng' name='description_$lng' class='input_txt' cols='75' rows='10' maxLength='".COMPANY_DESCRIPTIION_MAX_LENGHT."'>$langContent</textarea>";
															echo "<div class='clear'></div>";
														}
													}
													echo JHtml::_('tabs.end');
												} else {
													if(!$enablePackages || isset($this->item->package->features) && in_array(HTML_DESCRIPTION,$this->item->package->features)) {
														$editor = JFactory::getEditor();
														echo $editor->display('description', $this->item->description, '95%', '200', '70', '10', false);
													} else { ?>
														<textarea name="description" id="description" class="input_txt <?php echo $attributeConfig["description"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input"  cols="75" rows="10"  maxLength="<?php echo COMPANY_DESCRIPTIION_MAX_LENGHT?>" onkeyup="calculateLenght();"><?php echo $this->item->description ?></textarea>
														<div class="clear"></div>
														<span class="error_msg" id="frmDescription_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														<div class="description-counter">
															<input type="hidden" name="descriptionMaxLenght" id="descriptionMaxLenght" value="<?php echo COMPANY_DESCRIPTIION_MAX_LENGHT?>" />
															<label for="descriptionCounter">(Max. <?php echo COMPANY_DESCRIPTIION_MAX_LENGHT?> characters).</label>
															<?php echo JText::_('LNG_REMAINING')?><input type="text" value="0" id="descriptionCounter" name="descriptionCounter">
														</div>
													<?php
													} ?>
												<?php
												} ?>
											</div>
										<?php
										} ?>
										<?php
										if($attributeConfig["keywords"]!=ATTRIBUTE_NOT_SHOW) { ?>
											<div class="detail_box">
												<?php if($attributeConfig["keywords"] == ATTRIBUTE_MANDATORY){?>
													<div  class="form-detail req"></div>
												<?php }?>
												<label for="keywords"><?php echo JText::_('LNG_KEYWORDS')?> </label>
												<p class="small"><?php echo JText::_('LNG_COMPANY_KEYWORD_INFO')?></p>
												<input	type="text" name="keywords" class="input_txt <?php echo $attributeConfig["keywords"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" id="keywords" value="<?php echo $this->item->keywords ?>"  maxLength="75">
												<div class="clear"></div>
											</div>
										<?php
										} ?>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
						</div>
							<!-- box-info -->
					</div>
					<!-- panel-body -->
				</div>
				<!-- end box1 -->


				<!--box2 -->
				<?php
				if($this->appSettings->enable_attachments == 1) { ?>
				<div class="panel panel-default fie_wid">
				  <div class="panel-heading">
				  		<strong><h3><i class="fa fa-file-archive-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ATTACHMENTS');?></strong></h3> 
				  		<small><?php echo JText::_('LNG_ATTACHMENTS_INFORMATION_TEXT');?>.</small>
				  </div>
				  <div class="panel-body">
							
								<fieldset class="boxed ">
									<input type='button' name='btn_removefile_at' id='btn_removefile_at' value='x' style='display:none'>
									<input type='hidden' name='crt_pos_a' id='crt_pos_a' value=''>
									<input type='hidden' name='crt_path_a' id='crt_path_a' value=''>
									
							
									<TABLE class="admintable" align='center' id='table_company_attachments' name='table_company_attachments' >
													<?php
													if(!empty($this->item->attachments))
														foreach( $this->item->attachments as $attachment ) { ?>
															<TR>
														
																<TD align='left'>
																	<input type="text" name='attachment_name[]' id='attachment_name'   value="<?php echo $attachment->name ?>" /><br/>
																	<?php echo basename($attachment->path)?>
																</TD>
																<td align='center'>
																	<img class='btn_attachment_delete'
																		src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_options.gif"?>'
																		onclick="
																			if(!confirm('<?php echo JText::_('LNG_CONFIRM_DELETE_ATTACHMENT',true)?>'))
																				return;
																			var row = jQuery(this).parents('tr:first');
																			var row_idx = row.prevAll().length;
																			jQuery('#crt_pos_a').val(row_idx);
																			jQuery('#crt_path_a').val('<?php echo $attachment->path?>');
																			jQuery('#btn_removefile_at').click();"/>
																</td>
																<td align='center'>
																	<input type='hidden' value='<?php echo $attachment->status?>' name='attachment_status[]' id='attachment_status'>
																	<input type='hidden' value='<?php echo $attachment->path?>' name='attachment_path[]' id='attachment_path'>
																	<img class='btn_attachment_status'
																		src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/".($attachment->status ? 'checked' : 'unchecked').".gif"?>'
																		onclick="
																			var form = document.adminForm;
																			var v_status = null;
																			var pos = jQuery(this).closest('tr')[0].sectionRowIndex;

																			if( form.elements['attachment_status[]'].length == null ) {
																				v_status  = form.elements['attachment_status[]'];
																			}
																			else {
																				v_status  = form.elements['attachment_status[]'][pos];
																			}
																			if( v_status.value == '1') {
																				jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/unchecked.gif"?>');
																				v_status.value ='0';
																			}
																			else {
																				jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/checked.gif"?>');
																				v_status.value ='1';
																			}" />
																</td>
																<td align="center">
																	<span class="span_up" onclick='var row = jQuery(this).parents("tr:first");  row.insertBefore(row.prev());'>
																		<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/up-icon.png">
																	</span>
																	<span class="span_down" onclick='var row = jQuery(this).parents("tr:first"); row.insertAfter(row.next());'>
																		<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/down-icon.png">
																	</span>
																</td>
															</TR>			
									</TABLE>
											
											
											
											
									<TABLE class='picture-table' align='left' border='0'>
										
										<TR>
											
													<input type="file" name="uploadAttachment" id="multiFileUploader" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">
													<label for="multiFileUploader"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
											
											
										</TR>
									</TABLE>
								</fieldset>
							<?php
							} ?>
						</div>
				</div>

				<?php
					} 
				?>
				<!-- end box2 -->

				<!--box3 -->
				<div class="panel panel-default fie_wid ">
					<div class="panel-heading fieldset-business_hours">
					<label for="business_hours"><i class="icon-plus-circle"></i><?php echo JText::_('LNG_OPENING_HOURS');?> <small class="small_note">(<?php echo JText::_("LNG_OPTIONAL")?>)</small></label>
						<div class="field">
							<table>
								<tr>
									<th width="50%">&nbsp;</th>
									<th align="left"><?php echo JText::_("LNG_OPEN")?></th>
									<th align="left"><?php echo JText::_("LNG_CLOSE")?></th>
								</tr>

								<?php $dayNames = array(JText::_("MONDAY"),JText::_("TUESDAY"),JText::_("WEDNESDAY"),JText::_("THURSDAY"),JText::_("FRIDAY"),JText::_("SATURDAY"),JText::_("SUNDAY")); ?>
								<?php
								foreach($dayNames as $index=>$day){?>
									<tr>
										<td align="left"><?php echo $day?></td>
										<td align="left" class="business-hour"><input type="text" class="timepicker" name="business_hours[]" value="<?php echo !empty($this->item->business_hours)?$this->item->business_hours[$index*2]:"" ?>" class="regular-text"/></td>
										<td align="left" class="business-hour"><input type="text" class="timepicker" name="business_hours[]" value="<?php echo !empty($this->item->business_hours)?$this->item->business_hours[$index*2+1]:"" ?>" class="regular-text"/></td>
									</tr>
								<?php
								} ?>
							</table>
						</div>
					</div>
				</div>
				<!-- end box3 -->


<!-- box5 -->
					<?php
					if(!empty($this->item->customFields) && $this->item->containsCustomFields){?>
						<div class="panel panel-default fie_wid">
							<div class="panel-heading"><h3><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ADDITIONAL_INFO');?></h3> <small class="small_note"><?php echo JText::_('LNG_ADDITIONAL_INFO_TEXT');?></small></div>

						<div class="package_content panel-body" style="padding-top: 0px;">
						
							<div class="panel-body">
								<?php
								$packageFeatures = !empty($this->item->package->features)?$this->item->package->features:null;
								$renderedContent = AttributeService::renderAttributes($this->item->customFields, $enablePackages, $packageFeatures);
								echo $renderedContent;
								?>
							</div>
					
							</div>

						</div>
					<?php
					} ?>
				<!-- end box5 -->
				</div>
			<!-- End edit-tab1 -->		